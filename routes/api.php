<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api.'], function () {
	Route::group(['as' => 'select.', 'prefix' => 'select'], function () {
		Route::get('/division', 'Api\SelectController@division')->name('division');
		Route::post('/state', 'Api\SelectController@state')->name('state');
		Route::post('/city', 'Api\SelectController@city')->name('city');
		Route::post('/court', 'Api\SelectController@court')->name('court');
	});
});
Route::get('/cases', 'Api\SelectController@cases')->name('cases');

Route::get('/response', 'CaseController@response')->name('response');

Route::get('/state_list', 'CaseController@state_list')->name('state_list');

Route::post('/district_list', 'CaseController@district_list')->name('district_list');

Route::get('/benches_list', 'CaseController@benches_list')->name('benches_list');

Route::get('/case_type_list', 'CaseController@case_type_list')->name('case_type_list');

Route::get('/storage_link', 'Api\SelectController@storage_link')->name('storage_link');

Route::get('/generate_stripe_subscriptions', 'Api\SelectController@subscriptions');
