@extends('layouts.master', ['title' => __('case.Create New Case')])

@section('mainContent')

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">


                        <li class="nav-item">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-custom"
                                role="tab" aria-controls="pills-contact" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('case.Custom Case') }}</h3>
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-party-nm"
                                role="tab" aria-controls="pills-profile" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('Connected Matters') }}</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-advo-nm"
                                role="tab" aria-controls="pills-contact" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('Interlocatory') }}</h3>
                            </a>
                        </li> --}}


                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">

                        <!-- Connected Mtters 2 =-->
                        {{-- <div class="tab-pane fade" id="pills-party-nm" role="tabpanel" aria-labelledby="pills-party-tab">
                            <div class="col-12" id="party_tab">
                                <div class="white_box_50px box_shadow_white">
                                    {{ Form::open([
                                        'class' => 'form-horizontal',
                                        'files' => true,
                                        'route' => 'to_dos.store',
                                        'method' => 'POST',
                                        'enctype' => 'multipart/form-data',
                                        'onsubmit' => 'return validateToDoForm()',
                                    ]) }}

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12" id="sibling_class_div">
                                                    <div class="primary_input mb-15">
                                                        <label class="primary_input_label"
                                                            for="">{{ __('Select Primary Case') }}</label>
                                                        <select class="primary_input_field">
                                                            <option>Category 1</option>
                                                            <option>Category 2</option>
                                                            <option>Category 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row container1">
                                                <div class="col-lg-10" id="sibling_class_div">
                                                    <div class="primary_input mb-15">
                                                        <label class="primary_input_label"
                                                            for="">{{ __('Select Connected Matters') }}</label>
                                                        <select name="mytext[]" class="primary_input_field">
                                                            <option>Category 1</option>
                                                            <option>Category 2</option>
                                                            <option>Category 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2" id="sibling_class_div">
                                                    <div class="primary_input mb-15">
                                                        <label class="primary_input_label"
                                                            for="">{{ __('.') }}</label>
                                                        <a href="#"
                                                            class="primary-btn small fix-gr-bg add_form_field">
                                                            <span class="ti-plus pr-2"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="container1">
                                    <button class="add_form_field">Add New Field &nbsp;
                                      <span style="font-size:16px; font-weight:bold;">+ </span>
                                    </button>
                                    <div><input type="text" name="mytext[]"></div>
                                </div> -->

                                        </div>
                                    </div>
                                    {{ Form::close() }}

                                </div>
                            </div>


                        </div> --}}

                        <!-- InterLocatory 3 =-->
                        {{-- <div class="tab-pane fade" id="pills-advo-nm" role="tabpanel" aria-labelledby="pills-advocate-tab">
                            <div class="col-12" id="advocate_tab">
                                <div class="white_box_50px box_shadow_white">
                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'content_form',
                                        'files' => false,
                                        'method' => 'POST',
                                    ]) !!}
                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Hearing Court', __('Order No.')) }}
                                            {{ Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Order No.')]) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Advocate Number', __('Order Date')) }}
                                            {{ Form::date('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control', 'id' => 'previous_date', 'placeholder' => __('Order DATE')]) }}
                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Year', __('Order Details')) }}
                                            {{ Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Order Details')]) }}
                                        </div>
                                    </div>

                                    <div class="text-center mt-3">
                                        <button class="primary_btn_large submit" type="submit"
                                            style="display: none;><i
                                                class="ti-check"></i>{{ __('common.Create') }}
                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}

                                </div>
                            </div>

                        </div> --}}


                        <!-- tab 4 =-->
                        {{-- @dd($model->Brief_no); --}}
                        <div class="tab-pane fade active show" id="pills-custom" role="tabpanel"
                            aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case">
                                <div class="white_box_50px box_shadow_white">

                                    {!! Form::model($model, [
                                        'class' => 'form-validate-jquery',
                                        'id' => $model->id,
                                        'files' => true,
                                        'method' => 'PUT',
                                        'enctype' => 'multipart/form-data',
                                        'route' => ['case.update', $model->id],
                                    ]) !!}
                                    <div class="row form-group">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('CNR NO.', __('CNR NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('cnr_no', old('cnr_no'), ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO'), 'required' => 'required', 'readonly']) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF NO', __('case.BRIEF NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('Brief_no', old('Brief_no'), ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF NO'), 'required' => 'required']) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('state', __('STATE')) }}
                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">

                                            {{-- {{ Form::select('state_id', $data['states'], old('state_id'), ['required' => '', 'class' => 'primary_select', 'id' => 'custom_state_id', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error', 'required' => 'required']) }} --}}
                                            {{-- {{ dd($data['states']) }} --}}
                                            <select name="state_id" class="primary_select" id="custom_state_id">

                                                @foreach ($data['states'] as $key => $state)
                                                    <option value="{{ $key }}"
                                                        {{ $key == $model->state_id ? 'selected' : '' }}>
                                                        {{ $state }}</option>
                                                @endforeach
                                            </select>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('district_id', __('District')) }}
                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('district_id', $data['districts'], old('district_id'), ['required' => '', 'class' => 'primary_select', 'id' => 'custom_district_id', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br><br><br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Court/Bench', __('Tabs')) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- {{ Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'), 'data-parsley-errors-container' => '#court_id_error']) }} -->
                                            <select name="tabs" class="primary_select">
                                                <option value="District Courts and Tribunals"
                                                    {{ isset($model->tabs) ? ($model->tabs == 'District Courts and Tribunals' ? 'selected' : ' ') : '' }}>
                                                    District Courts and Tribunals
                                                </option>
                                                <option value="High Court"
                                                    {{ isset($model->tabs) ? ($model->tabs == 'High Court' ? 'selected' : ' ') : '' }}>
                                                    High
                                                    Court</option>
                                            </select>
                                            <span id="court_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Court/Bench', __('Court/Bench')) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('court_bench', null, ['class' => 'primary_input_field', 'placeholder' => __('Court/Bench')]) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>

                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-12">
                                            <div class="row">
                                                <div class="primary_input col-md-3">
                                                    <div class="d-flex justify-content-between">
                                                        {{ Form::label('district_id', __('CASE TYPE')) }}
                                                    </div>
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    <!-- {{ Form::select('case_category_id', $data['case_categories'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <span id="organization_id_error"></span> -->
                                                    {{ Form::text('case_category', old('case_category'), ['class' => 'primary_input_field', 'placeholder' => __('case.Case Category')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>


                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('case_no', __('case.Case No.')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::text('case_no', old('case_no'), ['class' => 'primary_input_field', 'placeholder' => __('case.Case No')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('CASE YEAR', __('CASE YEAR')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{-- {{ Form::text('case_year', old('case_year'), ['class' => 'primary_input_field primary-input form-control date', 'id' => 'next_date', 'placeholder' => date('d-m-Y')]) }} --}}
                                                    <select class="primary_select" name="case_year" id="case_year">
                                                        @php
                                                            $curr_year = date('Y', strtotime(\Carbon\Carbon::now()));
                                                        @endphp
                                                        @for ($i = 1970; $i <= $curr_year; $i++)
                                                            <option value="{{ $i }}"
                                                                {{ $model->case_year == $i ? 'selected' : '' }}>
                                                                {{ $i }}
                                                            </option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('PREVIOUS DATE', __('case.PREVIOUS DATE')) }}
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::date('previous_date', old('previous_date'), ['class' => 'primary_input_field primary-input form-control', 'id' => 'previous_date', 'value' => date('d-m-Y')]) }}
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('NEXT DATE', __('NEXT DATE')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::date('next_date', old('next_date'), ['class' => 'primary_input_field primary-input form-control', 'id' => 'next_date', 'value' => date('d-m-Y')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>



                                                @includeIf('case.edit_petitioner')


                                            </div>
                                            <br>
                                        </div>

                                        @includeIf('case.edit_respondent')

                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF FOR', __('JUDGE Name')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('judge_name', old('judge_name'), ['class' => 'primary_input_field', 'placeholder' => __('case.JUDGE Name')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('COURT ROOM NO', __('COURT ROOM NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('court_room_no', old('court_room_no'), ['class' => 'primary_input_field', 'placeholder' => __('case.Court room no')]) }}
                                            <!-- <select name="court_room_no" class="primary_select">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <option>Select Court</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <option value="1"> District</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <option value="2"> Highcourt</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </select> -->
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('CASE STAGE', __('case.CASE STAGE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">

                                            <!-- {{ Form::select('stag_id', $data['stages'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }} -->
                                            {{ Form::text('case_stage', old('case_stage'), ['class' => 'primary_input_field', 'placeholder' => __('case.Case Stage')]) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        {{-- <div class="primary_input col-md-3">
                                            {{ Form::label('Sr. No. in Court', __('Sr. No. in Court')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('sr_no_in_court', old('sr_no_in_court'), ['class' => 'primary_input_field', 'placeholder' => __('Sr. No. in Court')]) }}
                                        </div> --}}

                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF FOR', __('case.BRIEF FOR')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('brief_for', old('brief_for'), ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF FOR')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{-- <div class="d-flex justify-content-between"> --}}
                                            {{ Form::label('organization_id', __('case.ORGANIZATION')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="OrganizationModal()">+</button>
                                            {{-- </div> --}}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('organization_id', $data['orgs'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('TAGS/REFERENCE', __('case.TAGS/REFERENCE')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="ReferanceModal()">+</button>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('tags', $data['refs'], null, ['id' => 'custom_case_ref', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            {{-- {{ Form::text('tags', null, ['class' => 'primary_input_field', 'placeholder' => __('case.TAGS/REFERENCE')]) }} --}}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('POLICE STATION', __('case.POLICE STATION')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('police_station', old('police_station'), ['class' => 'primary_input_field', 'placeholder' => __('case.POLICE STATION')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">

                                        <div class="primary_input col-md-3">
                                            {{ Form::label('PETITIONER', __('Client Name')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="ClientModal()">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('client_id', $data['clients'], null, ['required' => '', 'id' => 'custom_case_client', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            <!-- {{ Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('Client Name')]) }} -->
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('DECIDED TOGGLE', __('DECIDED TOGGLE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <label class="switch_toggle" for="custom_active_checkbox1">
                                                <input name="decided_toggle" type="checkbox" id="custom_active_checkbox1"
                                                    {{-- value="{{ $model->decided_toggle }}" --}} onchange="custom_update_active_status(this)"
                                                    {{ $model->decided_toggle == 'check' ? 'checked' : '' }}>
                                                <div class="slider round">
                                                </div>
                                            </label>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('ABANDONED TOGGLE', __('ABANDONED TOGGLE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <label class="switch_toggle" for="custom_active_checkbox2">
                                                <input type="checkbox" name="abbondend_toggle"
                                                    id="custom_active_checkbox2" {{-- value="{{ $model->abbondend_toggle }}" --}}
                                                    onchange="custom_update_active_status(this)"
                                                    {{ $model->abbondend_toggle == 'check' ? 'checked' : '' }}>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Remarks', 'Remarks') }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::textarea('remarks', old('remarks'), ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Notes', 'Notes') }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} -->

                                            {{ Form::textarea('notes_description', old('notes_description'), [
                                                'class' => 'primary_input_field summernote',
                                                'placeholder' => __('court.Court  Description'),
                                                'id' => 'custom_description',
                                                'rows' => 5,
                                                'maxlength' => 1500,
                                                'data-parsley-errors-container' => '#description_error',
                                            ]) }}
                                        </div>
                                    </div>

                                    @includeIf('case.case_file_custom')


                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"
                                            onclick="update_custom();"><i class="ti-check"></i>Update
                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>



                        </div>


                    </div>
                    <!-- form end =-->
                </div>
            </div>


        </div>

        <!-- <div class="text-center mt-3">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <button class="primary_btn_large submit" type="submit"><i
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                class="ti-check"></i>{{ __('Save') }}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </button>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </button>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div> -->

    </section>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="custom_exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="custom_client_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Client Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="client_name">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="text" class="primary_input_field" placeholder="Contact"
                                        id="client_contact">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="text" class="primary_input_field" placeholder="Email"
                                        id="client_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_address">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Gender</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_gender">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Description</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_description">
                                </div>
                            </div>


                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Client()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>
                    <div id="custom_org_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-10">
                                    <!--  -->
                                </div>
                                <div class="primary_input col-md-2">
                                    <!-- <button type="button" class="primary-btn small fix-gr-bg add_field_button">+</button> -->
                                </div>
                            </div>

                            @includeIf('case.add_organization')
                            {{-- <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="cust_name_1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="number" class="primary_input_field" placeholder="Contact"
                                        id="cust_contact_1">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="email" class="primary_input_field" placeholder="Email"
                                        id="cust_email_1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="cust_org_address">
                                </div>
                            </div> --}}
                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Organization()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>
                    <div id="custom_ref_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">reference Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="cust_ref_name">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="text" class="primary_input_field" placeholder="Contact"
                                        id="cust_ref_contact">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="text" class="primary_input_field" placeholder="Email"
                                        id="cust_ref_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="cust_ref_address">
                                </div>
                            </div>


                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Reff()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Petitioner </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/action_page.php">
                        <div class="row">
                            <div class="primary_input col-md-10">
                                <!--  -->
                            </div>
                            <div class="primary_input col-md-2">
                                <button type="button" class="primary_btn_large add_field_button">+</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">PETITIONER</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Petitioner Advocate</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner Advocate">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input_fields_wrap primary_input col-md-12">

                            </div>
                        </div>


                        <div class="text-center mt-3">
                            <a class="primary_btn_large submit" type="submit"><i
                                    class="ti-check"></i>{{ __('common.Create') }}
                            </a>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <button type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</button>
                            <input class="primary-btn fix-gr-bg" type="submit" value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>
    <!--Petitioner Modal -->

    <!-- Respondent Modal -->
    <div class="modal fade" id="respondendModal" tabindex="-1" role="dialog" aria-labelledby="respondandModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="respondandModalLabel">Respondand </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/action_page.php">
                        <div class="row">
                            <div class="primary_input col-md-10">
                                <!--  -->
                            </div>
                            <div class="primary_input col-md-2">
                                <button type="button" class="primary_btn_large add_field_button_res">+</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent</label>
                                <input type="text" class="primary_input_field" placeholder="Respondent">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent Advocate</label>
                                <input type="text" class="primary_input_field" placeholder="Respondent Advocate">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input_fields_wrap_res primary_input col-md-12">

                            </div>
                        </div>


                        <div class="text-center mt-3">
                            <button class="primary_btn_large submit" type="submit"><i
                                    class="ti-check"></i>{{ __('common.Create') }}
                            </button>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <button type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</button>
                            <input class="primary-btn fix-gr-bg" type="submit" value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>
    <!--Respondent Modal -->

    <!-- Client Modal For View -->

    <div class="modal fade" id="clientModal1" tabindex="-1" role="dialog" aria-labelledby="clientModal1"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="respondentModalLabel">Client </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/action_page.php">
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Contact</label>
                                <input type="text" class="primary_input_field" placeholder="Contact">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Email</label>
                                <input type="text" class="primary_input_field" placeholder="Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Address</label>
                                <input type="text" class="primary_input_field" placeholder="Address">
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button class="primary_btn_large submit" type="submit" hidden><i
                                    class="ti-check"></i>{{ __('common.Create') }}
                            </button>
                        </div>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <button type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</button>
                            <input class="primary-btn fix-gr-bg" type="submit" value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>



@stop
@push('admin.scripts')
    <script>
        $(document).ready(function() {
            _formValidation();
            _componentAjaxChildLoad('#content_form', '#court_category_id', '#court_id', 'court');
            $(document).on('click', '#hearing_date_yes', function() {
                if (this.checked) {
                    $('#hearing_date').show();
                } else {
                    $('#hearing_date').hide();
                }
            });

            $(document).on('click', '#filling_date_yes', function() {
                if (this.checked) {
                    $('#filling_date').show();
                } else {
                    $('#filling_date').hide();
                }
            });

            $(document).on('click', '#judgement_date_yes', function() {
                if (this.checked) {
                    $('#judgement_date').show();
                    $('#judgement_row').show();
                } else {
                    $('#judgement_date').hide();
                    $('#judgement_row').hide();
                }
            });

            $(document).on('click', '#receiving_date_yes', function() {
                if (this.checked) {
                    $('#receiving_date').show();
                } else {
                    $('#receiving_date').hide();
                }
            });
        });

        _componentAjaxChildLoad('#client_quick_add_form', '#country_id', '#state_id', 'state')
        _componentAjaxChildLoad('#client_quick_add_form', '#state_id', '#city_id', 'city')
    </script>


    <script>
        $("#custom_state_id").change(function() {

            var id = $("#custom_state_id").val();
            $.ajax({
                url: '{{ url('custom/district') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                },
                type: "post",
                success: function(data) {
                    // console.log(data);
                    $("#custom_district_id").parent().find(".nice-select.primary_select .list").empty();
                    $("#custom_district_id").empty();
                    var selectoption = "";
                    var selectlist = "";
                    data.forEach(function(Value) {
                        selectoption = selectoption + '<option value="' + Value.id + '">' +
                            Value.name + '</option>';
                        selectlist = selectlist + '<li data-value="' + Value.id +
                            '" class="option">' + Value.name + '</li>';
                    });

                    $("#custom_district_id").parent().find(".nice-select.primary_select .list").append(
                        selectlist);
                    $("#custom_district_id").append(selectoption);

                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });

        });

        function update_custom() {

            $("#custom_update_description").val($("#custom_update_description").parent().find(".note-editable.card-block")
                .html());

            // $("#custom_form").submit();
        }


        function ClientModal() {
            $("#exampleModal").modal('show');
            $("#custom_client_add").css('display', 'block');
            $("#custom_org_add").css('display', 'none');
            $("#custom_ref_add").css('display', 'none');
            $("#custom_exampleModalLabel").text('Client');

        }

        function OrganizationModal() {
            $("#exampleModal").modal('show');
            $("#custom_org_add").css('display', 'block');
            $("#custom_client_add").css('display', 'none');
            $("#custom_ref_add").css('display', 'none');
            $("#custom_exampleModalLabel").text('Organization');
        }

        function ReferanceModal() {
            $("#exampleModal").modal('show');
            $("#custom_ref_add").css('display', 'block');
            $("#custom_org_add").css('display', 'none');
            $("#custom_client_add").css('display', 'none');
            $("#custom_exampleModalLabel").text('Reference');
        }

        function Add_Client() {
            $.ajax({
                url: '{{ url('add/client') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    name: document.getElementById('client_name').value,
                    mobile: document.getElementById('client_contact').value,
                    email: document.getElementById('client_email').value,
                    address: document.getElementById('client_address').value,
                    gender: document.getElementById('client_gender').value,
                    description: document.getElementById('client_description').value,
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#custom_case_client").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.model.id + '" class="option">' + data.model.name + '</li>'
                    );
                    $("#custom_case_client").append('<option value="' + data.model.id + '">' + data.model.name +
                        '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }


        function Add_Organization() {
            var email = [];
            var contact = [];
            var name = [];
            var i = 1;
            // alert($('#cust_name_'+ 1).val());
            for (; i <= custom_org_index; i++) {
                email[i] = $('#cust_email_' + i).val();
                contact[i] = $('#cust_contact_' + i).val();
                name[i] = $('#cust_name_' + i).val();
            }
            // console.log("this is list of email" + email);
            $.ajax({
                url: '{{ url('add/organization') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    organization_name: $('#cust_org_name').val(),
                    representator: name,
                    email: email,
                    contact: contact,
                    address: $('#cust_org_address').val()
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#organization_id").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.id + '" class="option">' + data.organization_name +
                        '</li>');
                    $("#organization_id").append('<option value="' + data.id + '">' + data.organization_name +
                        '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }

        function Add_Reff() {
            $.ajax({
                url: '{{ url('add/referance') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    name: $('#cust_ref_name').val(),
                    email: $('#cust_ref_email').val(),
                    contact: $('#cust_ref_contact').val(),
                    address: $('#cust_ref_address').val()
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#custom_case_ref").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.id + '" class="option">' + data.name + '</li>');
                    $("#custom_case_ref").append('<option value="' + data.id + '">' + data.name + '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }

        $(document).ready(function() {
            //set initial state.


            $('#custom_active_checkbox1').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox2').val()) {
                        $('#custom_active_checkbox2').prop('checked', false);
                    }

                }
            });
            $('#custom_active_checkbox2').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox1').val()) {
                        $('#custom_active_checkbox1').prop('checked', false);
                    }

                }
            });
            $('#cust_case_year').change(function() {
                alert(this.value);
                date = new Date(this.value);
                alert(date);
                year = date.getFullYear();
                alert(year);
                $(this).val(year);
                alert($(this).val());
            });

        });
    </script>
@endpush
