@extends('layouts.print')
@push('css_before')
@endpush
@section('mainContent')
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <div class="main-title">
                        <h3 class="mb-30">{{ $model->title }}</h3>
                    </div>
                </div>
            </div>


            <div class="row">
                {{--  <div class="col-lg-8 mt-25">
                    <!-- Rozanam -->
                    <div class="accordion" id="accordionExample">
                        <div class="card">


                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                        <h4>ROZNAMA/CASE HISTORY</h4>
                                        <div class="QA_section QA_section_heading_custom check_box_table">
                                            <div class="QA_table ">
                                                <!-- table-responsive -->
                                                <div class="">
                                                    <table class="table Crm_table_active3">
                                                        <thead>
                                                            <tr>

                                                                <th scope="col">Previous Date</th>
                                                                <th scope="col">Case History</th>
                                                                <th scope="col">Next Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>

                                                                <td>14-05-2022</td>
                                                                <td>Draft Affidavit</td>
                                                                <td>In Progress</td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                            <!-- end -->
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                        <h4>Worklist</h4>
                                        <div class="QA_section QA_section_heading_custom check_box_table">
                                            <div class="QA_table ">
                                                <!-- table-responsive -->
                                                <div class="">
                                                    <table class="table Crm_table_active3">
                                                        <thead>
                                                            <tr>

                                                                <th scope="col">Date</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>

                                                                <td>14-05-2022</td>
                                                                <td>Draft Affidavit</td>
                                                                <td>In Progress</td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                            <!-- end -->
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">

                                        <div class="row">
                                            <div class="primary_input col-md-12">
                                                {{ Form::label('CASE STAGE', __('case.CASE STAGE')) }}
                                                {{ Form::textarea('description', null, [
                                                    'class' => 'primary_input_field summernote',
                                                    'placeholder' => __('court.Court  Description'),
                                                    'rows' => 5,
                                                    'maxlength' => 1500,
                                                    'data-parsley-errors-container' => '#description_error',
                                                ]) }}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                            <!-- end -->
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                        <h4>Connected Matters</h4>
                                        <div class="QA_section QA_section_heading_custom check_box_table">
                                            <div class="QA_table ">
                                                <!-- table-responsive -->
                                                <div class="">
                                                    <table class="table Crm_table_active3">
                                                        <thead>
                                                            <tr>

                                                                <th scope="col">Sr No.</th>
                                                                <th scope="col">Case No.</th>
                                                                <th scope="col">Case Title</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>

                                                                <td>14-05-2022</td>
                                                                <td>Draft Affidavit</td>
                                                                <td>In Progress</td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                            <!-- end -->
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                        <h4>interlocutory orders Evidences or Judgements</h4>
                                        <div class="QA_section QA_section_heading_custom check_box_table">
                                            <div class="QA_table ">
                                                <!-- table-responsive -->
                                                <div class="">
                                                    <table class="table Crm_table_active3">
                                                        <thead>
                                                            <tr>

                                                                <th scope="col">Order No.</th>
                                                                <th scope="col">Order Date</th>
                                                                <th scope="col">Order Details</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>

                                                                <td>14-05-2022</td>
                                                                <td>Draft Affidavit</td>
                                                                <td>In Progress</td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                            <!-- end -->
                        </div>
                        <!-- worklist -->



                        <!-- card close -->
                        <div class="row">
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle1 -->
                                <h5>Decided</h5>
                                <div class="custom-control">
                                    <label class="switch_toggle" for="active_checkbox">
                                        <!-- <input type="text" id="active_checkbox"> -->
                                        <h6>True</h6>

                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle2 -->
                                <h5>Abandoned</h5>
                                <div class="custom-control">
                                    <label class="switch_toggle" for="active_checkbox">
                                        <h6>True</h6>
                                        <!-- <input type="checkbox" id="active_checkbox"
                                                                            value="check" onchange="update_active_status(this)" checked>
                                                                            <div class="slider round"></div> -->
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>  --}}

                <div class="col-lg-4 mt-25">
                    <div class="student-meta-box sticky-details">
                        <div class="white-box student-details pt-3">
                            <div class="single-meta">
                                <h3 class="mb-0">{{ __('case.Case Details') }} </h3>
                            </div>
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('CNR') }}:
                                    </div>
                                    <div class="value">
                                        {{-- <!-- {{$model->case_category? $model->case_category->name : '' }}
                                        - {{$model->case_no}} --> --}}
                                        <p>{{ $model->cnr_no }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Brief No,') }}:
                                    </div>
                                    <div class="value">
                                        {{-- <!-- @if ($model->case_category)
                                            <a href="{{route('category.case.show', $model->case_category_id)}}">
                                                {{$model->case_category? $model->case_category->name : '' }}
                                            </a>
                                        @endif --> --}}
                                        <p>{{ $model->Brief_no }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Case Type') }}:
                                    </div>
                                    <div class="value">
                                        <!-- {{ $model->file_no }} -->
                                        <p>{{ $model->case_category }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Case No.') }}:
                                    </div>
                                    <div class="value">
                                        <!-- {{ $model->ref_name }} -->
                                        <p>{{ $model->case_no }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Year') }}:
                                    </div>
                                    <div class="value">
                                        <!-- {{ $model->ref_mobile }} -->
                                        <p>{{ $model->case_year }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Previous Date') }}:
                                    </div>
                                    <div class="value">
                                        <!-- {{ formatDate($model->hearing_date) }} -->
                                        <p>{{ isset($model->previous_date) ? date('d-m-Y', strtotime($model->previous_date)) : '---' }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Next date') }}:
                                    </div>
                                    <div class="value">
                                        <p>{{ isset($model->next_date) ? date('d-m-Y', strtotime($model->next_date)) : '---' }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Case Stage') }}:
                                    </div>
                                    <div class="value">
                                        <!-- {{ formatDate($model->filling_date) }} -->
                                        <p>{{ $model->case_stage }}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Brief For') }}:
                                    </div>
                                    <div class="value">
                                        {{-- <!-- {!!$model->case_stage ? '<a
                                            href="'.route('master.stage.show', $model->stage_id).'">'. $model->case_stage->name
                                            .'</a>' : ''!!} --> --}}
                                        <p>{{ $model->brief_for }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">

                                        {{ __('State') }}:
                                    </div>
                                    <div class="value">
                                        {{-- <!-- {!!$model->case_stage ? '<a
                                            href="'.route('master.stage.show', $model->stage_id).'">'. $model->case_stage->name
                                            .'</a>' : ''!!} --> --}}
                                        <p>{{ $data['state']->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('District') }}:
                                    </div>
                                    <div class="value">
                                        {{-- <!-- {!!$model->case_stage ? '<a
                                            href="'.route('master.stage.show', $model->stage_id).'">'. $model->case_stage->name
                                            .'</a>' : ''!!} --> --}}
                                        <p>{{ $data['district']->name }}</p>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <h3 class="mb-30">{{ __('Parties & Hearing Aadvocates') }} </h3>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="single-meta mt-10">
                                <h3 class="mb-30">{{ __('Parties & Hearing Aadvocates') }} </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('PETITIONER/ADITIONAL') }}:
                                    </div>
                                    <div class="value">

                                        @foreach ($data['petitioners'] as $petitioner)
                                            <p>{{ $petitioner->petitioner }}</p>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('THEIR ADVOCATES') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        @foreach ($data['petitioners_advocate'] as $petitioner)
                                            <p>{{ $petitioner->petitioner_advocate }}</p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('RESPONDENT/ADITIONAL') }}:
                                    </div>
                                    <div class="value">
                                        @foreach ($data['respondents'] as $respondent)
                                            <p>{{ $respondent->respondent }}</p>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('THEIR ADVOCATES') }}:
                                    </div>
                                    <div class="value">
                                        @foreach ($data['respondents_advocate'] as $respondent)
                                            <p>{{ $respondent->respondent_advocate }}</p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta mt-10">
                                <h3 class="mb-30">{{ __('Court Details') }} </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Court
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $model->court_bench }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Judge Name') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $model->judge_name }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Court Room No.') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $model->court_room_no }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta mt-10">
                                <h3 class="mb-30">{{ __('Client Details') }} </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Client Name') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['client']->name }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Contact
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['client']->mobile }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Email') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['client']->email }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        {{ __('Address') }}:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['client']->address }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Organization
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['org']->organization_name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Tags/Refrence
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $data['refs']->name }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Police Station
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $model->police_station }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Remarks
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ $model->remarks }}</p>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Registration Date & Time
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>Registration date time casewise</p>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Registered at Casewise:
                                    </div>
                                    <div class="value">
                                        <!--  -->
                                        <p>{{ date('d/m/Y', strtotime($model->created_at)) }} at
                                            {{ date('h:i', strtotime($model->created_at)) }} hrs</p>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
    </section>
    <div class="modal fade animated file_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
        aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    @if (moduleStatusCheck('Finance'))
        <div class="modal fade animated payment_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
            aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
        </div>
    @endif
@endsection
@push('admin.scripts')
@endpush
