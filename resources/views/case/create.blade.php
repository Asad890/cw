@extends('layouts.master')

@section('mainContent')


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                        <li class="nav-item ">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-cnr-number"
                                role="tab" aria-controls="pills-contact" aria-selected="true">
                                <h3 class="mb-0 ">{{ __('CNR Number') }}</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-case-no" role="tab"
                                aria-controls="pills-home" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('case.By Case Number') }}</h3>
                            </a>
                        </li>
                        {{--  <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-party-nm"
                                role="tab" aria-controls="pills-profile" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('case.By Party Name') }}</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-advo-nm"
                                role="tab" aria-controls="pills-contact" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('case.By Advocate') }}</h3>
                            </a>
                        </li>  --}}
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-custom"
                                role="tab" aria-controls="pills-contact" aria-selected="false">
                                <h3 class="mb-0 ">{{ __('case.Custom Case') }}</h3>
                            </a>
                        </li>

                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade" id="pills-case-no" role="tabpanel" aria-labelledby="pills-case-no-tab">

                            <div class="col-12" id="case_no">
                                <div class="white_box_50px box_shadow_white">
                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'content_form',
                                        'files' => false,
                                        'method' => 'POST',
                                    ]) !!}
                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Hearing Court', __('Hearing Court')) }}
                                            {{ Form::select('court_id', $data['api_courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('State', __('State')) }}
                                            {{ Form::select('state_id', $data['api_states'], null, ['id' => 'case_no_state', 'required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Select State'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('District', __('District')) }}
                                            {{ Form::select('district_id', $data['api_district'], null, ['id' => 'case_no_district', 'required' => '', 'class' => 'primary_select', 'data-placeholder' => __('District'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Court/Bench', __('Court/Bench')) }}
                                            {{ Form::select('bench_id', $data['api_banches'], null, ['id' => 'case_no_bench', 'required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Case Type', __('Case Type')) }}
                                            {{ Form::select('case_type_id', $data['api_case_types'], null, ['id' => 'case_no_types', 'required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Case Type'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Case Number', __('Case Number')) }}
                                            {{ Form::text('case_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Number')]) }}
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Case Year', __('Case Year')) }}
                                            {{ Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')]) }}
                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg" type="submit"><i
                                                class="ti-check"></i>{{ __('Fetch') }}
                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            <!--  -->
                            <br>
                            <div class="tab-pane fade show active" id="pills-cnr-number_unde" role="tabpanel"
                                aria-labelledby="pills-contact-tab">
                                <div class="col-12" id="custom_case">
                                    <div class="white_box_50px box_shadow_white">

                                        {!! Form::open([
                                            'route' => 'case.store',
                                            'class' => 'form-validate-jquery',
                                            'id' => 'content_form',
                                            'files' => false,
                                            'method' => 'POST',
                                        ]) !!}
                                        <!--  -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no.', __('CNR NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="MHNG" class="primary_input_field"
                                                    placeholder="CNR NO." readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no.', __('Brief NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="225" class="primary_input_field"
                                                    placeholder="CNR NO." readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('State:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Maharashtra"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('District:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="MAC Court No.2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Type:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="M.A.C.P"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="802"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Year:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Previous Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Next Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <!-- stop -->

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#petitionerModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Parshant"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="P S Mirache"
                                                    class="primary_input_field" placeholder="Petitioner Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- Respondant -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondand:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#respondentModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondent Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="Respondent Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- start -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Judge Name:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Deshpande"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court Room No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="219"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Stage:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Defence Evidence"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief For:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Organization:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="ICICI Lombars"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Tags/Refrense:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Police Station:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="-----------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Clients Name:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="Modal" data-target="#clientModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Contact:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="0234XXXXXX"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Email Id:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="abc@gmail.com"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Address:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <!-- Decided Toggle -->
                                        <h5>Decided</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox1">
                                                <input type="checkbox" id="active_checkbox1" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Decided -->
                                        <!-- Abandoned -->
                                        <h5>Abandoned</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Abamdoned -->
                                        <div class="row">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Remarks', 'Remarks') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                {{ Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Notes', 'Notes') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <!-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} -->
                                                {{ Form::textarea('description', null, [
                                                    'class' => 'primary_input_field summernote',
                                                    'placeholder' => __('court.Court  Description'),
                                                    'rows' => 5,
                                                    'maxlength' => 1500,
                                                    'data-parsley-errors-container' => '#description_error',
                                                ]) }}
                                            </div>
                                        </div>

                                        @includeIf('case.file')
                                        <!--  -->
                                        <div class="text-center mt-3">
                                            <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                    class="ti-check"></i>{{ __('common.Save') }}
                                            </button>

                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>



                            </div>
                            <!--  -->

                        </div>


                        <!-- tab 2 =-->
                        <div class="tab-pane fade" id="pills-party-nm" role="tabpanel"
                            aria-labelledby="pills-party-tab">
                            <div class="col-12" id="party_tab">
                                <div class="white_box_50px box_shadow_white">
                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'content_form',
                                        'files' => false,
                                        'method' => 'POST',
                                    ]) !!}
                                    <div class="row form-group">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Hearing Court', __('Hearing Court')) }}
                                            {{ Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Court', __('case.Court')) }}
                                            {{ Form::select('bench_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Party Name', __('Party Name')) }}
                                            {{ Form::text('party_nm', null, ['class' => 'primary_input_field', 'placeholder' => __('Party Name')]) }}
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Case Year', __('Case Year')) }}
                                            {{ Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')]) }}
                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i>{{ __('Fetch') }}
                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            <!--  -->
                            <br>
                            <div class="tab-pane fade show active" id="pills-cnr-number_une" role="tabpanel"
                                aria-labelledby="pills-contact-tab">
                                <div class="col-12" id="custom_case">
                                    <div class="white_box_50px box_shadow_white">

                                        {!! Form::open([
                                            'route' => 'case.store',
                                            'class' => 'form-validate-jquery',
                                            'id' => 'content_form',
                                            'files' => false,
                                            'method' => 'POST',
                                        ]) !!}
                                        <!--  -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('CNR NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="MHNG" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="225" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('State:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Maharashtra"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('District:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="MAC Court No.2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Type:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="M.A.C.P"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="802"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Year:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Previous Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Next Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <!-- stop -->

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#petitionerModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Parshant"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="P S Mirache"
                                                    class="primary_input_field" placeholder="Petitioner Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- Respondant -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondand:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#respondentModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondent Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="Respondent Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- start -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Judge Name:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Deshpande"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court Room No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="219"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Stage:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Defence Evidence"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief For:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Organization:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="ICICI Lombars"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Tags/Refrense:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Police Station:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="-----------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Clients Name:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="Modal" data-target="#clientModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Contact:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="0234XXXXXX"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Email Id:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="abc@gmail.com"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no.', __('Address:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO." readonly>
                                            </div>
                                        </div>


                                        <!-- Decided Toggle -->
                                        <h5>Decided</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Decided -->
                                        <!-- Abandoned -->
                                        <h5>Abandoned</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Abamdoned -->
                                        <div class="row">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Remarks', 'Remarks') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                {{ Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Notes', 'Notes') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <!-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} -->
                                                {{ Form::textarea('description', null, [
                                                    'class' => 'primary_input_field summernote',
                                                    'placeholder' => __('court.Court  Description'),
                                                    'rows' => 5,
                                                    'maxlength' => 1500,
                                                    'data-parsley-errors-container' => '#description_error',
                                                ]) }}
                                            </div>
                                        </div>

                                        @includeIf('case.file')
                                        <!--  -->
                                        <div class="text-center mt-3">
                                            <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                    class="ti-check"></i>{{ __('common.Save') }}
                                            </button>

                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>



                            </div>
                            <!--  -->

                        </div>

                        <!-- tab 3 =-->
                        <div class="tab-pane fade" id="pills-advo-nm" role="tabpanel"
                            aria-labelledby="pills-advocate-tab">
                            <div class="col-12" id="advocate_tab">
                                <div class="white_box_50px box_shadow_white">
                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'content_form',
                                        'files' => false,
                                        'method' => 'POST',
                                    ]) !!}
                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Hearing Court', __('Hearing Court')) }}
                                            {{ Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'), 'data-parsley-errors-container' => '#court_id_error']) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Advocate Number', __('Advocate Name')) }}
                                            {{ Form::text('advocate_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Advocate Name')]) }}
                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            {{ Form::label('Year', __('case.Year')) }}
                                            {{ Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')]) }}
                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i>{{ __('Fetch') }}
                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}

                                </div>
                            </div>

                            <!--  -->
                            <br>
                            <div class="tab-pane fade show active" id="pills-cnr-number_uned" role="tabpanel"
                                aria-labelledby="pills-contact-tab">
                                <div class="col-12" id="custom_case">
                                    <div class="white_box_50px box_shadow_white">

                                        {!! Form::open([
                                            'route' => 'case.store',
                                            'class' => 'form-validate-jquery',
                                            'id' => 'content_form',
                                            'files' => false,
                                            'method' => 'POST',
                                        ]) !!}
                                        <!--  -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('CNR NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="MHNG" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="225" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('State:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Maharashtra"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('District:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="MAC Court No.2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Type:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="M.A.C.P"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="802"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Year:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Previous Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Next Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <!-- stop -->

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#petitionerModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Parshant"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="P S Mirache"
                                                    class="primary_input_field" placeholder="Petitioner Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- Respondant -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondand:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#respondentModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondent Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="Respondent Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        <!-- start -->
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Judge Name:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Deshpande"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court Room No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="219"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Stage:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Defence Evidence"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief For:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Organization:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="ICICI Lombars"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Tags/Refrense:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Police Station:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="-----------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Clients Name:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="Modal" data-target="#clientModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Contact:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="0234XXXXXX"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Email Id:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="abc@gmail.com"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Address:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>


                                        <!-- Decided Toggle -->
                                        <h5>Decided</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Decided -->
                                        <!-- Abandoned -->
                                        <h5>Abandoned</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Abamdoned -->
                                        <div class="row">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Remarks', 'Remarks') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                {{ Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Notes', 'Notes') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <!-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} -->
                                                {{ Form::textarea('description', null, [
                                                    'class' => 'primary_input_field summernote',
                                                    'placeholder' => __('court.Court  Description'),
                                                    'rows' => 5,
                                                    'maxlength' => 1500,
                                                    'data-parsley-errors-container' => '#description_error',
                                                ]) }}
                                            </div>
                                        </div>

                                        @includeIf('case.file')
                                        <!--  -->
                                        <div class="text-center mt-3">
                                            <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                    class="ti-check"></i>{{ __('common.Save') }}
                                            </button>

                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>



                            </div>
                            <!--  -->

                        </div>


                        <!-- tab 4 =-->

                        <div class="tab-pane fade" id="pills-custom" role="tabpanel"
                            aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case">
                                <div class="white_box_50px box_shadow_white">

                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'custom_form',
                                        'files' => true,
                                        'method' => 'POST',
                                        'enctype' => 'multipart/form-data',
                                    ]) !!}
                                    <div class="row form-group">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF NO.', __('CNR NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('cnr_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO'), 'required' => 'required']) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF NO', __('case.BRIEF NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF NO')]) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('state', __('STATE')) }}
                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('state_id', $data['api_states'], null, ['required' => '', 'class' => 'primary_select', 'id' => 'custom_state_id', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error', 'required' => 'required']) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('district_id', __('District')) }}
                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('district_id', $data['api_district'], null, ['required' => '', 'class' => 'primary_select', 'id' => 'custom_district_id', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br><br><br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Court/Bench', __('Tabs')) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- {{ Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'), 'data-parsley-errors-container' => '#court_id_error']) }} -->
                                            <select name="tabs" class="primary_select">
                                                <option value="District Courts and Tribunals">District Courts and Tribunals
                                                </option>
                                                <option value="High Court">High Court</option>
                                            </select>
                                            <span id="court_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Court/Bench', __('Court/Bench')) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('court_bench', null, ['class' => 'primary_input_field', 'placeholder' => __('Court/Bench')]) }}
                                            <span id="court_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>

                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-12">
                                            <div class="row">
                                                <div class="primary_input col-md-3">
                                                    <div class="d-flex justify-content-between">
                                                        {{ Form::label('district_id', __('CASE TYPE')) }}
                                                    </div>
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    <span id="organization_id_error"></span>
                                                    {{ Form::text('case_category', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Case Type')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>


                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('case_no', __('case.Case No.')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::text('case_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Case No')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('CASE YEAR', __('CASE YEAR')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{-- {{ Form::date('case_year', null, ['class' => 'primary_input_field primary-input form-control', 'id' => 'cust_case_year', 'onchange'=>'changeYear("cust_case_year")', 'placeholder' => date('d-m-Y'), 'required']) }} --}}
                                                    <select class="primary_select" name="case_year" id="case_year">
                                                        @php
                                                            $curr_year = date('Y', strtotime(\Carbon\Carbon::now()));
                                                        @endphp
                                                        @for ($i = 1990; $i <= $curr_year; $i++)
                                                            <option value="{{ $i }}"
                                                                {{ $curr_year == $i ? 'selected' : '' }}>
                                                                {{ $i }}
                                                            </option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('PREVIOUS DATE', __('case.PREVIOUS DATE')) }}
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::date('previous_date', null, ['class' => 'primary_input_field primary-input form-control', 'id' => 'previous_date', 'placeholder' => date('d-m-Y')]) }}
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                    {{ Form::label('NEXT DATE', __('NEXT DATE')) }}
                                                </div>
                                                <div class="primary_input col-md-9">
                                                    {{ Form::date('next_date', null, ['class' => 'primary_input_field primary-input form-control', 'id' => 'next_date', 'placeholder' => date('d-m-Y')]) }}
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                @includeIf('case.petitioner')


                                            </div>
                                            <br>
                                        </div>

                                        @includeIf('case.respondent')

                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF FOR', __('JUDGE Name')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('judge_name', null, ['class' => 'primary_input_field', 'placeholder' => __('case.JUDGE Name')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('COURT ROOM NO', __('COURT ROOM NO.')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('court_room_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Court room no')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('CASE STAGE', __('case.CASE STAGE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{-- {{ Form::select('stag_id', $data['stages'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }} --}}
                                            {{ Form::text('stage', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Case Stage')]) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Sr. No. in Court', __('Sr. No. in Court')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('sr_no_in_court', null, ['class' => 'primary_input_field', 'placeholder' => __('Sr. No. in Court')]) }}
                                        </div>

                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('BRIEF FOR', __('case.BRIEF FOR')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('brief_for', null, ['class' => 'primary_input_field', 'placeholder' => 'Hint: You can save as P-1 or R-3 which means you are briefing for Petitioner 1 or Respondent 3.']) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{-- <div class="d-flex justify-content-between"> --}}
                                            {{ Form::label('organization_id', __('case.ORGANIZATION')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="OrganizationModal()">+</button>
                                            {{-- </div> --}}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('organization_id', $data['orgs'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('TAGS/REFERENCE', __('case.TAGS/REFERENCE')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="ReferanceModal()">+</button>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('tags', $data['refs'], null, ['id' => 'custom_case_ref', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            {{-- {{ Form::text('tags', null, ['class' => 'primary_input_field', 'placeholder' => __('case.TAGS/REFERENCE')]) }} --}}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('POLICE STATION', __('case.POLICE STATION')) }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::text('police_station', null, ['class' => 'primary_input_field', 'placeholder' => __('case.POLICE STATION')]) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">

                                        <div class="primary_input col-md-3">
                                            {{ Form::label('PETITIONER', __('Client Name')) }}
                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm"
                                                onclick="ClientModal()">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::select('client_id', $data['clients'], null, ['required' => '', 'id' => 'custom_case_client', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'), 'data-parsley-errors-container' => '#organization_id_error']) }}
                                            {{-- {{ Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('Client Name')]) }} --}}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('DECIDED TOGGLE', __('DECIDED TOGGLE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <label class="switch_toggle" for="custom_active_checkbox1">
                                                <input name="decided_toggle" type="checkbox" id="custom_active_checkbox1"
                                                    value="check" onchange="custom_update_active_status(this)">
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                {{ Form::label('ABANDONED TOGGLE', __('ABANDONED TOGGLE')) }}
                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <label class="switch_toggle" for="custom_active_checkbox2">
                                                <input type="checkbox" name="switch_toggle" id="custom_active_checkbox2"
                                                    value="check" onchange="custom_update_active_status(this)">
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Remarks', 'Remarks') }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{ Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            {{ Form::label('Notes', 'Notes') }}
                                        </div>
                                        <div class="primary_input col-md-9">
                                            {{-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} --}}
                                            {{ Form::textarea('description', null, [
                                                'class' => 'primary_input_field summernote',
                                                'placeholder' => __('court.Court  Description'),
                                                'id' => 'custom_description',
                                                'rows' => 5,
                                                'maxlength' => 1500,
                                                'data-parsley-errors-container' => '#description_error',
                                            ]) }}
                                        </div>
                                    </div>

                                    @includeIf('case.case_file_custom')


                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"
                                            onclick="submit_custom();"><i class="ti-check"></i>Add
                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled
                                            style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>



                        </div>
                        {{-- Tab 5  --}}
                        <br>
                        <div class="tab-pane fade show active" id="pills-cnr-number" role="tabpanel"
                            aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case">
                                <div class="white_box_50px box_shadow_white">

                                    {!! Form::open([
                                        'route' => 'case.store',
                                        'class' => 'form-validate-jquery',
                                        'id' => 'content_form',
                                        'files' => false,
                                        'method' => 'POST',
                                    ]) !!}
                                    <div class="row">
                                        <div class="primary_input col-md-12">

                                            {{ Form::label('case_no', __('case.CNR NO.')) }}
                                            {{ Form::text('cnr_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO')]) }}
                                        </div>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i>{{ __('Fetch') }}
                                        </button>

                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>



                            <br>
                            <div class="tab-pane fade show active" id="pills-cnr-number_under" role="tabpanel"
                                aria-labelledby="pills-contact-tab">
                                <div class="col-12" id="custom_case">
                                    <div class="white_box_50px box_shadow_white">

                                        {!! Form::open([
                                            'route' => 'case.store',
                                            'class' => 'form-validate-jquery',
                                            'id' => 'content_form',
                                            'files' => false,
                                            'method' => 'POST',
                                        ]) !!}

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('CNR NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="MHNG" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief NO.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" value="225" class="primary_input_field"
                                                    placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('State:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Maharashtra"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('District:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="MAC Court No.2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Type:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="M.A.C.P"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="802"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Year:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Previous Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Next Date:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="20/07/2013"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        {{-- <!-- stop  --}}

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#petitionerModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Parshant"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Petitioner Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="P S Mirache"
                                                    class="primary_input_field" placeholder="Petitioner Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        {{-- Respondant  --}}
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondand:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="modal" data-target="#respondentModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Respondent Advocate:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Santosh"
                                                    class="primary_input_field" placeholder="Respondent Advocate"
                                                    readonly>
                                            </div>
                                        </div>


                                        {{-- start   --}}
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Judge Name:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Deshpande"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Court Room No.')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="219"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Case Stage:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Defence Evidence"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Brief For:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Organization:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="ICICI Lombars"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Tags/Refrense:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Police Station:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="-----------"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Clients Name:')) }}
                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button"
                                                    data-toggle="Modal" data-target="#clientModal">
                                                    +
                                                </button>
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Respondand No 2"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Contact:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="0234XXXXXX"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Email Id:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="abc@gmail.com"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('case_no', __('Address:')) }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <input type="text" name="" value="Nagpur"
                                                    class="primary_input_field" placeholder="CNR NO" readonly>
                                            </div>
                                        </div>


                                        {{-- Decided Toggle  --}}
                                        <h5>Decided</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Decided -->
                                        <!-- Abandoned -->
                                        <h5>Abandoned</h5>
                                        <div class="custom-control">
                                            <label class="switch_toggle" for="active_checkbox">
                                                <input type="checkbox" id="active_checkbox" value="check"
                                                    onchange="update_active_status(this)" checked>
                                                <div class="slider round"></div>
                                            </label>
                                        </div>
                                        <!-- Close Abamdoned -->
                                        <div class="row">
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Remarks', 'Remarks') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                {{ Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }}
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                                {{ Form::label('Notes', 'Notes') }}
                                            </div>
                                            <div class="primary_input col-md-9">
                                                <!-- {{ Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks']) }} -->
                                                {{ Form::textarea('description', null, [
                                                    'class' => 'primary_input_field summernote',
                                                    'placeholder' => __('court.Court  Description'),
                                                    'rows' => 5,
                                                    'maxlength' => 1500,
                                                    'data-parsley-errors-container' => '#description_error',
                                                ]) }}
                                            </div>
                                        </div>
                                        @includeIf('case.file')



                                        <!--  -->
                                        <div class="text-center mt-3">
                                            <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                    class="ti-check"></i>{{ __('common.Save') }}
                                            </button>

                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>



                            </div>
                            <!--  -->

                        </div>

                    </div>
                    <!-- form end =-->
                </div>
            </div>


        </div>


    </section>

    <!-- <div class="modal fade animated act_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated client_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated client_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated lawyer_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated case_stage_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated court_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated court_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="modal fade animated case_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         role="dialog"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div> -->




    <!-- Petitioner Modal -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="custom_exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="custom_exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="custom_client_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Client Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="client_name">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="text" class="primary_input_field" placeholder="Contact"
                                        id="client_contact">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="text" class="primary_input_field" placeholder="Email"
                                        id="client_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_address">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Gender</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_gender">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Description</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="client_description">
                                </div>
                            </div>


                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Client()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>
                    <div id="custom_org_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-10">
                                    <!--  -->
                                </div>
                                <div class="primary_input col-md-2">
                                    <!-- <button type="button" class="primary-btn small fix-gr-bg add_field_button">+</button> -->
                                </div>
                            </div>

                            @includeIf('case.add_organization')
                            {{-- <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="cust_name_1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="number" class="primary_input_field" placeholder="Contact"
                                        id="cust_contact_1">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="email" class="primary_input_field" placeholder="Email"
                                        id="cust_email_1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="cust_org_address">
                                </div>
                            </div> --}}
                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Organization()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>
                    <div id="custom_ref_add">
                        <form action="#">
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">reference Name</label>
                                    <input type="text" class="primary_input_field" placeholder="Name"
                                        id="cust_ref_name">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Contact</label>
                                    <input type="text" class="primary_input_field" placeholder="Contact"
                                        id="cust_ref_contact">
                                </div>

                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Email</label>
                                    <input type="text" class="primary_input_field" placeholder="Email"
                                        id="cust_ref_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="primary_input col-md-12">
                                    <label for="petitioner">Address</label>
                                    <input type="text" class="primary_input_field" placeholder="Address"
                                        id="cust_ref_address">
                                </div>
                            </div>


                            <div class="text-center mt-3">
                                <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Reff()"><i
                                        class="ti-check"></i>Add
                                </a>
                                <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                    data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}

    <!--Petitioner Modal -->
    <!-- Respondent Modal -->
    <div class="modal fade" id="respondendModal" tabindex="-2" role="dialog"
        aria-labelledby="respondandModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="respondandModalLabel">Respondand </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#">
                        <div class="row">
                            <div class="primary_input col-md-10">
                                <!--  -->
                            </div>
                            <div class="primary_input col-md-2">
                                <button type="button"
                                    class="primary-btn small fix-gr-bg add_field_button_res">+</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent</label>
                                <input type="text" class="primary_input_field" placeholder="Respondent">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent Advocate</label>
                                <input type="text" class="primary_input_field" placeholder="Respondent Advocate">
                            </div>
                        </div>

                        <div class="row">
                            <div class="input_fields_wrap_res primary_input col-md-12">

                            </div>
                        </div>


                        <div class="text-center mt-3">
                            <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                    class="ti-check"></i>{{ __('Add') }}
                            </button>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <a type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            <input class="primary-btn small fix-gr-bg" type="submit"
                                value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>

    <!--  -->

    <!--Respondent Modal -->

    <!-- Client Modal For View -->


    <!-- Petitioner modal for View -->
    <div class="modal fade" id="petitionerModal" tabindex="-1" role="dialog"
        aria-labelledby="petitionerModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="petitionerModalLabel">Petitioner </h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">PETITIONER 1</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner" readonly>
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Petitioner Advocate 1</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner Advocate"
                                    readonly>
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                                    class="ti-check"></i>{{ __('Add') }}
                            </button>
                        </div>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <a type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            <input class="primary-btn small fix-gr-bg" type="submit"
                                value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>

    <!-- Respodent Modal For View -->

    <div class="modal fade" id="respondentModal" tabindex="-1" role="dialog"
        aria-labelledby="respondentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="respondentModalLabel">Respondent </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Respondent Advocate</label>
                                <input type="text" class="primary_input_field" placeholder="Petitioner Advocate">
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                                    class="ti-check"></i>{{ __('Add') }}
                            </button>
                        </div>
                    </form>
                    <div class="col-lg-12 text-center">
                        <div class="mt-40 d-flex justify-content-between">
                            <a type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</a>
                            <input class="primary-btn small fix-gr-bg" type="submit"
                                value="{{ __('common.Save') }}">
                        </div>
                    </div>
                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>

    <!-- Client Modal For View -->

    <!-- Organization Modal For View -->
    <div class="modal fade" id="exampleModalorg" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalorgLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalorgLabel">Petitioner </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#">
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Name</label>
                                <input type="text" class="primary_input_field" placeholder="Name"
                                    id="client_name">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Contact</label>
                                <input type="text" class="primary_input_field" placeholder="Contact"
                                    id="client_contact">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Email</label>
                                <input type="text" class="primary_input_field" placeholder="Email"
                                    id="client_email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Address</label>
                                <input type="text" class="primary_input_field" placeholder="Address"
                                    id="client_address">
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Gender</label>
                                <input type="text" class="primary_input_field" placeholder="Address"
                                    id="client_gender">
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Description</label>
                                <input type="text" class="primary_input_field" placeholder="Address"
                                    id="client_description">
                            </div>
                        </div>


                        <div class="text-center mt-3">
                            <a href="#" class="primary-btn small fix-gr-bg" onclick="Add_Client()"><i
                                    class="ti-check"></i>Add
                            </a>
                            <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                data-dismiss="modal">{{ __('common.Cancel') }}</a>

                    </form>

                    <!-- close button -->
                </div>
            </div>
        </div>
    </div>
    <!--End Organization Modal For View -->

@stop

@push('admin.scripts')
    <script>
        $("#custom_state_id").change(function() {

            var id = $("#custom_state_id").val();
            $.ajax({
                url: '{{ url('custom/district') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                },
                type: "post",
                success: function(data) {
                    // console.log(data);
                    $("#custom_district_id").parent().find(".nice-select.primary_select .list").empty();
                    $("#custom_district_id").empty();
                    var selectoption = "";
                    var selectlist = "";
                    data.forEach(function(Value) {
                        selectoption = selectoption + '<option value="' + Value.id + '">' +
                            Value.name + '</option>';
                        selectlist = selectlist + '<li data-value="' + Value.id +
                            '" class="option">' + Value.name + '</li>';
                    });

                    $("#custom_district_id").parent().find(".nice-select.primary_select .list").append(
                        selectlist);
                    $("#custom_district_id").append(selectoption);

                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });

        });


        function submit_custom() {
            $("#custom_description").val($("#custom_description").parent().find(".note-editable.card-block").html());

            // $("#custom_form").submit();
        }


        function ClientModal() {
            $("#exampleModal").modal('show');
            $("#custom_exampleModalLabel").text('Client');
            $("#custom_client_add").css('display', 'block');
            $("#custom_org_add").css('display', 'none');
            $("#custom_ref_add").css('display', 'none');

        }

        function OrganizationModal() {
            $("#exampleModal").modal('show');
            $("#custom_exampleModalLabel").text('Organization');
            $("#custom_org_add").css('display', 'block');
            $("#custom_client_add").css('display', 'none');
            $("#custom_ref_add").css('display', 'none');
        }

        function ReferanceModal() {
            $("#exampleModal").modal('show');
            $("#custom_exampleModalLabel").text('Referance');
            $("#custom_ref_add").css('display', 'block');
            $("#custom_org_add").css('display', 'none');
            $("#custom_client_add").css('display', 'none');
        }

        function Add_Client() {
            $.ajax({
                url: '{{ url('add/client') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    name: document.getElementById('client_name').value,
                    mobile: document.getElementById('client_contact').value,
                    email: document.getElementById('client_email').value,
                    address: document.getElementById('client_address').value,
                    gender: document.getElementById('client_gender').value,
                    description: document.getElementById('client_description').value,
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#custom_case_client").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.model.id + '" class="option">' + data.model.name + '</li>'
                    );
                    $("#custom_case_client").append('<option value="' + data.model.id + '">' + data.model.name +
                        '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }



        function Add_Organization() {
            var email = [];
            var contact = [];
            var name = [];
            var i = 1;
            // alert($('#cust_name_'+ 1).val());
            for (; i <= custom_org_index; i++) {
                email[i] = $('#cust_email_' + i).val();
                contact[i] = $('#cust_contact_' + i).val();
                name[i] = $('#cust_name_' + i).val();
            }
            // console.log("this is list of email" + email);
            $.ajax({
                url: '{{ url('add/organization') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    organization_name: $('#cust_org_name').val(),
                    representator: name,
                    email: email,
                    contact: contact,
                    address: $('#cust_org_address').val()
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#organization_id").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.id + '" class="option">' + data.organization_name +
                        '</li>');
                    $("#organization_id").append('<option value="' + data.id + '">' + data.organization_name +
                        '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }

        function Add_Reff() {
            $.ajax({
                url: '{{ url('add/referance') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    name: $('#cust_ref_name').val(),
                    email: $('#cust_ref_email').val(),
                    contact: $('#cust_ref_contact').val(),
                    address: $('#cust_ref_address').val()
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    $("#custom_case_ref").parent().find(".nice-select.primary_select .list").append(
                        '<li data-value="' + data.id + '" class="option">' + data.name + '</li>');
                    $("#custom_case_ref").append('<option value="' + data.id + '">' + data.name + '</option>');
                    console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }

        $(document).ready(function() {
            //set initial state.
            $('#custom_active_checkbox1').prop('checked', false);
            $('#custom_active_checkbox2').prop('checked', false);

            $('#custom_active_checkbox1').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox2').val()) {
                        $('#custom_active_checkbox2').prop('checked', false);
                    }

                }
            });
            $('#custom_active_checkbox2').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox1').val()) {
                        $('#custom_active_checkbox1').prop('checked', false);
                    }

                }
            });
            $('#cust_case_year').change(function() {
                alert(this.value);
                date = new Date(this.value);
                alert(date);
                year = date.getFullYear();
                alert(year);
                $(this).val(year);
                alert($(this).val());
            });

        });
        // function changeYear(id){
        //     date = new Date(document.getElementById("current_date").value);
        //     alert(date);
        //     // year = date.getFullYear();
        //     // month = date.getMonth() + 1;
        //     // day = date.getDate();
        //     // document.getElementById("current_date").innerHTML = month + "/" + day + "/" + year;
        // }

        $("#case_no_state").change(function() {

            var id = $("#case_no_state").val();
            $.ajax({
                url: '{{ url('custom/district') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                },
                type: "post",
                success: function(data) {
                    // console.log(data);
                    $("#case_no_district").parent().find(".nice-select.primary_select .list").empty();
                    $("#case_no_district").empty();
                    var selectoption = "";
                    var selectlist = "";
                    data.forEach(function(Value) {
                        selectoption = selectoption + '<option value="' + Value.val + '">' +
                            Value.name + '</option>';
                        selectlist = selectlist + '<li data-value="' + Value.val +
                            '" class="option">' + Value.name + '</li>';
                    });

                    $("#case_no_district").parent().find(".nice-select.primary_select .list").append(
                        selectlist);
                    $("#case_no_district").append(selectoption);

                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });

        });
        $("#case_no_district").change(function() {

            var state_id = $("#case_no_state").val();
            var dist_id = $("#case_no_district").val();
            $.ajax({
                url: '{{ url('custom/bench') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    state_id: state_id,
                    dist_id: dist_id
                },
                type: "post",
                success: function(data) {
                    // console.log(data);
                    $("#case_no_bench").parent().find(".nice-select.primary_select .list").empty();
                    $("#case_no_bench").empty();
                    var selectoption = "";
                    var selectlist = "";
                    data.forEach(function(Value) {
                        selectoption = selectoption + '<option value="' + Value.val + '">' +
                            Value.name + '</option>';
                        selectlist = selectlist + '<li data-value="' + Value.val +
                            '" class="option">' + Value.name + '</li>';
                    });

                    $("#case_no_bench").parent().find(".nice-select.primary_select .list").append(
                        selectlist);
                    $("#case_no_bench").append(selectoption);

                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });

        });
        $("#case_no_bench").change(function() {

            var state_id = $("#case_no_state").val();
            var dist_id = $("#case_no_district").val();
            var bench_id = $("#case_no_bench").val();
            $.ajax({
                url: '{{ url('custom/case_type') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    state_id: state_id,
                    dist_id: dist_id,
                    bench_id: bench_id
                },
                type: "post",
                success: function(data) {
                    // console.log(data);
                    $("#case_no_types").parent().find(".nice-select.primary_select .list").empty();
                    $("#case_no_types").empty();
                    var selectoption = "";
                    var selectlist = "";
                    data.forEach(function(Value) {
                        selectoption = selectoption + '<option value="' + Value.val + '">' +
                            Value.name + '</option>';
                        selectlist = selectlist + '<li data-value="' + Value.val +
                            '" class="option">' + Value.name + '</li>';
                    });

                    $("#case_no_types").parent().find(".nice-select.primary_select .list").append(
                        selectlist);
                    $("#case_no_types").append(selectoption);

                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });

        });
    </script>
@endpush
