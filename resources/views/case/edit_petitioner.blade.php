<div class="col-12">
    <div class="row attach-petitioner-row">
        @foreach ($data['petitioners'] as $key => $row)
            <div class="col-12" id="petetioner-{{ $key }}">
                <div class="row attach-item">
                    <div class="primary_input col-md-3">
                        {{ Form::label('PETITIONER', __('case.PETITIONER')) }}
                    </div>
                    <div class="primary_input col-md-8">
                        {{ Form::text('petitioner[]', $row->petitioner, ['class' => 'primary_input_field']) }}
                        <br><br>
                    </div>
                    <div class="primary_input col-md-1">

                        @if ($key == 0)
                            <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button"
                                onclick="petitioner_add();"> <i class="ti-plus"></i> </span>
                        @else
                            <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button"
                                onclick="remove_petetioner('petetioner-{{ $key }}');"> <i class="ti-trash"></i>
                            </span>
                        @endif
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="primary_input col-md-3">
                        {{ Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE')) }}
                    </div>
                    <div class="primary_input col-md-9">
                        {{ Form::text('petitioner_advocate[]', $data['petitioners_advocate'][$key]->petitioner_advocate, ['class' => 'primary_input_field']) }}
                        <br><br>

                    </div>
                    @if ($key < count($data['petitioners']))
                        <br>
                        <br>
                        <br>
                    @endif

                </div>
            </div>
        @endforeach
    </div>
</div>

<script>
    var index = 0;

    function petitioner_add() {
        index = $('.attach-item').length
        addNewPetitioner(index)
    }

    function addNewPetitioner(index) {
        "use strict";

        // var attachFile = '<div class="attach-file-section d-flex align-items-center">\n' +
        //     '        <div class="primary_input flex-grow-1">\n' +
        //     '            <div class="primary_file_uploader">\n' +
        //     '                <input class="primary-input" type="text" id="placeholderStaffsName" placeholder="' + trans('js.Browse File') + '" readonly>\n' +
        //     '                <button class="" type="button">\n' +
        //     '                    <label class="primary-btn small fix-gr-bg"\n' +
        //     '                           for="attach_file_' + index + '">' + trans('js.Browse') + '</label>\n' +
        //     '                    <input type="file" class="d-none file-upload-multi" name="file[]" id="attach_file_' + index + '">\n' +
        //     '                </button>\n' +
        //     '            </div>\n' +
        //     '        </div>\n' +
        //     '        <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only case-attach" type="button" > <i class="ti-trash"></i> </span>\n' +
        //     '    </div>';

        var add_petitioner = `
        <div class="col-12" id="petetioner-` + index +
            `">
            <div class="row">    
                <div class="primary_input col-md-3">
                    {{ Form::label('PETITIONER', __('case.PETITIONER')) }}
                </div>
                <div class="primary_input col-md-8">
                    {{ Form::text('petitioner[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER'), 'required' => 'required']) }}
                </div>
                <div class="primary_input col-md-1">
                    <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" onclick="remove_petetioner('petetioner-` +
            index + `');" type="button" > <i class="ti-trash"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                    {{ Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE')) }}
                </div>
                <div class="primary_input col-md-9">
                    {{ Form::text('petitioner_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE'), 'required' => 'required']) }}
                </div>
                <br><br><br>
            </div>
        </div>`

        $('.attach-petitioner-row').append(add_petitioner);
    }


    function remove_petetioner(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
</script>
