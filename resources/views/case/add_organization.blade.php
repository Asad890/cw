{{-- <div class="org_attach-item">
    <div class="row">
        <div class="primary_input col-md-12">
            <label for="petitioner">Name</label>
            <input type="text" class="primary_input_field" placeholder="Name" id="cust_name_1">
        </div>
    </div>
    <div class="row">
        <div class="primary_input col-md-12">
            <label for="petitioner">Contact</label>
            <input type="number" class="primary_input_field" placeholder="Contact" id="cust_contact_1">
        </div>

    </div>
    <div class="row">
        <div class="primary_input col-md-12">
            <label for="petitioner">Email</label>
            <input type="email" class="primary_input_field" placeholder="Email" id="cust_email_1">
        </div>
    </div>
    <div class="row">
        <div class="primary_input col-md-12">
            <label for="petitioner">Address</label>
            <input type="text" class="primary_input_field" placeholder="Address" id="cust_org_address">
        </div>
    </div>
    <div class="primary_input col-md-1">
        <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button"
            onclick="organization_add();"> <i class="ti-plus"></i> </span>
    </div>
</div> --}}

<div class="col-12">
    <div class="row attach-organization-row">
        <div class="col-12">
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Name<b style="color:red">*</b></label>
                    <input type="text" class="primary_input_field" placeholder="Organization Name" id="cust_org_name"
                        required>
                </div>
            </div>
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Address<b style="color:red">*</b></label>
                    <input type="text" class="primary_input_field" placeholder="Organization Address"
                        id="cust_org_address" required>
                </div>
            </div>
            <div class="row attach-org">
                <div class="primary_input col-md-11">
                    <label for="petitioner">Name<b style="color:red">*</b></label>
                    <input type="text" class="primary_input_field" placeholder="Authorized Person Name"
                        id="cust_name_1" required>
                </div>

                <div class="primary_input col-md-1">
                    <span style="cursor:pointer; top:50%;" class="primary-btn small fix-gr-bg icon-only" type="button"
                        onclick="organization_add();"> <i class="ti-plus"></i> </span>
                </div>
            </div>
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Contact<b style="color:red">*</b></label>
                    <input type="number" class="primary_input_field" placeholder="Contact" id="cust_contact_1"
                        required>
                </div>

            </div>
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Email<b style="color:red">*</b></label>
                    <input type="email" class="primary_input_field" placeholder="Email" id="cust_email_1" required>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var custom_org_index = 1;

    function organization_add() {
        custom_org_index = custom_org_index + $('.attach-org').length
        // alert(custom_org_index);
        addNewOrganization(custom_org_index)
    }

    function addNewOrganization(custom_org_index) {
        "use strict";
        var add_organization = `
        <div class="col-12" id="organization-` + custom_org_index +
            `">
                <div class="row">
                <div class="primary_input col-md-10">
                    <label for="petitioner">Name<b style="color:red">*</b></label>
                    <input type="text" class="primary_input_field" placeholder="Name" id="cust_name_` +
            custom_org_index +
            `">
                </div>

                <div class="primary_input col-md-1">
                    <span style="cursor:pointer; top:50%;" class="primary-btn small fix-gr-bg icon-only" type="button"
                        onclick="organization_add();"> <i class="ti-plus"></i> </span>
                </div>
                <div class="primary_input col-md-1">
                    <span style="cursor:pointer; top:50%;" class="primary-btn small fix-gr-bg icon-only" onclick="remove_organization('organization-` +
            custom_org_index + `');" type="button" > <i class="ti-trash"></i> </span>
                </div>
            </div>
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Contact<b style="color:red">*</b></label>
                    <input type="number" class="primary_input_field" placeholder="Contact" id="cust_contact_` +
            custom_org_index + `">
                </div>

            </div>
            <div class="row">
                <div class="primary_input col-md-12">
                    <label for="petitioner">Email<b style="color:red">*</b></label>
                    <input type="email" class="primary_input_field" placeholder="Email" id="cust_email_` +
            custom_org_index + `">
                </div>
            </div>
             
        </div>`

        $('.attach-organization-row').append(add_organization);
    }


    function remove_organization(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
</script>
