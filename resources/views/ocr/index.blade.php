@extends('layouts.master', ['title' => __('Create New Case')])

@section('mainContent')

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                <h3 style="text-align: center;">Image to Text</h3>
                                <div class="row form-group">
                                    <div class="primary_input col-md-2">
                                    
                                    <div class="upload-btn-wrapper">
                                    <button class="primary-btn small fix-gr-bg">Select File</button>
                                    <input type="file" name="myfile" />
                                    </div>

                                    </div>
                                    <div class="primary_input col-md-4">
                                        <select class="form-control">
                                            <option>English</option>
                                            <option>Afrikans</option>
                                            <option>Akbanian</option>
                                            <option>Basque</option>
                                            <option>Barazilian</option>
                                            <option>Bulgarian</option>
                                            <option>Bayelorussiun</option>
                                            <option>Catalan</option>
                                            <option>Chinesesimplified</option>
                                        </select>
                                    </div>
                                    <div class="primary_input col-md-4">
                                        <select class="form-control">
                                            <option>Microsoft Word (Docx)</option>
                                            <option>Microsoft Excel (xlsx)</option>
                                            <option>Text Pain (txt)</option>
                                        </select>
                                    </div>
                                    <div class="primary_input col-md-2">
                                        <div class="upload-btn-wrapper">
                                            <button class="primary-btn small fix-gr-bg">Convert</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                      
                </div>
                    <!-- form end =-->
               

        </div>

        <!--  -->
                            

        </section>
@stop
      