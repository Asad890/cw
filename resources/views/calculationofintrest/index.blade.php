@extends('layouts.master', ['title' => __('Create New Case')])

@section('mainContent')

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->

                                <div class="row form-group">
                                    <div class="primary_input col-md-3">
                                            <label>Interest Type:</label>
                                            <select class="primary_input_field">
                                                <option>simple interest</option>
                                                <option>Compound interest</option>
                                            </select>
                                    </div>
                                </div>
                                    <!-- Close -->
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label for="ABC"> Principal Amount:</label>
                                    </div>
                                    <div class="col-3">
                                    <input class="primary_input_field" placeholder="0">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label for="ABC"> Annual Rate :</label>
                                    </div>
                                    <div class="col-3">
                                    <input class="primary_input_field" placeholder="0">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                    <label>From:</label>
                                    </div>
                                    <div class="col-3">
                                    <input type="text" name="previous_date" class="primary_input_field primary-input form-control datetime" placeholder="From">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                    <label>To:</label>
                                    </div>
                                    <div class="col-3">
                                    <input type="text" name="previous_date" class="primary_input_field primary-input form-control datetime" placeholder="From">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                    <label>No. of Days:</label>
                                    </div>
                                    <div class="col-3">
                                    <input class="primary_input_field" placeholder="23" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                    <label>Interest Earned:</label>
                                    </div>
                                    <div class="col-3">
                                    <input class="primary_input_field" placeholder="23" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                    <label>Total Value :</label>
                                    </div>
                                    <div class="col-3">
                                    <input class="primary_input_field" placeholder="23" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="primary_input col-md-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                    class="ti-check"></i>{{ __('Calculate') }}
                                            </button>
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- form end =-->
               

        </div>

        <!--  -->
                            

        </section>
@stop
      