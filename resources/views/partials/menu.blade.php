@php
    Illuminate\Support\Facades\Cache::rememberForever('languages', function () {
        return \Modules\Localization\Entities\Language::where('status', 1)->get();
    });
    $pending = App\Models\Notification::where('status', 0)->count();
    $latest = App\Models\Notification::latest()
        ->where('status', 0)
        ->get();
@endphp
<div class="container-fluid no-gutters d-print-none">
    <!-- <div class="row">
   <div class="col-lg-12 p-0">
    <div class="marquee-container">
     <div class="Marquee">
     <div class="Marquee-content">
      <div class="Marquee-tag">1</div>
      <div class="Marquee-tag">2</div>
      <div class="Marquee-tag">3</div>
      <div class="Marquee-tag">1</div>
      <div class="Marquee-tag">2</div>
      <div class="Marquee-tag">3</div>
      <div class="Marquee-tag">1</div>
      <div class="Marquee-tag">2</div>
      <div class="Marquee-tag">3</div>
      <div class="Marquee-tag">1</div>
      <div class="Marquee-tag">2</div>
      <div class="Marquee-tag">3</div>
     </div>
     </div>
     </div>
   </div>
 </div> -->
    <div class="row">
        <div class="col-lg-12 p-0">
            <div class="header_iner d-flex justify-content-between align-items-center">
                <div class="small_logo_crm d-lg-none">
                    <a href="{{ url('/home') }}"> <img src="" alt=""></a>

                </div>
                <div id="sidebarCollapse" class="sidebar_icon  d-lg-none">
                    <i class="ti-menu"></i>
                </div>
                <div class="collaspe_icon open_miniSide">
                    <i class="ti-menu"></i>
                </div>
                <div class="serach_field-area ml-40">
                    @if (auth()->user()->role_id)
                        <div class="search_inner">
                            <form action="#">
                                <div class="search_field">
                                    <input type="text" placeholder="{{ __('common.Search') }}" id="search"
                                        onkeyup="showResult(this.value)">
                                </div>

                            </form>
                        </div>
                        <div id="livesearch"></div>
                    @endif
                </div>
                <div class="header_middle d-none d-md-block">
                    @if (auth()->user()->role_id)
                        <div class="select_style d-flex">

                        </div>
                    @endif
                </div>
                <div class="header_right d-flex justify-content-between align-items-center">
                    <div class="header_notification_warp d-flex align-items-center">
                        <!-- Notificaiton -->
                        <div class="dropdown">
                            <div class="badge-top-container" role="button" id="dropdownNotification"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-primary">{{ $pending }}</span>
                                <i class="ti-bell text-muted header-icon"></i>
                            </div>
                            <!-- Notification dropdown -->

                            <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none"
                                aria-labelledby="dropdownNotification" data-perfect-scrollbar
                                data-suppress-scroll-x="true">

                                <div class="dropdown-item d-flex">

                                    <div class="notification-details flex-grow-1">
                                        @forelse ($latest as $item)
                                            <div class="row">
                                                <div class="col-3">

                                                </div>
                                                <div class="col-9"
                                                    style="background-color:#D4F5E6; border: 2px solid #83DBA3; border-radius: 10px;">
                                                    <div class="clearfix">
                                                        <!-- <div class="pull-right">$50</div> -->
                                                        <p class="pull-right" style="color:#83DBA3 ;">
                                                            {{ $item->title }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="m-0 d-flex align-items-center">
                                                <span
                                                    class="badge badge-pill badge-primary">{{ $item->created_at->format('h:i:s A') }}</span>
                                            </p>
                                            <p class="m-0 d-flex align-items-center">

                                                <span><small>Section :</small></span>

                                                <span class="flex-grow-1"></span>
                                                <span class="text-small text-muted ml-auto"><small> Year </small>
                                                    <span class="text-small text-info">
                                                        {{ $item->created_at->format('y') }}</span></span>
                                            </p>
                                            <a href="{{ route('status', $item->id) }}" class="text-warning">
                                                <p class="text-small  m-0">
                                                    {{ $item->content }}
                                                </p>
                                            </a>
                                            <p class="text-small text-muted">
                                                @if ($item->status == 0)
                                                    <p class="pull-right" style="color:#fd0000 ;">Not Read</p>
                                                @else
                                                    <p class="pull-right" style="color:#15fd00 ;">Read</p>
                                                @endif
                                            </p>
                                        @empty
                                            <p class="text-center">You Have No Notification for Today</p>
                                        @endforelse
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Notificaiton End -->

                    </div>


                    <div class="profile_info">
                        {{--                        <img src="{{ file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public\backEnd/img/staff.jpg') }}" alt="#"> --}}
                        <div class="userThumb_40"
                            style="background-image: url('{{ file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public/backEnd/img/staff.jpg') }}')">

                        </div>
                        <div class="profile_info_iner">
                            <div class="use_info d-flex align-items-center">
                                <div class="thumb">
                                    <div class="userThumb_50"
                                        style="background-image: url('{{ file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public/backEnd/img/staff.jpg') }}')">

                                    </div>
                                    {{--                                    <img src="{{ file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public\backEnd/img/staff.jpg') }}" alt="#"> --}}
                                </div>
                                <div class="user_text">
                                    <h5><a href="{{ route('profile_view') }}">{{ auth()->user()->name }}</a></h5>
                                    <span>{{ auth()->user()->email }}</span>

                                </div>
                            </div>

                            <div class="profile_info_details">
                                @if (permissionCheck('setting.index'))
                                    <a href="{{ route('setting.index') }}"> <i class="ti-settings"></i>
                                        <span>{{ __('common.Setting') }}</span>
                                    </a>
                                @endif
                                @if (auth()->user()->role_id)
                                    <a href="{{ route('profile_view') }}">
                                        <i class="ti-user"></i>
                                        <span>{{ __('common.Profile') }}</span>
                                    </a>
                                @else
                                    <a href="{{ route('client.my_profile') }}">
                                        <i class="ti-user"></i>
                                        <span>{{ __('common.Profile') }}</span>
                                    </a>
                                @endif

                                <a href="{{ route('change_password') }}">
                                    <i class="ti-key"></i>
                                    <span>{{ __('common.Change Password') }}</span>
                                </a>

                                <a href="{{ route('logout') }}" id="logout">
                                    <i class="ti-unlock"></i>
                                    <span>{{ __('common.Logout') }}</span>
                                </a>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script type="text/javascript">
        function change_Language() {
            var code = $('#language_code').val();
            $.post('{{ route('language.change') }}', {
                _token: '{{ csrf_token() }}',
                code: code
            }, function(data) {

                if (data.success) {
                    location.reload();
                    toastr.success(data.success);
                } else {
                    toastr.error(data.error);
                }
            });
        }
    </script>
@endpush
