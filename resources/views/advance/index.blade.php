@extends('layouts.master', ['title' => __('Create New Case')])

@section('mainContent')

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                    <li class="nav-item">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-district-court" role="tab" aria-controls="pills-contact" aria-selected="true"><h3 class="mb-0 ">{{__('District Courts and Tribunals')}}</h3></a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-high-court" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 ">{{__('High Court')}}</h3></a>
                        </li>
                        
                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                
                                <div class="row">
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Breif No.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Breif No</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case No.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case No</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Type.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Type</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Year.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Year</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Prev Date')}}</label>
                                                    <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Previous Date"> 
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Next date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Next Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('From Previous Date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="From Previous Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('To Next Date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="To Previous Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Petitioner')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Petitioner</option>
                                                    </select>
                                    </div>
                                    
                                    
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Respondent')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Respondent</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Petitioner Advocate')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Petitioner Advocate</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Respondent Advocate')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Respondent Advocate</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Courts')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Courts</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Stage')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Stage</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Judge Name')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Judge Name</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Brief For')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Brief For</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Organization')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Organization</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Tags/Ref')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Tags/Ref</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Police station')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Police station</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('District')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>District</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('State')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>State</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Registered on casewise From')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Registered on casewise From</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Registered on casewise To')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Registered on casewise To</option>
                                                    </select>
                                    </div>
                                </div>
                                    
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i>{{ __('common.Search') }}
                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    <!-- Close Form -->
                                   
                                    <!-- Table Start -->
                                    
                                                    
                                </div>
                                </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                <div class="col-lg-12">
                                <div class="QA_section QA_section_heading_custom check_box_table">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <div class="">
                                            <table class="table Crm_table_active3">
                                                <thead>
                                                    <tr>

                                                        <th scope="col">{{ __('Sr.No.') }}</th>
                                                        <th scope="col">{{ __('Brief No.') }}</th>
                                                        <th scope="col">{{ __('Prev Date') }}</th>
                                                        <th scope="col">{{ __('Courts') }}</th>
                                                        <th scope="col">{{ __('Case No.') }}</th>
                                                        <th scope="col">{{ __('Petitioner') }}</th>
                                                        <th scope="col">{{ __('Respondent') }}</th>
                                                        <th scope="col">{{ __('Stage') }}</th>
                                                        <th scope="col">{{ __('not Understand') }}</th>
                                                        <th scope="col">{{ __('Court room no') }}</th>
                                                        <th scope="col">{{ __('Judge Name') }}</th>
                                                        <th scope="col">{{ __('Brief For') }}</th>
                                                        <th scope="col">{{ __('Organization') }}</th>
                                                        <th scope="col">{{ __('Tags/Refrence') }}</th>
                                                        <th scope="col">{{ __('Police Station') }}</th>
                                                        <th scope="col">{{ __('Remarks') }}</th>
                                                        <th scope="col">{{ __('Next Date') }}</th>
                                                        <th scope="col">{{ __('Action') }}</th>
                                                        <th scope="col">{{ __('Select') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td> <a href=""> 1 </a></td>
                                                        <td> <a href="">313 </a></td>
                                                        <td> <a href="">23/3/2020 </a></td>
                                                        <td> <a href="">Mact 2 </a></td>
                                                        <td> <a href="">MACP/205/2022 </a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Petitioner" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Hirendra</a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">Arguement
                                                            </a></td>
                                                        <td> <a href="">50 </a></td>
                                                        <td> <a href="">219 </a></td>
                                                        <td> <a href="">R.K </a></td>
                                                        <td> <a href="">R-2 </a></td>
                                                        <td> <a href="">Royal Sundaram </a></td>
                                                        <td> <a href="">Blank </a></td>
                                                        <td> <a href="">Blank </a></td>
                                                        <td> <a href="">To File Ws </a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">23/3/2020
                                                            </a></td>
                                                        <td><a href="#" data-target="#reference" data-toggle="modal"data-modal-size="modal-md">
                                                            <span class="fa fa-refresh"></span>
                                                            </a>
                                                        </td>
                                                        <td> <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        Select
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <a href="#"
                                                           class="dropdown-item"><i
                                                                class="icon-file-eye"></i> 
                                                            View</a>
                                                        
                                                                <a href="#"
                                                                   class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i>Edit
                                                                </a>
                                                            
                                                       
                                                            <span id="delete_item" data-id="#" data-url="#" class="dropdown-item"><i class="icon-trash"></i> Delete
                                                            </span>
                                                       

                                                    </div>
                                                </div>
                                            </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Tab 2 -->
                        <div class="tab-pane fade" id="pills-high-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                <div class="row">
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Breif No.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Breif No</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case No.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case No</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Type.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Type</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Year.')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Year</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Prev Date')}}</label>
                                                    <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Previous Date"> 
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Next date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Next Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('From Previous Date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="From Previous Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('To Next Date')}}</label>
                                                           <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="To Previous Date">
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('To Next Date')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>To Next Date</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Petitioner')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Petitioner</option>
                                                    </select>
                                    </div>
                                    
                                    
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Respondent')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Respondent</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Petitioner Advocate')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Petitioner Advocate</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Respondent Advocate')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Respondent Advocate</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Courts')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Courts</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Case Stage')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Case Stage</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Judge Name')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Judge Name</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Brief For')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Brief For</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Organization')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Organization</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Tags/Ref')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Tags/Ref</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Police station')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Police station</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('District')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>District</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('State')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>State</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Registered on casewise From')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Registered on casewise From</option>
                                                    </select>
                                    </div>
                                    <div class="col-lg-3">
                                            <label class="primary_input_label"
                                                           for="">{{__('Registered on casewise To')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Registered on casewise To</option>
                                                    </select>
                                    </div>
                                </div>

                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i>{{ __('common.Search') }}
                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i>{{ __('common.Creating') . '...' }}
                                        </button>

                                    </div>

                                    <!-- Close Form -->
                                    
                                    <!-- Table Start -->
                                                                                        
                                </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <div class="col-lg-12">
                                <div class="QA_section QA_section_heading_custom check_box_table">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <div class="">
                                            <table class="table Crm_table_active3">
                                                <thead>
                                                    <tr>

                                                        <th scope="col">{{ __('Sr.No.') }}</th>
                                                        <th scope="col">{{ __('Brief No.') }}</th>
                                                        <th scope="col">{{ __('Prev Date') }}</th>
                                                        <th scope="col">{{ __('Courts') }}</th>
                                                        <th scope="col">{{ __('Case No.') }}</th>
                                                        <th scope="col">{{ __('Petitioner') }}</th>
                                                        <th scope="col">{{ __('Respondent') }}</th>
                                                        <th scope="col">{{ __('Stage') }}</th>
                                                        <th scope="col">{{ __('not Understand') }}</th>
                                                        <th scope="col">{{ __('Court room no') }}</th>
                                                        <th scope="col">{{ __('Judge Name') }}</th>
                                                        <th scope="col">{{ __('Brief For') }}</th>
                                                        <th scope="col">{{ __('Organization') }}</th>
                                                        <th scope="col">{{ __('Tags/Refrence') }}</th>
                                                        <th scope="col">{{ __('Police Station') }}</th>
                                                        <th scope="col">{{ __('Remarks') }}</th>
                                                        <th scope="col">{{ __('Next Date') }}</th>
                                                        <th scope="col">{{ __('Action') }}</th>
                                                        <th scope="col">{{ __('Select') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td> <a href=""> 1 </a></td>
                                                        <td> <a href="">313 </a></td>
                                                        <td> <a href="">23/3/2020 </a></td>
                                                        <td> <a href="">Mact 2 </a></td>
                                                        <td> <a href="">MACP/205/2022 </a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Petitioner" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Hirendra</a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">Arguement
                                                            </a></td>
                                                        <td> <a href="">50 </a></td>
                                                        <td> <a href="">219 </a></td>
                                                        <td> <a href="">R.K </a></td>
                                                        <td> <a href="">R-2 </a></td>
                                                        <td> <a href="">Royal Sundaram </a></td>
                                                        <td> <a href="">Blank </a></td>
                                                        <td> <a href="">Blank </a></td>
                                                        <td> <a href="">To File Ws </a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">23/3/2020
                                                            </a></td>
                                                        <td><a href="#" data-target="#reference" data-toggle="modal"data-modal-size="modal-md">
                                                            <span class="fa fa-refresh"></span>
                                                            </a>
                                                        </td>
                                                        <td> <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        Select
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <a href="#"
                                                           class="dropdown-item"><i
                                                                class="icon-file-eye"></i> 
                                                            View</a>
                                                        
                                                                <a href="#"
                                                                   class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i>Edit
                                                                </a>
                                                            
                                                       
                                                            <span id="delete_item" data-id="#" data-url="#" class="dropdown-item"><i class="icon-trash"></i> Delete
                                                            </span>
                                                       

                                                    </div>
                                                </div>
                                            </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                 </div>
                    <!-- form end =-->
                </div>
            </div>

            

        </div>

        <!--  -->
        <br>
        <br>
        <br>
        <!-- Table start -->
        
        <!--  -->
    </section>
@stop
      


