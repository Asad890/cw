@extends('layouts.master', ['title' => __('Create New Case')])

@section('mainContent')

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
        <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                        <a href="#" data-toggle="modal" class="primary-btn small fix-gr-bg"
                               data-target="#add_to_do"
                               title="Add To Do" data-modal-size="modal-md">
                                <span class="ti-plus pr-2"></span>
                                @lang('Link Matters')
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 text-right">
                            
                        </div>
                    </div>
            <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                          <!-- Table start -->
                    <div class="col-lg-12">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <div class="">
                                                <table class="table Crm_table_active3">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Sr No</th>
                                                            <th scope="col">Connected Matters</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>

                                                            <td>1</td>
                                                            <td>Case A </td>
                                                                
                                                        </tr>
                                                       
                                                        

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- close -->
                    </div>
                </div>
                     
                      
                </div>
                    <!-- form end =-->
               

        </div>

   
        </section>

        <div class="modal fade admin-query" id="add_to_do">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{trans('Add Connected Matters')}}</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()']) }}

                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for="">{{__('Select Primary Case')}}</label>
                                                    <select class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row container1">
                                            <div class="col-lg-10" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for="">{{__('Select Connected Matters')}}</label>
                                                    <select name="mytext[]" class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-2" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                <label class="primary_input_label"
                                                           for="">{{__('.')}}</label>
                                                <a href="#" class="primary-btn small fix-gr-bg add_form_field">
                                                    <span class="ti-plus pr-2"></span>
                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="container1">
    <button class="add_form_field">Add New Field &nbsp; 
      <span style="font-size:16px; font-weight:bold;">+ </span>
    </button>
    <div><input type="text" name="mytext[]"></div>
</div> -->

                                        <div class="col-lg-12 text-center">
                                            <div class="mt-40 d-flex justify-content-between">
                                                <input class="primary-btn fix-gr-bg" type="submit" value="{{ __('common.Save') }}">
                                                <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal">{{__('common.Cancel')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
        </div>
@stop
      


