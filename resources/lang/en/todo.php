<?php



return array (

  'To Do Created Successfully' => 'To Do Created Successfully',

  'To Do Has Been Marked as Complete' => 'To Do Has Been Marked as Complete',

  'no_do_lists_assigned_yet' => 'No do lists assigned yet',

  'Add To Do' => 'Add To Do',

  'Worklist' => 'Worklist',
  'worklist' => 'Worklist',

  'Incomplete' => 'Incomplete',

  'Inprogress' => 'Inprogress',
  'inprogress' => 'Inprogress',

  'Completed' => 'Completed',

  'to_do_list' => 'To Do List',

  'incomplete' => 'Incomplete',

  'completed' => 'Completed',

);

