<?php $__env->startSection('mainContent'); ?>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="box_header">
                    <div class="main-title d-flex justify-content-between w-100">
                        <h3 class="mb-0 mr-30"><?php echo e(__('court.New Court Category')); ?></h3>

                    </div>
                </div>
            </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                    <?php echo Form::open(['route' => 'category.court.store', 'class' => 'form-validate-jquery', 'id' =>
                    'content_form', 'files' => false, 'method' => 'POST']); ?>

                    <div class="primary_input">
                        <?php echo e(Form::label('name', __('court.Name'), ['class' => 'required'])); ?>

                        <?php echo e(Form::text('name', null, ['required' => '', 'class' => 'primary_input_field', 'placeholder' => __('court.Court Category Name')])); ?>

                    </div>

                    <div class="primary_input">
                        <?php echo e(Form::label('description', __('court.Description'))); ?>

                        <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field', 'placeholder' =>  __('court.Client Category Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' => '#description_error' ])); ?>

                        <span id="description_error"></span>
                    </div>

                    <div class="text-center mt-3">
                    <button class="primary_btn_large submit" type="submit"><i class="ti-check"></i><?php echo e(__('common.Create')); ?>

                    </button>

                    <button class="primary_btn_large submitting" type="submit" disabled style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                    </button>
                    </div>
                    <?php echo Form::close(); ?>



                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function () {
            _formValidation();
        });
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', ['title' => __('court.Create New Court Category')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/category/court/create.blade.php ENDPATH**/ ?>