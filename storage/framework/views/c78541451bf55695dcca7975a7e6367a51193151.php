<?php $__env->startSection('mainContent'); ?>
    <?php echo $__env->make("backEnd.partials.alertMessage", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('leave.Approve Leave Requests')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="apply_leave_list">
                                <table class="table Crm_table_active3">
                                    <thead>
                                    <tr>
                                        <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.Type')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.Staff')); ?></th>
                                        <th scope="col"><?php echo e(__('common.Email')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.From')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.To')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.Apply Date')); ?></th>
                                        <th scope="col"><?php echo e(__('common.Status')); ?></th>
                                        <th scope="col"><?php echo e(__('leave.Approved By')); ?></th>
                                        <th scope="col"><?php echo e(__('common.Action')); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $approved_leaves; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $apply_leave): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($key + 1); ?></td>
                                            <td><?php echo e($apply_leave->leave_type->name); ?></td>
                                            <td><?php echo e($apply_leave->user->name); ?></td>
                                            <td><?php echo e($apply_leave->user->email); ?></td>
                                             <td><?php echo e(formatDate($apply_leave->start_date)); ?></td>
                                           <td><?php echo e($apply_leave->end_date != '0000-00-00' ? formatDate($apply_leave->end_date) : ''); ?></td>
                                            <td><?php echo e(formatDate($apply_leave->apply_date)); ?></td>
                                            <td>
                                                <?php if($apply_leave->status == 0): ?>
                                                    <span class="badge_3"><?php echo e(__('leave.Pending')); ?></span>
                                                <?php elseif($apply_leave->status == 1): ?>
                                                    <span class="badge_1"><?php echo e(__('leave.Approved')); ?></span>
                                                <?php else: ?>
                                                    <span class="badge_4"><?php echo e(__('leave.Cancelled')); ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td><?php echo e($apply_leave->approved_by ? $apply_leave->approveUser->name : ''); ?></td>
                                            <td>
                                                <input type="hidden" name="user_id" id="user_id"
                                                       value="<?php echo e($apply_leave->user_id); ?>">
                                                <!-- shortby  -->
                                                <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <?php if(permissionCheck('languages.edit_modal')): ?>
                                                            <a href="#" class="dropdown-item"
                                                               onclick="edit_apply_leave_modal(<?php echo e($apply_leave->id); ?>)"><?php echo e(__('common.View')); ?></a>
                                                        <?php endif; ?>
                                                        <?php if(1): ?>
                                                            <a onclick="confirm_modal('<?php echo e(route('apply_leave.destroy', $apply_leave->id)); ?>');"
                                                               class="dropdown-item"><?php echo e(__('common.Delete')); ?></a>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <!-- shortby  -->
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="edit_form">

    </div>
    <?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
        function edit_apply_leave_modal(el) {
            var user_id = $('#user_id').val();
            $.post('<?php echo e(route('apply_leave.view')); ?>', {
                _token: '<?php echo e(csrf_token()); ?>',
                id: el,
                user_id: user_id
            }, function (data) {
                $('.edit_form').html(data);
                $('#Apply_Leave_Edit').modal('show');
                $('select').niceSelect();
            });
        }
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/apply_approvals/approval_list.blade.php ENDPATH**/ ?>