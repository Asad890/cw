<!-- <div class="col-12">
           <div class="row attach-respondent-row">
                <div class="primary_input col-md-3">
                      <?php echo e(Form::label('RESPONDENT', __('RESPONDENT'))); ?>

                </div>
                <div class="primary_input col-md-8">
                      <?php echo e(Form::text('respondent', null, ['class' => 'primary_input_field', 'placeholder' => __('case.RESPONDENT')])); ?>

                </div>
                <div class="col-md-1">
                      <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button" onclick="respondent_add();"> <i class="ti-plus"></i> </span>
                </div>
                <br>
                <br>
                <br>

                <div class="primary_input col-md-3">
                      <?php echo e(Form::label('RESPONDENT ADVOCATE', __('RESPONDENT ADVOCATE'))); ?>

                </div>
                <div class="primary_input col-md-9">
                      <?php echo e(Form::text('respondent_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('RESPONDENT ADVOCATE')])); ?>

                </div>
                
          </div>

</div>

<script>

var index = 0;
function respondent_add() {
    index = $('.attach-item').length
    addNewRespondent(index)
}


function addNewRespondent(index) {
    "use strict";

    var attachFile = '<div class="attach-file-section d-flex align-items-center">\n' +
        '        <div class="primary_input flex-grow-1">\n' +
        '            <div class="primary_file_uploader">\n' +
        '                <input class="primary-input" type="text" id="placeholderStaffsName" placeholder="' + trans('js.Browse File') + '" readonly>\n' +
        '                <button class="" type="button">\n' +
        '                    <label class="primary-btn small fix-gr-bg"\n' +
        '                           for="attach_file_' + index + '">' + trans('js.Browse') + '</label>\n' +
        '                    <input type="file" class="d-none file-upload-multi" name="file[]" id="attach_file_' + index + '">\n' +
        '                </button>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only case-attach" type="button" > <i class="ti-trash"></i> </span>\n' +
        '    </div>';

    $('.attach-respondent-row').append(attachFile);
}

</script> -->


<!-- <div class="attach-petitioner-row">
        <div class="primary_input col-md-3">
        <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

        </div>
        <div class="primary_input col-md-9">
        <?php echo e(Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER')])); ?>

        </div>
        <br>
        <br>
        <br>
        <div class="primary_input col-md-3">
        <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

        </div>
        <div class="primary_input col-md-8">
        <?php echo e(Form::text('petitioner_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE')])); ?>

        </div>
        <div class="primary_input col-md-1">
                <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button" id="petitioner_add"> <i class="ti-plus"></i> </span>
        </div>
</div> -->
<div class="col-12">
    <div class="row attach-respondent-row">
        <div class="col-12">
            <div class="row">
                <div class="primary_input col-md-3">
                <?php echo e(Form::label('RESPONDENT', __('RESPONDENT'))); ?>

                </div>
                <div class="primary_input col-md-8">
                <?php echo e(Form::text('respondent[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.RESPONDENT')])); ?>

                </div>
                <div class="primary_input col-md-1">
                <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button" onclick="respondent_add();"> <i class="ti-plus"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                <?php echo e(Form::label('RESPONDENT ADVOCATE', __('RESPONDENT ADVOCATE'))); ?>

                </div>
                <div class="primary_input col-md-9">
                <?php echo e(Form::text('respondent_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('RESPONDENT ADVOCATE')])); ?>

                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>

<script>
var index = 0;
function respondent_add() {
    index = $('.attach-item').length
    addNewRespondent(index)
}



function addNewRespondent(index) {
    "use strict";

    // var attachFile = '<div class="attach-file-section d-flex align-items-center">\n' +
    //     '        <div class="primary_input flex-grow-1">\n' +
    //     '            <div class="primary_file_uploader">\n' +
    //     '                <input class="primary-input" type="text" id="placeholderStaffsName" placeholder="' + trans('js.Browse File') + '" readonly>\n' +
    //     '                <button class="" type="button">\n' +
    //     '                    <label class="primary-btn small fix-gr-bg"\n' +
    //     '                           for="attach_file_' + index + '">' + trans('js.Browse') + '</label>\n' +
    //     '                    <input type="file" class="d-none file-upload-multi" name="file[]" id="attach_file_' + index + '">\n' +
    //     '                </button>\n' +
    //     '            </div>\n' +
    //     '        </div>\n' +
    //     '        <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only case-attach" type="button" > <i class="ti-trash"></i> </span>\n' +
    //     '    </div>';

    var add_respondent = `
        <div class="col-12" id="respondent-`+index+`">
        <div class="row">
                <div class="primary_input col-md-3">
                <?php echo e(Form::label('RESPONDENT', __('RESPONDENT'))); ?>

                </div>
                <div class="primary_input col-md-8">
                <?php echo e(Form::text('respondent[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.RESPONDENT')])); ?>

                </div>
                <div class="primary_input col-md-1">
                <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" onclick="remove_respondent('respondent-`+index+`')" type="button" > <i class="ti-trash"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                <?php echo e(Form::label('RESPONDENT ADVOCATE', __('RESPONDENT ADVOCATE'))); ?>

                </div>
                <div class="primary_input col-md-9">
                <?php echo e(Form::text('respondent_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('RESPONDENT ADVOCATE')])); ?>

                </div>
                <br>
                <br>
                <br>
            </div>
        </div>`

    $('.attach-respondent-row').append(add_respondent);
}


function remove_respondent(id){
    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
}
</script>


<?php /**PATH D:\updates 123\cw\resources\views/case/respondent.blade.php ENDPATH**/ ?>