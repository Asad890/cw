<!DOCTYPE html>

<?php if(rtl()): ?>
    <html dir="rtl"  class="rtl">
<?php else: ?>
    <html>
<?php endif; ?>
<head>

<!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Popover -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"> -->
    <!-- Close Popover -->
    <link rel="icon" href="<?php echo e(asset(config('configs')->where('key','favicon_logo')->first()->value)); ?>" type="image/png" />

    <title><?php echo e(isset($title) ? $title .' | '. config('configs')->where('key','site_title')->first()->value :  config('configs')->where('key','site_title')->first()->value); ?></title>

    <meta name="_token" content="<?php echo csrf_token(); ?>"/>


    <!-- Bootstrap CSS -->
    


    <?php if(rtl()): ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/bootstrap.min.css')); ?>"/>
    <?php else: ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/bootstrap.css"/>
    <?php endif; ?>

  
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/jquery-ui.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/vendors/text_editor/summernote-bs4.css" />
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/jquery.data-tables.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/responsive.dataTables.min.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/vendors/font_awesome/css/all.min.css" />

    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/themify-icons.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/flaticon.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/nice-select.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/magnific-popup.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/fastselect.min.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/toastr.min.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/select2/select2.css"/>

    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/vendors/css/fullcalendar.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/vendors/css/daterangepicker.css')); ?>"/>


    <!-- color picker  -->

    <!-- metis menu  -->
    <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/css/metisMenu.css">

    <?php echo $__env->yieldContent('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/loade.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/css/parsley.css')); ?>"/>



        <?php if(rtl()): ?>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/style.css')); ?>"/>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/infix.css')); ?>"/>
        <?php else: ?>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/style.css')); ?>"/>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/infix.css')); ?>"/>
        <?php endif; ?>

        <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/css/style.css" />
        <!--  -->
        <?php echo $__env->yieldPushContent('css_before'); ?>

<style>
 
 .upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.btn {
  border: 2px solid gray;
  color: white;
  background-color: #9634F2;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
}
.btn1 {
  border: 1px solid gray;
  color: white;
  background-color: #9634F2;
  padding: 4px 10px;
  border-radius: 4px;
  font-size: 10px;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}

.btn-custom {
  padding:0.6em 2em;
  /* padding: 15px; */
  border-radius:14px;
  color:#fff;
  background-color:#A40E08;
  text-transform: uppercase;
  font-family: "Poppins", sans-serif;
  font-size:0.6em;
  border:0;
  cursor:pointer;
  margin:1em;
}

.btn-custom:hover {
  padding:0.8em 1.8em;
  border-radius:14px;
  color:#fff;
  background-color:#A40E08;
  text-transform: uppercase;
  font-family: "Poppins", sans-serif;
  font-size:0.6em;
  border:0;
  cursor:pointer;
  margin:1em;
}

.nav-link
{
    color: #000000;
}
.nav-link:hover
{
    color: #000000;
}

.center {
  text-align: center;
}


/* For Modal Scroll */
#add_to_do { overflow-y:scroll }

/* For Modal */

body {font-family: Arial, Helvetica, sans-serif;}


</style>

         <script>
            const SET_DOMAIN="<?php echo e(url('/')); ?>"

            const RTL = <?php echo e(rtl() ? "true" : "false"); ?>;
            const LANG = "<?php echo e(session()->get('locale', Config::get('app.locale'))); ?>";

        </script>
</head>

<body class="admin">

<div class="preloader">
    <h3 data-text="<?php echo e(config('configs')->where('key','preloader')->first()->value); ?>.."><?php echo e(config('configs')->where('key','preloader')->first()->value); ?>..</h3>
</div>

<div class="main-wrapper" style="min-height: 600px">

    <?php
        if (file_exists(config('configs')->where('key','site_logo')->first()->value)) {
            $tt = file_get_contents(url('/').'/'.config('configs')->where('key','site_logo')->first()->value);
        } else {
            $tt = file_get_contents(asset('/public/uploads/settings/logo.png'));
        }

    ?>
    <input type="text" hidden value="<?php echo e(base64_encode($tt)); ?>" id="logo_img">
    <!-- Sidebar  -->
<?php echo $__env->make('partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Page Content  -->
    <div id="main-content">
<?php echo $__env->make('partials.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/partials/header.blade.php ENDPATH**/ ?>