<?php
    $task = ['task.index', 'task.edit', 'task.show', 'task.create'];
    
    $nav = array_merge($task, ['my-task', 'completed-task']);
?>

<li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">

    <a href="javascript:;" class="has-arrow"  aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
        <div class="nav_icon_small">
            <span class="fas fa-th"></span>
        </div>
        <div class="nav_title">
            <span><?php echo e(__('task.Task')); ?></span>
        </div>
    </a>
    <ul>
        <?php if(permissionCheck('task.index')): ?>
        <li>
            <a href="<?php echo e(route('task.index')); ?>" class="<?php echo e(spn_active_link($task, 'active')); ?>">  <?php echo e(__('task.Task List')); ?></a>
        </li>
        <?php endif; ?>
        <li>
            <a href="<?php echo e(route('my-task')); ?>" class="<?php echo e(spn_active_link('my-task', 'active')); ?>"><?php echo e(__('task.My Task')); ?></a>
        </li>

        <li>
            <a href="<?php echo e(route('completed-task')); ?>" class="<?php echo e(spn_active_link('completed-task', 'active')); ?>"><?php echo e(__('task.Completed Task')); ?></a>
        </li>
    </ul>
</li><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Task/Resources/views/menu.blade.php ENDPATH**/ ?>