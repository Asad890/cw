<?php $__env->startSection('mainContent'); ?>



    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <div class="main-title">
                        <h3 class="mb-30"><?php echo e($model->title); ?></h3>
                    </div>
                </div>

                <div class="col-lg-12 d-print-none">
                    <a class="primary-btn small fix-gr-bg " href="<?php echo e(route('case.show', $model->id)); ?>"><i class="ti-file mr-2"></i>
                        <?php echo e(__('case.Case')); ?>

                    </a>

                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">


                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row mb-4">
                                <div class="col-lg-4 no-gutters">
                                    <div class="main-title">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="student-meta-box mb-20">
                                        <div class="white-box student-details pt-2 pb-3">

                                            <?php echo Form::open(['url' => route('file.store', ['case' => $model->id]), 'method' => 'post', 'id' => 'content_form', 'files' =>true, 'data-parsley-focus' => 'none' ]); ?>

                                            <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                                            <div class="col-xl-12 text-center mt-3">
                                                <button class="primary_btn_large submit" type="submit"><i
                                                        class="ti-check"></i><?php echo e(__('common.Create')); ?>

                                                </button>
                                                <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                                    <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                                </button>
                                            </div>

                                            <?php echo Form::close(); ?>



                                        </div>
                                    </div>


                                </div>
                                <div class="col-lg-12">


                                    <div class="student-meta-box mb-20">
                                        <div class="white-box student-details pt-2 pb-3">

                                            <?php if ($__env->exists('case.file_show', ['files' => $model->allFiles, 'type' => 'case'])) echo $__env->make('case.file_show', ['files' => $model->allFiles, 'type' => 'case'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade animated file_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog" aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        _formValidation();

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Case Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/file/index.blade.php ENDPATH**/ ?>