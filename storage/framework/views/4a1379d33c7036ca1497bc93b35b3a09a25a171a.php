<?php $__env->startSection('mainContent'); ?>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('case.Add Put Up Date')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">
                        <?php echo Form::open(['route' => 'putlist.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-12">
                                <?php echo e(Form::hidden('case', $case)); ?>

                                <?php echo e(Form::label('hearing_date', __('case.Put Up Date'),['class' => 'required'])); ?>

                                <?php echo e(Form::text('hearing_date', date('Y-m-d H:i'), ['required'=>'','class' => 'primary_input_field primary-input datetime form-control', 'placeholder' => __('case.Put Up Date')])); ?>

                            </div>
                        </div>

                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('case.Put Up Order'),['class' => 'required'])); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('case.Put Up Order'),'required', 'rows' => 5, 'data-parsley-errors-container' =>
                            '#description_error'])); ?>

                            <span id="description_error"></span>
                        </div>
                        <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        <div class="text-center mt-3">
                            <button class="primary-btn semi_large2 fix-gr-bg submit" type="submit"><i
                                    class="ti-check"></i><?php echo e(__('common.Create')); ?>

                            </button>

                            <button class="primary-btn semi_large2 fix-gr-bg submitting" type="submit" disabled
                                    style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                            </button>

                        </div>

                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {
            _formValidation();

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.New Put Up Date')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/putlist/create.blade.php ENDPATH**/ ?>