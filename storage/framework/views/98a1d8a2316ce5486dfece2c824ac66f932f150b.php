

<?php echo Form::open(['url' => route('updateLoginBG'), 'method' => 'post', 'id' => 'updateLoginBG', 'files' =>true ]); ?>

    <?php echo csrf_field(); ?>
    <input type="hidden" name="g_set" value="1">

    <div class="General_system_wrap_area d-block">

        <div class="single_system_wrap">
            <div class="single_system_wrap_inner text-center">
                <div class="logo ">
                    <span><?php echo e(__('setting.Login Backgroud Image')); ?></span>
                </div>
                <div class="logo_img ml-auto mr-auto">
                    <img class="img-fluid" src="<?php echo e(asset(getConfigValueByKey($config,'login_backgroud_image'))); ?>" alt="Login background image" id="login_bg_image">
                </div>
                <div class="update_logo_btn">
                    <button class="primary-btn small fix-gr-bg " type="button">
                        <input placeholder="Upload Image" type="file" name="login_backgroud_image"  onchange="imageChangeWithFile(this, '#login_bg_image' )" id="login_backgroud_image">
                        <?php echo e(__('setting.Upload Image')); ?>

                    </button>
                </div>

            </div>
        </div>

    </div>

    <div class="submit_btn text-center mt-4">
        <button class="primary_btn_large submit" type="submit"><i class="ti-check"></i><?php echo e(__('common.Save')); ?>

        </button>

        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Saving')); ?>

        </button>

    </div>
<?php echo Form::close(); ?>

<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Setting/Resources/views/page_components/login_bg.blade.php ENDPATH**/ ?>