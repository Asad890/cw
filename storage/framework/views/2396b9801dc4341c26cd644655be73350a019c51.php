<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('client.Client List')); ?></h3>
                            <ul class="d-flex">
                                <?php if(permissionCheck('client.store')): ?>
                                    <li><a class="primary-btn radius_30px mr-10 fix-gr-bg"
                                           href="<?php echo e(route('client.create')); ?>"><i class="ti-plus"></i><?php echo e(__
                        ('client.New Client')); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                    <tr>
                                        <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                        <th><?php echo e(__('client.Client')); ?></th>
                                        <th><?php echo e(__('client.Contact')); ?></th>
                                        <th><?php echo e(__('client.Category')); ?></th>
                                        <th><?php echo e(__('client.Address')); ?></th>
                                        <th><?php echo e(__('common.Actions')); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td>
                                                <a href="<?php echo e(route('client.show', $model->id)); ?>"><?php echo e($model->name); ?></a>
                                            </td>
                                            <td>
                                                <?php echo e(__('client.Mobile')); ?>: <?php echo e($model->mobile); ?> <br>
                                                <?php echo e(__('client.Email')); ?>: <?php echo e($model->email); ?>

                                            </td>
                                            <td><?php echo e(@$model->category->name); ?></td>
                                            <td>
                                                <?php echo $model->address ? $model->address  .', <br>' : ''; ?>

                                                <?php echo e($model->state ? $model->state->name .', ' : ''); ?>

                                                <?php echo e($model->city ? $model->city->name .', ' : ''); ?>

                                                <?php echo e($model->country ? $model->country->name : ''); ?>

                                            </td>

                                            <td>


                                                <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <?php if(permissionCheck('client.show')): ?>
                                                            <a href="<?php echo e(route('client.show', $model->id)); ?>"
                                                               class="dropdown-item edit_brand"><?php echo e(__('common.Show')); ?></a>
                                                        <?php endif; ?>
                                                        <?php if(permissionCheck('client.edit')): ?>
                                                            <a href="<?php echo e(route('client.edit', $model->id)); ?>"
                                                               class="dropdown-item edit_brand"><?php echo e(__('common.Edit')); ?></a>
                                                        <?php endif; ?>
                                                        <?php if(permissionCheck('client.destroy')): ?>
                                                            <span id="delete_item" data-id="<?php echo e($model->id); ?>" data-url="<?php echo e(route
                                                                    ('client.destroy', $model->id)); ?>"
                                                                  class="dropdown-item"><i class="icon-trash"></i>
                                                                        <?php echo e(__('common.Delete')); ?> </span>
                                                        <?php endif; ?>


                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <?php echo $__env->make('partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>


<?php $__env->startPush('admin.scripts'); ?>


    <script>
        $(document).ready(function () {
        });

    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', ['title' => __('client.Client')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/client/index.blade.php ENDPATH**/ ?>