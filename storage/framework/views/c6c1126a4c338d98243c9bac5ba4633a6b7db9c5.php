<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <div class="main-title">
                        <h3 class="mb-30"><?php echo e($model->title); ?></h3>
                    </div>
                </div>

                <div class="col-lg-6 d-print-none">

                    <?php if($model->judgement_status=='Open' OR $model->judgement_status=='Reopen'): ?>
                        <?php if(permissionCheck('date.store')): ?>
                            <a href="<?php echo e(route('date.create', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('putlist.store')): ?>
                            <a href="<?php echo e(route('putlist.create', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Put Up Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('lobbying.store')): ?>
                            <a href="<?php echo e(route('lobbying.create', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Lobbying Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.create', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Judgement Date')); ?></a>
                        <?php endif; ?>

                    <?php endif; ?>
                    <?php if($model->judgement_status=='Judgement'): ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.reopen', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.Re-open')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.close', ['case' => $model->id])); ?>"
                               class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.Close')); ?></a>
                        <?php endif; ?>

                    <?php endif; ?>

                    <a class="primary-btn small fix-gr-bg print_window"
                       href="<?php echo e(route('case.show', [$model->id, 'print' => true])); ?>" target="_blank"><i
                            class="ti-printer mr-2"></i>
                        <?php echo e(__('case.Print')); ?>

                    </a>
                    <a class="primary-btn small fix-gr-bg " href="<?php echo e(route('file.index', ['case' => $model->id])); ?>"><i
                            class="ti-file mr-2"></i>
                        <?php echo e(__('case.File')); ?>

                    </a>

                </div>
                <div class="col-lg-6 d-print-none text-right">
                <a class="primary-btn small fix-gr-bg " href="<?php echo e(route('case.edit', $model->id)); ?>"><i
                            class="ti-file mr-2"></i>
                        <?php echo e(__('Edit Case')); ?>

                    </a>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-8 mt-25">
                    <!-- Rozanam -->
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                        

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <!-- body -->
                            <div class="col-lg-12">
                                <!-- Start -->
                                <details class="deque-expander" id="dream">
                                    <summary class="deque-expander-summary">
                                    <span class="toggle-indicator"></span>
                                    <b> ROZNAMA/CASE HISTORY</b>
                                    </summary>
                                
                                    <div class="deque-expander-content">
                                    <div class="col-lg-12">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Previous Date</th>
                                                                    <th scope="col">Case History</th>
                                                                    <th scope="col">Next Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>

                                                                    <td>14-05-2022</td>
                                                                    <td>Draft Affidavit</td>
                                                                    <td>14-05-2022</td>
                                                                        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </details>
                                    <!-- close -->
                                    <!-- Start -->
                                <details class="deque-expander" id="dream">
                                    <summary class="deque-expander-summary">
                                    <span class="toggle-indicator"></span>
                                   <b> Worklist </b>
                                    </summary>
                                
                                    <div class="deque-expander-content">
                                    <div class="col-lg-12">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Description</th>
                                                                    <th scope="col">Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>

                                                                    <td>14-05-2022</td>
                                                                    <td>Draft Affidavit</td>
                                                                    <td>In Progress</td>
                                                                        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </details>
                                    <!-- close -->
                                    <!-- Start -->
                                <details class="deque-expander" id="dream">
                                    <summary class="deque-expander-summary">
                                    <span class="toggle-indicator"></span>
                                   <b> Notes </b>
                                    </summary>
                                
                                    <div class="deque-expander-content">
                                    <div class="col-lg-12">
                                        
                                        <div class="row">
                                            <div class="primary_input col-md-12">
                                                <?php echo e(Form::label('CASE STAGE', __('case.CASE STAGE'))); ?>

                                                <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                            </div>
                                           
                                        </div>
                                    </div>
      
                                </details>
                                    <!-- close -->
                                    <!-- Start -->
                                <details class="deque-expander" id="dream">
                                    <summary class="deque-expander-summary">
                                    <span class="toggle-indicator"></span>
                                   <b> Connected Matters </b>
                                    </summary>
                                
                                    <div class="deque-expander-content">
                                    <div class="col-lg-12">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Sr No.</th>
                                                                    <th scope="col">Case No.</th>
                                                                    <th scope="col">Case Title</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>

                                                                    <td>1</td>
                                                                    <td>Draft Affidavit</td>
                                                                    <td>In Progress</td>
                                                                        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </details>
                                    <!-- close -->
                                    <!-- Start -->
                                <details class="deque-expander" id="dream">
                                    <summary class="deque-expander-summary">
                                    <span class="toggle-indicator"></span>
                                   <b> Interlocutory orders Evidences or Judgements </b>
                                    </summary>
                                
                                    <div class="deque-expander-content">
                                    <div class="col-lg-12">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Order No.</th>
                                                                    <th scope="col">Order Date</th>
                                                                    <th scope="col">Order Details</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>

                                                                    <td>14-05-2022</td>
                                                                    <td>Draft Affidavit</td>
                                                                    <td>In Progress</td>
                                                                        
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </details>
                                    <!-- close -->
                            </div>
                            </div>
                            <!-- close body -->
                        </div>
                    </div>
                        <!-- worklist -->
                        
                        
                       
                        <!-- card close -->
                        <div class="row">
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle1 -->
                                <h5>Decided</h5>
                                <div class="custom-control">
                                <label class="switch_toggle" for="active_checkbox">
                                    <input type="checkbox" id="active_checkbox" 
                                    value="check" onchange="update_active_status(this)" checked>
                                    <div class="slider round"></div>
                                </label>
                                </div>
                            </div>
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle2 -->
                                <h5>Abandoned</h5>
                                <div class="custom-control">
                                <label class="switch_toggle" for="active_checkbox">
                                    <input type="checkbox" id="active_checkbox" 
                                    value="check" onchange="update_active_status(this)" checked>
                                    <div class="slider round"></div>
                                </label>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <div class="col-lg-4 mt-25">
                    <div class="student-meta-box sticky-details">
                        <div class="white-box student-details pt-3">
                            <div class="single-meta">
                                <h3 class="mb-0"><?php echo e(__('case.Case Details')); ?> </h3>
                            </div>
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('CNR')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->case_category? $model->case_category->name : ''); ?>

                                        - <?php echo e($model->case_no); ?> -->
                                        <p>GHHFD4543454</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Brief No,')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php if($model->case_category): ?>
                                            <a href="<?php echo e(route('category.case.show', $model->case_category_id)); ?>">
                                                <?php echo e($model->case_category? $model->case_category->name : ''); ?>

                                            </a>
                                        <?php endif; ?> -->
                                        <p>GHHFD4543454</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case Type')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->file_no); ?> -->
                                        <p>MACP</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case No.')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->ref_name); ?> -->
                                        <p>244444445434</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Year')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->ref_mobile); ?> -->
                                        <p>2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Previous Date')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(formatDate($model->hearing_date)); ?> -->
                                        <p>10-20-3030</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case Stage')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(formatDate($model->filling_date)); ?> -->
                                        <p>Public Notice</p>
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Brief For')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo $model->case_stage ? '<a
                                            href="'.route('master.stage.show', $model->stage_id).'">'. $model->case_stage->name
                                            .'</a>' : ''; ?> -->
                                            <p>Residense No.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                    <h3 class="mb-30"><?php echo e(__('Parties & Hearing Aadvocates')); ?> </h3>
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('PETITIONER/ADITIONAL AND THEIR ADVOCATES')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Rajesh Naresh Mothgare Advicates</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('RESPONDENT/ADITIONAL PETITIONERS AND THEIR ADVOCATES')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>HDFC ERGO gen Ins, Co Ltd</p>
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta mt-10">
                                <h3 class="mb-30"><?php echo e(__('Court Details')); ?> </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Court
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Mact Court No.2</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Judge Name')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>R.K</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Court Room No.')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>210</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta mt-10">
                                <h3 class="mb-30"><?php echo e(__('Client Details')); ?> </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Client Name')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Shirvallabh Madok</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Contact
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>9890344242 </p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Email')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Shirvallabh@gmail.com</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Address')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>ABC Adres</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Organization
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>HDFC ERGO</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Tags/Refrence
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Mr. Dushmukh</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Police Station
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>ABHHH</p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Remarks
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Registration date time casewise</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Registration Date & Time
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Registration date time casewise</p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Casewise:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p>Register at casewise  date time casewise on 11/5/2023 at 9:39 hrs,</p>
                                    </div>
                                </div>
                            </div>


                          

                </div>
            </div>
        </div>
    </section>
    <div class="modal fade animated file_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <?php if(moduleStatusCheck('Finance')): ?>
        <div class="modal fade animated payment_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
             aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>


    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Case Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/show.blade.php ENDPATH**/ ?>