

<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                <div class="row form-group">
                                    <div class="primary_input col-md-6">
                                            <h4>SELECT COURT</h4>
                                    </div>
                                    <!-- <div class="primary_input col-md-3">
                                    <button class="primary_btn_large submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <div class="primary_input col-md-4">
                                            <select class="primary_input_field">
                                                <option>Drop Down</option>
                                                <option>Drop Down</option>
                                                <option>Drop Down</option>
                                                <option>Drop Down</option>
                                            </select>
                                    </div>
                                    <!-- <div class="primary_input col-md-3">
                                    <button class="primary_btn_large submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <div class="primary_input col-md-4">
                                        <b><label>Amount</label></b>
                                            <input type="text" class="primary_input_field"> 
                                    </div>
                                    <!-- <div class="primary_input col-md-3">
                                    <button class="primary_btn_large submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <div class="primary_input col-md-4">
                                        <b><label>Court Fee</label></b>
                                            <input type="text" class="primary_input_field"> 
                                    </div>
                                    <!-- <div class="primary_input col-md-3">
                                    <button class="primary_btn_large submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <!--  -->
                    </div>
                        <!-- Table start -->
                </div>
                    <!-- form end =-->
               

        </div>

   
        </section>
<?php $__env->stopSection(); ?>
      



<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/courtfee/index.blade.php ENDPATH**/ ?>