<?php $__env->startSection('mainContent'); ?>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-district-court"
                    role="tab" aria-controls="pills-contact" aria-selected="true">
                    <h3 class="mb-0 "><?php echo e(__('District Courts and Tribunals')); ?></h3>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-high-court" role="tab"
                    aria-controls="pills-contact" aria-selected="false">
                    <h3 class="mb-0 "><?php echo e(__('High Court')); ?></h3>
                </a>
            </li>

        </ul>

        <div class="row justify-content-center">
            <div class="col-12">
                <div class='row'>
                    <div class="col-md-4">
                        <!-- <input type='date' class="primary_input_field" value='2006-01-01'> -->
                    </div>
                    <div class="col-md-4">
                        <!-- <input type='date' class="primary_input_field" value='2006-01-01'> -->
                    </div>
                </div>
                <h4 style="text-align:center"><?php echo e(ucwords(auth()->user()->name)); ?></h4>
                <p style="text-align:center"> <span id="date-time"> <?php echo e(date(DATE_RFC822)); ?></span></p>
                <div class="box_header common_table_header xs_mb_0">
                    <div class="main-title d-md-flex">
                        <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('Today Cases')); ?>

                            <a class="btn primary-btn radius_30px mr-10 fix-gr-bg" href="<?php echo e(route('case.create')); ?>"><i
                                    class="ti-plus"></i><?php echo e('New Case'); ?></a>
                            <!-- <div class="col-0 btn-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <button type="button" class="primary-btn radius_30px mr-10 fix-gr-bg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Template</button>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div class="dropdown-menu">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <a class="dropdown-item" href="#">Action</a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <a class="dropdown-item" href="#">Another action</a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <a class="dropdown-item" href="#">Something else here</a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="dropdown-divider"></div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <a class="btn1 btn-primary" href="#"8k
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        data-toggle="modal" data-target="#add_to_do" data-modal-size="mod8kal-md">SAVE
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div> -->
                        </h3>
                        <ul class="d-flex">
                            <li>

                            </li>
                        </ul>
                    </div>
                </div>


                <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                    <!-- tab 1 =-->
                    <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel"
                        aria-labelledby="pills-contact-tab">
                        <style>
                            .color th,
                            .color td,
                            .color a {
                                color: black !important;
                            }
                        </style>
                        <div class="col-lg-12">
                            <div class="QA_section QA_section_heading_custom check_box_table">
                                <div class="QA_table ">
                                    <!-- table-responsive -->
                                    <div class="color">
                                        <style>
                                            .dropdown-toggle::after {
                                                display: none !important;
                                            }
                                        </style>
                                        <table class="table Crm_table_active3">
                                            <thead>
                                                <tr>

                                                    <th scope="col"><?php echo e(__('SN.')); ?></th>
                                                    <th scope="col"><?php echo e(__('Prev Date')); ?></th>
                                                    <th scope="col"><?php echo e(__('Case No.')); ?></th>
                                                    <th scope="col"><?php echo e(__('Parties Details')); ?></th>
                                                    <th scope="col"><?php echo e(__('Court Details')); ?></th>
                                                    <th scope="col"><?php echo e(__('Stage')); ?></th>
                                                    <th scope="col" style="width:180px;"><?php echo e(__('Next date')); ?></th>
                                                    <th scope="col"><?php echo e(__('Remarks')); ?></th>
                                                    <!-- <th scope="col"><?php echo e(__('Remarks')); ?></th>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <th scope="col"><?php echo e(__('Action')); ?></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $__currentLoopData = $today_cases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $active): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e($key + 1); ?></a>
                                                        </td>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(isset($active->previous_date) ? date('d-m-Y', strtotime($active->previous_date)) : '---'); ?></a>
                                                        </td>
                                                        <td style="width:150px!important;">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <a
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(mb_strimwidth($active->case_category, 0, 13, '...') . '/ ' . $active->case_no . '/ ' . date('Y', strtotime($active->case_year))); ?></a>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td style="width:1100px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Parties: </strong>
                                                                    <?php

                                                                        $petitioners = DB::table('petitioners')
                                                                            ->where('case_id', $active->id)
                                                                            ->first();
                                                                        $petitioner = '---';
                                                                        if ($petitioners != null) {
                                                                            $petitioners_name = DB::table('petitioners')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('petitioner');
                                                                            $petitioner_advocates = DB::table('petitioner_advocates')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('petitioner_advocate');
                                                                            $petitioner = '';
                                                                            foreach ($petitioners_name as $key => $name) {
                                                                                $petitioner = $petitioner . ($key + 1) . ') Petitioner ' . $name . ' Advocate ' . $petitioner_advocates[$key] . "\r\n";
                                                                            }
                                                                        }

                                                                        $respondents = DB::table('respondents')
                                                                            ->where('case_id', $active->id)
                                                                            ->first();
                                                                        $respondent = '---';
                                                                        if ($respondents != null) {
                                                                            $respondents_name = DB::table('respondents')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('respondent');
                                                                            $respondent_advocates = DB::table('respondent_advocates')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('respondent_advocate');
                                                                            $respondent = '';
                                                                            foreach ($respondents_name as $key => $name) {
                                                                                $respondent = $respondent . ($key + 1) . ') Respondent ' . $name . ' Advocate ' . $respondent_advocates[$key] . "\r\n";
                                                                            }
                                                                        }

                                                                    ?>

                                                                    <a href="" class=" popover-demo mb-2"
                                                                        data-toggle="popover" title="Petitioners"
                                                                        data-content="<?php echo e($petitioner); ?>"><?php echo e($petitioners ? $petitioners->petitioner : '---'); ?></a><strong>
                                                                        VS </strong><a href=""
                                                                        class=" popover-demo mb-2"
                                                                        data-toggle="popover" title="Respondents"
                                                                        data-content="<?php echo e($respondent); ?>"><?php echo e($respondents ? $respondents->respondent : '---'); ?></a>
                                                                    <!-- <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td> -->
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Brief: </strong>
                                                                    <?php echo e($active->brief_for); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Organization: </strong>
                                                                    <?php
                                                                        $org = DB::table('organizations')
                                                                            ->where('id', $active->organization_id)
                                                                            ->first();
                                                                    ?>
                                                                    <?php echo e($org->organization_name); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Brief#: </strong>
                                                                    <?php echo e(($active->Brief_no != '' and $active->Brief_no != null) ? $active->Brief_no : '-'); ?>

                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td style="width:1100px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Court: </strong>
                                                                    <?php echo e($active->court_bench); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Room#: </strong>
                                                                    <?php echo e($active->court_room_no); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Judge: </strong>
                                                                    <?php echo e($active->judge_name); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Sr no in court: </strong>
                                                                    <?php echo e($active->sr_no_in_court); ?>

                                                                </div>
                                                            </div>


                                                        </td>
                                                        <?php

                                                            $stage = DB::table('cases')
                                                                ->where('id', $active->id)
                                                                ->value('case_stage');

                                                        ?>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e($stage); ?></a>
                                                        </td>

                                                        <td style="width:180px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <a
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(isset($active->next_date) ? date('d-m-Y', strtotime($active->next_date)) : '---'); ?></a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <a
                                                                        onclick="active_case_refresh(<?php echo e($active->id); ?> , '<?php echo e($active->case_stage); ?>', '<?php echo e($active->next_date); ?>');"><i
                                                                            class="fa fa-refresh" aria-hidden="true"
                                                                            style="color:blue; cursor:pointer;"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="dropdown ml-auto">
                                                                        <a class="dropdown-toggle" type="button"
                                                                            id="dropdownMenuButton"
                                                                            data-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="fa fa-ellipsis-v"
                                                                                aria-hidden="true"></i>
                                                                        </a>

                                                                        <div class="dropdown-menu dropdown-menu-right"
                                                                            aria-labelledby="dropdownMenuButton">

                                                                            <a class="dropdown-item"
                                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><i
                                                                                    class="fa fa-eye"
                                                                                    aria-hidden="true"
                                                                                    style="color:green;"></i> View</a>
                                                                            <a class="dropdown-item"
                                                                                href="<?php echo e(url('case/' . $active->id . '/edit')); ?>"><i
                                                                                    class="fa fa-pencil-square-o"
                                                                                    aria-hidden="true"
                                                                                    style="color:black;"></i> Edit</a>
                                                                            <a class="dropdown-item"
                                                                                onclick="Delete(<?php echo e($active->id); ?>)"><i
                                                                                    class="fa fa-trash"
                                                                                    aria-hidden="true"
                                                                                    style="color:red; cursor:pointer;"></i>
                                                                                Delete</a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>





                                                            
                                                        </td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <a class="popover-demo mb-2" data-toggle="popover"
                                                                        title="Remarks"
                                                                        data-content="<?php echo e($active->remarks); ?>"
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>">
                                                                        <?php echo e(mb_strimwidth($active->remarks, 0, 10, '...')); ?>

                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </td>


                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- Tab 2 -->
                    <div class="tab-pane fade" id="pills-high-court" role="tabpanel"
                        aria-labelledby="pills-contact-tab">
                        <div class="col-lg-12">
                            <div class="QA_section QA_section_heading_custom check_box_table">
                                <div class="QA_table ">
                                    <!-- table-responsive -->
                                    <div class="color">
                                        <table class="table Crm_table_active3">
                                            <thead>
                                                <tr>

                                                    <th scope="col"><?php echo e(__('SN.')); ?></th>
                                                    <th scope="col"><?php echo e(__('Prev Date')); ?></th>
                                                    <th scope="col"><?php echo e(__('Case No.')); ?></th>
                                                    <th scope="col"><?php echo e(__('Parties Details')); ?></th>
                                                    <th scope="col"><?php echo e(__('Court Details')); ?></th>
                                                    <th scope="col"><?php echo e(__('Stage')); ?></th>
                                                    <th scope="col"><?php echo e(__('Next date')); ?></th>
                                                    <th scope="col"><?php echo e(__('Remarks')); ?></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $today_cases_high_court; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $active): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e($key + 1); ?></a>
                                                        </td>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(isset($active->previous_date) ? date('d-m-Y', strtotime($active->previous_date)) : '---'); ?></a>
                                                        </td>
                                                        <td style="width:150px!important;">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <a
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(mb_strimwidth($active->case_category, 0, 13, '...') . '/ ' . $active->case_no . '/ ' . date('Y', strtotime($active->case_year))); ?></a>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td style="width:1100px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Parties: </strong>
                                                                    <?php

                                                                        $petitioners = DB::table('petitioners')
                                                                            ->where('case_id', $active->id)
                                                                            ->first();
                                                                        $petitioner = '---';
                                                                        if ($petitioners != null) {
                                                                            $petitioners_name = DB::table('petitioners')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('petitioner');
                                                                            $petitioner_advocates = DB::table('petitioner_advocates')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('petitioner_advocate');
                                                                            $petitioner = '';
                                                                            foreach ($petitioners_name as $key => $name) {
                                                                                $petitioner = $petitioner . ($key + 1) . ') Petitioner ' . $name . ' Advocate ' . $petitioner_advocates[$key] . "\r\n";
                                                                            }
                                                                        }

                                                                        $respondents = DB::table('respondents')
                                                                            ->where('case_id', $active->id)
                                                                            ->first();
                                                                        $respondent = '---';
                                                                        if ($respondents != null) {
                                                                            $respondents_name = DB::table('respondents')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('respondent');
                                                                            $respondent_advocates = DB::table('respondent_advocates')
                                                                                ->where('case_id', $active->id)
                                                                                ->get()
                                                                                ->pluck('respondent_advocate');
                                                                            $respondent = '';
                                                                            foreach ($respondents_name as $key => $name) {
                                                                                $respondent = $respondent . ($key + 1) . ') Respondent ' . $name . ' Advocate ' . $respondent_advocates[$key] . "\r\n";
                                                                            }
                                                                        }

                                                                    ?>

                                                                    <a href="" class=" popover-demo mb-2"
                                                                        data-toggle="popover" title="Petitioners"
                                                                        data-content="<?php echo e($petitioner); ?>"><?php echo e($petitioners ? $petitioners->petitioner : '---'); ?></a><strong>
                                                                        VS </strong><a href=""
                                                                        class=" popover-demo mb-2"
                                                                        data-toggle="popover" title="Respondents"
                                                                        data-content="<?php echo e($respondent); ?>"><?php echo e($respondents ? $respondents->respondent : '---'); ?></a>
                                                                    <!-- <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td> -->
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Brief: </strong>
                                                                    <?php echo e($active->brief_for); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Organization: </strong>
                                                                    <?php
                                                                        $org = DB::table('organizations')
                                                                            ->where('id', $active->organization_id)
                                                                            ->first();
                                                                    ?>
                                                                    <?php echo e($org->organization_name); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Brief#: </strong>
                                                                    <?php echo e(($active->Brief_no != '' and $active->Brief_no != null) ? $active->Brief_no : '-'); ?>

                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td style="width:1100px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <strong>Court: </strong>
                                                                    <?php echo e($active->court_bench); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Room#: </strong>
                                                                    <?php echo e($active->court_room_no); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Judge: </strong>
                                                                    <?php echo e($active->judge_name); ?>

                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12">
                                                                    <strong>Sr no in court: </strong>
                                                                    <?php echo e($active->sr_no_in_court); ?>

                                                                </div>
                                                            </div>


                                                        </td>
                                                        <?php

                                                            $stage = DB::table('cases')
                                                                ->where('id', $active->id)
                                                                ->value('case_stage');

                                                        ?>
                                                        <td><a
                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e($stage); ?></a>
                                                        </td>

                                                        <td style="width:180px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <a
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>"><?php echo e(isset($active->next_date) ? date('d-m-Y', strtotime($active->next_date)) : '---'); ?></a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <a
                                                                        onclick="active_case_refresh(<?php echo e($active->id); ?> , '<?php echo e($active->case_stage); ?>', '<?php echo e($active->next_date); ?>');"><i
                                                                            class="fa fa-refresh" aria-hidden="true"
                                                                            style="color:blue; cursor:pointer;"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="dropdown ml-auto">
                                                                        <a class="dropdown-toggle" type="button"
                                                                            id="dropdownMenuButton"
                                                                            data-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            <i class="fa fa-ellipsis-v"
                                                                                aria-hidden="true"></i>
                                                                        </a>

                                                                        <div class="dropdown-menu dropdown-menu-right"
                                                                            aria-labelledby="dropdownMenuButton">

                                                                            <a class="dropdown-item"
                                                                                href="<?php echo e(url('case/' . $active->id)); ?>"><i
                                                                                    class="fa fa-eye"
                                                                                    aria-hidden="true"
                                                                                    style="color:green;"></i> View</a>
                                                                            <a class="dropdown-item"
                                                                                href="<?php echo e(url('case/' . $active->id . '/edit')); ?>"><i
                                                                                    class="fa fa-pencil-square-o"
                                                                                    aria-hidden="true"
                                                                                    style="color:black;"></i> Edit</a>
                                                                            <a class="dropdown-item"
                                                                                onclick="Delete(<?php echo e($active->id); ?>)"><i
                                                                                    class="fa fa-trash"
                                                                                    aria-hidden="true"
                                                                                    style="color:red; cursor:pointer;"></i>
                                                                                Delete</a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>





                                                            
                                                        </td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <a class="popover-demo mb-2" data-toggle="popover"
                                                                        title="Remarks"
                                                                        data-content="<?php echo e($active->remarks); ?>"
                                                                        href="<?php echo e(url('case/' . $active->id)); ?>">
                                                                        <?php echo e(mb_strimwidth($active->remarks, 0, 10, '...')); ?>

                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </td>


                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- form end =-->
            </div>
        </div>


    </div>


</section>


<div class="modal fade" id="activeexampleModal" tabindex="-1" role="dialog"
    aria-labelledby="activeexampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="activeexampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="custom_ref_add">
                    <form action="#">
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Next Date</label>
                                <input type="date" data-date="" data-date-format="DD MMMM YYYY"
                                    class="primary_input_field" placeholder="Name" id="active_next_date"
                                    onchange="dateformat();">
                            </div>

                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <label for="petitioner">Stage</label>
                                <input type="text" class="primary_input_field" placeholder="Contact"
                                    id="active_stage">
                                <input type="hidden" class="primary_input_field" placeholder="Contact"
                                    id="active_case_id">
                            </div>

                        </div>
                        


                        <div class="text-center mt-3">
                            <a href="#" class="primary-btn small fix-gr-bg" onclick="Update_next_date()"><i
                                    class="ti-check"></i>Add
                            </a>
                            <a type="button" class="primary-btn small fix-gr-bg tr-bg"
                                data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

    
    
    
    <script>
        var dt = new Date();
        document.getElementById('date-time').innerHTML=dt;
        </script>

    <script>
        // function dateformat() {
        //     $("#active_next_date").setAttribute(
        //         "data-date",
        //         moment(this.value, "MM-DD-YYYY")
        //         .format(this.getAttribute("data-date-format"))
        //     );
        //     $("#active_next_date").val(this.attr('data-date'));

        // }


        function active_case_refresh(id, stage, next_date, ) {
            $("#activeexampleModal").modal('show');
            $("#active_case_id").val(id);
            $("#active_next_date").val(next_date);
            $("#active_stage").val(stage);
            // $("#active_next_date").datepicker({
            // dateFormat: 'dd-mm-yyyy'
            // }).val();



            // $("#custom_exampleModalLabel").text('Referance');
            // $("#custom_ref_add").css('display', 'block');
            // $("#custom_org_add").css('display', 'none');
            // $("#custom_client_add").css('display', 'none');
        }

        function Update_next_date() {
            $.ajax({
                url: '<?php echo e(url('update/next_date')); ?>',
                data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    next_date: $('#active_next_date').val(),
                    stage: $('#active_stage').val(),
                    id: $('#active_case_id').val(),
                },
                type: "Post",
                success: function(data) {
                    $("#exampleModal").modal('hide');
                    location.reload();
                    // $("#custom_case_ref").parent().find(".nice-select.primary_select .list").append(
                    // '<li data-value="' + data.id + '" class="option">' + data.name + '</li>');
                    // $("#custom_case_ref").append('<option value="' + data.id + '">' + data.name + '</option>');
                    // console.log(data);
                    // document.getElementById(ID).value
                },
                error: function(error) {
                    Snackbar.show({
                        text: 'Somthing Went Wrong',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }
    </script>


<?php $__env->stopSection(); ?>

<script>
    // function Delete(id) {
    //     swal.fire({
    //         title: "Are you sure",
    //         text: "Will not able to recover",
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#DD6B55",
    //         confirmButtonText: "Yes, delete it",
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax({
    //                 type: "GET",
    //                 url: '<?php echo e(url('case_delete/')); ?>/' + id,
    //                 success: function(data) {
    //                     location.reload();
    //                     swal.fire("success", "Case deleted Successfully", "success")
    //                 },
    //                 error: function(error) {
    //                     Snackbar.show({
    //                         text: " Somthing Went Wrong ",
    //                         pos: 'top-right',
    //                         actionTextColor: '#fff',
    //                         backgroundColor: '#E7515A'
    //                     });
    //                 }
    //             });
    //         } else {
    //             swal.fire("Info", "Slot not deleted", "info")
    //         }
    //     });
    // }
    function Delete(id) {
        var flag = confirm('Are you sure you want to delete case? ');
        if (flag) {
            $.ajax({
                type: "GET",
                url: '<?php echo e(url('case_delete/')); ?>/' + id,
                success: function(data) {
                    location.reload();
                    swal.fire("success", "Case deleted Successfully", "success")
                },
                error: function(error) {
                    Snackbar.show({
                        text: " Somthing Went Wrong ",
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#E7515A'
                    });
                }
            });
        }
    }
</script>

<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/today.blade.php ENDPATH**/ ?>