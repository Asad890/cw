<?php $__env->startSection('mainContent'); ?>


    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1><?php echo app('translator')->get('event.Event'); ?></h1>
                <div class="bc-pages">
                    <a href="<?php echo e(route('home')); ?>"><?php echo app('translator')->get('common.Dashboard'); ?></a>
                    <a href="#"><?php echo app('translator')->get('common.Human Resource'); ?></a>
                    <a href="#"><?php echo app('translator')->get('event.Event'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">
            <?php if(isset($editData)): ?>
                <div class="row">
                    <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">

                            <a href="<?php echo e(route('events.index')); ?>" class="primary-btn small fix-gr-bg">
                                <span class="ti-plus pr-2"></span>
                                <?php echo app('translator')->get('event.Add'); ?>
                            </a>

                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h3 class="mb-25"><?php if(isset($editData)): ?>
                                        <?php echo app('translator')->get('event.Edit'); ?>
                                    <?php else: ?>
                                        <?php echo app('translator')->get('event.Add'); ?>
                                    <?php endif; ?>
                                    <?php echo app('translator')->get('event.Event'); ?>
                                </h3>
                            </div>
                            <?php if(isset($editData)): ?>
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => ['events.update', $editData], 'method' => 'PUT', 'enctype' => 'multipart/form-data'])); ?>

                            <?php else: ?>
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'events.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                            <?php endif; ?>
                            <div class="white-box">
                                <div class="add-visitor">
                                    <div class="row">
                                        <?php if(session()->has('message-success')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success')); ?>

                                            </div>
                                        <?php elseif(session()->has('message-danger')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger')); ?>

                                            </div>
                                        <?php endif; ?>

                                        <div class="col-lg-12">
                                            <div class="primary_input mb-25">
                                                <label class="primary_input_label required" for=""><?php echo e(__('event.Title')); ?>

                                                    </label>
                                                <input name="title" id="current_address"
                                                       class="primary_input_field"
                                                       value="<?php echo e(isset($editData) ? $editData->title : old('title')); ?>"
                                                       placeholder="<?php echo e(__('event.Title')); ?>" type="text" required>
                                                <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="primary_input mb-25">
                                                <label class="primary_input_label required" for=""><?php echo e(__('event.for_whom')); ?>

                                                    </label>
                                                <select class="primary_select mb-25" name="for_whom"
                                                        id="employment_type">
                                                    <option
                                                        value="all" <?php echo e(isset($editData) && $editData->for_whom == 'all' ? 'selected' : ''); ?>><?php echo e(__('event.All')); ?></option>
                                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($role->id); ?>" <?php echo e(isset($editData) && $editData->for_whom == $role->name ? 'selected' : ''); ?>><?php echo e($role->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <span class="text-danger"><?php echo e($errors->first('for_whom')); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="primary_input mb-25">
                                                <label class="primary_input_label required" for=""><?php echo e(__('event.Location')); ?>

                                                    </label>
                                                <input name="location" id="current_address"
                                                       class="primary_input_field name"
                                                       placeholder="<?php echo e(__('event.Location')); ?>"
                                                       value="<?php echo e(isset($editData) ? $editData->location : old('location')); ?>" type="text"
                                                       required>
                                                <span class="text-danger"><?php echo e($errors->first('location')); ?></span>
                                            </div>
                                        </div>
                                            <div class="col-xl-12 date_of_joining_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label required"
                                                           for=""><?php echo e(__('event.Start Date')); ?>

                                                        </label>
                                                    <div class="primary_datepicker_input">
                                                        <div class="no-gutters input-right-icon">
                                                            <div class="col">
                                                                <div class="">
                                                                    <input placeholder="<?php echo e(__('common.Date')); ?>"
                                                                           class="primary_input_field primary-input date form-control"
                                                                           id="date_of_joining" type="text"
                                                                           name="from_date"
                                                                           value="<?php echo e(isset($editData)? date('Y-m-d', strtotime($editData->from_date)): date('Y-m-d')); ?>"
                                                                           autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                            <button class="" type="button">
                                                                <i class="ti-calendar" id="start-date-icon"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <span class="text-danger"><?php echo e($errors->first('from_date')); ?></span>
                                                </div>
                                            </div>

                                            <div class="col-xl-12 date_of_joining_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label required" for=""><?php echo e(__('event.To Date')); ?>

                                                        </label>
                                                    <div class="primary_datepicker_input">
                                                        <div class="no-gutters input-right-icon">
                                                            <div class="col">
                                                                <div class="">
                                                                    <input placeholder="<?php echo e(__('common.Date')); ?>"
                                                                           class="primary_input_field primary-input date form-control"
                                                                           type="text" name="to_date"
                                                                           value="<?php echo e(isset($editData)? date('Y-m-d', strtotime($editData->to_date)): date('Y-m-d')); ?>"
                                                                           autocomplete="off" required>
                                                                </div>
                                                            </div>
                                                            <button class="" type="button">
                                                                <i class="ti-calendar" id="start-date-icon"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <span class="text-danger"><?php echo e($errors->first('from_date')); ?></span>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="primary_input mb-25">
                                                    <label class="primary_input_label required"
                                                           for=""><?php echo e(__('event.Description')); ?>

                                                        </label>
                                                    <input name="description" id="current_address"
                                                           class="primary_input_field"
                                                           value="<?php echo e(isset($editData) ? $editData->description : old('description')); ?>"
                                                           placeholder="<?php echo e(__('event.Description')); ?>" type="text"
                                                           required>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip">
                                                    <span class="ti-check"></span>
                                                    <?php if(isset($editData)): ?>
                                                        <?php echo app('translator')->get('common.Update'); ?>
                                                    <?php else: ?>
                                                        <?php echo app('translator')->get('common.Save'); ?>
                                                    <?php endif; ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php echo e(Form::close()); ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9">
                        <?php if(session()->has('message-success-delete')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session()->get('message-success-delete')); ?>

                            </div>
                        <?php elseif(session()->has('message-danger-delete')): ?>
                            <div class="alert alert-danger">
                                <?php echo e(session()->get('message-danger-delete')); ?>

                            </div>
                        <?php endif; ?>



                        <div class="container-fluid p-0">
        <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                        <h3 class="mb-0"><?php echo app('translator')->get('event.Event List'); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table SplitDivTable">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                            <table class="table Crm_table_active3">

                                                <thead>
                                                <tr>
                                                    <th><?php echo app('translator')->get('event.Title'); ?></th>
                                                    <th><?php echo app('translator')->get('event.for_whom'); ?></th>
                                                    <th><?php echo app('translator')->get('event.Start Date'); ?></th>
                                                    <th><?php echo app('translator')->get('event.To Date'); ?></th>
                                                    <th><?php echo app('translator')->get('event.Location'); ?></th>
                                                    <th><?php echo app('translator')->get('common.Action'); ?></th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php if(isset($events)): ?>
                                                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>

                                                            <td><?php echo e(@$event->title); ?></td>
                                                            <td>
                                                            <?php if($event->for_whom == 'all'): ?>

                                                                <?php echo e(@$event->for_whom); ?>

                                                            <?php else: ?>

                                                            <?php echo e($event->role->name); ?>

                                                            <?php endif; ?>
                                                                </td>

                                                            <td><?php echo e($event->from_date); ?></td>


                                                            <td><?php echo e($event->to_date); ?></td>

                                                            <td><?php echo e(@$event->location); ?></td>

                                                            <td>

                                                                <div class="dropdown CRM_dropdown">
                                                                    <button class="btn btn-secondary dropdown-toggle"
                                                                            type="button"
                                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                                            aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                        <?php echo e(__('common.Select')); ?>

                                                                    </button>
                                                                    <div class="dropdown-menu dropdown-menu-right"
                                                                         aria-labelledby="dropdownMenu2">
                                                                        <?php if(permissionCheck('events.edit')): ?>
                                                                        <a class="dropdown-item"
                                                                           href="<?php echo e(route('events.edit',$event->id)); ?>"><?php echo app('translator')->get('event.Edit'); ?></a>
                                                                        <?php endif; ?>
                                                                        <?php if(permissionCheck('events.destroy')): ?>
                                                                        <a onclick="confirm_modal('<?php echo e(route('events.delete', $event->id)); ?>')"
                                                                           class="dropdown-item edit_brand"><?php echo e(__('common.Delete')); ?></a>
                                                                            <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


                    </div>
                </div>
            </div>
    </section>
    <?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', ['title' => 'Event'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Attendance/Resources/views/events/index.blade.php ENDPATH**/ ?>