<?php $__env->startSection('mainContent'); ?>




    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('All Cases')); ?></h3>
                            <ul class="d-flex">
                                <?php if(permissionCheck('case.store')): ?>
                                    <li><a class="primary-btn radius_30px mr-10 fix-gr-bg"
                                           href="<?php echo e(route('case.create')); ?>"><i class="ti-plus"></i><?php echo e(__
                        ('case.New Case')); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3 table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox"></th>
                                        <th scope="col"><?php echo e(__('SR No.')); ?></th>
                                        <th scope="col"><?php echo e(__('BRIEF NO')); ?></th>
                                        <th scope="col"><?php echo e(__('PREVIOUS DATE')); ?></th>
                                        <th scope="col"><?php echo e(__('Court/Benches')); ?></th>
                                        <th scope="col"><?php echo e(__('CASE NO')); ?></th>
                                        <th scope="col"><?php echo e(__('PETITIONER ')); ?></th>
                                        <th scope="col"><?php echo e(__('PETITIONER’S ADVOCATE')); ?></th>
                                        <th scope="col"><?php echo e(__('RESPONDENT')); ?></th>
                                        <th scope="col"><?php echo e(__('RESPONDENT’S ADVOCATE')); ?></th>
                                        <th scope="col"><?php echo e(__('CASE STAGE')); ?></th>
                                        <th scope="col"><?php echo e(__('Sr. No. in Court')); ?></th>
                                        <th scope="col"><?php echo e(__('COURT ROOM NO.')); ?></th>
                                        <th scope="col"><?php echo e(__('JUDGE NAME')); ?></th>
                                        <th scope="col"><?php echo e(__('BRIEF FOR')); ?></th>
                                        <th scope="col"><?php echo e(__('ORGANIZATION')); ?></th>
                                        <th scope="col"><?php echo e(__('TAGS/REFERENCE')); ?></th>
                                        <th scope="col"><?php echo e(__('POLICE STATION')); ?></th>
                                        <th scope="col"><?php echo e(__('REMARKS COLUMN')); ?></th>
                                        <th scope="col"><?php echo e(__('NEXT DATE')); ?></th>
                                        <th scope="col"><?php echo e(__('CNR NO.')); ?></th>
                                        <th scope="col"><?php echo e(__('State')); ?></th>
                                        <th scope="col"><?php echo e(__('District ')); ?></th>
                                        <th scope="col"><?php echo e(__('Case Type')); ?></th>
                                        <th scope="col"><?php echo e(__('Case Year')); ?></th>
                                        <th scope="col"><?php echo e(__('Decided')); ?></th>
                                        <th scope="col"><?php echo e(__('Abandoned')); ?></th>
                                        <th scope="col"><?php echo e(__('Clients Name')); ?></th>
                                        <th scope="col"><?php echo e(__('common.Actions')); ?></th>
                                    </tr>
                                    </thead>
                                <tbody>
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                            <td><input type="checkbox"></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td>
                                                <label class="switch_toggle" for="active_checkbox">
                                                        <input type="checkbox" id="active_checkbox" 
                                                        value="check" onchange="update_active_status(this)" checked>
                                                        <div class="slider round"></div>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="switch_toggle" for="active_checkbox">
                                                        <input type="checkbox" id="active_checkbox" 
                                                        value="check" onchange="update_active_status(this)">
                                                        <div class="slider round"></div>
                                                </label>
                                            </td>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            

                                            <td>


                                                <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <a href="<?php echo e(route('case.show', $model->id)); ?>"
                                                           class="dropdown-item"><i
                                                                class="icon-file-eye"></i> <?php echo e(__
                                                            ('common.View')); ?></a>
                                                        <?php if(!$model->judgement): ?>
                                                            <?php if(permissionCheck('case.edit')): ?>
                                                                <a href="<?php echo e(route('case.edit', $model->id)); ?>"
                                                                   class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i><?php echo e(__('common.Edit')); ?>

                                                                </a>
                                                            <?php endif; ?>
                                                            <!-- <?php if(permissionCheck('date.store')): ?>
                                                                <a href="<?php echo e(route('date.create', ['case' => $model->id])); ?>"
                                                                   class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Date')); ?>

                                                                </a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('putlist.store')): ?>
                                                                <a href="<?php echo e(route('putlist.create', ['case' => $model->id])); ?>"
                                                                   class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Put Up Date')); ?>

                                                                </a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('lobbying.store')): ?>
                                                                <a href="<?php echo e(route('lobbying.create', ['case' => $model->id])); ?>"
                                                                   class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Lobbying Date')); ?>

                                                                </a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('judgement.store')): ?>
                                                                <a href="<?php echo e(route('judgement.create', ['case' => $model->id])); ?>"
                                                                   class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Judgement Date')); ?>

                                                                </a>
                                                            <?php endif; ?> -->
                                                        <?php endif; ?>
                                                        <?php if(permissionCheck('case.destroy')): ?>
                                                            <span id="delete_item" data-id="<?php echo e($model->id); ?>" data-url="<?php echo e(route
                                                                ('case.destroy', $model->id)); ?>" class="dropdown-item"><i class="icon-trash"></i> <?php echo e(__('common.Delete')); ?>

                                                            </span>
                                                        <?php endif; ?>

                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>

datatable(
  iris, extensions = 'Buttons', options = list(
    dom = 'Bfrtip',
    buttons = c('copy', 'csv', 'excel', 'pdf', 'print')
  )
)

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/index.blade.php ENDPATH**/ ?>