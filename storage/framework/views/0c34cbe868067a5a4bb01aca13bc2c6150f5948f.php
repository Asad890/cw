

<?php $__env->startSection('mainContent'); ?>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('case.Add Date')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">
                        <?php echo Form::open(['route' => 'date.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-12">
                                <div class="d-flex justify-content-between">
                                    <?php echo e(Form::label('stage_id', __('case.Case Stage'))); ?>

                                    <?php if(permissionCheck('master.stage.store')): ?>
                                        <label class="primary_input_label green_input_label" for="">
                                            <a href="<?php echo e(route('master.stage.create', ['quick_add' => true])); ?>"
                                               class="btn-modal"
                                               data-container="case_stage_add_modal"><?php echo e(__('case.Create New')); ?>

                                                <i class="fas fa-plus-circle"></i></a></label>
                                    <?php endif; ?>
                                </div>
                                <?php echo e(Form::select('stage_id', $stages, $case_model->stage_id, ['class' => 'primary_select', 'data-placeholder' => __('case.Case Stage')])); ?>

                            </div>

                            <div class="primary_input col-md-12">
                                <?php echo e(Form::hidden('case', $case)); ?>

                                <?php echo e(Form::label('hearing_date', __('case.Hearing Date'), ['class' => 'required'])); ?>

                                <?php echo e(Form::text('hearing_date', date('Y-m-d H:i'), ['required' => '','class' => 'primary_input_field primary-input datetime form-control', 'placeholder' => __('case.Hearing Date')])); ?>

                            </div>

                        </div>

                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('case.Court Order'), ['class' => 'required'])); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('case.Court Order'), 'rows' => 5, 'required', 'data-parsley-errors-container' =>
                            '#description_error' ])); ?>

                            <span id="description_error"></span>
                        </div>

                        <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                        <div class="text-center mt-3">
                            <button class="primary_btn_large submit" type="submit"><i
                                    class="ti-check"></i><?php echo e(__('common.Create')); ?>

                            </button>
                            <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                            </button>
                        </div>

                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade animated case_stage_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {
            _formValidation();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.New Date')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/date/create.blade.php ENDPATH**/ ?>