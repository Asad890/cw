<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('task.Add Task')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::open(['route' => 'task.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-12">
                                <?php echo e(Form::label('name', __('task.Name'), ['class' => 'required'])); ?>

                                <?php echo e(Form::text('name', null, ['required' => '','class' => 'primary_input_field', 'placeholder' => __('task.Name')])); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('case_id', __('task.Case'), ['class' => 'required'])); ?>

                                <?php echo e(Form::select('case_id', $cases, null, ['required' => '', 'class' => 'primary_select', 'data-parsley-errors-container' => '#case_id_error'])); ?>

                                <span id="case_id_error"></span>
                            </div>

                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('assignee_id', __('task.Assignee'), ['class' => 'required'])); ?>

                                <?php echo e(Form::select('assignee_id', $users, null, ['required' => '', 'class' => 'primary_select', 'data-parsley-errors-container' => '#assignee_id_error'])); ?>

                                <span id="assignee_id_error"></span>
                            </div>

                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('priority', __('task.Priority'), ['class' => 'required'])); ?>

                                <?php echo e(Form::select('priority', ['Low'=> __('common.Low') ,'Medium'=> __('common.Medium'),'High'=> __('common.High')], null, ['required' => '', 'class' => 'primary_select', 'data-parsley-errors-container' => '#priority_error'])); ?>

                                <span id="priority_error"></span>
                            </div>


                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('stage_id', __('task.Stage'))); ?>

                                <?php echo e(Form::select('stage_id', $stages, null, ['class' => 'primary_select', 'data-parsley-errors-container' => '#stage_id_error'])); ?>

                                <span id="stage_id_error"></span>
                            </div>


                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('due_date', __('task.Due Date'))); ?>

                                <?php echo e(Form::text('due_date', date('Y-m-y H:i'), ['required' => '','class' => 'primary_input_field primary-input datetime form-control', "id"=>"fromDate",'placeholder' => __('task.Date')])); ?>

                            </div>
                        </div>

                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('task.Description'))); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('task.Description'), 'rows' => 5, 'data-parsley-errors-container' =>
                            '#description_error' ])); ?>

                            <span id="description_error"></span>
                        </div>
                        <div class="text-center mt-3">
                            <button class="primary-btn semi_large2 fix-gr-bg submit" type="submit"><i
                                    class="ti-check"></i><?php echo e(__('common.Create')); ?>

                            </button>

                            <button class="primary-btn semi_large2 fix-gr-bg submitting" type="submit" disabled
                                    style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                            </button>

                        </div>
                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {
            _formValidation();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('task.Create New Task')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Task/Resources/views/task/create.blade.php ENDPATH**/ ?>