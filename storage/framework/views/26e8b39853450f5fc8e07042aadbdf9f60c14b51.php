<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('appointment.Appointment Details')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <table>
                            <tbody>
                            <tr>
                                <td class="p-2"><?php echo e(__('common.Title')); ?> </td>
                                <td>:</td>
                                <td><?php echo e($model->title); ?></td>
                            </tr>

                            <tr>
                                <td class="p-2"><?php echo e(__('appointment.Contact name')); ?> </td>
                                <td>:</td>
                                <td><?php echo e($model->contact->name); ?></td>
                            </tr>


                            <tr>
                                <td class="p-2"><?php echo e(__('appointment.Motive')); ?> </td>
                                <td>:</td>
                                <td><?php echo e($model->motive); ?></td>
                            </tr>

                            <tr>
                                <td class="p-2"><?php echo e(__('common.Date')); ?> </td>
                                <td>:</td>
                                <td><?php echo e(formatDate($model->date)); ?></td>
                            </tr>


                            <tr>
                                <td class="p-2"><?php echo e(__('appointment.Notes')); ?> </td>
                                <td>:</td>
                                <td><?php echo $model->notes; ?></td>
                            </tr>
                            <?php if(moduleStatusCheck('CustomField') and $model->customFields): ?>
                                <?php if ($__env->exists('customfield::details.show', ['customFields' => $model->customFields, 'file' => 'tr'])) echo $__env->make('customfield::details.show', ['customFields' => $model->customFields, 'file' => 'tr'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $('.printMe').click(function () {
            window.print();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('appointment.Appointment Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/appointment/show.blade.php ENDPATH**/ ?>