

<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row">
            <div class="col-lg-6 d-print-none">
                
                </div>

                <div class="col-lg-6 d-print-none text-right">
                <a href="#" data-toggle="modal" class="primary-btn small fix-gr-bg"
                               data-target="#add_to_do"
                               title="Add To Do" data-modal-size="modal-md">
                                <span class="ti-plus pr-2"></span>
                                <?php echo app('translator')->get('Add New Client'); ?>
                            </a>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12 mt-25">
                    <!-- Rozanam -->
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn1 primary-btn small fix-gr-bg" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                INDIVIDUAL
                                </button>
                            </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Sr No.</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Address</th>
                                                                    <th scope="col">Contact No</th>
                                                                    <th scope="col">Email ID</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <tr>

                                                                    <td>1</td>
                                                                    <td> Aditya Washimkar</td>
                                                                    <td>Nagpur</td>
                                                                    <td>9730xxxxxx</td>
                                                                    <td>so&so@soso.com</td>
                                                                    <td style="width: 30%;"> 
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">SMS</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">Email</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="far fa-trash-alt"></i></button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="fas fa-edit"></i></button>
                                                                    </td> 
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- close -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- worklist -->
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn1 primary-btn small fix-gr-bg collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    ORGANIZATION
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                        <!-- start -->
                                        <div class="col-lg-12">
                                            <div class="QA_section QA_section_heading_custom check_box_table">
                                                <div class="QA_table ">
                                                    <!-- table-responsive -->
                                                    <div class="">
                                                    <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Sr No.</th>
                                                                    <th scope="col">Org Name</th>
                                                                    <th scope="col">Auth Officer</th>
                                                                    <th scope="col">Auth Contact No</th>
                                                                    <th scope="col">Auth Email ID</th>
                                                                    <th scope="col">Org Address</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <tr>

                                                                    <td>1</td>
                                                                    <td> Aditya Washimkar</td>
                                                                    <td>Nagpur</td>
                                                                    <td>9730xxxxxx</td>
                                                                    <td>so&so@soso.com</td>
                                                                    <td>Nagpur</td>
                                                                    <td style="width: 30%;"> 
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">SMS</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">Email</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="far fa-trash-alt"></i></button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="fas fa-edit"></i></button>
                                                                    </td> 
                                                                </tr>

                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end -->
                                </div>
                            </div>
                        </div>
                        <!-- Connected Matters -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn1 primary-btn small fix-gr-bg collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                REFERENCE
                                </button>
                            </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <!-- start -->
                            <div class="col-lg-12">
                                <div class="QA_section QA_section_heading_custom check_box_table">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <div class="">
                                        <table class="table Crm_table_active3">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Sr No.</th>
                                                                    <th scope="col">Referent Name</th>
                                                                    <th scope="col">Referent Address</th>
                                                                    <th scope="col">Referent Contact No</th>
                                                                    <th scope="col">Referent Email ID</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <tr>

                                                                    <td>1</td>
                                                                    <td> Aditya Washimkar</td>
                                                                    <td>Nagpur</td>
                                                                    <td>9730xxxxxx</td>
                                                                    <td>so&so@soso.com</td>
                                                                    <td style="width: 30%;"> 
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">SMS</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg">Email</button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="far fa-trash-alt"></i></button>
                                                                        <button type="button" class="btn1 primary-btn small fix-gr-bg"><i class="fas fa-edit"></i></button>
                                                                    </td> 
                                                                </tr>

                                                            </tbody>
                                                    </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                <!-- end -->
                            </div>
                            </div>
                        </div>
                        <!-- card close -->
                       
                        
                    </div>

                </div>

               
    </section>
   

    <div class="modal fade admin-query" id="add_to_do">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Client Type</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()'])); ?>


                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Type')); ?></label>
                                                    <select class="primary_input_field">
                                                                <option>INDIVIDUAL</option>
                                                                <option>ORGANIZATION</option>
                                                                <option>REFERENCE</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>INDIVIDUAL</h4>
                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Client Name')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Client Name')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Contact Number')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Contact number')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Email ID')); ?>*</label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Email ID')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Address')); ?></label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Address')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Org -->
                                        <h4>ORGANIZATION</h4>
                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('ORGANIZATION Name')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Organization Name')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-25">
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Name')); ?>*</label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Name')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Contact No')); ?></label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Contact No')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Email Id')); ?></label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Email Id')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-25">
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Name')); ?>*</label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Name')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Contact No')); ?></label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Contact No')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Email Id')); ?></label>
                                                    <input  class="primary_input_field"
                                                           placeholder="<?php echo e(__('Email Id')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('ORGANIZATION Address')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('ORGANIZATION Address')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                        <h4>REFERENCE</h4>
                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Referent Name')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Referent Name')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Contact Number')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Contact number')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-25">
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Email ID')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Email Id')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Referent Address')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Referent Address')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 text-center">
                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo e(Form::close()); ?>

                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>


    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Case Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\updates 123\cw\resources\views/myclient/index.blade.php ENDPATH**/ ?>