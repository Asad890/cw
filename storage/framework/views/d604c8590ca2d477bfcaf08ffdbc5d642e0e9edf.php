<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_admin_visitor empty_table_tab">
        <div class="container-fluid p-0">


            <div class="row">
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if(!Illuminate\Support\Facades\Config::get('app.app_sync')): ?>
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'setting.updateSystem.submit1', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>


                            <?php endif; ?>
                            <div class="white-box sm_mb_20 sm2_mb_20 md_mb_20 ">
                                <div class="main-title">
                                    <h3 class="mb-30"><?php echo app('translator')->get('setting.Upload From Local Directory'); ?></h3>
                                </div>
                                <div class="add-visitor">

                                    <div class="row no-gutters input-right-icon mb-20">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input
                                                    class="primary-input form-control <?php echo e($errors->has('content_file') ? ' is-invalid' : ''); ?>"
                                                    readonly="true" type="text"
                                                    placeholder="<?php echo e(isset($editData->file) && @$editData->file != ""? getFilePath3(@$editData->file):trans('common.Browse')); ?> "
                                                    id="placeholderUploadContent" name="content_file">
                                                <span class="focus-border"></span>
                                                <?php if($errors->has('content_file')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('content_file')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="primary-btn-small-input" type="button">
                                                <label class="primary-btn small fix-gr-bg"
                                                       for="upload_content_file"><?php echo app('translator')->get('common.Browse'); ?></label>
                                                <input type="file" class="d-none form-control" name="updateFile"
                                                       required
                                                       id="upload_content_file">
                                            </button>

                                        </div>
                                    </div>
                                    <?php
                                        $tooltip = "";

                                if (permissionCheck('setting.updateSystem.submit')){
                                                $tooltip = "";
                                            }else{
                                                $tooltip = "You have no permission to add";
                                            }
                                if (Illuminate\Support\Facades\Config::get('app.app_sync')){
                                    $tooltip =trans('For the demo version, you cannot change this');
                                }
                                    ?>
                                    <div class="row mt-40">
                                        <div class="col-lg-12 text-center">
                                            <button class="primary-btn fix-gr-bg" data-toggle="tooltip"
                                                    title="<?php echo e(@$tooltip); ?>">
                                                <span class="ti-check"></span>
                                                <?php if(isset($session)): ?>
                                                    <?php echo app('translator')->get('common.Update'); ?>
                                                <?php else: ?>
                                                    <?php echo app('translator')->get('common.Save'); ?>
                                                <?php endif; ?>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo e(Form::close()); ?>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box">
                                <div class="main-title">
                                    <h3 class="mb-30"><?php echo e(__('setting.About System')); ?></h3>
                                </div>
                                <div class="add-visitor">
                                    <table style="width:100%; box-shadow: none;"
                                           class="display school-table school-table-style">

                                        <tr>
                                            <td><?php echo e(__('setting.Software Version')); ?></td>
                                            <td><?php echo e(config('configs')->where('key','system_version')->first()->value); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo e(__('setting.Check update')); ?></td>
                                            <td><a href="https://casewise.in"
                                                   target="_blank"> <i
                                                        class="ti-new-window"> </i> <?php echo e(__('setting.Update')); ?> </a></td>
                                        </tr>
                                        <tr>
                                            <td> <?php echo e(__('setting.PHP Version')); ?></td>
                                            <td><?php echo e(phpversion()); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo e(__('setting.Curl Enable')); ?></td>
                                            <td><?php
                                                    if  (in_array  ('curl', get_loaded_extensions())) {
                                                        echo 'enable';
                                                    }
                                                    else {
                                                        echo 'disable';
                                                    }
                                                ?></td>
                                        </tr>


                                        <tr>
                                            <td><?php echo e(__('setting.Purchase code')); ?></td>
                                            <td><?php echo e(__('Verified')); ?>


                                                <?php if(!env('APP_SYNC')): ?>

                                                    <?php if(auth()->user()->role_id == 1): ?>
                                                        <?php if ($__env->exists('service::license.revoke')) echo $__env->make('service::license.revoke', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                    <?php endif; ?>

                                                <?php else: ?>
                                                    <button data-toggle="tooltip" title="Restricted in demo mode" class="primary-btn small fix-gr-bg ml-2">
                                                        Revoke License
                                                    </button>
                                                <?php endif; ?>

                                            </td>
                                        </tr>


                                        <tr>
                                            <td><?php echo e(__('setting.Install Domain')); ?></td>
                                            <td><?php echo e(config('configs')->where('key','system_domain')->first()->value); ?></td>
                                        </tr>


                                        <tr>
                                            <td><?php echo e(__('setting.System Activated Date')); ?></td>
                                            <td><?php echo e(dateFormat(config('configs')->where('key','system_activated_date')->first()->value)); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo e(__('setting.Last Update')); ?></td>
                                            <td><?php echo e(dateFormat(config('configs')->where('key','last_updated_date')->first()->value)); ?></td>
                                        </tr>


                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/updateSystem.blade.php ENDPATH**/ ?>