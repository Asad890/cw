
<div class="col-12">
    <div class="row attach-petitioner-row">
        <div class="col-12 attach-petetioner">
            <div class="row">
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

                </div>
                <div class="primary_input col-md-8">
                    <?php echo e(Form::text('petitioner[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER')])); ?>

                </div>
                <div class="primary_input col-md-1">
                    <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button" onclick="petitioner_add();"> <i class="ti-plus"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

                </div>
                <div class="primary_input col-md-9">
                    <?php echo e(Form::text('petitioner_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE')])); ?>

                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>

<script>
var petetioner_count=1;
var index = 0;
function petitioner_add() {
    index = $('.attach-petetioner').length;
    petetioner_count = petetioner_count + 1;
    addNewPetitioner(index);
}



function addNewPetitioner(index) {
    "use strict";

    var add_petitioner = `
        <div class="col-12 attach-petetioner" id="petetioner-`+index+`">
            <div class="row">
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?> `+petetioner_count+`
                </div>
                <div class="primary_input col-md-8">
                    <?php echo e(Form::text('petitioner[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER')  , 'required'=>'required'])); ?>

                </div>
                <div class="primary_input col-md-1">
                    <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" onclick="remove_petetioner('petetioner-`+index+`');" type="button" > <i class="ti-trash"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?> `+petetioner_count+`
                </div>
                <div class="primary_input col-md-9">
                    <?php echo e(Form::text('petitioner_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE') , 'required'=>'required'])); ?>

                </div>
                <br><br><br>
            </div>
        </div>`
    $('.attach-petitioner-row').append(add_petitioner);
}


function remove_petetioner(id){
    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
}

</script>


<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/petitioner.blade.php ENDPATH**/ ?>