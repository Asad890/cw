<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <div class="main-title">
                        <h3 class="mb-30"><?php echo e($model->title); ?></h3>
                    </div>
                </div>
                <div class="col-lg-6 d-print-none">

                    <?php if($model->judgement_status == 'Open' or $model->judgement_status == 'Reopen'): ?>
                        <?php if(permissionCheck('date.store')): ?>
                            <a href="<?php echo e(route('date.create', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('putlist.store')): ?>
                            <a href="<?php echo e(route('putlist.create', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Put Up Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('lobbying.store')): ?>
                            <a href="<?php echo e(route('lobbying.create', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Lobbying Date')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.create', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.New Judgement Date')); ?></a>
                        <?php endif; ?>

                    <?php endif; ?>
                    <?php if($model->judgement_status == 'Judgement'): ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.reopen', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.Re-open')); ?></a>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.store')): ?>
                            <a href="<?php echo e(route('judgement.close', ['case' => $model->id])); ?>"
                                class="primary-btn small fix-gr-bg"><i
                                    class="ti-calendar mr-2"></i><?php echo e(__('case.Close')); ?></a>
                        <?php endif; ?>

                    <?php endif; ?>

                    <a class="primary-btn small fix-gr-bg print_window"
                        href="<?php echo e(route('case.show', [$model->id, 'print' => true])); ?>" target="_blank"><i
                            class="ti-printer mr-2"></i>
                        <?php echo e(__('case.Print')); ?>

                    </a>
                    <a class="primary-btn small fix-gr-bg " href="<?php echo e(route('file.index', ['case' => $model->id])); ?>"><i
                            class="ti-file mr-2"></i>
                        Attachment (<?php echo e($data['attachements']); ?>)
                    </a>

                    <a class="primary-btn small fix-gr-bg " onclick="Delete(<?php echo e($model->id); ?>)">
                        <i class="ti-trash mr-2"></i>
                        Delete
                    </a>

                </div>
                <div class="col-lg-6 d-print-none text-right">
                    <a class="primary-btn small fix-gr-bg " href="<?php echo e(route('case.edit', $model->id)); ?>"><i
                            class="ti-file mr-2"></i>
                        <?php echo e(__('Edit Case')); ?>

                    </a>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-8 mt-25">
                    <!-- Rozanam -->
                    <div class="accordion" id="accordionExample">
                        <div class="card">


                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <!-- body -->
                                    <div class="col-lg-12">
                                        <!-- Start -->
                                        <details class="deque-expander" id="dream">
                                            <summary class="deque-expander-summary">
                                                <span class="toggle-indicator"></span>
                                                <b> Roznama/Case History</b>
                                            </summary>

                                            <div class="deque-expander-content">
                                                
                                                <div class="col-lg-12">

                                                    <div class="row">
                                                        <div class="primary_input col-md-12">
                                                            <?php echo e(Form::label('Case History', __('Case History'))); ?>

                                                            <?php echo e(Form::textarea('notes_description', null, [
                                                                'class' => 'primary_input_field summernote',
                                                                'placeholder' => __('court.Court  Description'),
                                                                'rows' => 5,
                                                                'maxlength' => 1500,
                                                                'data-parsley-errors-container' => '#description_error',
                                                            ])); ?>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </details>
                                        <!-- close -->
                                        <!-- Start -->
                                        <details class="deque-expander" id="dream">
                                            <summary class="deque-expander-summary">
                                                <span class="toggle-indicator"></span>
                                                <b> Worklist </b>
                                            </summary>

                                            <div class="deque-expander-content">
                                                <div class="col-lg-12">
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                                        <div class="QA_table ">
                                                            <!-- table-responsive -->
                                                            <div class="">
                                                                <table class="table Crm_table_active3">
                                                                    <thead>
                                                                        <tr>

                                                                            <th scope="col">Date</th>
                                                                            <th scope="col">Description</th>
                                                                            <th scope="col">Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>

                                                                            <td>14-05-2022</td>
                                                                            <td>Draft Affidavit</td>
                                                                            <td>In Progress</td>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </details>
                                        <!-- close -->
                                        <!-- Start -->
                                        <details class="deque-expander" id="dream">
                                            <summary class="deque-expander-summary">
                                                <span class="toggle-indicator"></span>
                                                <b> Notes </b>
                                            </summary>

                                            <div class="deque-expander-content">
                                                <div class="col-lg-12">

                                                    <div class="row">
                                                        <div class="primary_input col-md-12">
                                                            <?php echo e(Form::label('Notes', __('Notes'))); ?>

                                                            <?php echo e(Form::textarea('notes_description', $model->notes_description, [
                                                                'class' => 'primary_input_field summernote',
                                                                'placeholder' => __('court.Court  Description'),
                                                                'rows' => 5,
                                                                'maxlength' => 1500,
                                                                'data-parsley-errors-container' => '#description_error',
                                                            ])); ?>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </details>
                                        <!-- close -->
                                        <!-- Start -->
                                        <details class="deque-expander" id="dream">
                                            <summary class="deque-expander-summary">
                                                <span class="toggle-indicator"></span>
                                                <b> Connected Matters </b>
                                            </summary>

                                            <div class="deque-expander-content">
                                                <div class="col-lg-12">
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                                        <div class="QA_table ">
                                                            <!-- table-responsive -->
                                                            <div class="">
                                                                <table class="table Crm_table_active3">
                                                                    <thead>
                                                                        <tr>

                                                                            <th scope="col">Sr No.</th>
                                                                            <th scope="col">Case No.</th>
                                                                            <th scope="col">Case Title</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>

                                                                            <td>1</td>
                                                                            <td>Draft Affidavit</td>
                                                                            <td>In Progress</td>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </details>
                                        <!-- close -->
                                        <!-- Start -->
                                        <details class="deque-expander" id="dream">
                                            <summary class="deque-expander-summary">
                                                <span class="toggle-indicator"></span>
                                                <b> Interlocutory orders Evidences or Judgements </b>
                                            </summary>

                                            <div class="deque-expander-content">
                                                
                                                <div class="col-lg-12">

                                                    <div class="row">
                                                        <div class="primary_input col-md-12">
                                                            <?php echo e(Form::label('Case History', __('Case History'))); ?>

                                                            <?php echo e(Form::textarea('notes_description', null, [
                                                                'class' => 'primary_input_field summernote',
                                                                'placeholder' => __('court.Court  Description'),
                                                                'rows' => 5,
                                                                'maxlength' => 1500,
                                                                'data-parsley-errors-container' => '#description_error',
                                                            ])); ?>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </details>
                                        <!-- close -->
                                    </div>
                                </div>
                                <!-- close body -->
                            </div>
                        </div>
                        <!-- worklist -->



                        <!-- card close -->
                        <div class="row">
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle1 -->
                                <h5>Decided</h5>
                                <div class="custom-control">
                                    <label class="switch_toggle" for="custom_active_checkbox1">
                                        <input name="decided_toggle" type="checkbox" id="custom_active_checkbox1"
                                             onchange="custom_update_active_status(this)"
                                            <?php echo e($model->decided_toggle == 'check' ? 'checked' : ''); ?>>
                                        <div class="slider round">
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6 mt-25">
                                <!-- Toggle2 -->
                                <h5>Abandoned</h5>
                                <div class="custom-control">
                                    <label class="switch_toggle" for="custom_active_checkbox2">
                                        <input type="checkbox" name="abbondend_toggle" id="custom_active_checkbox2"
                                             onchange="custom_update_active_status(this)"
                                            <?php echo e($model->abbondend_toggle == 'check' ? 'checked' : ''); ?>>
                                        <div class="slider round"></div>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-lg-4 mt-25">
                    <div class="student-meta-box sticky-details">
                        <div class="white-box student-details pt-3">
                            <div class="single-meta">
                                <h3 class="mb-0"><?php echo e(__('case.Case Details')); ?> </h3>
                            </div>
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('CNR')); ?>:
                                    </div>
                                    <div class="value">
                                        
                                        <p><?php echo e($model->cnr_no); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Brief No,')); ?>:
                                    </div>
                                    <div class="value">
                                        
                                        <p><?php echo e($model->Brief_no); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case Type')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->file_no); ?> -->
                                        <p><?php echo e($model->case_category); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case No.')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->ref_name); ?> -->
                                        <p><?php echo e($model->case_no); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Year')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e($model->ref_mobile); ?> -->
                                        <p><?php echo e($model->case_year); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Previous Date')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(formatDate($model->hearing_date)); ?> -->
                                        <p><?php echo e(isset($model->previous_date) ? date('d-m-Y', strtotime($model->previous_date)) : '---'); ?>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Next date')); ?>:
                                    </div>
                                    <div class="value">
                                        <p><?php echo e(isset($model->next_date) ? date('d-m-Y', strtotime($model->next_date)) : '---'); ?>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Case Stage')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(formatDate($model->filling_date)); ?> -->
                                        <p><?php echo e($model->case_stage); ?></p>
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Brief For')); ?>:
                                    </div>
                                    <div class="value">
                                        
                                        <p><?php echo e($model->brief_for); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">

                                        <?php echo e(__('State')); ?>:
                                    </div>
                                    <div class="value">
                                        
                                        <p><?php echo e($data['state']->name); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('District')); ?>:
                                    </div>
                                    <div class="value">
                                        
                                        <p><?php echo e($data['district']->name); ?></p>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="single-meta mt-10">
                                <h3 class="mb-30"><?php echo e(__('Parties & Hearing Aadvocates')); ?> </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('PETITIONER/ADITIONAL')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <?php $__currentLoopData = $data['petitioners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $petitioner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($petitioner->petitioner); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('THEIR ADVOCATES')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <?php $__currentLoopData = $data['petitioners_advocate']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $petitioner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($petitioner->petitioner_advocate); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('RESPONDENT/ADITIONAL')); ?>:
                                    </div>
                                    <div class="value">
                                        <?php $__currentLoopData = $data['respondents']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respondent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($respondent->respondent); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('THEIR ADVOCATES')); ?>:
                                    </div>
                                    <div class="value">
                                        <?php $__currentLoopData = $data['respondents_advocate']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respondent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($respondent->respondent_advocate); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="single-meta mt-10">
                                <h3 class="mb-30"><?php echo e(__('Court Details')); ?> </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Court
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($model->court_bench); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Judge Name')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($model->judge_name); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Court Room No.')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($model->court_room_no); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta mt-10">
                                <h3 class="mb-30"><?php echo e(__('Client Details')); ?> </h3>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Client Name')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['client']->name); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Contact
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['client']->mobile); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Email')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['client']->email); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('Address')); ?>:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['client']->address); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Organization
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['org']->organization_name); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Tags/Refrence
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($data['refs']->name); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Police Station
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($model->police_station); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Remarks
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e($model->remarks); ?></p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Registered at Casewise:
                                    </div>
                                    <div class="value">
                                        <!-- <?php echo e(amountFormat($model->case_charge)); ?> -->
                                        <p><?php echo e(date('d/m/Y', strtotime($model->created_at))); ?> at
                                            <?php echo e(date('h:i', strtotime($model->created_at))); ?> hrs</p>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
    </section>
    <div class="modal fade animated file_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
        aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <?php if(moduleStatusCheck('Finance')): ?>
        <div class="modal fade animated payment_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
            aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        function Delete(id) {
            var flag = confirm("Are you sure you want to delete case?");
            if (flag) {
                $.ajax({
                    type: "GET",
                    url: '<?php echo e(url('case_delete/')); ?>/' + id,
                    success: function(data) {
                        window.location.href = "<?php echo e(env('BASE_URL')); ?>/case/active";
                        swal.fire("success", "Case deleted Successfully", "success")
                    },
                    error: function(error) {
                        Snackbar.show({
                            text: " Somthing Went Wrong ",
                            pos: 'top-right',
                            actionTextColor: '#fff',
                            backgroundColor: '#E7515A'
                        });
                    }
                });
            }
        }


        $(document).ready(function() {
            //set initial state.
            $('#custom_active_checkbox1').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox2').val()) {
                        $('#custom_active_checkbox2').prop('checked', false);
                    }

                }
            });
            $('#custom_active_checkbox2').change(function() {
                if (this.checked) {
                    // var returnVal = confirm("Are you sure?");
                    $(this).prop("checked", true);
                    if ($('#custom_active_checkbox1').val()) {
                        $('#custom_active_checkbox1').prop('checked', false);
                    }

                }
            });
            // $('#cust_case_year').change(function() {
            //     alert(this.value);
            //     date = new Date(this.value);
            //     alert(date);
            //     year = date.getFullYear();
            //     alert(year);
            //     $(this).val(year);
            //     alert($(this).val());
            // });

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Case Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/case/show.blade.php ENDPATH**/ ?>