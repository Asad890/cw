<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                    <li class="nav-item">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-district-court" role="tab" aria-controls="pills-contact" aria-selected="true"><h3 class="mb-0 "><?php echo e(__('Decided Cases')); ?></h3></a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-high-court" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('Abandoned Cases')); ?></h3></a>
                        </li>
                        
                    </ul>

                    <!-- form start =-->
                <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                           
                            <br>
                            <br>
                            <br>
                        <!-- Table -->
                                <div class="col-lg-12">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <div class="">
                                                <table class="table Crm_table_active3">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Sr No.</th>
                                                            <th scope="col">Brief No.</th>
                                                            <th scope="col">Case No.</th>
                                                            <th scope="col">Petitioner</th>
                                                            <th scope="col">Respondent</th>
                                                            <th scope="col">Organization</th>
                                                            <th scope="col">Decided On</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>

                                                            <td>1</td>
                                                            <td>Bussiness</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                                
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Table Close -->
                    </div>
                        <!-- Tab 2 -->
                        <div class="tab-pane fade" id="pills-high-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <!-- Table Starts -->
                                <br>
                                <br>
                                <br>
                                <div class="col-lg-12">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <div class="">
                                                <table class="table Crm_table_active3">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Sr No.</th>
                                                            <th scope="col">Brief No.</th>
                                                            <th scope="col">Case No.</th>
                                                            <th scope="col">Petitioner</th>
                                                            <th scope="col">Respondent</th>
                                                            <th scope="col">Organization</th>
                                                            <th scope="col">Abandoned On</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>

                                                            <td>1</td>
                                                            <td>Bussiness</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                            <td>02-05-2022</td>
                                                                
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Table close -->
                        </div>
                      
                </div>
                    <!-- form end =-->
            </div>
            </div>


        </div>
    </div>
                            

        </section>
<?php $__env->stopSection(); ?>
      



<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/archive/index.blade.php ENDPATH**/ ?>