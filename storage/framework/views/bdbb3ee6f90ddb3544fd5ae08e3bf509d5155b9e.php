
<?php $__env->startSection('title', __('Permission Denied')); ?>
<?php $__env->startSection('code', '403'); ?>

<?php $__env->startSection('content'); ?>
    <div class="max-w-sm m-8">
        <div class="text-black text-5xl md:text-15xl font-black">
            <img src="<?php echo e(asset('public/backEnd/img/403.png')); ?>" alt="" class="img img-fluid"></div>

        <div class="w-16 h-1 bg-purple-light my-3 md:my-6"></div>

        <p class="text-grey-darker text-2xl md:text-3xl font-light mb-8 leading-normal text-white">
            <?php echo e(__($exception->getMessage() ?: 'Permission Denied, you have no permission to access this page !')); ?>

        </p>

        <a href="<?php echo e(url('/')); ?>">
            <button
                class="bg-transparent text-grey-darkest font-bold uppercase tracking-wide py-3 px-6 border-2 border-grey-light hover:border-grey rounded-lg text-white">
                Go Home
            </button>
        </a>
        <a href="<?php echo e(URL::previous()); ?>">
            <button
                class="bg-transparent text-grey-darkest font-bold uppercase tracking-wide py-3 px-6 border-2 border-grey-light hover:border-grey rounded-lg text-white">
                Go Back
            </button>
        </a>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('errors::minimal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\updates 123\cw\resources\views/errors/403.blade.php ENDPATH**/ ?>