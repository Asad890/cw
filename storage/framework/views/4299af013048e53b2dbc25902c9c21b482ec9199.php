<?php $__currentLoopData = $email_templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!$template->module or moduleStatusCheck($template->module)): ?>
        <div class="tab-pane fade white_box_30px" id="<?php echo e($template->type); ?>" role="tabpanel"
             aria-labelledby="<?php echo e($template->type); ?>-tab">

        <?php echo Form::open(['url' => route('template_update'), 'method' => 'post', 'id' => "template_update_{$template->type}", 'files' =>true ]); ?>

        <?php echo csrf_field(); ?>
        <!-- content  -->
            <div class="row">
                <div class="col-xl-8">
                    <div class="primary_input mb-25">
                        <?php echo e(Form::label('subject', __('setting.Subject') , ['class' => 'primary_input_label required'])); ?>

                        <?php echo e(Form::text('subject', $template->subject , ["class" => "primary_input_field", "placeholder" => __('setting.Subject'), "required"])); ?>

                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="primary_input mb-25 mt-40">
                        <label class="primary_checkbox d-flex  align-items-center ">
                            <?php echo e(Form::checkbox('status', 1, $template->status)); ?>

                            <span class="checkmark mr-12"></span>
                            <p><?php echo e(__('common.Active')); ?></p>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="primary_input mb-25">

                        <?php echo e(Form::label('value', __('setting.'.$template->name) , ['class' => 'primary_input_label required'])); ?>


                        <?php echo e(Form::textarea('value', $template->value , ["class" => "summernote3", "placeholder" =>''])); ?>

                    </div>
                </div>
            </div>
            <?php echo e(Form::hidden('name', $template->type)); ?>


            <div class="submit_btn text-center mb-20 pt_15">
                <button class="primary_btn_large submit" type="submit"><i class="ti-check"></i> <?php echo e(__('common.Save')); ?>

                </button>

                <button class="primary_btn_large submitting" type="submit" disabled style="display: none;"><i
                        class="ti-check"></i> <?php echo e(__('common.Saving')); ?></button>
            </div>
            <!-- content  -->
            <?php echo Form::close(); ?>


            <div class="row mb-80">
                <div class="col-xl-12">
                    <strong><?php echo e(__('setting.Available variables')); ?>:</strong>
                    <p><?php echo e($template->available_variable); ?></p>
                </div>
            </div>
        </div>

        <?php $__env->startPush('js_after'); ?>

            <script>
                _formValidation2('<?php echo e("template_update_{$template->type}"); ?>');
            </script>

        <?php $__env->stopPush(); ?>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Setting/Resources/views/page_components/email_template.blade.php ENDPATH**/ ?>