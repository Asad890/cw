<?php $__env->startSection('mainContent'); ?>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="box_header">
                    <div class="main-title d-flex justify-content-between w-100">
                        <h3 class="mb-0 mr-30"><?php echo e(__('client.New Client Category')); ?></h3>

                    </div>
                </div>
            </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">
                    <?php echo Form::open(['route' => 'category.client.store', 'class' => 'form-validate-jquery', 'id' =>
                    'content_form', 'files' => false, 'method' => 'POST']); ?>

                    <div class="primary_input">
                        <?php echo e(Form::label('name', __('client.Name'),['class' => 'required'])); ?>

                        <?php echo e(Form::text('name', null, ['required' => '', 'class' => 'primary_input_field', 'placeholder' => __('client.Client Category Name')])); ?>

                    </div>

                    <div class="primary_input">
                        <?php echo e(Form::label('description', __('client.Description'))); ?>

                        <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field', 'placeholder' =>  __('client.Client Category Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' => '#description_error' ])); ?>

                        <span id="description_error"></span>
                    </div>

                    <div class="form-check mt-3">
                        <label class="switch_toggle" for="checkbox">
                            <input type="checkbox" id="checkbox" name="plaintiff">
                            <div class="slider round"></div>

                        </label>

                    <?php echo e(__('client.On behalf of Plaintiff')); ?>

                    </div>

                    <div class="text-center">
                    <button class="primary_btn_large submit" type="submit"><i class="ti-check"></i><?php echo e(__('common.Create')); ?>

                    </button>

                    <button class="primary_btn_large submitting" type="submit" disabled style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                    </button>
                    </div>
                    <?php echo Form::close(); ?>



                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function () {
            _formValidation();

        });
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', ['title' => __('client.Create New Client Category')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/category/client/create.blade.php ENDPATH**/ ?>