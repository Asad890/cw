

<?php $__env->startSection('mainContent'); ?>
<div class="container-fluid">
        <?php if(permissionCheck('dashboard_quick_summery.index')): ?>

            <div class="row">
            <div class="col-lg">
                <div class="main-title">
                    <h3 class="mb-0">Dashboard</h3>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="main-title">
                    <h3><a href="<?php echo e(route('case.create')); ?>" class="primary-btn small fix-gr-bg">
                                <span class="ti-plus pr-2"></span>New Case
                            </a></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <!--div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('client.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Client')); ?> </h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Client')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(App\Models\Client::all()->count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div-->
            <!--div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('lawyer.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Lawyer')); ?></h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Lawyer')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(App\Models\Lawyer::all()->count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div-->

            <!--div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('contact.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Contact')); ?></h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Contact')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(App\Models\Contact::all()->count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div-->

            <div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('Active Cases')); ?></h3>
                                <!-- <p class="mb-0"><?php echo e(__('Active Cases')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>
			
			<div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3>Today's Cases</h3>
                                <!-- <p class="mb-0"><?php echo e(__('Todays Cases')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>
			
			<div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3>Tomorrow's Cases</h3>
                                <!-- <p class="mb-0"><?php echo e(__('Tomorrows Cases')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>
			
			
            <div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index', ['status' => 'Waiting'])); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('Date Awaited Cases')); ?></h3>
                                <!-- <p class="mb-0"><?php echo e(__('Date Awaited Cases')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('hearing_date', '<', date('Y-m-d'))->where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>
			
			<div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index', ['status' => 'Waiting'])); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('Daily Board')); ?></h3>
                                <!-- <p class="mb-0"><?php echo e(__('Daily Board')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('hearing_date', '<', date('Y-m-d'))->where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>

			<div class="col-lg-4 col-md-6">
                <a href="<?php echo e(route('case.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('Decided Cases')); ?></h3>
                                <!-- <p class="mb-0"><?php echo e(__('Active Cases')); ?></p> -->
                            </div>
                            <!-- <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('status', 'Open')->where('judgement_status', '=', 'Open')->count()); ?>

                            </h1> -->
                            <p><b>District and Tribunals: 5</b></p>
                            <p><b>High  Court: 10</b></p>
                        </div>
                    </div>
                </a>
            </div>
            
            <!--div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('case.index', ['status' => 'Archieved'])); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Closed Cases')); ?></h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Closed Cases')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(App\Models\Cases::where('judgement_status', 'Close')->count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('staffs.index')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Staff')); ?></h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Staff')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(App\Staff::count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div-->



            <!--div class="col-lg-3 col-md-6">
                <a href="<?php echo e(route('my-task')); ?>" class="d-block">
                    <div class="white-box single-summery">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h3><?php echo e(__('dashboard.Pending Task')); ?></h3>
                                <p class="mb-0"><?php echo e(__('dashboard.Total Pending task')); ?></p>
                            </div>
                            <h1 class="gradient-color2"><?php echo e(Modules\Task\Entities\Task::where('status', 0)->count()); ?>

                            </h1>
                        </div>
                    </div>
                </a>
            </div-->


        </div>

        <?php endif; ?>


        <div class="row mt-40">
         <?php if(permissionCheck('dashboad_calender.index')): ?>
                <div class="col-lg-<?php echo e(permissionCheck('dashboad_calender.index') ? 7 : 12); ?>">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="main-title">
                            <h3 class="mb-20"><?php echo e(__('common.Calendar')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="white-box">
                    <div class='common-calendar'>
                    </div>
                </div>
            </div>
            <?php endif; ?>
 <?php if(permissionCheck('dashboard_todo.index')): ?>
                 <div class="col-<?php echo e(permissionCheck('dashboard_todo.index') ? 5 : 6); ?>">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="main-title">
                                <h3 class="mb-25"><?php echo app('translator')->get('Worklist'); ?></h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 text-right">
                            <a href="#" data-toggle="modal" class="primary-btn small fix-gr-bg"
                               data-target="#add_to_do"
                               title="Add To Do" data-modal-size="modal-md">
                                <span class="ti-plus pr-2"></span>
                                <?php echo app('translator')->get('Add Worklist'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box school-table">
                                  <!-- Category -->
                                  <div class="row form-group">
            
            <div class="primary_input col-md-5">
               <select class="primary_input_field" id="sel1">
                <option>Select Category</option>
                <option>Category 1</option>
                <option>Category 2</option>
                <option>Category 3</option>
               </select>
            </div>
            <div class="primary_input col-md-3">
            <a href="#" data-toggle="modal" class="primary-btn medium fix-gr-bg"
       data-target="#"
       title="Add To Do" data-modal-size="modal-md">
        <?php echo app('translator')->get('Search'); ?>
          </a>
            </div>
            <div class="primary_input col-md-4">
            <a href="" class="primary-btn medium fix-gr-bg" data-toggle="modal" data-target="#categoryModal" data-whatever="">
            <!-- <span class="ti-plus pr-2"></span> -->
               + Add</a>
            </div>
         </div>
        <!-- /category -->
                                <div class="row to-do-list mb-20">
                                    <div class="col-md-6 col-6">
                                        <button class="primary-btn small fix-gr-bg"
                                                id="toDoList"><?php echo app('translator')->get('todo.Incomplete'); ?></button>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <button class="primary-btn small tr-bg"
                                                id="toDoListsCompleted"><?php echo app('translator')->get('todo.Completed'); ?></button>
                                    </div>
                                </div>

                                <input type="hidden" id="url" value="<?php echo e(url('/')); ?>">


                                <div class="toDoList">
                                    <?php if(count(@$toDos->where('status',0)) > 0): ?>

                                        <?php $__currentLoopData = $toDos->where('status',0); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $toDoList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="single-to-do d-flex justify-content-between toDoList"
                                                 id="to_do_list_div<?php echo e(@$toDoList->id); ?>">
                                                <div>
                                                    <input type="checkbox" id="midterm<?php echo e(@$toDoList->id); ?>"
                                                           class="common-checkbox complete_task" name="complete_task"
                                                           value="<?php echo e(@$toDoList->id); ?>">

                                                    <label for="midterm<?php echo e(@$toDoList->id); ?>">

                                                        <input type="hidden" id="id" value="<?php echo e(@$toDoList->id); ?>">
                                                        <input type="hidden" id="url" value="<?php echo e(url('/')); ?>">
                                                        <h5 class="d-inline"><?php echo e(@$toDoList->title); ?></h5>
                                                        <p class="ml-35">

                                                            <?php echo e($toDoList->date); ?>


                                                        </p>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <div class="single-to-do d-flex justify-content-between">
                                            <?php echo app('translator')->get('todo.no_do_lists_assigned_yet'); ?>
                                        </div>

                                    <?php endif; ?>
                                </div>
                                <input type="checkbox">

                                <div class="toDoListsCompleted">
                                    <?php if(count(@$toDos->where('status',1))>0): ?>

                                        <?php $__currentLoopData = $toDos->where('status',1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $toDoListsCompleted): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <div class="single-to-do d-flex justify-content-between"
                                                 id="to_do_list_div<?php echo e(@$toDoListsCompleted->id); ?>">
                                                <div>
                                                    <h5 class="d-inline"><?php echo e(@$toDoListsCompleted->title); ?></h5>
                                                    <p class="">

                                                        <?php echo e(@$toDoListsCompleted->date); ?>


                                                    </p>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <div class="single-to-do d-flex justify-content-between">
                                            <?php echo app('translator')->get('todo.no_do_lists_assigned_yet'); ?>
                                        </div>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

<?php endif; ?>
        </div>


        <div class="row mt-40">

            <?php if(permissionCheck('dashboard_appointment.index')): ?>
 <div class="col-lg-6 col-md-6">

                    <div class="white_box_30px mb_30">
                        <div class="box_header common_table_header ">
                            <div class="main-title d-md-flex">
                                <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('dashboard.Appointment')); ?></h3>
                            </div>
                        </div>


                        <div class="QA_section3 QA_section_heading_custom th_padding_l0 ">
                            <div class="QA_table">
                                <!-- table-responsive -->
                                <div class="table-responsive">
                                <table class="table pt-0 shadow_none pb-0 ">
                                        <tbody>
                                            <tr>
                                                <th><?php echo e(__('dashboard.Title')); ?></th>
                                                <th><?php echo e(__('dashboard.Date')); ?></th>
                                                <th><?php echo e(__('dashboard.Contact')); ?></th>
                                            </tr>

                                            <?php $__currentLoopData = $appointments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $appointment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <tr>
                                                <td><a href="<?php echo e(route('appointment.show', $appointment->id)); ?>"><?php echo e($appointment->title); ?></a></td>
                                                <td><?php echo e(formatDate($appointment->date)); ?></td>
                                                <td><?php echo e($appointment->contact->name); ?></td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endif; ?>



<?php if(permissionCheck('dashboard_appointment.index')): ?>
                <div class="col-lg-6 col-md-6">

                    <div class="white_box_30px mb_30">
                        <div class="box_header common_table_header ">
                            <div class="main-title d-md-flex">
                                <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('dashboard.Upcomming Date')); ?></h3>
                            </div>
                        </div>


                        <div class="QA_section3 QA_section_heading_custom th_padding_l0 ">
                            <div class="QA_table">
                                <!-- table-responsive -->
                                <div class="table-responsive">
                                <table class="table pt-0 shadow_none pb-0 ">
                                        <tbody>
                                        <tr>
                                            <th><?php echo e(__('dashboard.Case Name')); ?></th>
                                            <th><?php echo e(__('dashboard.Date')); ?></th>
                                        </tr>
                                        <?php $__currentLoopData = $upcommingdate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <tr>
                                                <td><a href="<?php echo e(route('case.show', $date->id)); ?>"><?php echo e($date->title); ?></a></td>
                                                <td><?php echo e(formatDate($date->date)); ?></td>
                                            </tr>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>



        </div>


      

</div>
<!-- Add Modal -->
            <div class="modal fade admin-query" id="add_to_do">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo e(trans('todo.Add To Do')); ?></h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()'])); ?>


                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Category')); ?></label>
                                                    <select class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Title')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('common.Title')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Description')); ?>*</label>
                                                    <textarea  class="primary_textarea"
                                                           placeholder="<?php echo e(__('common.Description')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('common.Date')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('common.Date')); ?>"
                                                                   class="primary_input_field primary-input date form-control"
                                                                   id="startDate" type="text" name="date"
                                                                   value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
											
											<div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('Time')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('Time')); ?>"
                                                                   class="primary_input_field primary-input time form-control"
                                                                   id="startTime" type="text" name="time"
                                                                   value="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="ti-alarm-clock"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- Repeat -->
                                            <div class="col-md-6 col-lg-6 col-sm-6 text-left">
                                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Small modal</button> -->
                                            <a href="" class="text-left" data-toggle="modal" data-target="#exampleModal" data-whatever="">Repeat</a>
                                            </div>
                                            <!-- Close Repeat -->
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Case Link')); ?></label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Case Link')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 text-center">
                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo e(Form::close()); ?>

                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>

<!-- Repeat Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Repeat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <!-- day + week -->
            <div class="row form-group">
            <div class="primary_input col-md-4">
                                    <label>Repeats Every</label>
            </div>
            
        </div>
        <div class="row form-group">
            <div class="primary_input col-md-4">

                <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
            </div>
            <div class="primary_input col-md-8">
                <select class="primary_input_field" id="sel1">
                    <option>day</option>
                    <option>week</option>
                    <option>month</option>
                    <option>year</option>
                </select>
            </div>

        </div>
        <!-- close day + week -->
        <!-- week fields Drop down -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <select class="primary_input_field" id="sel1">
                    <option>Monday</option>
                    <option>Tuesday</option>
                    <option>Wednesday</option>
                    <option>Thursday</option>
                    <option>Friday</option>
                    <option>Saturday</option>
                    <option>Sunday</option>
                </select>
            </div>
        </div>
        <!-- close week fields -->
        <!-- days -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num"  checked>
            </div>
            <div class="primary_input col-md-10">
                <select class="primary_input_field" id="sel1">
                    <option>Day 1</option>
                    <option>Day 2</option>
                    <option>Day 3</option>
                    <option>Day 4</option>
                    <option>Day 5</option>
                    <option>Day 6</option>
                    <option>Day 7</option>
                    <option>Day 8</option>
                    <option>Day 9</option>
                    <option>Day 10</option>
                    <option>Day 11</option>
                    <option>Day 12</option>
                    <option>Day 13</option>
                    <option>Day 14</option>
                    <option>Day 15</option>
                    <option>Day 16</option>
                    <option>Day 17</option>
                    <option>Day 18</option>
                    <option>Day 19</option>
                    <option>Day 20</option>
                    <option>Day 21</option>
                    <option>Day 22</option>
                    <option>Day 23</option>
                    <option>Day 24</option>
                    <option>Day 25</option>
                    <option>Day 26</option>
                    <option>Day 27</option>
                    <option>Day 28</option>
                    <option>Day 29</option>
                    <option>Day 30</option>
                    <option>Day 31</option>
                    <option>Last Day</option>
                </select>
            </div>

        </div>
        <!-- close days -->
        <!-- First or days -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num">
            </div>
            <div class="primary_input col-md-4">
                <select class="primary_input_field" id="sel1">
                    <option>First</option>
                    <option>Second</option>
                    <option>Third</option>
                    <option>Fourth</option>
                    <option>Last</option>
                </select>
            </div>
            <div class="primary_input col-md-6">
                <select class="primary_input_field" id="sel1">
                <option>Monday</option>
                    <option>Tuesday</option>
                    <option>Wednesday</option>
                    <option>Thursday</option>
                    <option>Friday</option>
                    <option>Saturday</option>
                    <option>Sunday</option>
                </select>
            </div>

        </div>
        <!-- close first or days -->
        <!-- Set Time -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <select class="primary_input_field" id="sel1">
                    <option value="">Set ime</option>
                    <option>00:00</option>
                    <option>00:30</option>
                    <option>01:00</option>
                    <option>01:30</option>
                    <option>02:00</option>
                    <option>02:30</option>
                    <option>03:00</option>
                    <option>03:30</option>
                    <option>04:00</option>
                    <option>04:30</option>
                    <option>05:00</option>
                    <option>05:30</option>
                    <option>06:00</option>
                    <option>06:30</option>
                    <option>07:00</option>
                    <option>07:30</option>
                    <option>08:00</option>
                    <option>08:30</option>
                    <option>09:00</option>
                    <option>09:30</option>
                    <option>10:00</option>
                    <option>10:30</option>
                    <option>11:00</option>
                    <option>11:30</option>
                    <option>12:00</option>
                    <option>12:30</option>
                    <option>13:00</option>
                    <option>13:30</option>
                    <option>14:00</option>
                    <option>14:30</option>
                    <option>15:00</option>
                    <option>15:30</option>
                    <option>16:00</option>
                    <option>16:30</option>
                    <option>17:00</option>
                    <option>17:30</option>
                    <option>18:00</option>
                    <option>18:30</option>
                    <option>19:00</option>
                    <option>19:30</option>
                    <option>20:00</option>
                    <option>20:30</option>
                    <option>21:00</option>
                    <option>21:30</option>
                    <option>22:00</option>
                    <option>22:30</option>
                    <option>23:00</option>
                    <option>23:30</option>
                </select>
            </div>

        </div>
        <!-- Close set time -->
        <!-- Starts -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <label for="starts">Starts</label>
                <select class="primary_input_field" id="sel1">
                    <option>Augest</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                    <option>Jan 2023</option>
                    <option>Feb 2023</option>
                    <option>Mar 2023</option>
                    <option>Apr 2023</option>
                    <option>May 2023</option>
                    <option>Jun 2023</option>
                    <option>Jul 2023</option>
                </select>
            </div>
        </div>
        <!-- close starts -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" class="larger" name="day_num" checked>
            
            </div>
            <div class="primary_input col-md-10">
                 <label>Never</label>
            </div>
        </div>
        <!-- Never close -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num" checked>
            </div>
            <div class="primary_input col-md-2">
                 <label>On</label>
            </div>
            <div class="primary_input col-md-8">
                 <input type="text" placeholder="date" class="primary_input_field primary-input form-control datetime" >
                 <!-- <?php echo e(Form::text('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field form-control datetime', "id"=>"previous_date",'placeholder' => __('case.PREVIOUS DATE')])); ?> -->
            </div>
        </div>
        <!-- Never close -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num" checked>
            </div>
            <div class="primary_input col-md-2">
                 <label>After</label>
            </div>
            <div class="primary_input col-md-3">
            <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
            </div>
            <div class="primary_input col-md-5">
            <input type="text" name="day_num" class="primary_input_field" value="occurrence">
            </div>
        </div>
        <!-- Never close -->
        </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>

 <!-- Add Category -->
 <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <!-- Category -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <input type="text" name="category" class="primary_input_field" placeholder="Add Category" >
                   
            </div>
        </div>
        <!-- Category -->
        </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
                        <!-- /Add Category -->
<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('public/backEnd/vendors/js/fullcalendar.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/backEnd/vendors/js/fullcallendar-locale-all.js')); ?>"></script>
    <script>

        setTimeout(function () {
            $('#yearly').fadeOut(500);
            $('#weekly_profit').fadeOut(500);
            $('#monthly_profit').fadeOut(500);
            $('#yearly_profit').fadeOut(500);
            $('.purchase_table').fadeOut(500);
        }, 1000);

        if ($('.common-calendar').length) {
            $('.common-calendar').fullCalendar({
                locale: LANG,
                rtl : RTL,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(event.title);
                    $('#modalBody').html(event.description);

                    $('#image').attr('src', event.url);
                    $('#fullCalModal').modal();
                    return false;
                },
                height: 650,
                events: <?php echo json_encode($calendar_events);?> ,
            });
        }

        $(document).on('click', '.filtering', function () {
            $('.filtering').removeClass('active');
            $(this).addClass('active');
            let type = $(this).data('type');
            $('.gradient-color2').hide();
            $('.demo_wait').show();
            $.ajax({
                method: "POST",
                url: "<?php echo e(url('dashboard-cards-info')); ?>" + "/" + type,
                success: function (data) {
                    $('.total_purchase').text(data.purchase_amount);
                    $('.total_sale').text(data.sale_amount);
                    $('.expenses').text(data.expense);
                    $('.purchase_due').text(data.purchase_due);
                    $('.invoice_due').text(data.sale_due);
                    $('.total_bank').text(data.bank);
                    $('.total_cash').text(data.cash);
                    $('.total_income').text(data.income);
                    $('.gradient-color2').show();
                    $('.demo_wait').hide();
                }
            })
        });





$(".complete_task").on("click", function() {
        var url = $("#url").val();
        var id = $(this).val();
        var formData = {
            id: $(this).val(),
        };

        // get section for student
        $.ajax({
            type: "GET",
            data: formData,
            dataType: "json",
            url: url + "/" + "complete-to-do",
            success: function(data) {


                setTimeout(function() {
                    toastr.success(
                        "Operation Success!",
                        "Success Alert", {
                            iconClass: "customer-info",
                        }, {
                            timeOut: 2000,
                        }
                    );
                }, 500);

                $("#to_do_list_div" + id + "").remove();

                $("#toDoListsCompleted").children("div").remove();
            },
            error: function(data) {

            },
        });
    });


    $(document).ready(function() {
        $(".toDoListsCompleted").hide();
    });

        $("#toDoList").on("click", function(e) {
            e.preventDefault();

            if ($(this).hasClass("tr-bg")) {
                $(this).removeClass("tr-bg");
                $(this).addClass("fix-gr-bg");
            }

            if ($("#toDoListsCompleted").hasClass("fix-gr-bg")) {
                $("#toDoListsCompleted").removeClass("fix-gr-bg");
                $("#toDoListsCompleted").addClass("tr-bg");
            }

            $(".toDoList").show();
            $(".toDoListsCompleted").hide();
        });


        $("#toDoListsCompleted").on("click", function(e) {
            e.preventDefault();

            if ($(this).hasClass("tr-bg")) {
                $(this).removeClass("tr-bg");
                $(this).addClass("fix-gr-bg");
            }

            if ($("#toDoList").hasClass("fix-gr-bg")) {
                $("#toDoList").removeClass("fix-gr-bg");
                $("#toDoList").addClass("tr-bg");
            }

            $(".toDoList").hide();
            $(".toDoListsCompleted").show();

            var formData = {
                id: 0,
            };

            var url = $("#url").val();

            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "get-to-do-list",
                success: function(data) {
                    $(".toDoListsCompleted").empty();

                    $.each(data, function(i, value) {
                        var appendRow = "";

                        appendRow +=
                            "<div class='single-to-do d-flex justify-content-between'>";
                        appendRow += "<div>";
                        appendRow += "<h5 class='d-inline'>" + value.title + "</h5>";
                        appendRow += "<p>" + value.date + "</p>";
                        appendRow += "</div>";
                        appendRow += "</div>";

                        $(".toDoListsCompleted").append(appendRow);
                    });
                },
                error: function(data) {

                },
            });
        });

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/home.blade.php ENDPATH**/ ?>