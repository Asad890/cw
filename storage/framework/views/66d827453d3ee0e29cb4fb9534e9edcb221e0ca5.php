<?php $__env->startSection('mainContent'); ?>
    <section class="mb-40 student-details">
        <?php if(session()->has('message-success')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('message-success')); ?>

            </div>
        <?php elseif(session()->has('message-danger')): ?>
            <div class="alert alert-danger">
                <?php echo e(session()->get('message-danger')); ?>

            </div>
        <?php endif; ?>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('common.Change Password')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="white_box_50px box_shadow_white">
                        <?php echo Form::open(['route' => 'change_password']); ?>

                        <div class="row form">
                            <div class="col-xl-4">
                                <div class="primary_input mb-25">
                                    <label class="primary_input_label" for="current_password"><?php echo e(__('common.Current Password')); ?> * </label>
                                    <input name="current_password" class="primary_input_field name"
                                           placeholder="<?php echo e(__('common.Current Password')); ?>" type="password"  id="current_password" required>
                                    <?php if($errors->has('current_password')): ?>
                                        <span class="text-danger" role="alert">
                                            <strong><?php echo e($errors->first('current_password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="primary_input mb-25">
                                    <label class="primary_input_label" for=""><?php echo e(__('common.Password')); ?> (<?php echo e(trans('Minimum 8 Letter')); ?>) * </label>
                                    <input name="password" class="primary_input_field name"
                                           placeholder="<?php echo e(__('common.Password')); ?>" type="password" minlength="8" required>
                                    <?php if($errors->has('password')): ?>
                                        <span class="text-danger" role="alert">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="primary_input mb-25">
                                    <label class="primary_input_label" for=""><?php echo e(__('common.Re-Password')); ?> (<?php echo e(trans('Minimum 8 Letter')); ?>) *</label>
                                    <input name="password_confirmation" class="primary_input_field name"
                                           placeholder="<?php echo e(__('common.Re-Password')); ?>" type="password" minlength="6" required>
                                    <?php if($errors->has('password_confirmation')): ?>
                                        <span class="text-danger" role="alert">
                                            <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-12 mt-2">
                                <div class="submit_btn text-center ">
                                    <button class="primary-btn semi_large2 fix-gr-bg" type="submit"><i
                                            class="ti-check"></i><?php echo e(__('common.Change Password')); ?>

                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>


            </div>
        </div>
    </section>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/backEnd/profiles/password.blade.php ENDPATH**/ ?>