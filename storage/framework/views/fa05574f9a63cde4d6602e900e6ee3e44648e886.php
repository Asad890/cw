

<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                    <li class="nav-item">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-district-court" role="tab" aria-controls="pills-contact" aria-selected="true"><h3 class="mb-0 "><?php echo e(__('Bare Acts')); ?></h3></a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-high-court" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('Proforma')); ?></h3></a>
                        </li>
                        
                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                <div class="row form-group">
                                    <div class="primary_input col-md-9">
                                            <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Search">
                                    </div>
                                    <div class="primary_input col-md-3">
                                    <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <!-- form -->
                        <div class="col-lg-12">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <div class="">
                                                <table class="table Crm_table_active3" style="width:100%">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Sr No.</th>
                                                            <th scope="col" style="width:90%">Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>

                                                            <td>1</td>
                                                            <td>A</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>2</td>
                                                            <td>B</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>3</td>
                                                            <td>C</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>4</td>
                                                            <td>D</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>5</td>
                                                            <td>E</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>6</td>
                                                            <td>F</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>7</td>
                                                            <td>G</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>8</td>
                                                            <td>H</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>I</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>J</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>K</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>10</td>
                                                            <td>L</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>11</td>
                                                            <td>M</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>12</td>
                                                            <td>N</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>13</td>
                                                            <td>O</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>14</td>
                                                            <td>P</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>15</td>
                                                            <td>Q</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>16</td>
                                                            <td>R</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>17</td>
                                                            <td>S</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>18</td>
                                                            <td>T</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>19</td>
                                                            <td>U</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>20</td>
                                                            <td>V</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>21</td>
                                                            <td>W</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>22</td>
                                                            <td>X</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>23</td>
                                                            <td>Y</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>24</td>
                                                            <td>Z</td>
                                                                
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- close -->
        </div>
                        <!-- /form -->
                    </div>
                        <!-- Tab 2 -->
                        <div class="tab-pane fade" id="pills-high-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form -->
                                <div class="row form-group">
                                    <div class="primary_input col-md-9">
                                            <input type="text" class="primary_input_field primary-input form-control datetime" placeholder="Search">
                                    </div>
                                    <div class="primary_input col-md-3">
                                    <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('common.Search')); ?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <!-- form -->
                        <div class="col-lg-12">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <div class="">
                                                <table class="table Crm_table_active3" style="width:100%">
                                                    <thead>
                                                        <tr>

                                                            <th scope="col">Sr No.</th>
                                                            <th scope="col" style="width:90%">Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>

                                                            <td>1</td>
                                                            <td>A</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>2</td>
                                                            <td>B</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>3</td>
                                                            <td>C</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>4</td>
                                                            <td>D</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>5</td>
                                                            <td>E</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>6</td>
                                                            <td>F</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>7</td>
                                                            <td>G</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>8</td>
                                                            <td>H</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>I</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>J</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>9</td>
                                                            <td>K</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>10</td>
                                                            <td>L</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>11</td>
                                                            <td>M</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>12</td>
                                                            <td>N</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>13</td>
                                                            <td>O</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>14</td>
                                                            <td>P</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>15</td>
                                                            <td>Q</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>16</td>
                                                            <td>R</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>17</td>
                                                            <td>S</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>18</td>
                                                            <td>T</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>19</td>
                                                            <td>U</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>20</td>
                                                            <td>V</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>21</td>
                                                            <td>W</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>22</td>
                                                            <td>X</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>23</td>
                                                            <td>Y</td>
                                                                
                                                        </tr>
                                                        <tr>

                                                            <td>24</td>
                                                            <td>Z</td>
                                                                
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- close -->
        </div>
                 </div>
                    <!-- form end =-->
                </div>
                <!-- end -->
            </div>


        </div>


        </section>
<?php $__env->stopSection(); ?>
      



<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/bareacts/index.blade.php ENDPATH**/ ?>