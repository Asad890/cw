<?php if(permissionCheck('setting.index')): ?>
    <?php
        $lang = ['languages.index', 'languages.edit', 'languages.show', 'languages.create' , 'language.translate_view'];
        $nav = array_merge(['setting', 'modulemanager.index'],  ['setting.updatesystem'])
    ?>

    <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
        <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
            <div class="nav_icon_small">

                <span class="fa fa-cog"></span>
            </div>
            <div class="nav_title">
                <span><?php echo e(__('setting.Settings')); ?></span>
            </div>
        </a>
        <ul>
            <li>
                <a href="<?php echo e(url('setting')); ?>"
                   class="<?php echo e(spn_active_link('setting', 'active')); ?>">  <?php echo e(__('setting.General Settings')); ?></a>
            </li>
            <?php if(permissionCheck('modulemanager.index')): ?>
                <li>
                    <a href="<?php echo e(route('modulemanager.index')); ?>"
                       class="<?php echo e(spn_active_link('modulemanager.index', 'active')); ?>"><?php echo e(__('common.Module Manager')); ?></a>
                </li>
            <?php endif; ?>

            <?php if(permissionCheck('languages.index')): ?>
                <li>
                    <a href="<?php echo e(route('languages.index')); ?>"
                       class="<?php echo e(spn_active_link($lang, 'active')); ?>"><?php echo e(__('common.Language')); ?></a>
                </li>
            <?php endif; ?>

            <?php if(permissionCheck('setting.updatesystem')): ?>
                <li>
                    <a href="<?php echo e(route('setting.updatesystem')); ?>"
                       class="<?php echo e(spn_active_link('setting.updatesystem', 'active')); ?>"><?php echo e(__('setting.Update')); ?></a>
                </li>
            <?php endif; ?>

        </ul>
    </li>


<?php endif; ?>
<?php if(permissionCheck('utilities')): ?>
    <li class="<?php echo e(spn_active_link('utilities', 'mm-active')); ?>">
        <a href="<?php echo e(route('utilities')); ?>">
            <div class="nav_icon_small">
                <span class="fas fa-store"></span>
            </div>
            <div class="nav_title">
                <span><?php echo e(__('setting.Utilities')); ?></span>
            </div>
        </a>
    </li>
<?php endif; ?>


<?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\Modules/Setting\Resources/views/menu.blade.php ENDPATH**/ ?>