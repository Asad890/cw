<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('court.Court Details')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <table>
                            <tbody>
                            <tr>
                                <td class="p-2"><?php echo e(__('court.Category Name')); ?> </td>
                                <td>:</td>
                                <td>
                                    <?php if($model->court_category): ?>
                                        <a href="<?php echo e(route('category.court.show', $model->court_category->id)); ?>"> <?php echo e($model->court_category->name); ?> </a>
                                    <?php else: ?>
                                        <span class="badge_4"><?php echo e(__('court.No Category')); ?></span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-2"><?php echo e(__('court.Name')); ?> </td>
                                <td>:</td>
                                <td><?php echo e($model->name); ?></td>
                            </tr>

                            <tr>
                                <td class="p-2"><?php echo e(__('court.Room No')); ?> </td>
                                <td>:</td>
                                <td><?php echo e($model->room_number ? $model->room_number : ''); ?></td>
                            </tr>

                            <tr>
                                <td class="p-2"><?php echo e(__('court.Location')); ?> </td>
                                <td>:</td>
                                <td>
                                    <?php echo e($model->state ? $model->state->name .', ' : ''); ?>

                                    <?php echo e($model->city ? $model->city->name .', ' : ''); ?>

                                    <?php echo e($model->location); ?>

                                </td>
                            </tr>

                            <?php if(moduleStatusCheck('EmailtoCL')): ?>
                                <tr>
                                    <td class="p-2"><?php echo e(__('case.Email')); ?> </td>
                                    <td>:</td>
                                    <td>
                                        <?php echo e($model->email); ?>

                                    </td>
                                </tr>
                            <?php endif; ?>

                            <tr>
                                <td class="p-2"><?php echo e(__('court.Description')); ?> </td>
                                <td>:</td>
                                <td><?php echo $model->description; ?></td>
                            </tr>
                            <?php if(moduleStatusCheck('CustomField') and $model->customFields): ?>
                                <?php if ($__env->exists('customfield::details.show', ['customFields' => $model->customFields, 'file' => 'tr'])) echo $__env->make('customfield::details.show', ['customFields' => $model->customFields, 'file' => 'tr'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php endif; ?>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('court.Court')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/master/court/show.blade.php ENDPATH**/ ?>