<?php if($files): ?>
    <?php $__currentLoopData = $files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="single-meta">
            <div class="d-flex align-items-center">
                <div class="name btn-modal flex-grow-1  btn-modal" data-container="file_modal" data-href="<?php echo e(route('file.show', $file->uuid)); ?>" style="cursor: pointer;">
                    <?php echo e($loop->index + 1); ?>. <?php echo e($file->user_filename); ?>

                </div>

                <div class="value mt-1">
                    <?php if(permissionCheck($type.'.edit')): ?>
                        <span  class="primary-btn small fix-gr-bg icon-only  btn-modal" data-container="file_modal" data-href="<?php echo e(route('file.edit', $file->uuid)); ?>" style="cursor: pointer;"><i class="ti-pencil"></i></span>
                    <?php endif; ?>
                    <?php if(permissionCheck($type.'.destroy')): ?>
                        <span style="cursor: pointer;"
                              data-url="<?php echo e(route('file.destroy', $file->uuid)); ?>" id="delete_item" class="primary-btn small fix-gr-bg icon-only"><i class="ti-trash"></i></span>
                    <?php endif; ?>
                </div>

            </div>

        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/file_show.blade.php ENDPATH**/ ?>