<?php $__env->startSection('mainContent'); ?>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="box_header common_table_header">
                    <div class="main-title d-md-flex">
                        <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('payroll.Payroll')); ?></h3>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="white_box_50px box_shadow_white">
                    <form class="" action="<?php echo e(route('staff_search_for_payroll')); ?>" method="GET">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="primary_input mb-15">
                                    <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Role')); ?></label>
                                    <select class="primary_select mb-15" name="role_id" id="role_id">
                                        <option selected disabled><?php echo e(__('payroll.Choose One')); ?></option>
                                        <?php $__currentLoopData = \Modules\RolePermission\Entities\Role::where('type', 'regular_user')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($r)): ?>
                                                <option value="<?php echo e($role->id); ?>"<?php if($r == $role->id): ?> selected <?php endif; ?>><?php echo e($role->name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="text-danger"><?php echo e($errors->first('role_id')); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="primary_input mb-15">
                                    <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Month')); ?></label>
                                    <select class="primary_select mb-15" name="month" id="month">
                                        <?php $__currentLoopData = $months; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $month): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($m)): ?>
                                                <option value="<?php echo e($month); ?>"<?php if($m == $month): ?> selected <?php endif; ?>><?php echo e(__('common.'.$month)); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($month); ?>" <?php echo e(date('F') == $month ? 'selected' : ''); ?> ><?php echo e(__('common.'.$month)); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="text-danger"><?php echo e($errors->first('month')); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="primary_input mb-15">
                                    <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Year')); ?></label>
                                    <select class="primary_select mb-15" name="year" id="year">
                                        <?php $__currentLoopData = range(\carbon\Carbon::now()->year, 2015); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($y)): ?>
                                                <option value="<?php echo e($year); ?>"<?php if($y == $year): ?> selected <?php endif; ?>><?php echo e($year); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($year); ?>"><?php echo e($year); ?></option>
                                            <?php endif; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>'
                                    </select>
                                    <span class="text-danger"><?php echo e($errors->first('year')); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 mb-2 mt-3 text-right">
                                
                                    <button type="submit" class="primary-btn btn-sm fix-gr-bg"id="save_button_parent"><i class="ti-search"></i><?php echo e(__('payroll.Search')); ?></button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php if(isset($users)): ?>
                <div class="col-lg-12 mt-75 pt-75">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table payroll">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                        <tr>
                                            <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                            <th scope="col"><?php echo e(__('staff.Staff')); ?></th>
                                            <th scope="col"><?php echo e(__('role.Role')); ?></th>
                                            <th scope="col"><?php echo e(__('payroll.Phone')); ?></th>
                                            <th scope="col"><?php echo e(__('payroll.Basic Salary')); ?></th>
                                            <th scope="col"><?php echo e(__('common.Status')); ?></th>
                                            <th scope="col"><?php echo e(__('common.Action')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($user->staff): ?>
                                                <tr>
                                                    <td><?php echo e($key + 1); ?></td>
                                                    <td><?php echo e($user->name); ?></td>

                                                    <td>
                                                        <?php if($user->role): ?>
                                                            <?php echo e(@$user->role->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($user->staff): ?>
                                                            <?php echo e(@$user->staff->phone); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($user->staff): ?>
                                                            <?php echo e(@$user->staff->basic_salary); ?>

                                                        <?php endif; ?>
                                                    </td>

                                                    <?php
                                                        if ($user->staff) {
                                                            $getPayrollDetails = \Modules\Payroll\Entities\Payroll::getPayrollDetails($user->staff->id, $m, $y);
                                                        }
                                                    ?>
                                                    <td>
                                                    <div class="dropdown CRM_dropdown">
                                                        <?php if(!empty($getPayrollDetails)): ?>
                                                            <?php if($getPayrollDetails->payroll_status == 'G'): ?>
                                                            <button class=" dropdown-item primary-btn small bg-warning text-white border-0"> <?php echo e(__('payroll.Generated')); ?></button>
                                                            <?php endif; ?>

                                                           <?php if($getPayrollDetails->payroll_status == 'P'): ?>
                                                            <button class="dropdown-item primary-btn small bg-success text-white border-0"> <?php echo e(__('payroll.Paid')); ?></button>
                                                            <?php endif; ?>
                                                            <?php else: ?>
                                                            <button class="dropdown-item primary-btn small bg-danger text-white border-0"><?php echo e(__('payroll.Not generated')); ?></button>

                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    <div class="dropdown CRM_dropdown">
                                                        <?php if(!empty($getPayrollDetails)): ?>
                                                            <?php if($getPayrollDetails->payroll_status == 'G'): ?>
                                                                <a title="Proceed to pay" onclick="payrollPayment(<?php echo e($getPayrollDetails->id); ?>,<?php echo e($user->role_id); ?>)"><button class="primary-btn small tr-bg dropdown-item"><?php echo e(__('payroll.Proceed To Pay')); ?></button></a>
                                                                <div class="mt-1"></div>
                                                                <a onclick="viewSlip(<?php echo e($getPayrollDetails); ?>)" data-toggle="modal" data-target="#SlipForm"><button class="primary-btn small tr-bg dropdown-item"><?php echo e(__('payroll.View PaySlip')); ?></button></a>
                                                            <?php endif; ?>
                                                            <?php if($getPayrollDetails->payroll_status == 'P'): ?>
                                                                <a onclick="viewSlip(<?php echo e($getPayrollDetails); ?>)" data-toggle="modal" data-target="#SlipForm"><button class="primary-btn small tr-bg dropdown-item"><?php echo e(__('payroll.View PaySlip')); ?></button></a>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <a class="" href="<?php echo e(url('hr/payroll/generate-Payroll/'.$user->id.'/'.$m.'/'.$y)); ?>"><button class="primary-btn small tr-bg dropdown-item"> <?php echo e(__('payroll.Generate Payroll')); ?></button></a>
                                                        <?php endif; ?>
                                                    </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<div class="form_payment">

</div>
<div class="modal fade admin-query" id="SlipForm">
    <div class="modal-dialog modal_800px modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo e(__('payroll.View Payslip Details')); ?></h4>
                <button type="button" onclick="modal_close()" class="close" data-dismiss="modal">
                    <i class="ti-close"></i>
                </button>
            </div>

            <div class="modal-body">
                <div id="printablePos">
                    <div class="row mb-25">
                        <div class="col-lg-12 text-center">
                            <h3><?php echo e(config('configs')->where('key','company_name')->first()->value); ?></h3>
                            <h6><?php echo e(config('configs')->where('key','address')->first()->value); ?></h6>
                        </div>
                        <div class="col-lg-12 text-center">
                            <h5><?php echo e(__('payroll.Payslip for the period of')); ?><span class="period"></span></h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 payslip">
                            <p><?php echo e(__('payroll.Payslip')); ?> - <span class="payroll_id"></span></p>
                        </div>
                        <div class="col-md-7 text-right">
                            <p class="payment_date"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <div class="primary_input mb-25">
                                <label class="primary_input_label" for=""><?php echo e(__('common.Name')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Payment Method')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Basic Salary')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Total Earning')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Total Deduction')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Net Salary')); ?>:</label>
                                <label class="primary_input_label" for=""><?php echo e(__('payroll.Gross Salary')); ?>:</label>
                            </div>
                        </div>
                        <div class="primary_input mb-25">
                            <label class="primary_input_label user_name" for=""></label>
                            <label class="primary_input_label payment_mode" for=""></label>
                            <label class="primary_input_label basic_salary" for=""></label>
                            <label class="primary_input_label total_earning" for=""></label>
                            <label class="primary_input_label total_deduction" for=""></label>
                            <label class="primary_input_label net_salary" for=""></label>
                            <label class="primary_input_label gross_salary" for=""></label>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <a href="" target="_blank" class="primary-btn fix-gr-bg mr-2 pdf_btn"><?php echo e(__('payroll.PDF')); ?></a>
                    <button type="button" onclick="printDiv('printablePos')" class="primary-btn fix-gr-bg mr-2"><?php echo e(__('common.Print')); ?></button>
                    <button type="button" onclick="modal_close()" class="primary-btn fix-gr-bg"><?php echo e(__('common.Close')); ?></button>
                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" name="app_base_url" id="app_base_url" value="<?php echo e(URL::to('/')); ?>">
<?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
        function payrollPayment(el, role_id)
        {
            $.post('<?php echo e(route('payroll_payment_modal')); ?>',{_token:'<?php echo e(csrf_token()); ?>', id:el, role_id:role_id}, function(data){
                $(".form_payment").html(data);
                $('#PaymentForm').modal('show');
                $('select').niceSelect();
            });
        }
        function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
                setTimeout(function(){ window.location.reload(); }, 15000);
            }
            function modal_close(){
                $('#SlipForm').remove();
                $('.modal-backdrop').remove();
            }

        function viewSlip(payroll)
        {
            let currency = '<?php echo e(config('configs')->where('key','currency_symbol')->first()->value); ?>';
            $('.period').text(' '+payroll.payroll_month+' - '+payroll.payroll_year);
            $('.payroll_id').text(payroll.id);
            $('.payment_date').text(payroll.payment_date);
            $('.user_name').text(payroll.staff.user.name);
            if (payroll.payment_mode ==  null) {
                $('.payment_mode').text("Not Payment Yet.");
            }
            else {
                $('.payment_mode').text(payroll.payment_mode);
            }
            $('.basic_salary').text(currency +' '+payroll.basic_salary);
            $('.total_earning').text(currency +' '+payroll.total_earning);
            $('.total_deduction').text(currency +' '+payroll.total_deduction);
            $('.net_salary').text(currency +' '+payroll.net_salary);
            $('.gross_salary').text(currency +' '+payroll.gross_salary);
            var baseUrl = $('#app_base_url').val();
            var url = baseUrl + "/hr/payroll/pdf/" + payroll.id;
            $('.pdf_btn').attr('href', url);
        }
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => 'Payroll'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Payroll/Resources/views/payrolls/index.blade.php ENDPATH**/ ?>