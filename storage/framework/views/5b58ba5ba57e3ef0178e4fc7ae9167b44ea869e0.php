<table class="table Crm_table_active3">
    <thead>
    <tr>
        <th><?php echo e(__('common.SL')); ?></th>
        <th><?php echo e(__('attendance.Year')); ?></th>
        <th><?php echo e(__('common.Action')); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $years; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($key+1); ?></td>
            <td><?php echo e($year->year); ?></td>
            <td>
                <div class="dropdown CRM_dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenu2" data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <?php echo e(__('common.Select')); ?>

                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenu2">
                         <?php if(permissionCheck('view.year.data')): ?>
                        <a href="<?php echo e(route('view.year.data',$year->id)); ?>"
                           class="dropdown-item edit_brand"><?php echo e(__('common.View')); ?></a>
                           <?php endif; ?>
                            <?php if(permissionCheck('year.data')): ?>
                        <a href="<?php echo e(route('year.data',$year->id)); ?>"
                           class="dropdown-item edit_brand"><?php echo e(__('common.Edit')); ?></a>
                           <?php endif; ?>
                            <?php if(permissionCheck('holiday.delete')): ?>
                        <a onclick="confirm_modal('<?php echo e(route('holiday.delete', $year->year)); ?>');"
                           class="dropdown-item"><?php echo e(__('common.Delete')); ?></a>
                           <?php endif; ?>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/holiday_setup/components/table.blade.php ENDPATH**/ ?>