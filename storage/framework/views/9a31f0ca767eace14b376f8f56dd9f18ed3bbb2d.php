


<?php $__env->startSection('mainContent'); ?>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="main-title">
                                <h3 class="mb-30"><?php echo app('translator')->get('todo.Worklist'); ?></h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 text-right">
                            <a href="#" data-toggle="modal" class="primary-btn small fix-gr-bg"
                               data-target="#add_to_do"
                               title="Add To Do" data-modal-size="modal-md">
                                <span class="ti-plus pr-2"></span>
                                <?php echo app('translator')->get('Add a worklist'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box school-table">
                                <!-- Category -->
                                <div class="row form-group">
            
                                    <div class="primary_input col-md-6">
                                       <select class="primary_input_field" id="sel1">
                                        <option>Select Category</option>
                                        <option>Category 1</option>
                                        <option>Category 2</option>
                                        <option>Category 3</option>
                                       </select>
                                    </div>
                                    <div class="primary_input col-md-2">
                                    <a href="#" data-toggle="modal" class="primary-btn medium fix-gr-bg"
                               data-target="#"
                               title="Add To Do" data-modal-size="modal-md">
                                <?php echo app('translator')->get('Search'); ?>
                                  </a>
                                    </div>
                                    <div class="primary_input col-md-4">
                                    <a href="" class="primary-btn medium fix-gr-bg" data-toggle="modal" data-target="#categoryModal" data-whatever="">
                                    <span class="ti-plus pr-2"></span>
                                        Add Category</a>
                                    </div>
                                 </div>
                                <!-- /category -->
                                <div class="row to-do-list mb-20">
                                    <div class="col-md-6 col-6">
                                        <button class="primary-btn small fix-gr-bg"
                                                id="toDoList"><?php echo app('translator')->get('todo.inprogress'); ?></button>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <button class="primary-btn small tr-bg"
                                                id="toDoListsCompleted"><?php echo app('translator')->get('todo.completed'); ?></button>
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="primary_input col-md-6">
                                        <!-- <button>Edit</button> -->
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-6 text-right">
                                        <a href="#" data-toggle="modal" class="primary-btn small fix-gr-bg"
                                        data-target="#edit_worklist"
                                        title="Add To Do" data-modal-size="modal-md">
                                        <i class='fas'>&#xf039;</i>
                                            <?php echo app('translator')->get('event.Edit'); ?>
                                        </a>
                                    </div>
                                    </div>
                                <input type="hidden" id="url" value="<?php echo e(url('/')); ?>">


                                <div class="toDoList">
                                    <?php if(count(@$toDos->where('status',0)) > 0): ?>

                                        <?php $__currentLoopData = $toDos->where('status',0); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $toDoList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="single-to-do d-flex justify-content-between toDoList"
                                                 id="to_do_list_div<?php echo e(@$toDoList->id); ?>">
                                                <div>
                                                    <input type="checkbox" id="midterm<?php echo e(@$toDoList->id); ?>"
                                                           class="common-checkbox complete_task" name="complete_task"
                                                           value="<?php echo e(@$toDoList->id); ?>">

                                                    <label for="midterm<?php echo e(@$toDoList->id); ?>">

                                                        <input type="hidden" id="id" value="<?php echo e(@$toDoList->id); ?>">
                                                        <input type="hidden" id="url" value="<?php echo e(url('/')); ?>">
														
                                                        <h5 class="d-inline"> <?php echo e(@$toDoList->title); ?>  
														<span class="txtDt"> <?php echo e($toDoList->date); ?> / 14:15</span> <i class="d-inline ti-flag-alt text-danger"> </i></h5>
                                                        <br>														
                                                        <p class="more"><?php echo e(@$toDoList->description); ?></p>                                                        
                                                    </label>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <div class="single-to-do d-flex justify-content-between">
                                            <?php echo app('translator')->get('todo.no_do_lists_assigned_yet'); ?>
                                        </div>

                                    <?php endif; ?>
                                </div>
                                 <input type="checkbox">
                                <div class="toDoListsCompleted">
                                 <?php if(count(@$toDos->where('status',1))>0): ?>
                                        <?php $__currentLoopData = $toDos->where('status',1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $toDoListsCompleted): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <div class="single-to-do d-flex justify-content-between"
                                                 id="to_do_list_div<?php echo e(@$toDoListsCompleted->id); ?>">
                                                <div>
                                                <h5 class="d-inline"><?php echo e(@$toDoListsCompleted->title); ?> 
													<span class="txtDt"> <?php echo e($toDoListsCompleted->date); ?> / 14:15</span></h5><br>
                                                    <p class="more"><?php echo e(@$toDoListsCompleted->description); ?></p>
                                                     
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                    <?php else: ?>
                                        <div class="single-to-do d-flex justify-content-between">
                                            <?php echo app('translator')->get('todo.no_do_lists_assigned_yet'); ?>
                                        </div>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            <div class="modal fade admin-query" id="add_to_do">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo e(trans('todo.Add To Do')); ?></h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()'])); ?>


                                <div class="row form-group">
                                    <div class="col-lg-12">
                                    <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Category')); ?></label>
                                                    <select class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Title')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('common.Title')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Description')); ?>*</label>
                                                    <textarea  class="primary_textarea"
                                                           placeholder="<?php echo e(__('common.Description')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('common.Date')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('common.Date')); ?>"
                                                                   class="primary_input_field primary-input date form-control"
                                                                   id="startDate" type="text" name="date"
                                                                   value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
											
											<div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('Time')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('Time')); ?>"
                                                                   class="primary_input_field primary-input time form-control"
                                                                   id="startTime" type="text" name="time"
                                                                   value="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="ti-alarm-clock"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- Repeat -->
                                            <div class="col-md-6 col-lg-6 col-sm-6 text-left">
                                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Small modal</button> -->
                                            <a href="" class="text-left" data-toggle="modal" data-target="#exampleModal" data-whatever="">Repeat</a>
                                            </div>
                                            <!-- Close Repeat -->
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Case Link')); ?></label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Case Link')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 text-center">
                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo e(Form::close()); ?>

                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>

            <!-- Close Todo Modal -->
            <!--Edit Modal   -->
            <div class="modal fade admin-query" id="edit_worklist">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo e(trans('Edit')); ?></h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="ti-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()'])); ?>


                                    <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Category')); ?></label>
                                                    <select class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Title')); ?>*</label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('common.Title')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                    <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-25">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('common.Description')); ?>*</label>
                                                    <textarea  class="primary_textarea"
                                                           placeholder="<?php echo e(__('common.Description')); ?>" name="description"
                                                               ></textarea>
                                                    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('common.Date')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('common.Date')); ?>"
                                                                   class="primary_input_field primary-input date form-control"
                                                                   id="startDate" type="text" name="date"
                                                                   value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
											
											<div class="col-lg-6" id="">
                                                <label class="primary_input_label" for=""><?php echo e(__('Time')); ?> *</label>
                                            <div class="primary_datepicker_input">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="">
                                                            <input placeholder="<?php echo e(__('Time')); ?>"
                                                                   class="primary_input_field primary-input time form-control"
                                                                   id="startTime" type="text" name="time"
                                                                   value="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="ti-alarm-clock"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- Repeat -->
                                            <div class="col-md-6 col-lg-6 col-sm-6 text-left">
                                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Small modal</button> -->
                                            <a href="" class="text-left" data-toggle="modal" data-target="#edit_repeat" data-whatever="">Repeat</a>
                                            </div>
                                            <!-- Close Repeat -->
                                        </div>

                                        <div class="row mt-25">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Case Link')); ?></label>
                                                    <input type="text" class="primary_input_field"
                                                           placeholder="<?php echo e(__('Case Link')); ?>" name="title"
                                                           value="<?php echo e(old('title')); ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 text-center">
                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                                            </div>
                                        </div>
                                    
                                    <?php echo e(Form::close()); ?>

                            
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
                        <!-- start Repeat Modal -->
                        

<div class="modal fade admin-query" id="exampleModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Repeat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="ti-close"></i>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <!-- day + week -->
            <div class="row form-group">
            <div class="primary_input col-md-4">
                <label>Repeats Every</label>
            </div>
            
        </div>
        <div class="row form-group">
            <div class="primary_input col-md-4">

                <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
            </div>
            <div class="primary_input col-md-8">
                <select class="primary_input_field" id="sel1">
                    <option>day</option>
                    <option>week</option>
                    <option>month</option>
                    <option>year</option>
                </select>
            </div>

        </div>
        <!-- close day + week -->
        <!-- week fields Drop down -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <select class="primary_input_field" id="sel1">
                    <option>Monday</option>
                    <option>Tuesday</option>
                    <option>Wednesday</option>
                    <option>Thursday</option>
                    <option>Friday</option>
                    <option>Saturday</option>
                    <option>Sunday</option>
                </select>
            </div>
        </div>
        <!-- close week fields -->
        <!-- days -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num"  checked>
            </div>
            <div class="primary_input col-md-10">
                <select class="primary_input_field" id="sel1">
                    <option>Day 1</option>
                    <option>Day 2</option>
                    <option>Day 3</option>
                    <option>Day 4</option>
                    <option>Day 5</option>
                    <option>Day 6</option>
                    <option>Day 7</option>
                    <option>Day 8</option>
                    <option>Day 9</option>
                    <option>Day 10</option>
                    <option>Day 11</option>
                    <option>Day 12</option>
                    <option>Day 13</option>
                    <option>Day 14</option>
                    <option>Day 15</option>
                    <option>Day 16</option>
                    <option>Day 17</option>
                    <option>Day 18</option>
                    <option>Day 19</option>
                    <option>Day 20</option>
                    <option>Day 21</option>
                    <option>Day 22</option>
                    <option>Day 23</option>
                    <option>Day 24</option>
                    <option>Day 25</option>
                    <option>Day 26</option>
                    <option>Day 27</option>
                    <option>Day 28</option>
                    <option>Day 29</option>
                    <option>Day 30</option>
                    <option>Day 31</option>
                    <option>Last Day</option>
                </select>
            </div>

        </div>
        <!-- close days -->
        <!-- First or days -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num">
            </div>
            <div class="primary_input col-md-4">
                <select class="primary_input_field" id="sel1">
                    <option>First</option>
                    <option>Second</option>
                    <option>Third</option>
                    <option>Fourth</option>
                    <option>Last</option>
                </select>
            </div>
            <div class="primary_input col-md-6">
                <select class="primary_input_field" id="sel1">
                <option>Monday</option>
                    <option>Tuesday</option>
                    <option>Wednesday</option>
                    <option>Thursday</option>
                    <option>Friday</option>
                    <option>Saturday</option>
                    <option>Sunday</option>
                </select>
            </div>

        </div>
        <!-- close first or days -->
        <!-- Set Time -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <select class="primary_input_field" id="sel1">
                    <option value="">Set ime</option>
                    <option>00:00</option>
                    <option>00:30</option>
                    <option>01:00</option>
                    <option>01:30</option>
                    <option>02:00</option>
                    <option>02:30</option>
                    <option>03:00</option>
                    <option>03:30</option>
                    <option>04:00</option>
                    <option>04:30</option>
                    <option>05:00</option>
                    <option>05:30</option>
                    <option>06:00</option>
                    <option>06:30</option>
                    <option>07:00</option>
                    <option>07:30</option>
                    <option>08:00</option>
                    <option>08:30</option>
                    <option>09:00</option>
                    <option>09:30</option>
                    <option>10:00</option>
                    <option>10:30</option>
                    <option>11:00</option>
                    <option>11:30</option>
                    <option>12:00</option>
                    <option>12:30</option>
                    <option>13:00</option>
                    <option>13:30</option>
                    <option>14:00</option>
                    <option>14:30</option>
                    <option>15:00</option>
                    <option>15:30</option>
                    <option>16:00</option>
                    <option>16:30</option>
                    <option>17:00</option>
                    <option>17:30</option>
                    <option>18:00</option>
                    <option>18:30</option>
                    <option>19:00</option>
                    <option>19:30</option>
                    <option>20:00</option>
                    <option>20:30</option>
                    <option>21:00</option>
                    <option>21:30</option>
                    <option>22:00</option>
                    <option>22:30</option>
                    <option>23:00</option>
                    <option>23:30</option>
                </select>
            </div>

        </div>
        <!-- Close set time -->
        <!-- Starts -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <label for="starts">Starts</label>
                <select class="primary_input_field" id="sel1">
                    <option>Augest</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                    <option>Jan 2023</option>
                    <option>Feb 2023</option>
                    <option>Mar 2023</option>
                    <option>Apr 2023</option>
                    <option>May 2023</option>
                    <option>Jun 2023</option>
                    <option>Jul 2023</option>
                </select>
            </div>
        </div>
        <!-- close starts -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                 <input type="checkbox" class="larger" name="day_num" checked>
                 <label>Never</label>
            </div>
           
        </div>
        <!-- Never close -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-2">
                 <input type="checkbox" name="day_num" checked>
                 <label>On</label>
            </div>
            <div class="primary_input col-md-2">
                 
            </div>
            <div class="primary_input col-md-8">
                 <input type="text" placeholder="date" class="primary_input_field primary-input form-control datetime" >
                 <!-- <?php echo e(Form::text('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field form-control datetime', "id"=>"previous_date",'placeholder' => __('case.PREVIOUS DATE')])); ?> -->
            </div>
        </div>
        <!-- Never close -->
        <!-- Never -->
        <div class="row form-group">
            <div class="primary_input col-md-4">
                 <input type="checkbox" name="day_num" checked>
                 <label>After</label>
            </div>
   
            <div class="primary_input col-md-3">
            <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
            </div>
            <div class="primary_input col-md-5">
            <input type="text" name="day_num" class="primary_input_field" value="occurrence">
            </div>
        </div>
        <!-- Never close -->
        </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
                        <!-- Close Repeat Modal -->

                        <!-- Repeat for Edit -->
        <div class="modal fade" id="edit_repeat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Repeat</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <!-- day + week -->
                                        <div class="row form-group">
                                        <div class="primary_input col-md-4">
                                                                <label>Repeats Every</label>
                                        </div>
                                        
                                    </div>
                                    <div class="row form-group">
                                        <div class="primary_input col-md-4">

                                            <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
                                        </div>
                                        <div class="primary_input col-md-8">
                                            <select class="primary_input_field" id="sel1">
                                                <option>day</option>
                                                <option>week</option>
                                                <option>month</option>
                                                <option>year</option>
                                            </select>
                                        </div>

                                    </div>
                                    <!-- close day + week -->
                                    <!-- week fields Drop down -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-12">
                                            <select class="primary_input_field" id="sel1">
                                                <option>Monday</option>
                                                <option>Tuesday</option>
                                                <option>Wednesday</option>
                                                <option>Thursday</option>
                                                <option>Friday</option>
                                                <option>Saturday</option>
                                                <option>Sunday</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- close week fields -->
                                    <!-- days -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-2">
                                            <input type="checkbox" name="day_num"  checked>
                                        </div>
                                        <div class="primary_input col-md-10">
                                            <select class="primary_input_field" id="sel1">
                                                <option>Day 1</option>
                                                <option>Day 2</option>
                                                <option>Day 3</option>
                                                <option>Day 4</option>
                                                <option>Day 5</option>
                                                <option>Day 6</option>
                                                <option>Day 7</option>
                                                <option>Day 8</option>
                                                <option>Day 9</option>
                                                <option>Day 10</option>
                                                <option>Day 11</option>
                                                <option>Day 12</option>
                                                <option>Day 13</option>
                                                <option>Day 14</option>
                                                <option>Day 15</option>
                                                <option>Day 16</option>
                                                <option>Day 17</option>
                                                <option>Day 18</option>
                                                <option>Day 19</option>
                                                <option>Day 20</option>
                                                <option>Day 21</option>
                                                <option>Day 22</option>
                                                <option>Day 23</option>
                                                <option>Day 24</option>
                                                <option>Day 25</option>
                                                <option>Day 26</option>
                                                <option>Day 27</option>
                                                <option>Day 28</option>
                                                <option>Day 29</option>
                                                <option>Day 30</option>
                                                <option>Day 31</option>
                                                <option>Last Day</option>
                                            </select>
                                        </div>

                                    </div>
                                    <!-- close days -->
                                    <!-- First or days -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-2">
                                            <input type="checkbox" name="day_num">
                                        </div>
                                        <div class="primary_input col-md-4">
                                            <select class="primary_input_field" id="sel1">
                                                <option>First</option>
                                                <option>Second</option>
                                                <option>Third</option>
                                                <option>Fourth</option>
                                                <option>Last</option>
                                            </select>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <select class="primary_input_field" id="sel1">
                                            <option>Monday</option>
                                                <option>Tuesday</option>
                                                <option>Wednesday</option>
                                                <option>Thursday</option>
                                                <option>Friday</option>
                                                <option>Saturday</option>
                                                <option>Sunday</option>
                                            </select>
                                        </div>

                                    </div>
                                    <!-- close first or days -->
                                    <!-- Set Time -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-12">
                                            <select class="primary_input_field" id="sel1">
                                                <option value="">Set ime</option>
                                                <option>00:00</option>
                                                <option>00:30</option>
                                                <option>01:00</option>
                                                <option>01:30</option>
                                                <option>02:00</option>
                                                <option>02:30</option>
                                                <option>03:00</option>
                                                <option>03:30</option>
                                                <option>04:00</option>
                                                <option>04:30</option>
                                                <option>05:00</option>
                                                <option>05:30</option>
                                                <option>06:00</option>
                                                <option>06:30</option>
                                                <option>07:00</option>
                                                <option>07:30</option>
                                                <option>08:00</option>
                                                <option>08:30</option>
                                                <option>09:00</option>
                                                <option>09:30</option>
                                                <option>10:00</option>
                                                <option>10:30</option>
                                                <option>11:00</option>
                                                <option>11:30</option>
                                                <option>12:00</option>
                                                <option>12:30</option>
                                                <option>13:00</option>
                                                <option>13:30</option>
                                                <option>14:00</option>
                                                <option>14:30</option>
                                                <option>15:00</option>
                                                <option>15:30</option>
                                                <option>16:00</option>
                                                <option>16:30</option>
                                                <option>17:00</option>
                                                <option>17:30</option>
                                                <option>18:00</option>
                                                <option>18:30</option>
                                                <option>19:00</option>
                                                <option>19:30</option>
                                                <option>20:00</option>
                                                <option>20:30</option>
                                                <option>21:00</option>
                                                <option>21:30</option>
                                                <option>22:00</option>
                                                <option>22:30</option>
                                                <option>23:00</option>
                                                <option>23:30</option>
                                            </select>
                                        </div>

                                    </div>
                                    <!-- Close set time -->
                                    <!-- Starts -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-12">
                                            <label for="starts">Starts</label>
                                            <select class="primary_input_field" id="sel1">
                                                <option>Augest</option>
                                                <option>September</option>
                                                <option>October</option>
                                                <option>November</option>
                                                <option>December</option>
                                                <option>Jan 2023</option>
                                                <option>Feb 2023</option>
                                                <option>Mar 2023</option>
                                                <option>Apr 2023</option>
                                                <option>May 2023</option>
                                                <option>Jun 2023</option>
                                                <option>Jul 2023</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- close starts -->
                                    <!-- Never -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-2">
                                            <input type="checkbox" class="larger" name="day_num" checked>
                                        
                                        </div>
                                        <div class="primary_input col-md-10">
                                            <label>Never</label>
                                        </div>
                                    </div>
                                    <!-- Never close -->
                                    <!-- Never -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-2">
                                            <input type="checkbox" name="day_num" checked>
                                        </div>
                                        <div class="primary_input col-md-2">
                                            <label>On</label>
                                        </div>
                                        <div class="primary_input col-md-8">
                                            <input type="text" placeholder="date" class="primary_input_field primary-input form-control datetime" >
                                            <!-- <?php echo e(Form::text('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field form-control datetime', "id"=>"previous_date",'placeholder' => __('case.PREVIOUS DATE')])); ?> -->
                                        </div>
                                    </div>
                                    <!-- Never close -->
                                    <!-- Never -->
                                    <div class="row form-group">
                                        <div class="primary_input col-md-2">
                                            <input type="checkbox" name="day_num" checked>
                                        </div>
                                        <div class="primary_input col-md-2">
                                            <label>After</label>
                                        </div>
                                        <div class="primary_input col-md-3">
                                        <input type="number" name="day_num" class="primary_input_field" value="1" min="1" max="99">
                                        </div>
                                        <div class="primary_input col-md-5">
                                        <input type="text" name="day_num" class="primary_input_field" value="occurrence">
                                        </div>
                                    </div>
                                    <!-- Never close -->
                                    </form>
                                    <div class="col-lg-12 text-center">
                                        <div class="mt-40 d-flex justify-content-between">
                                            <button type="button" class="primary-btn tr-bg"
                                                data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                            <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                                        </div>
                                    </div>
                                    <!-- close button -->
                                </div>
                                </div>
                </div>
        </div>

                        <!-- Add Category -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        <!-- Category -->
        <div class="row form-group">
            <div class="primary_input col-md-12">
                <input type="text" name="category" class="primary_input_field" placeholder="Add Category" >
                   
            </div>
        </div>
        <!-- Category -->
        </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
                        <!-- /Add Category -->

  <?php $__env->stopSection(); ?>
  <?php $__env->startPush('admin.scripts'); ?>
  <script>
  </script>
  <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => 'Todo'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\updates 123\cw\Modules/Todo\Resources/views/index.blade.php ENDPATH**/ ?>