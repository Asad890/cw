<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
        <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-district-court" role="tab" aria-controls="pills-contact" aria-selected="true"><h3 class="mb-0 "><?php echo e(__('District Courts and Tribunals')); ?></h3></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-high-court" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('High Court')); ?></h3></a>
            </li>

        </ul>

            <div class="row justify-content-center">
                <div class="col-12">
                    
                    <h4 style="text-align:center">Adv. Aditiya Joshi</h4>
                    <p style="text-align:center"> <span id="date-time"></span></p>
                    
                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="pills-district-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px" ><?php echo e(__('Todays Cases')); ?>

                            <a class="btn primary-btn radius_30px mr-10 fix-gr-bg"
                            href="<?php echo e(route('case.create')); ?>"><i class="ti-plus"></i><?php echo e(('New Case')); ?></a> 
                            <div class="col-0 btn-group">
                                <button type="button" class="primary-btn radius_30px mr-10 fix-gr-bg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Template</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                    <a class="btn btn-primary dropdown-item" href="#" 
                                        data-toggle="modal" data-target="#add_to_do" data-modal-size="modal-md">SAVE
                                    </a>
                                </div>
                            </div>
                            </h3>
                            <ul class="d-flex">
                                <li>
                                
                                </li>
                            </ul>
                        </div>
                           <br>
                            <div class="col-lg-12">
                                <div class="QA_section QA_section_heading_custom check_box_table">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <div class="">
                                            <table class="table Crm_table_active3">
                                                <thead>
                                                    <tr>
                                                    <th scope="col"><input type="checkbox"></th>
                                                        <th scope="col"><?php echo e(__('Sr.No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Brief No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Prev Date.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Courts.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Case No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Petitioner.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Respondent.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Stage.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Sr.no. in Court')); ?></th>
                                                        <th scope="col"><?php echo e(__('Court room no')); ?></th>
                                                        <th scope="col"><?php echo e(__('Judge Name')); ?></th>
                                                        <th scope="col"><?php echo e(__('Brief For')); ?></th>
                                                        <th scope="col"><?php echo e(__('Organization')); ?></th>
                                                        <th scope="col"><?php echo e(__('Tags/Refrence')); ?></th>
                                                        <th scope="col"><?php echo e(__('Police Station')); ?></th>
                                                        <th scope="col"><?php echo e(__('Remarks')); ?></th>
                                                        <th scope="col"><?php echo e(__('Next Date')); ?></th>
                                                        <th scope="col"><?php echo e(__('Action')); ?></th>
                                                        <th scope="col"><?php echo e(__('Select')); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                    <td><input type="checkbox"></td>
                                                        <td> <a href="#"> 1</a></td>
                                                        <td> <a href="#">313 </a></a></td>
                                                        <td> <a href="#">23/3/2020 </a></td>
                                                        <td> <a href="#">Mact 2 </a></td>
                                                        <td> <a href="#">MACF43 </a></td>
                                                        <td> <a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Petitioner" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Hirendra</a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">Arguement
                                                            </a></td>
                                                        <td> <a href="#">50 </a></td>
                                                        <td> <a href="#">219 </a></td>
                                                        <td> <a href="#">R.K </a></td>
                                                        <td> <a href="#">R-2 </a></td>
                                                        <td> <a href="#">Royal Sundaram</a></td>
                                                        <td> <a href="#">Blank </a></td>
                                                        <td> <a href="#">Blank </a></td>
                                                        <td> <a href="#">To File Ws </a></td>
                                                        <td><a href="#" data-target="#change_date" data-toggle="modal"data-modal-size="modal-md">23/3/2020
                                                            </a></td>
                                                        <td><a href="#" data-target="#reference" data-toggle="modal"data-modal-size="modal-md">
                                                            <span class="fa fa-refresh"></span>
                                                            </a>
                                                        </td>
                                                        <td> <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        Select
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <a href="#"
                                                           class="dropdown-item"><i
                                                                class="icon-file-eye"></i> 
                                                            View</a>
                                                        
                                                                <a href="#"
                                                                   class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i>Edit
                                                                </a>
                                                            
                                                       
                                                            <span id="delete_item" data-id="#" data-url="#" class="dropdown-item"><i class="icon-trash"></i> Delete
                                                            </span>
                                                       

                                                    </div>
                                                </div>
                                            </td>
                                                        
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- Tab 2 -->
                        <div class="tab-pane fade" id="pills-high-court" role="tabpanel" aria-labelledby="pills-contact-tab">
                            
                            <!-- <h4 style="text-align:center">Adv. Aditiya Joshi</h4> -->
                            <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px" ><?php echo e(__('Todays Cases')); ?>

                            <a class="btn primary-btn radius_30px mr-10 fix-gr-bg"
                            href="<?php echo e(route('case.create')); ?>"><i class="ti-plus"></i><?php echo e(('New Case')); ?></a> 
                            <div class="col-0 btn-group">
                                <button type="button" class="primary-btn radius_30px mr-10 fix-gr-bg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Template</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                    <a class="btn btn-primary dropdown-item" href="#" 
                                        data-toggle="modal" data-target="#add_to_do" data-modal-size="modal-md">SAVE
                                    </a>
                                </div>
                            </div>
                            </h3>
                            <ul class="d-flex">
                                <li>
                                
                                </li>
                            </ul>
                        </div>
                           <br>
                            <div class="col-lg-12">
                                <div class="QA_section QA_section_heading_custom check_box_table">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <div class="">
                                            <table class="table Crm_table_active3">
                                                <thead>
                                                    <tr>
                                                    <th scope="col"><input type="checkbox"></th>
                                                        <th scope="col"><?php echo e(__('Sr.No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Brief No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Prev Date')); ?></th>
                                                        <th scope="col"><?php echo e(__('Courts')); ?></th>
                                                        <th scope="col"><?php echo e(__('Case No.')); ?></th>
                                                        <th scope="col"><?php echo e(__('Petitioner')); ?></th>
                                                        <th scope="col"><?php echo e(__('Respondent')); ?></th>
                                                        <th scope="col"><?php echo e(__('Stage')); ?></th>
                                                        <th scope="col"><?php echo e(__('Sr.no. in Court')); ?></th>
                                                        <th scope="col"><?php echo e(__('Court room no')); ?></th>
                                                        <th scope="col"><?php echo e(__('Judge Name')); ?></th>
                                                        <th scope="col"><?php echo e(__('Brief For')); ?></th>
                                                        <th scope="col"><?php echo e(__('Organization')); ?></th>
                                                        <th scope="col"><?php echo e(__('Tags/Refrence')); ?></th>
                                                        <th scope="col"><?php echo e(__('Police Station')); ?></th>
                                                        <th scope="col"><?php echo e(__('Remarks')); ?></th>
                                                        <th scope="col"><?php echo e(__('Next Date')); ?></th>
                                                        <th scope="col"><?php echo e(__('Action')); ?></th>
                                                        <th scope="col"><?php echo e(__('select')); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                    <td><input type="checkbox"></td>
                                                        <td> <a href="#">1</a></td>
                                                        <td><a href="#">313</a></td>
                                                        <td><a href="#">23/3/2020</a></td>
                                                        <td><a href="#">Mact 2</a></td>
                                                        <td><a href="#">MACF43</a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Respondent" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Evidence</a></td>
                                                        <td><a href="" class=" popover-demo mb-2" data-toggle="popover" title="Petitioner" data-content="1) Manoj Purushottam Chinchulkar (Deceased), P.S.Khapa 289/2021Advocate- A.P.P.2) Sonu Tekam">Hirendra</a></td>
                                                        <td><a href="#">Evidence</a></td>
                                                        <td><a href="#">50</a></td>
                                                        <td><a href="#">219</a></td>
                                                        <td><a href="#">R.K</a></td>
                                                        <td><a href="#">R-2</a></td>
                                                        <td><a href="#">Royal Sundaram</a></td>
                                                        <td><a href="#">Blank</a></td>
                                                        <td><a href="#">Blank</a></td>
                                                        <td><a href="#">To File Ws</a></td>
                                                        <td><a href="#">23/3/2020</a></td>
                                                        <td><a href="#" data-target="#reference" data-toggle="modal"data-modal-size="modal-md">
                                                            <span class="fa fa-refresh"></span>
                                                            </a>
                                                        </td>
                                                        <td> <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        Select
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <a href="#"
                                                           class="dropdown-item"><i
                                                                class="icon-file-eye"></i> 
                                                            View</a>
                                                        
                                                                <a href="#"
                                                                   class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i>Edit
                                                                </a>
                                                            
                                                       
                                                            <span id="delete_item" data-id="#" data-url="#" class="dropdown-item"><i class="icon-trash"></i> Delete
                                                            </span>
                                                       

                                                    </div>
                                                </div>
                                            </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                 </div>
                    <!-- form end =-->
                </div>
            </div>


        </div>

     
    </section>
    <div class="modal fade" id="reference" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Refresh</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <!-- Category -->
                <div class="row form-group">
                    <div class="primary_input col-md-12">
                        <label>Next Date</label>
                    <input placeholder="<?php echo e(__('common.Date')); ?>"
                        class="primary_input_field primary-input date form-control"
                        id="startDate" type="text" name="date"
                        value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="primary_input col-md-12">
                        <label>Previous Date</label>
                    <input placeholder="<?php echo e(__('common.Date')); ?>"
                        class="primary_input_field primary-input date form-control"
                        id="startDate" type="text" name="date"
                        value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="primary_input col-md-12">
                        <label>Stage</label>
                        <input type="text" name="category" class="primary_input_field" placeholder="Add Stage" >
                        
                    </div>
                </div>
                <!-- Category -->
                </form>
                <div class="col-lg-12 text-center">
                    <div class="mt-40 d-flex justify-content-between">
                        <button type="button" class="primary-btn tr-bg"
                            data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                        <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                    </div>
                </div>
                <!-- close button -->
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="change_date" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Date Or Stage</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <!-- Category -->
                <div class="row form-group">
                    <div class="primary_input col-md-12">
                        <label>Next Date</label>
                    <input placeholder="<?php echo e(__('common.Date')); ?>"
                        class="primary_input_field primary-input date form-control"
                        id="startDate" type="text" name="date"
                        value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="primary_input col-md-12">
                        <label>Stage</label>
                        <input type="text" name="category" class="primary_input_field" placeholder="Add Stage" >
                        
                    </div>
                </div>
                <!-- Category -->
                </form>
                <div class="col-lg-12 text-center">
                    <div class="mt-40 d-flex justify-content-between">
                        <button type="button" class="primary-btn tr-bg"
                            data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                        <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
                    </div>
                </div>
                <!-- close button -->
            </div>
            </div>
        </div>
    </div>
    <script>
var dt = new Date();
document.getElementById('date-time').innerHTML=dt;
</script>
<?php $__env->stopSection(); ?>
      



<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/today.blade.php ENDPATH**/ ?>