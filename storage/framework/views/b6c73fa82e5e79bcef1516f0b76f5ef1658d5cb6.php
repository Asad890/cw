

<?php $__env->startPush('css'); ?>

<style>

.login-resistration-area .main-login-area.login-res-v2::before {
    background-image: url(<?php echo e(asset(config('configs')->where('key', 'login_backgroud_image')->first()->value)); ?>)
}

</style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

<div class="main-content">
		<div class="logo_img">
			<a href="<?php echo e(route('home')); ?>">
				<img src="<?php echo e(asset(config('configs')->where('key', 'site_logo')->first()->value)); ?>" alt="Logo Image" class="img img-responsive">
			</a>
		</div>

			<h3 class="sho_web d-none d-md-block"><?php echo e(__('auth.Forgot Your Password?')); ?></h3>

            <p> <?php echo e(__('auth.No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.')); ?></p>





			<form  method="POST" action="<?php echo e(route('password.email')); ?>"  id="content_form" class="customer-input" >

			<?php echo csrf_field(); ?>

				<input required name="email" type="text" placeholder="<?php echo e(__('auth.Enter email address')); ?>" id="email" autofocus class="" autocomplete="current-password">



				<div class="forgot-pass">
						<a href="<?php echo e(route('login')); ?>">
							<?php echo e(__('auth.Back to login')); ?>

						</a>
				</div>


				<button type="submit" class="login-res-btn submit"><?php echo e(__('auth.Send Instruction')); ?></button>
				<button type="button" class="login-res-btn submitting" style="display:none" disabled><?php echo e(__('auth.Sending Instructions')); ?>...</button>
			</form>


	</div>



<?php $__env->stopSection(); ?>


<?php $__env->startPush('js_after'); ?>

<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.guest', ['title' => 'Forgot Password'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/auth/forgot-password.blade.php ENDPATH**/ ?>