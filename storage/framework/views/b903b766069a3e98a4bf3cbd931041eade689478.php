<div class="modal fade deleteAdvocateItemModal" id="deleteAdvocateItemModal" >
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo app('translator')->get('common.Delete'); ?> </h4>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="ti-close "></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h4><?php echo app('translator')->get('common.Are you sure to delete ?'); ?></h4>
                    <p><?php echo app('translator')->get('common.Once deleted, it will deleted all related Data!'); ?></p>
                </div>
                <div class="mt-40 d-flex justify-content-between">
                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo app('translator')->get('common.Cancel'); ?></button>
                    <form id="item_delete_form" action="">
                        <?php echo method_field('delete'); ?>
                        <button class="primary-btn fix-gr-bg submit" type="submit" ><?php echo app('translator')->get('common.Delete'); ?></button>
                        <button class="primary-btn fix-gr-bg submitting" disabled type="button" style="display: none;" ><?php echo app('translator')->get('common.Deleting'); ?></button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/partials/deleteModalAjax.blade.php ENDPATH**/ ?>