<?php $__env->startSection('mainContent'); ?>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="box_header">
                    <div class="main-title d-flex justify-content-between w-100">
                        <h3 class="mb-0 mr-30"><?php echo e(__('case.Edit Case Stage')); ?></h3>
                       
                    </div>
                </div>
            </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::model($model, ['route' => ['category.case.update', $model->id], 'class' =>
                        'form-validate-jquery', 'id' => 'content_form', 'method' => 'PUT']); ?>

                        <div class="primary_input">
                            <?php echo e(Form::label('name', __('case.Name'),['class' => 'required'])); ?>

                            <?php echo e(Form::text('name', null, ['required' => '', 'class' => 'primary_input_field', 'placeholder' => __('case.Case Category Name')])); ?>

                        </div>

                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('case.Description'))); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field', 'placeholder' =>
                            __('case.Case Category Description'), 'rows' => 5 ])); ?>

                        </div>

                        <div class="text-center mt-3">
                        <button class="primary_btn_large submit" type="submit"><i class="ti-check"></i><?php echo e(__('common.Update')); ?>

                        </button>

                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Updating') . '...'); ?>

                        </button>
                        </div>
                        <?php echo Form::close(); ?>


                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

  
<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {

            _formValidation();
            
        });
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', ['title' => __('case.Update Case Category')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/category/case/edit.blade.php ENDPATH**/ ?>