<div class="">
    <!-- table-responsive -->
    <table class="table Crm_table_active3">
        <thead>
        <tr>
            <th scope="col"><?php echo e(__('common.SL')); ?></th>
            <th scope="col"><?php echo e(__('common.Name')); ?></th>
            <th scope="col"><?php echo e(__('common.Status')); ?></th>
            <th scope="col"><?php echo e(__('common.Action')); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $LeaveTypeList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <th><?php echo e($key + 1); ?></th>
                <td><?php echo e($item->name); ?></td>
                <td class="pending">
                    <?php if($item->status == 0): ?>
                        <h6><span class="badge_4"><?php echo e(__('common.DeActive')); ?></span></h6>
                    <?php else: ?>
                        <h6><span class="badge_1"><?php echo e(__('common.Active')); ?></span></h6>
                    <?php endif; ?>
                </td>
                <td>
                    <!-- shortby  -->
                    <div class="dropdown CRM_dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <?php echo e(__('common.Select')); ?>

                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                            <?php if(permissionCheck('leave_types.edit')): ?>
                                <a href="#" class="dropdown-item edit_brand"
                                   onclick="editItem(<?php echo e($item); ?>)"><?php echo e(__('common.Edit')); ?></a>
                            <?php endif; ?>
                            <?php if(permissionCheck('leave_types.delete')): ?>
                                <a href="#" class="dropdown-item edit_brand"
                                   onclick="showDeleteModal(<?php echo e($item->id); ?>)"><?php echo e(__('common.Delete')); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- shortby  -->
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/leave_types/components/list.blade.php ENDPATH**/ ?>