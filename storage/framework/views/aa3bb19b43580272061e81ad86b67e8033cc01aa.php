<?php $__env->startSection('mainContent'); ?>
    <section class="mb-40 student-details">
        <?php if(session()->has('message-success')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('message-success')); ?>

            </div>
        <?php elseif(session()->has('message-danger')): ?>
            <div class="alert alert-danger">
                <?php echo e(session()->get('message-danger')); ?>

            </div>
        <?php endif; ?>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-3">
                    <!-- Start Student Meta Information -->
                    <div class="main-title">
                        <h3 class="mb-20"><?php echo app('translator')->get('lawyer.Lawyer Info'); ?></h3>
                    </div>
                    <div class="student-meta-box">
                        <div class="student-meta-top"></div>
                        <img class="student-meta-img img-100"
                             src="<?php echo e(asset('frontend/img/user.png')); ?>"
                             alt="">
                        <div class="white-box">
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('lawyer.Name')); ?>

                                    </div>
                                    <div class="value">
                                        <?php if(isset($model)): ?><?php echo e(@$model->name); ?><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('lawyer.Mobile')); ?>

                                    </div>
                                    <div class="value">
                                        <?php if(isset($model)): ?><?php echo e(@$model->mobile_no); ?><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Student Meta Information -->
                </div>
                <!-- Start Student Details -->
                <div class="col-lg-9 staff-details">
                    <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#studentProfile" role="tab"
                               data-toggle="tab"><?php echo e(__('lawyer.Profile')); ?></a>
                        </li>

                        <li class="nav-item edit-button">
                        <?php if(permissionCheck('lawyer.edit')): ?>
                            <a href="<?php echo e(route('lawyer.edit', $model->id)); ?>" class="primary-btn small fix-gr-bg"
                               ><?php echo e(__('common.Edit')); ?>

                            </a>
                        <?php endif; ?>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start Profile Tab -->
                        <div role="tabpanel" class="tab-pane fade show active" id="studentProfile">
                            <div class="white-box">
                                <h4 class="stu-sub-head"><?php echo e(__('lawyer.Personal Info')); ?></h4>
                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('lawyer.Name')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->name); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('lawyer.Mobile')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->mobile_no); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('lawyer.Description')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo $model->description; ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if(moduleStatusCheck('CustomField') and $model->customFields): ?>
                                    <?php if ($__env->exists('customfield::details.show', ['customFields' => $model->customFields])) echo $__env->make('customfield::details.show', ['customFields' => $model->customFields], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php endif; ?>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('lawyer.Lawyer Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/lawyer/show.blade.php ENDPATH**/ ?>