<?php $__env->startSection('mainContent'); ?>

<section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('case.Lobbying List')); ?> | <?php echo e(__('case.Date')); ?> :<?php echo e(formatDate($date)); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-3">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::open(['route' => 'lobbying.index', 'method' => 'get']); ?>


                            <div class="row">
                                <div class="col">
                                    <div class="primary_input mb-15">
                                        <label class="primary_input_label" for=""><?php echo e(__('common.Date From')); ?></label>
                                        <div class="primary_datepicker_input">
                                            <div class="no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="">
                                                    <?php echo e(Form::text('date', date('Y-m-d'), ['class' => 'primary_input_field primary-input date form-control', "id"=>"fromDate",'placeholder' => __('common.Date')])); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col mt-4">
                                    <div class="primary_input mt-3">
                                        <button type="submit" class="primary-btn fix-gr-bg" id="submit" value="submit" style="width: 100%;"><i class="ti-search"></i><?php echo e(__('common.Get List')); ?></button>
                                    </div>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                    </div>
                </div>
                <div class="col-12 mt-15">
                        <div class="box_header common_table_header">
                            <div class="main-title d-md-flex">
                                <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('case.Lobbying')); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="QA_section QA_section_heading_custom check_box_table">
                            <div class="QA_table ">
                                <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                        <tr>
                                            <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                            <th><?php echo e(__('case.Case')); ?></th>
                                            <th><?php echo e(__('case.Client')); ?></th>
                                            <th><?php echo e(__('case.Details')); ?></th>
                                            <th><?php echo e(__('case.Date')); ?></th>
                                            <th><?php echo e(__('case.Status')); ?></th>
                                            <th class="noprint"><?php echo e(__('common.Actions')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td>
                                                <b><?php echo e(__('case.Case No')); ?>: </b>
                                                <?php echo e($model->cases->case_category? $model->cases->case_category->name : ''); ?>/<?php echo e($model->cases->case_no); ?>

                                                <br>
                                                <a href="<?php echo e(route('category.case.show', $model->cases->case_category_id)); ?>"><b><?php echo e(__('case.Category')); ?>:
                                                    </b> <?php echo e($model->cases->case_category? $model->cases->case_category->name : ''); ?></a>
                                                <br>
                                                <a href="<?php echo e(route('case.show', $model->cases->id)); ?>"><b><?php echo e(__('case.Case No')); ?><?php echo e(__('case.Title')); ?>:
                                                    </b><?php echo e($model->cases->title); ?>

                                                </a>
                                                <br>
                                                <b><?php echo e(__('case.Next Hearing Date')); ?>: </b> <?php echo e(formatDate($model->cases->hearing_date)); ?> <br>
                                                <b><?php echo e(__('case.Filing Date')); ?>: </b> <?php echo e(formatDate($model->cases->filling_date)); ?>

                                            </td>
                                            <td>
                                                <?php if($model->cases->client == 'Plaintiff' and $model->cases->plaintiff_client): ?>
                                                <a href="<?php echo e(route('client.show', $model->cases->plaintiff_client->id)); ?>"><b><?php echo e(__('case.Name')); ?></b>:
                                                    <?php echo e($model->cases->plaintiff_client->name); ?></a> <br>
                                                <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->cases->plaintiff_client->mobile); ?> <br>
                                                <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->cases->plaintiff_client->email); ?> <br>
                                                <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->cases->plaintiff_client->address); ?>

                                                <?php echo e($model->cases->plaintiff_client->city ? ', '. $model->cases->plaintiff_client->city->name : ''); ?>

                                                <?php echo e($model->cases->plaintiff_client->state ? ', '. $model->cases->plaintiff_client->state->name : ''); ?>

                                                <?php elseif($model->cases->client == 'Opposite' and $model->cases->opposite_client): ?>
                                                <a href="<?php echo e(route('client.show', $model->cases->opposite_client->id)); ?>"><b><?php echo e(__('case.Name')); ?></b>:
                                                    <?php echo e($model->cases->opposite_client->name); ?></a> <br>
                                                <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->cases->opposite_client->mobile); ?> <br>
                                                <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->cases->opposite_client->email); ?> <br>
                                                <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->cases->opposite_client->address); ?>

                                                <?php echo e($model->cases->opposite_client->city ? ', '. $model->cases->opposite_client->city->name : ''); ?>

                                                <?php echo e($model->cases->opposite_client->state ? ', '. $model->cases->opposite_client->state->name : ''); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if($model->cases->court): ?>
                                                <a href="<?php echo e(route('master.court.show', $model->cases->court_id)); ?>"><b><?php echo e(__('case.Court')); ?></b>:
                                                    <?php echo e($model->cases->court->name); ?> </a><br>
                                                <a href="<?php echo e(route('category.court.show', $model->cases->court_category_id)); ?>">
                                                    <b><?php echo e(__('case.Category')); ?></b>:
                                                    <?php echo e($model->cases->court->court_category ? $model->cases->court->court_category->name : ''); ?>

                                                </a><br>
                                                <b><?php echo e(__('case.Room No')); ?>: </b> <?php echo e($model->cases->court->room_number); ?> <br>
                                                <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->cases->court->location); ?>

                                                <?php echo e($model->cases->court->city ? ', '. $model->cases->court->city->name : ''); ?>

                                                <?php echo e($model->cases->court->state ? ', '. $model->cases->court->state->name : ''); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php echo e(formatDate($model->date)); ?>

                                            </td>
                                            <td>
                                            <?php echo $model->status ? '<span class="badge_1"> Complete </span>' : '<span class="badge_4"> Incomplete </span>'; ?>


                                            </td>

                                            <td>


                                                <div class="dropdown CRM_dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                                                id="dropdownMenu2" data-toggle="dropdown"
                                                                aria-haspopup="true"
                                                                aria-expanded="false">
                                                            <?php echo e(__('common.Select')); ?>

                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">

                                                        <a href="<?php echo e(route('lobbying.show', [$model->id, 'case' => $model->cases_id])); ?>" class="dropdown-item"><i
                                                                class="icon-file-eye"></i> <?php echo e(__('common.View')); ?></a>
                                                        <?php if(!$model->judgement): ?>
                                                            <?php if(permissionCheck('lobbying.edit')): ?>
                                                            <a href="<?php echo e(route('lobbying.edit', [$model->id, 'case' => $model->cases_id])); ?>"
                                                                class="dropdown-item"><i
                                                                    class="icon-pencil mr-2"></i><?php echo e(__('common.Edit')); ?></a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('lobbying.destroy')): ?>
                                                            <a data-url="<?php echo e(route('lobbying.destroy', $model->id)); ?>"
                                                                class="dropdown-item" id="delete_item"><i
                                                                    class="icon-trash mr-2"></i><?php echo e(__('common.Delete')); ?></a>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        </div>
                                                    </div>


                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

<script>
    $(document).ready(function () {


    });

</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Lobbying')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/lobbying/index.blade.php ENDPATH**/ ?>