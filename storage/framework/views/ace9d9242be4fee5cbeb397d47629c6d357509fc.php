<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-4 holiday_setup mt-30">
                    <h3 class="mb-0 mr-30  "><?php echo e(__('holiday.Holiday Setup')); ?>

                        <?php if(permissionCheck('holiday.add')): ?>
                            <a href="#" data-toggle="modal" data-target="#year_add"
                               class="primary-btn small fix-gr-bg text-uppercase mb-2 bord-rad float-right">
                                <?php echo e(__('common.Add')); ?> <span class="ti-plus"></span>
                            </a>
                        <?php endif; ?>

                    </h3>
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table">
                            <!-- table-responsivaae -->
                            <div class="">
                                <?php echo $__env->make('leave::holiday_setup.components.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 mt-30">

                    <?php if(isset($edit)): ?>
                     <?php if(permissionCheck('year.data')): ?>
                        <div class="white_box">
                            <div class="row p-0">
                                <div class="col-lg-12">
                                    <div class="primary_input mb-15">
                                        <label class="primary_input_label"><?php echo e(__('holiday.Holiday Copy From')); ?></label>
                                        <select name="year" onchange="lastYearData()" class="primary_select last_year">
                                            <?php if(isset($holiday)): ?>
                                                <?php $__currentLoopData = $years->where('year','!=',$holiday->year); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($year->year); ?>"><?php echo e($year->year); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('year')); ?></span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <form class="" action="<?php echo e(route('holidays.store')); ?>" method="POST"><?php echo csrf_field(); ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="primary_input mb-15">
                                            <label class="primary_input_label"
                                                   for=""><?php echo e(__('attendance.Select Year')); ?></label>
                                            <input type="text" name="year" class="primary_input_field"
                                                   value="<?php echo e(isset($holiday) ? $holiday->year : ''); ?>">

                                            <span class="text-danger"><?php echo e($errors->first('year')); ?></span>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-borderless">
                                    <tbody class="holiday_table">
                                    <?php echo $__env->make('leave::holiday_setup.components.row', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php if(session()->has('holidays')): ?>
                                        <?php echo $__env->make('leave::holiday_setup.components.session', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php else: ?>
                                        <?php echo $__env->make('leave::holiday_setup.components.edit', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                                <div class="row justify-content-center mt-2">
                                    <button type="submit"
                                            class="primary-btn btn-sm fix-gr-bg"><?php echo e(__('holiday.Submit')); ?></button>
                                </div>
                            </form>
                        </div>
                        <?php endif; ?>
                    <?php else: ?>
                    <?php if(permissionCheck('holidays.index')): ?>
                        <?php if($holiday): ?>
                            <?php echo $__env->make('leave::holiday_setup.components.view', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>
            </div>

        </div>

        <div class="modal fade admin-query" id="year_add">
            <div class="modal-dialog modal_800px modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><?php echo e(__('holiday.Add New Year')); ?></h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="ti-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form enctype="multipart/form-data" action="<?php echo e(route('holiday.add')); ?>" method="post"><?php echo csrf_field(); ?>
                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="primary_input mb-25">
                                        <label class="primary_input_label"
                                               for=""><?php echo e(__('holiday.Year')); ?> *</label>
                                        <input name="year" class="primary_input_field"
                                               placeholder="<?php echo e(__('holiday.Year')); ?>"
                                               type="text" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <div class="d-flex justify-content-center pt_20">
                                        <button type="submit" class="primary-btn semi_large2 fix-gr-bg"><i
                                                class="ti-check"></i>
                                            <?php echo e(__('common.Save')); ?>

                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script>
        function addRow() {
            $('.holiday_table tr:last').after(`<tr class="add_row">
        <td>
            <div class="primary_input mb-15">
                <label class="primary_input_label"
                       for=""><?php echo e(__('holiday.Holiday Name')); ?></label>
                <input type="text" name="holiday_name[]" class="primary_input_field"
                       placeholder="<?php echo e(__('holiday.Holiday Name')); ?>">
            </div>
        </td>
        <td>
            <div class="primary_input mb-15">
                <label class="primary_input_label"
                       for=""><?php echo e(__('holiday.Select Type')); ?> *</label>
                <select class="primary_select mb-15 type" name="type[]">
                    <option value="0"><?php echo e(__('holiday.Single Day')); ?></option>
                    <option value="1"><?php echo e(__('holiday.Multiple Day')); ?></option>
                </select>
            </div>
        </td>
        <td>
            <div class="single_date">
                <div class="primary_input mb-15">
                    <label class="primary_input_label" for=""><?php echo e(__('sale.Date')); ?>

            *</label>
        <div class="primary_datepicker_input">
            <div class="no-gutters input-right-icon">
                <div class="col">
                    <div class="">
                        <input placeholder="${ trams('js.Date') }"
                               class="primary_input_field primary-input date form-control"
                               type="text" name="date[]"
                               value="<?php echo e(date('Y-m-d')); ?>" autocomplete="off">
                                </div>
                            </div>
                            <button class="" type="button">
                                <i class="ti-calendar"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="multiple_date" style="display: none">
                <div class="primary_input mb-15">
                    <label class="primary_input_label" for=""><?php echo e(__('holiday.Start Date')); ?>

            *</label>
        <div class="primary_datepicker_input">
            <div class="no-gutters input-right-icon">
                <div class="col">
                    <div class="">
                        <input placeholder="${ trans('js.Date') }"
                               class="primary_input_field primary-input date form-control"
                               type="text" name="start_date[]"
                               value="<?php echo e(date('Y-m-d')); ?>"
                                           autocomplete="off">
                                </div>
                            </div>
                            <button class="" type="button">
                                <i class="ti-calendar"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="primary_input mb-15">
                    <label class="primary_input_label" for=""><?php echo e(__('holiday.End Date')); ?>

            *</label>
        <div class="primary_datepicker_input">
            <div class="no-gutters input-right-icon">
                <div class="col">
                    <div class="">
                        <input placeholder="${ trans('js.Date') }"
                               class="primary_input_field primary-input date form-control"
                               type="text" name="end_date[]"
                               value="<?php echo e(date('Y-m-d')); ?>"
                                           autocomplete="off">
                                </div>
                            </div>
                            <button class="" type="button">
                                <i class="ti-calendar"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <a class="primary-btn primary-circle mt-30 delete_row fix-gr-bg"
               href="javascript:void(0)"> <i
                    class="ti-trash"></i></a>
        </td>
    </tr>`)
            $('select').niceSelect();
            $(".date").datepicker({
                format : 'yyyy-mm-dd',
                todayHighlight : true,
                autoclose : true,
            });

        }

        function changeYear() {
            let year = $('.year').val();

            $.ajax({
                url: "<?php echo e(route('add.row')); ?>",
                method: "POST",
                data: {
                    year: year,
                    _token: "<?php echo e(csrf_token()); ?>",
                },
                success: function (result) {
                    $(".add_row").each(function (index, element) {
                        element.remove();
                    });
                    $(".holiday_table").append(result);
                    $('select').niceSelect();
                }
            })
        }

        function lastYearData() {
            let year = $('.last_year').val();

            $.ajax({
                url: "<?php echo e(route('last.year.data')); ?>",
                method: "POST",
                data: {
                    year: year,
                    _token: "<?php echo e(csrf_token()); ?>",
                },
                success: function (result) {
                    $(".holiday_table").append(result);
                    $('select').niceSelect();
                }
            })
        }

        $(document).on('change', '.type', function () {
            let value = $(this).val();
            var whichtr = $(this).closest("tr");
            if (value == 0) {
                whichtr.find($('.single_date')).show();
                whichtr.find($('.multiple_date')).hide();
            } else {
                whichtr.find($('.single_date')).hide();
                whichtr.find($('.multiple_date')).show();
            }
        });

        $(document).on('click', '.delete_row', function () {
            var whichtr = $(this).closest("tr");
            whichtr.remove();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/holiday_setup/index.blade.php ENDPATH**/ ?>