    <!-- SMTP form  -->
    <div class="main-title mb-25">
        <h3 class="mb-0"><?php echo e(__('setting.SMTP Settings')); ?> <span class="badge_1"> <?php echo e((config('configs')->where('key','mail_protocol')->first()->value == "smtp") ? 'SMPT' : 'PHP Mail'); ?>  </span></h3>
    </div>

    <form action="<?php echo e(route('smtp_gateway_credentials_update')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="smtp_set" value="1">
        <div class="row">
            <div class="col-xl-12">
                <div class="primary_input">
                    <input type="hidden" name="types[]" value="MAIL_MAILER">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Email Protocol')); ?></label>
                    <select class="primary_select mb-25" name="mail_protocol" onchange="smtp_form()" id="mail_mailer">
                        <option value="smtp"<?php if(config('configs')->where('key','mail_protocol')->first()->value == "smtp"): ?> selected <?php endif; ?>>SMTP</option>
                        <option value="sendmail"<?php if(config('configs')->where('key','mail_protocol')->first()->value == "sendmail"): ?> selected <?php endif; ?>>PHP Mail</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_FROM_NAME">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.From Name')); ?>*</label>
                    <input class="primary_input_field" placeholder="Casewise CRM" type="text" name="MAIL_FROM_NAME" value="<?php echo e(env('MAIL_FROM_NAME')); ?>">
                </div>
            </div>

            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_FROM_ADDRESS">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.From Mail')); ?>*</label>
                    <input class="primary_input_field" placeholder="demo@infix.com" type="email" name="MAIL_FROM_ADDRESS" value="<?php echo e(env('MAIL_FROM_ADDRESS')); ?>">
                </div>
            </div>
        </div>
        <div class="row" id="smtp">

            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_HOST">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Mail Host')); ?></label>
                    <input class="primary_input_field" placeholder="-" type="text" name="MAIL_HOST" value="<?php echo e(env('MAIL_HOST')); ?>">
                </div>
            </div>

            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_PORT">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Mail Port')); ?></label>
                    <input class="primary_input_field" placeholder="-" type="text" name="MAIL_PORT" value="<?php echo e(env('MAIL_PORT')); ?>">
                </div>
            </div>

            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_USERNAME">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Mail Username')); ?></label>
                    <input class="primary_input_field" placeholder="-" type="text" name="MAIL_USERNAME" value="<?php echo e(env('MAIL_USERNAME')); ?>">
                </div>
            </div>

            <div class="col-xl-6">
                <div class="primary_input mb-25">
                    <input type="hidden" name="types[]" value="MAIL_PASSWORD">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Mail Password')); ?></label>
                    <input class="primary_input_field" placeholder="-" type="password" name="MAIL_PASSWORD" value="<?php echo e(env('MAIL_PASSWORD')); ?>">
                </div>
            </div>

            <div class="col-xl-6">
                <div class="primary_input">
                    <input type="hidden" name="types[]" value="MAIL_ENCRYPTION">
                    <label class="primary_input_label" for=""><?php echo e(__('setting.Mail Encryption')); ?></label>
                    <select name="MAIL_ENCRYPTION" class="primary_select mb-25">
                        <option value="ssl" <?php if(env('MAIL_ENCRYPTION') == "ssl"): ?> selected <?php endif; ?>>SSL</option>
                        <option value="tls" <?php if(env('MAIL_ENCRYPTION') == "tls"): ?> selected <?php endif; ?>>TLS</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12 mb-45 pt_15">
                <div class="submit_btn text-center">
                    <button class="primary_btn_large" type="submit"> <i class="ti-check"></i> <?php echo e(__('common.Save')); ?></button>
                </div>
            </div>
        </div>
    </form>

    <?php echo Form::open(['url' => route('test_mail.send'), 'method' => 'post', 'id' => "test_mail_send", 'files' =>true ]); ?>

        <?php echo csrf_field(); ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="primary_input mb-25">
                    <?php echo e(Form::label('email', __('setting.Send a Test Email to') , ['class' => 'primary_input_label required'])); ?>

                    <?php echo e(Form::email('email', null , ["class" => "primary_input_field", "placeholder" => __('setting.Send a Test Email to'), "required"])); ?>


                </div>
            </div>
            <div class="col-xl-12">
                <div class="primary_input mb-25">
                    <?php echo e(Form::label('content', __('setting.Mail Text') , ['class' => 'primary_input_label required'])); ?>

                    <?php echo e(Form::text('content', 'This is test mail' , ["class" => "primary_input_field", "placeholder" => __('setting.Mail Text'), "required"])); ?>

                </div>
            </div>
        </div>
        <div class="submit_btn text-center mb-100 pt_15">
                <button class="primary_btn_2 submit" type="submit"><i class="ti-check"></i><?php echo e(__('common.Send')); ?>

                </button>

                <button class="primary_btn_2 submitting" type="submit" disabled style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Sending')); ?>

                </button>
            </div>

    <?php echo Form::close(); ?>

<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Setting/Resources/views/page_components/smtp_setting.blade.php ENDPATH**/ ?>
