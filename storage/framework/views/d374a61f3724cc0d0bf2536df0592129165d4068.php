<?php $__env->startSection('mainContent'); ?>
    <div id="contact_settings">
        <section class="admin-visitor-area up_st_admin_visitor">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="box_header">
                            <div class="main-title d-flex">
                                <h3 class="mb-0 mr-30"><?php echo e(__('common.Settings')); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="white_box_50px box_shadow_white">
                            <!-- Prefix  -->
                            <form action="<?php echo e(route('client.settings')); ?>" method="POST" id="content_form">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-3 d-flex">
                                                <p class="text-uppercase fw-500 mb-10"><?php echo e(__('common.Login Permission')); ?> </p>
                                            </div>
                                            <div class="col-lg-9">

                                                <div class="radio-btn-flex ml-20">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="">
                                                                <input type="radio" name="client_login" id="enable" value="1" class="common-radio relationButton" <?php echo e((config('configs')->where('key','client_login')->first()->value) ? 'checked' : ''); ?> >
                                                                <label for="enable"><?php echo e(__('common.Enable')); ?></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="">
                                                                <input type="radio" name="client_login" id="disable" value="0" <?php echo e((config('configs')->where('key','client_login')->first()->value) ? '' : 'checked'); ?> class="common-radio relationButton" >
                                                                <label for="disable"><?php echo e(__('common.Disable')); ?></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 text-center">
                                                            <button class="primary-btn fix-gr-bg" id="_submit_btn_admission">
                                                                <span class="ti-check"></span>
                                                                <?php echo e(__('common.Save')); ?>

                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>
    <script>
        _formValidation();
    </script>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/ClientLogin/Resources/views/settings.blade.php ENDPATH**/ ?>