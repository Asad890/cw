<?php $__env->startSection('mainContent'); ?>
    <section class="mb-40 student-details">
        <?php if(session()->has('message-success')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('message-success')); ?>

            </div>
        <?php elseif(session()->has('message-danger')): ?>
            <div class="alert alert-danger">
                <?php echo e(session()->get('message-danger')); ?>

            </div>
        <?php endif; ?>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-3">
                    <!-- Start Student Meta Information -->
                    <div class="main-title">
                        <h3 class="mb-20"><?php echo app('translator')->get('client.Client Details'); ?></h3>
                    </div>
                    <div class="student-meta-box">
                        <div class="student-meta-top"></div>
                        <img class="student-meta-img img-100"
                             src="<?php echo e(file_exists($model->avatar) ? asset($model->avatar) : asset('public\backEnd/img/staff.jpg')); ?>"
                             alt="">
                        <div class="white-box">
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('client.Name')); ?>

                                    </div>
                                    <div class="value">
                                        <?php if(isset($model)): ?><?php echo e(@$model->name); ?><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('client.Mobile')); ?>

                                    </div>
                                    <div class="value">
                                        <?php if(isset($model)): ?><?php echo e(@$model->mobile); ?><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo e(__('client.Email')); ?>

                                    </div>
                                    <div class="value">
                                        <?php if(isset($model)): ?><?php echo e(@$model->email); ?><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Student Meta Information -->
                </div>
                <!-- Start Student Details -->
                <div class="col-lg-9 staff-details">
                    <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#studentProfile" role="tab"
                               data-toggle="tab"><?php echo e(__('client.Profile')); ?></a>
                        </li>

                        <?php if(moduleStatusCheck('Finance')): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="#invoicesTab" role="tab"
                                   data-toggle="tab"><?php echo e(__('finance.Invoices')); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#transactionsTab" role="tab"
                                   data-toggle="tab"><?php echo e(__('finance.Transactions')); ?></a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item edit-button">
                            <a href="<?php echo e(route('client.edit', $model->id)); ?>" class="primary-btn small fix-gr-bg"
                            ><?php echo e(__('common.Edit')); ?>

                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start Profile Tab -->
                        <div role="tabpanel" class="tab-pane fade show active" id="studentProfile">
                            <div class="white-box">
                                <h4 class="stu-sub-head"><?php echo e(__('client.Client Info')); ?></h4>
                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Name')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->name); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Mobile')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->mobile); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Email')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->email); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Client Category')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model->category)): ?><?php echo e(@$model->category->name); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Address')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo e($model->address); ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Country')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php echo e($model->country ? $model->country->name : ''); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.State')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php echo e($model->state ? $model->state->name : ''); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.City')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php echo e($model->city ? $model->city->name : ''); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <?php echo e(__('client.Description')); ?>

                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php if(isset($model)): ?><?php echo $model->description; ?><?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if(moduleStatusCheck('CustomField') and $model->customFields): ?>
                                    <?php if ($__env->exists('customfield::details.show', ['customFields' => $model->customFields])) echo $__env->make('customfield::details.show', ['customFields' => $model->customFields], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php endif; ?>


                            </div>
                        </div>


                        <?php if(moduleStatusCheck('Finance')): ?>
                            <div role="tabpanel" class="tab-pane fade" id="invoicesTab">
                                <div class="white-box">
                                    <div class="QA_section QA_section_heading_custom check_box_table">
                                        <div class="QA_table ">
                                    <table class="check_box_table table table-sm Crm_table_active ">
                                        <thead>
                                        <tr>
                                            <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                            <th><?php echo e(__('finance.Date')); ?></th>
                                            <th><?php echo e(__('finance.Invoice No')); ?></th>
                                            <th><?php echo e(__('case.Case')); ?></th>
                                            <th><?php echo e(__('finance.Total Amount')); ?></th>
                                            <th><?php echo e(__('finance.Paid')); ?></th>
                                            <th><?php echo e(__('finance.Due')); ?></th>
                                            <th class="d-print-none text-center"><?php echo e(__('common.Actions')); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $model->invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($loop->index + 1); ?></td>
                                                <td>
                                                    <?php echo e(formatDate($invoice->transaction_date)); ?>

                                                </td>
                                                <td>
                                                    <a href="<?php echo e(route('invoice.incomes.show', $invoice->id)); ?>"
                                                       target="_blank"> <?php echo e($invoice->invoice_no); ?> </a>
                                                </td>
                                                <td>

                                                    <?php if($invoice->case): ?>
                                                        <a href="<?php echo e(route('case.show', $invoice->case_id)); ?>" target="_blank"><?php echo e($invoice->case->title); ?></a>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php echo e(amountFormat($invoice->grand_total)); ?></td>
                                                <td><?php echo e(amountFormat($invoice->paid)); ?></td>
                                                <td><?php echo e(amountFormat($invoice->due)); ?></td>


                                                <td class="d-print-none text-center">

                                                    <div class="dropdown CRM_dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle"
                                                                type="button"
                                                                id="dropdownMenu2" data-toggle="dropdown"
                                                                aria-haspopup="true"
                                                                aria-expanded="false">
                                                            <?php echo e(__('common.Select')); ?>

                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right"
                                                             aria-labelledby="dropdownMenu2">
                                                            <?php if(permissionCheck('invoice.incomes.show')): ?>
                                                                <a href="<?php echo e(route('invoice.incomes.show', $invoice->id)); ?>"
                                                                   class="dropdown-item edit_brand"><?php echo e(__('common.Show')); ?></a>
                                                            <?php endif; ?>

                                                            <?php if(permissionCheck('invoice.incomes.show')): ?>
                                                                <a
                                                                    class="dropdown-item edit_brand print_window"
                                                                    href="<?php echo e(route('invoice.print',$invoice->id)); ?>"><?php echo e(__('common.Print')); ?></a>
                                                            <?php endif; ?>

                                                            <?php if($invoice->due >  0 and permissionCheck('invoice.payment.add')): ?>
                                                                <a class="dropdown-item edit_brand btn-modal"
                                                                   data-container="payment_modal"
                                                                   href="<?php echo e(route('invoice.payment.add',$invoice->id)); ?>"><?php echo e(__('finance.Add Payment')); ?></a>
                                                            <?php endif; ?>

                                                            <?php if(permissionCheck('invoice.incomes.edit')): ?>
                                                                <a href="<?php echo e(route('invoice.incomes.edit', $invoice->id)); ?>"
                                                                   class="dropdown-item edit_brand"><?php echo e(__('common.Edit')); ?></a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('invoice.incomes.destroy')): ?>
                                                                <span id="delete_item"
                                                                      data-id="<?php echo e($invoice->id); ?>"
                                                                      data-url="<?php echo e(route('invoice.incomes.destroy', $invoice->id)); ?>"
                                                                      class="dropdown-item"><i
                                                                        class="icon-trash"></i>
                                                                        <?php echo e(__('common.Delete')); ?> </span>
                                                            <?php endif; ?>


                                                        </div>
                                                    </div>


                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="transactionsTab">
                                <div class="white-box">

                                    <?php
                                        $model->invoice_type = 'income';
                                    ?>
                                    <?php if ($__env->exists('finance::invoice.payment_table', ['dataTable' => 'Crm_table_active '])) echo $__env->make('finance::invoice.payment_table', ['dataTable' => 'Crm_table_active '], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php if(moduleStatusCheck('Finance')): ?>
    <div class="modal fade animated payment_modal infix_biz_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', ['title' => __('client.Client Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/client/show.blade.php ENDPATH**/ ?>