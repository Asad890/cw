<?php $__env->startSection('mainContent'); ?>


<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                <?php if(isset($role)): ?>
                                    <?php echo app('translator')->get('common.Edit'); ?>
                                <?php else: ?>
                                    <?php echo app('translator')->get('common.Add'); ?>
                                <?php endif; ?>
                                    <?php echo app('translator')->get('role.Role'); ?>
                            </h3>
                        </div>
                        <?php if(isset($role)): ?>
                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => route('permission.roles.update',$role->id),'method' => 'PUT'])); ?>

                        <?php else: ?>
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'url' => route('permission.roles.store')])); ?>

                        <?php endif; ?>
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <?php if(session()->has('message-success')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger')); ?>

                                        </div>
                                        <?php endif; ?>
                                        <div class="input-effect">
                                            <label><?php echo app('translator')->get('role.Name'); ?> <span>*</span></label>
                                            <input class="primary_input_field form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>"
                                                type="text" name="name" autocomplete="off" value="<?php echo e(isset($role)? @$role->name: ''); ?>" required="1">
                                            <input type="hidden" name="id" value="<?php echo e(isset($role)? @$role->id: ''); ?>">
                                            <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="input-effect">
                                            <label>Price</label>
                                            <input class="primary_input_field form-control<?php echo e($errors->has('price') ? ' is-invalid' : ''); ?>"
                                                type="text" name="price" autocomplete="off" value="<?php echo e(isset($role)? @$role->price: ''); ?>" required="1">
                                            <?php if($errors->has('price')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('price')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="input-effect d-none">
                                            <div class="primary_input mb-25">
                                                <label class="primary_input_label" for=""><?php echo e(__('role.Type')); ?> *</label>
                                                <select class="primary_select mb-25" name="type" id="type" required>
                                                    <?php if(isset($role)): ?>
                                                        <option value="system_user"<?php if($role->type == "system_user"): ?> selected <?php endif; ?>><?php echo e(__('role.System User')); ?></option>
                                                        <option value="regular_user"<?php if($role->type == "regular_user"): ?> selected <?php endif; ?>><?php echo e(__('role.Regular User')); ?></option>
                                                    <?php else: ?>
                                                        <option value="system_user"><?php echo e(__('role.System User')); ?></option>
                                                        <option value="regular_user" selected><?php echo e(__('role.Regular User')); ?></option>
                                                    <?php endif; ?>
                                                </select>
                                                <?php if($errors->has('type')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('type')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    $tooltip = "";

                                ?>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <?php if(permissionCheck('permission.roles.edit') || permissionCheck('permission.roles.store')): ?>
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="<?php echo e(@$tooltip); ?>">
                                            <span class="ti-check"></span>
                                            <?php echo e(!isset($role)? 'save' : 'update'); ?>


                                        </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0"><?php echo app('translator')->get('common.Role'); ?> <?php echo app('translator')->get('common.List'); ?></h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <div class="QA_section QA_section_heading_custom check_box_table SplitDivTable">
                            <div class="QA_table ">
                                <!-- table-responsive -->
                                <div class="mt-30">
                                <table class="table Crm_table_active3">
                                        <thead>
                                           <?php echo $__env->make('backEnd.partials.alertMessagePageLevelAll', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                            <tr>
                                                <th width="30%"><?php echo app('translator')->get('role.Role'); ?></th>
                                                <th width="30%"><?php echo app('translator')->get('role.Type'); ?></th>
                                                <th width="40%"><?php echo app('translator')->get('role.Action'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $RoleList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e(@$role->name); ?></td>
                                                <td><?php echo e(str_replace('_', ' ', @$role->type)); ?></td>
                                                <td>
                                                    <div class="d-flex align-items-center flex-wrap">
                                                        <div class="dropdown CRM_dropdown d-inline ml-1">
                                                        <button class="btn btn-secondary dropdown-toggle mb-1" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                                                            <?php if($role->id > 2): ?>
                                                                <?php if(permissionCheck('permission.roles.edit')): ?>
                                                                <a href="<?php echo e(route('permission.roles.edit',$role->id)); ?>" class="dropdown-item" type="button"><?php echo app('translator')->get('common.Edit'); ?></a>
                                                                <?php endif; ?>

                                                                <?php if(permissionCheck('permission.roles.destroy')): ?>
                                                                <a href="#"  class="dropdown-item"  type="button" data-toggle="modal" href="#" data-id="<?php echo e(@$role->id); ?>" data-target="#deleteItem_<?php echo e(@$role->id); ?>"><?php echo app('translator')->get('role.Delete'); ?></a>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <a href="javascript:void(0)" class="dropdown-item"> <?php echo app('translator')->get('role.System Role'); ?> </a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <!-- shortby  -->
                                                    <?php if(@$role->id != 1): ?>
                                                        <a href="<?php echo e(route('permission.permissions.index', [ 'id' => @$role->id])); ?>" class="ml-1"   >
                                                            <button type="button" class="primary-btn small fix-gr-bg"> <?php echo app('translator')->get('role.assign_permission'); ?> </button>
                                                        </a>
                                                    <?php endif; ?>
                                                    </div>

                                                </td>
                                                
                                                <?php echo $__env->make('backEnd.partials.deleteModalMessage',[
                                                    'item_id' => @$role->id,
                                                    'item_name' => 'Role',
                                                    'route_url' => route('permission.roles.destroy',$role->id)], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', ['title' =>'Role'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\Modules/RolePermission\Resources/views/role.blade.php ENDPATH**/ ?>