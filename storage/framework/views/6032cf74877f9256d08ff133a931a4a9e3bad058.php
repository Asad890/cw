<?php
    Illuminate\Support\Facades\Cache::rememberForever('languages', function() {
       return \Modules\Localization\Entities\Language::where('status', 1)->get();
    });
?>
<div class="container-fluid no-gutters d-print-none">
	<!-- <div class="row">
			<div class="col-lg-12 p-0">
				<div class="marquee-container">
					<div class="Marquee">
					  <div class="Marquee-content">
						<div class="Marquee-tag">1</div>
						<div class="Marquee-tag">2</div>
						<div class="Marquee-tag">3</div>
						<div class="Marquee-tag">1</div>
						<div class="Marquee-tag">2</div>
						<div class="Marquee-tag">3</div>
						<div class="Marquee-tag">1</div>
						<div class="Marquee-tag">2</div>
						<div class="Marquee-tag">3</div>
						<div class="Marquee-tag">1</div>
						<div class="Marquee-tag">2</div>
						<div class="Marquee-tag">3</div>
					  </div>
					</div>
					</div>
			</div>
	</div> -->
    <div class="row">
        <div class="col-lg-12 p-0">
            <div class="header_iner d-flex justify-content-between align-items-center">
                <div class="small_logo_crm d-lg-none">
                    <a href="<?php echo e(url('/home')); ?>"> <img
                            src="<?php echo e(asset(config('configs')->where('key', 'site_logo')->first()->value)); ?>" alt=""></a>
                </div>
                <div id="sidebarCollapse" class="sidebar_icon  d-lg-none">
                    <i class="ti-menu"></i>
                </div>
                <div class="collaspe_icon open_miniSide">
                    <i class="ti-menu"></i>
                </div>
                <div class="serach_field-area ml-40">
                    <?php if(auth()->user()->role_id): ?>

                        <div class="search_inner">
                            <form action="#">
                                <div class="search_field">
                                    <input type="text" placeholder="<?php echo e(__('common.Search')); ?>" id="search"
                                           onkeyup="showResult(this.value)">
                                </div>
                                  
                            </form>
                        </div>
                        <div id="livesearch"></div>

                    <?php endif; ?>
                </div>
                <div class="header_middle d-none d-md-block">
                    <?php if(auth()->user()->role_id): ?>
                        <div class="select_style d-flex">

                            <?php
                                if(session()->has('locale')){
                                    $locale = session()->get('locale');
                                }
                                else{

                                    session()->put('locale', config('configs')->where('key','language_name')->first()->value);
                                    $locale = session()->get('locale');
                                }
                            ?>

                            <!-- <select name="code" id="language_code" class="nice_Select bgLess mb-0"
                                    onchange="change_Language()">
                                <?php $__currentLoopData = Illuminate\Support\Facades\Cache::get('languages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($language->code); ?>"
                                            <?php if($locale == $language->code): ?> selected <?php endif; ?>><?php echo e($language->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select> -->
                        </div>
                    <?php endif; ?>
                </div>
                <div class="header_right d-flex justify-content-between align-items-center">
                    <div class="header_notification_warp d-flex align-items-center">
						<!-- Notificaiton -->
							<div class="dropdown">
								<div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="badge badge-primary">3</span>
									<i class="ti-bell text-muted header-icon"></i>
								</div>
								<!-- Notification dropdown -->
								<div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
									
									
									<div class="dropdown-item d-flex">										 
										<div class="notification-details flex-grow-1">
                                            <div class="row">
                                                <div class="col-3">
                                                    
                                                </div>
                                                <div class="col-9" style="background-color:#D4F5E6; border: 2px solid #83DBA3; border-radius: 10px;">
                                                    <div class="clearfix">
                                                        <!-- <div class="pull-right">$50</div> -->
                                                        <p class="pull-right" style="color:#83DBA3 ;">Search Result for "Ashish" are Ready</p>
                                                    </div>
                                                </div>
                                            </div>
											<p class="m-0 d-flex align-items-center">
												<span class="badge badge-pill badge-primary">10:52 PM</span>
											</p>
											<p class="m-0 d-flex align-items-center">												  
												<span><small>Party Name :</small> Ashish</span>												
												<span class="flex-grow-1"></span>
												<span class="text-small text-muted ml-auto"><small> Year </small>
													<span class="text-small text-info"> 2015</span></span>
											</p>
											<p class="text-small  m-0">You have found 0 results for your resent case search</p>
											<p class="text-small text-muted">
												Maharashtra District Courts
											</p>
										</div>
									</div>
									
									<div class="dropdown-item d-flex">										 
										<div class="notification-details flex-grow-1">
                                        <div class="row">
                                                <div class="col-3">
                                                    
                                                </div>
                                                <div class="col-9" style="background-color:#D4F5E6; border: 2px solid #83DBA3; border-radius: 10px;">
                                                    <div class="clearfix">
                                                        <!-- <div class="pull-right">$50</div> -->
                                                        <p class="pull-right" style="color:#83DBA3 ;">Search Result for "Ashish" are Ready</p>
                                                    </div>
                                                </div>
                                            </div>
											<p class="m-0 d-flex align-items-center">
												<span class="badge badge-pill badge-primary">10:52 PM</span>
											</p>
											<p class="m-0 d-flex align-items-center">												  
												<span><small>Party Name :</small> Ashish</span>												
												<span class="flex-grow-1"></span>
												<span class="text-small text-muted ml-auto"><small> Year </small>
													<span class="text-small text-info"> 2015</span></span>
											</p>
											<p class="text-small  m-0">You have found 0 results for your resent case search</p>
											<p class="text-small text-muted">
												Maharashtra District Courts
											</p>
										</div>
									</div>
									 
									 <div class="dropdown-item d-flex">										 
										<div class="notification-details flex-grow-1">
                                        <div class="row">
                                                <div class="col-3">
                                                    
                                                </div>
                                                <div class="col-9" style="background-color:#D4F5E6; border: 2px solid #83DBA3; border-radius: 10px;">
                                                    <div class="clearfix">
                                                        <!-- <div class="pull-right">$50</div> -->
                                                        <p class="pull-right" style="color:#83DBA3 ;">Search Result for "Ashish" are Ready</p>
                                                    </div>
                                                </div>
                                            </div>
											<p class="m-0 d-flex align-items-center">
												<span class="badge badge-pill badge-primary">10:52 PM</span>
											</p>
											<p class="m-0 d-flex align-items-center">												  
												<span><small>Party Name :</small> Ashish</span>												
												<span class="flex-grow-1"></span>
												<span class="text-small text-muted ml-auto"><small> Year </small>
													<span class="text-small text-info"> 2017</span></span>
											</p>
											<p class="text-small  m-0">You have found 0 results for your resent case search</p>
											<p class="text-small text-muted">
												Maharashtra District Courts
											</p>
										</div>
									</div>
									 
									 
								</div>
							</div>
							<!-- Notificaiton End -->

                    </div>


                    <div class="profile_info">

                        <div class="userThumb_40"  style="background-image: url('<?php echo e(file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public/backEnd/img/staff.jpg')); ?>')">

                        </div>
                        <div class="profile_info_iner">
                            <div class="use_info d-flex align-items-center">
                                <div class="thumb">
                                    <div class="userThumb_50" style="background-image: url('<?php echo e(file_exists(auth()->user()->avatar) ? asset(auth()->user()->avatar) : asset('public/backEnd/img/staff.jpg')); ?>')">

                                    </div>

                                </div>
                                <div class="user_text">
                                    <h5><a href="<?php echo e(route('profile_view')); ?>"><?php echo e(auth()->user()->name); ?></a></h5>
                                    <span><?php echo e(auth()->user()->email); ?></span>

                                </div>
                            </div>

                            <div class="profile_info_details">
                                <?php if(permissionCheck('setting.index')): ?>
                                    <a href="<?php echo e(route('setting.index')); ?>"> <i
                                            class="ti-settings"></i>
                                        <span><?php echo e(__('common.Setting')); ?></span>
                                    </a>
                                <?php endif; ?>
                                <?php if(auth()->user()->role_id): ?>
                                    <a href="<?php echo e(route('profile_view')); ?>">
                                        <i class="ti-user"></i>
                                        <span><?php echo e(__('common.Profile')); ?></span>
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('client.my_profile')); ?>">
                                        <i class="ti-user"></i>
                                        <span><?php echo e(__('common.Profile')); ?></span>
                                    </a>
                                <?php endif; ?>

                                <a href="<?php echo e(route('change_password')); ?>">
                                    <i class="ti-key"></i>
                                    <span><?php echo e(__('common.Change Password')); ?></span>
                                </a>

                                <a href="<?php echo e(route('logout')); ?>" id="logout">
                                    <i class="ti-unlock"></i>
                                    <span><?php echo e(__('common.Logout')); ?></span>
                                </a>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
        function change_Language() {
            var code = $('#language_code').val();
            $.post('<?php echo e(route('language.change')); ?>', {_token: '<?php echo e(csrf_token()); ?>', code: code}, function (data) {

                if (data.success) {
                    location.reload();
                    toastr.success(data.success);
                } else {
                    toastr.error(data.error);
                }
            });
        }
    </script>
<?php $__env->stopPush(); ?>

<?php /**PATH D:\updates 123\cw\resources\views/partials/menu.blade.php ENDPATH**/ ?>