<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('court.New Court')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">


                        <?php echo Form::open(['route' => 'master.court.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-4">
                                <?php echo e(Form::label('country_id', __('court.Country'))); ?>

                                <?php echo e(Form::select('country_id', $countries, config('configs')->where('key','country_id')->first()->value, ['class' => 'primary_select', 'id' => 'country_id', 'data-placeholder' => __('court.Select country'),  'data-parsley-errors-container' => '#country_id_error'])); ?>

                                <span id="country_id_error"></span>
                            </div>

                            <div class="primary_input col-md-4">
                                <?php echo e(Form::label('state_id', __('court.State'))); ?>

                                <?php echo e(Form::select('state_id', $states, null, ['class' => 'primary_select','id' => 'state_id', 'data-placeholder' => __('court.Select state'), 'data-parsley-errors-container' => '#state_id_error'])); ?>

                                <span id="state_id_error"></span>
                            </div>

                            <div class="primary_input col-md-4">
                                <?php echo e(Form::label('city_id', __('court.City'))); ?>

                                <?php echo e(Form::select('city_id',[''=> __('common.Select State First')], null, ['class' => 'primary_select','id' => 'city_id', 'data-placeholder' => __('court.Select city'), 'data-parsley-errors-container' => '#city_id_error'])); ?>

                                <span id="city_id_error"></span>
                            </div>

                        </div>
                        <div class="row">

                            <div class="primary_input col-md-6">

                                <div class="d-flex justify-content-between">
                                    <?php echo e(Form::label('court_category_id', __('court.Court Category'), ['class' => 'required'])); ?>

                                    <?php if(permissionCheck('category.court.store')): ?>
                                        <label class="primary_input_label green_input_label" for="">
                                            <a href="<?php echo e(route('category.court.create', ['quick_add' => true])); ?>"
                                               class="btn-modal"
                                               data-container="court_category_add_modal"><?php echo e(__('case.Create New')); ?>

                                                <i class="fas fa-plus-circle"></i></a></label>
                                    <?php endif; ?>
                                </div>
                                <?php echo e(Form::select('court_category_id', $court_categories, null, ['class' => 'primary_select', 'data-placeholder' => __('court.Court Category'), 'data-parsley-errors-container' => '#court_category_error', 'required'])); ?>

                                <span id="court_category_error"></span>
                            </div>
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('location', __('court.Location/ Police Station'))); ?>

                                <?php echo e(Form::text('location', null, ['class' => 'primary_input_field', 'placeholder' => __('court.Location/ Police Station')])); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('name', __('court.Court Name'),['class' => 'required'])); ?>

                                <?php echo e(Form::text('name', null, ['required' => '', 'class' => 'primary_input_field', 'placeholder' => __('court.Court Name')])); ?>

                            </div>
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('room_number', __('court.Court Room Number'))); ?>

                                <?php echo e(Form::text('room_number', null, ['class' => 'primary_input_field', 'placeholder' => __('court.Court Room Number')])); ?>

                            </div>
                        </div>
                        <?php if(moduleStatusCheck('EmailtoCL')): ?>
                            <div class="row">

                                <div class="primary_input col-12">
                                    <?php echo e(Form::label('email', __('case.Email'))); ?>

                                    <?php echo e(Form::email('email', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Email')])); ?>

                                </div>

                            </div>
                        <?php endif; ?>
                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('court.Description'))); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                            '#description_error' ])); ?>

                            <span id="description_error"></span>
                        </div>

                        <div class="text-center mt-3">
                            <button class="primary-btn semi_large2 fix-gr-bg submit" type="submit"><i
                                    class="ti-check"></i><?php echo e(__('common.Create')); ?>

                            </button>

                            <button class="primary-btn semi_large2 fix-gr-bg submitting" type="submit" disabled
                                    style="display: none;"><i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                            </button>

                        </div>
                        <?php echo Form::close(); ?>



                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade animated court_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function () {
            _componentAjaxChildLoad('#content_form', '#country_id', '#state_id', 'state')
            _componentAjaxChildLoad('#content_form', '#state_id', '#city_id', 'city')
            _formValidation();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('court.Create New Court')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/master/court/create.blade.php ENDPATH**/ ?>