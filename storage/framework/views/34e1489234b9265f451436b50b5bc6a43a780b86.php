
<!-- table-responsive -->
    <table class="table Crm_table_active3">
        <thead>
        <tr>

            <th scope="col"><?php echo e(__('common.SL')); ?></th>
            <th scope="col"><?php echo e(__('role.Role')); ?></th>
            <th scope="col"><?php echo e(__('leave.Leave Type')); ?></th>
            <th scope="col"><?php echo e(__('leave.Total Days')); ?></th>
            <th scope="col"><?php echo e(__('leave.Max Forward Balance')); ?> (<?php echo e(__('leave.Days')); ?>)</th>
            <th scope="col"><?php echo e(__('common.Action')); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $LeaveDefineList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>

                <th><?php echo e($key + 1); ?></th>
                <td><?php echo e($item->role->name); ?></td>
                <td><?php echo e($item->leave_type->name); ?></td>
                <td><?php echo e($item->total_days); ?></td>
                <td><?php echo e($item->max_forward); ?></td>
                <td>
                    <!-- shortby  -->
                    <div class="dropdown CRM_dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <?php echo e(__('common.Select')); ?>

                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                            <a href="#" class="dropdown-item edit_brand" onclick="editLeaveDefine(<?php echo e($item); ?>)"><?php echo e(__('common.Edit')); ?></a>
                            <a href="#" class="dropdown-item edit_brand" onclick="showDeleteModal(<?php echo e($item->id); ?>)" ><?php echo e(__('common.Delete')); ?></a>
                        </div>
                    </div>
                    <!-- shortby  -->
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/page-components/leave_define_list.blade.php ENDPATH**/ ?>