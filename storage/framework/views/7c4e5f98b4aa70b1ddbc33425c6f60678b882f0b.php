<?php if(permissionCheck('client.settings')): ?>
    <li>
        <a href="<?php echo e(route('client.settings')); ?>"  class="<?php echo e(spn_active_link('client.settings')); ?>"> <?php echo e(__('common.Setting')); ?> </a>
    </li>
<?php endif; ?>
<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/ClientLogin/Resources/views/menu.blade.php ENDPATH**/ ?>