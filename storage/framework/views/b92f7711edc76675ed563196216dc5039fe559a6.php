<div id="edit_item_modal">
    <div class="modal fade" id="item_edit">
        <div class="modal-dialog modal_800px modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <?php echo e(__('common.Update')); ?>

                        <?php echo e(__('leave.Leave Type')); ?>

                    </h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>

                <div class="modal-body item_edit_form">
                    <input type="hidden" name="id" id="item_id">
                    <?php echo $__env->make('leave::leave_types.components.form',['form_id' => 'item_edit_form', 'button_level_name' => __('common.Update') ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/leave_types/components/edit.blade.php ENDPATH**/ ?>