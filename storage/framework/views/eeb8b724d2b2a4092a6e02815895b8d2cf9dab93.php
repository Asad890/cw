<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('payroll.Payroll Reports')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-3">
                    <div class="white_box_50px box_shadow_white">
                        <form class="" action="<?php echo e(route('payroll_reports.search')); ?>" method="GET">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="primary_input mb-15">
                                        <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Role')); ?></label>
                                        <select class="primary_select mb-15" name="role_id" id="role_id">
                                            <option selected disabled><?php echo e(__('payroll.Choose One')); ?></option>
                                            <?php $__currentLoopData = \Modules\RolePermission\Entities\Role::where('id', '!=', 1)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($r)): ?>
                                                    <option value="<?php echo e($role->id); ?>"<?php if($r == $role->id): ?> selected <?php endif; ?>><?php echo e($role->name); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('role_id')); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="primary_input mb-15">
                                        <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Month')); ?></label>
                                        <select class="primary_select mb-15" name="month" id="month">
                                            <?php $__currentLoopData = $months; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $month): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($m)): ?>
                                                    <option value="<?php echo e($month); ?>"<?php if($m == $month): ?> selected <?php endif; ?>><?php echo e(__('common.'.$month)); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($month); ?>"><?php echo e(__('common.'.$month)); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('month')); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="primary_input mb-15">
                                        <label class="primary_input_label" for=""><?php echo e(__('payroll.Select Year')); ?></label>
                                        <select class="primary_select mb-15" name="year" id="year">
                                            <?php $__currentLoopData = range(\carbon\Carbon::now()->year, 2015); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($y)): ?>
                                                    <option value="<?php echo e($year); ?>"<?php if($y == $year): ?> selected <?php endif; ?>><?php echo e($year); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($year); ?>"><?php echo e($year); ?></option>
                                                <?php endif; ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('year')); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 mb-2 mt-3 text-right">
                                        <button type="submit" class="primary-btn btn-sm fix-gr-bg"id="save_button_parent"><i class="ti-search"></i><?php echo e(__('payroll.Search')); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php if(isset($payrolls)): ?>
                    <div class="col-12">
                        <div class="box_header common_table_header">
                            <div class="main-title d-md-flex">
                                <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('payroll.Payroll')); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-40">
                        <div class="QA_section QA_section_heading_custom check_box_table">
                            <div class="QA_table ">
                                <!-- table-responsive -->
                                <div class="">
                                    <table class="table Crm_table_active3">
                                        <thead>
                                            <tr>
                                                <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                                <th scope="col"><?php echo e(__('staff.Staff')); ?></th>
                                                <th scope="col"><?php echo e(__('role.Role')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Month')); ?> - <?php echo e(__('payroll.Year')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Basic Salary')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Gross Salary')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Earnings')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Deductions')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Tax')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Paid Date')); ?></th>
                                                <th scope="col"><?php echo e(__('payroll.Net Salary')); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $payrolls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $payroll): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($key + 1); ?></td>
                                                    <td>
                                                        <?php if($payroll->staff && $payroll->staff->user): ?>
                                                            <?php echo e($payroll->staff->user->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($payroll->role): ?>
                                                            <?php echo e($payroll->role->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php echo e($payroll->payroll_month); ?> - <?php echo e($payroll->payroll_year); ?></td>
                                                    <td><?php echo e($payroll->basic_salary); ?></td>
                                                    <td><?php echo e($payroll->gross_salary); ?></td>
                                                    <td><?php echo e($payroll->total_earning); ?></td>
                                                    <td><?php echo e($payroll->total_deduction); ?></td>
                                                    <td><?php echo e($payroll->tax); ?></td>
                                                    <td><?php echo e(($payroll->payment_date != null) ? formatDate($payroll->payment_date) : "X"); ?></td>
                                                    <td><?php echo e($payroll->net_salary); ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', ['title' => 'Payroll'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Payroll/Resources/views/payroll_reports/payroll.blade.php ENDPATH**/ ?>