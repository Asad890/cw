<?php $__env->startSection('mainContent'); ?>

<section class="student-details mb-40">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-4 no-gutters">
                <div class="main-title">
                    <h3 class="mb-2"><?php echo e(__('payroll.Generate Payroll')); ?></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="student-meta-box">
                    <div class="student-meta-top staff-meta-top"></div>
                    <img class="student-meta-img img-100" src="<?php echo e(asset($staffDetails->avatar ?? 'public/backEnd/img/staff.jpg')); ?>"  alt="">
                    <div class="white-box">
                        <div class="single-meta mt-20">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="name">
                                        <?php echo e(__('common.Name')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="value text-left">
                                        <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->name); ?><?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-3">
                                    <div class="value text-left">
                                        <?php echo e(__('payroll.Month')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-9 d-flex">
                                    <div class="col-lg-3 col-md-9 d-flex">
                                        <div class="value" data-toggle="tooltip" title="<?php echo e(__('attendance.Present')); ?>">
                                           <?php echo e(__('attendance.P')); ?>

                                        </div>
                                        <div class="value ml-20" data-toggle="tooltip" title="<?php echo e(__('attendance.Late')); ?>">
                                            <?php echo e(__('attendance.L')); ?>

                                        </div>
                                        <div class="value ml-20" data-toggle="tooltip" title="<?php echo e(__('attendance.Absent')); ?>">
                                            <?php echo e(__('attendance.A')); ?>

                                        </div>
                                        <div class="value ml-20" data-toggle="tooltip" title="<?php echo e(__('attendance.Half Day')); ?>">
                                            <?php echo e(__('attendance.F')); ?>

                                        </div>
                                        <div class="value ml-20" data-toggle="tooltip" title="<?php echo e(__('attendance.Holiday')); ?>">
                                            <?php echo e(__('attendance.H')); ?>

                                        </div>

                                </div>
                            </div>
                        </div>

                        <div class="single-meta">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="name">
                                        <?php echo e(__('common.Phone')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="value text-left">
                                       <?php if(isset($staffDetails) && $staffDetails->staff): ?><?php echo e($staffDetails->staff->phone); ?><?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-3">
                                    <div class="value text-left">
                                        <?php echo e(__('common.'.$payroll_month)); ?>

                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-9 d-flex">
                                    <div class="value ml-20">
                                        <?php echo e($p); ?>

                                    </div>
                                    <div class="value ml-20">
                                        <?php echo e($l); ?>

                                    </div>
                                    <div class="value ml-20">
                                        <?php echo e($a); ?>

                                    </div>
                                    <div class="value ml-20">
                                        <?php echo e($f); ?>

                                    </div>
                                    <div class="value ml-20">
                                        <?php echo e($h); ?>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="name">
                                        <?php echo e(__('role.Role')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="value text-left">
                                        <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->role->name); ?><?php endif; ?>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="name">
                                        <?php echo e(__('common.Email')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="value text-left">
                                        <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->email); ?><?php endif; ?>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="single-meta">
                            <div class="row">
                                <div class="col-lg-2 col-md-6">
                                    <div class="name">
                                        <?php echo e(__('common.Date of Joining')); ?>

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="value text-left">
                                        <?php if(isset($staffDetails) && $staffDetails->staff): ?>
                                           <?php echo e($staffDetails->staff->date_of_joining ? formatDate($staffDetails->staff->date_of_joining) : ''); ?>

                                        <?php endif; ?>
                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 <form class="" action="<?php echo e(route('save_payroll')); ?>" method="post" enctype="multipart/form-data">
     <?php echo csrf_field(); ?>
     <section class="">
         <div class="container-fluid p-0">
             <div class="row">
                 <div class="col-lg-4 no-gutters">
                     <div class="d-flex justify-content-between mb-20">
                         <div class="main-title">
                             <h3><?php echo e(__('payroll.Earnings')); ?></h3>
                         </div>

                         <div>
                             <button type="button" class="primary-btn icon-only fix-gr-bg" onclick="addMoreEarnings()">
                                 <span class="ti-plus"></span>
                             </button>
                         </div>
                     </div>

                     <div class="white-box">
                         <table class="w-100 table-responsive" id="tableID">
                             <tbody id="addEarningsTableBody">
                                 <tr id="row0">
                                     <td width="80%" class="pr-30">
                                         <div class="input-effect mt-10">
                                             <input class="primary-input form-control" type="text" id="earningsType0" name="earningsType[]">
                                             <label for="earningsType0"><?php echo e(__('payroll.Type')); ?></label>
                                             <span class="focus-border"></span>
                                         </div>
                                     </td>
                                     <td width="20%">
                                         <div class="input-effect mt-10">
                                             <input class="primary-input form-control" type="number" oninput="this.value = Math.abs(this.value)" id="earningsValue0"  name="earningsValue[]">
                                             <label for="earningsValue0"><?php echo e(__('payroll.Value')); ?></label>
                                             <span class="focus-border"></span>
                                         </div>
                                     </td>

                                 </tr>
                             </tbody>
                         </table>
                     </div>
                 </div>

                 <div class="col-lg-4 no-gutters">
                     <div class="d-flex justify-content-between mb-20">
                         <div class="main-title">
                             <h3><?php echo e(__('payroll.Deductions')); ?></h3>
                         </div>

                         <div>
                             <button type="button" class="primary-btn icon-only fix-gr-bg" onclick="addDeductions()">
                                 <span class="ti-plus"></span>
                             </button>
                         </div>
                     </div>

                     <div class="white-box">
                     <table class="w-100 table-responsive" id="tableDeduction">
                             <tbody id="addDeductionsTableBody">
                                 <?php if(isset($loans)): ?>
                                     <?php if(count($loans) > 0): ?>
                                         <?php $__currentLoopData = $loans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $loan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <input type="hidden" name="loan_id[]" value="<?php echo e($loan->id); ?>">
                                             <input type="hidden" name="paid_loans[]" value="<?php echo e($loan->monthly_installment); ?>">
                                             <tr id="DeductionRow<?php echo e($key+1); ?>">
                                                 <td width="60%" class="pr-30">
                                                     <div class="input-effect mt-10">
                                                         <input class="primary-input form-control" type="text" id="deductionstype0" name="deductionstype[]" value="<?php echo e($loan->title); ?>" readonly>
                                                         <label for="deductionstype0"><?php echo e(__('payroll.Type')); ?></label>
                                                         <span class="focus-border"></span>
                                                     </div>
                                                 </td>
                                                 <td width="20%">
                                                     <div class="input-effect mt-10">
                                                         <input class="primary-input form-control" type="number" oninput="this.value = Math.abs(this.value)" value="<?php echo e($loan->monthly_installment); ?>" id="deductionsValue0" name="deductionsValue[]">
                                                         <label for="deductionsValue0"><?php echo e(__('payroll.Value')); ?></label>
                                                         <span class="focus-border"></span>
                                                     </div>
                                                 </td>

                                             </tr>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                     <?php else: ?>
                                         <tr id="DeductionRow0">
                                             <td width="60%" class="pr-30">
                                                 <div class="input-effect mt-10">
                                                     <input class="primary-input form-control" type="text" id="deductionstype0" name="deductionstype[]">
                                                     <label for="deductionstype0"><?php echo e(__('payroll.Type')); ?></label>
                                                     <span class="focus-border"></span>
                                                 </div>
                                             </td>
                                             <td width="20%">
                                                 <div class="input-effect mt-10">
                                                     <input class="primary-input form-control" type="number" oninput="this.value = Math.abs(this.value)" id="deductionsValue0" name="deductionsValue[]">
                                                     <label for="deductionsValue0"><?php echo e(__('payroll.Value')); ?></label>
                                                     <span class="focus-border"></span>
                                                 </div>
                                             </td>

                                         </tr>
                                     <?php endif; ?>

                                     <?php else: ?>
                                      <tr id="DeductionRow0">
                                             <td width="60%" class="pr-30">
                                                 <div class="input-effect mt-10">
                                                     <input class="primary-input form-control" type="text" id="deductionstype0" name="deductionstype[]">
                                                     <label for="deductionstype0"><?php echo e(__('payroll.Type')); ?></label>
                                                     <span class="focus-border"></span>
                                                 </div>
                                             </td>
                                             <td width="20%">
                                                 <div class="input-effect mt-10">
                                                     <input class="primary-input form-control" type="number" oninput="this.value = Math.abs(this.value)" id="deductionsValue0" name="deductionsValue[]">
                                                     <label for="deductionsValue0"><?php echo e(__('payroll.Value')); ?></label>
                                                     <span class="focus-border"></span>
                                                 </div>
                                             </td>

                                         </tr>
                                 <?php endif; ?>
                             </tbody>
                         </table>
                     </div>
                 </div>

                 <div class="col-lg-4 no-gutters">
                     <div class="d-flex justify-content-between mb-20">
                         <div class="main-title">
                             <h3><?php echo e(__('payroll.Payroll Summary')); ?></h3>
                         </div>

                         <div>
                             <button type="button" class="primary-btn small fix-gr-bg" onclick="calculateSalary()">
                                 <?php echo e(__('payroll.Calculate')); ?>

                             </button>
                         </div>
                     </div>

                     <input type="hidden" name="staff_id" <?php if($staffDetails->staff): ?> value="<?php echo e($staffDetails->staff->id); ?>" <?php endif; ?>>
                     <input type="hidden" name="payroll_month" value="<?php echo e($payroll_month); ?>">
                     <input type="hidden" name="payroll_year" value="<?php echo e($payroll_year); ?>">
                     <input type="hidden" name="role_id" value="<?php echo e($staffDetails->role_id); ?>">


                     <div class="white-box">
                     <table class="w-100 table-responsive">
                             <tbody class="d-block">
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <label class="primary_input_label" for=""><?php echo e(__('payroll.Basic Salary')); ?></label>
                                         <input name="basic_salary" id="basicSalary" readonly class="primary_input_field" type="text" <?php if($staffDetails->staff): ?> value="<?php echo e($staffDetails->staff->basic_salary); ?>" <?php endif; ?>>
                                     </td>
                                 </tr>
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <label class="primary_input_label" for=""><?php echo e(__('payroll.Earnings')); ?></label>
                                         <input name="total_earnings" id="total_earnings" readonly class="primary_input_field" type="text" value="0">
                                     </td>
                                 </tr>
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <label class="primary_input_label" for=""><?php echo e(__('payroll.Deductions')); ?></label>
                                         <input name="total_deduction" id="total_deduction" readonly class="primary_input_field" type="text" value="0">
                                     </td>
                                 </tr>
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <label class="primary_input_label" for=""><?php echo e(__('payroll.Gross Salary')); ?></label>
                                         <input name="final_gross_salary" id="final_gross_salary" readonly class="primary_input_field" type="text" value="0">
                                     </td>
                                 </tr>
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <label class="primary_input_label" for=""><?php echo e(__('payroll.Tax')); ?></label>
                                         <input name="tax" id="tax" class="primary_input_field" type="text" value="0">
                                     </td>
                                 </tr>
                                 <tr class="d-block">
                                     <td width="100%" class="pr-30 d-block">
                                         <div class="input-effect mt-30 mb-30">
                                             <input class="primary-input form-control<?php echo e($errors->has('net_salary') ? ' is-invalid' : ''); ?>" readonly type="text" id="net_salary" name="net_salary">
                                             <label for="net_salary"<?php echo e(__('payroll.Net Salary')); ?>></label>
                                             <span class="focus-border"></span>

                                             <?php if($errors->has('net_salary')): ?>
                                             <span class="invalid-feedback" role="alert">
                                                 <strong><?php echo e($errors->first('net_salary')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                         </div>
                                     </td>
                                 </tr>

                                 <tr class="d-block">
                                    <td width="100%" class="pr-30 d-block">
                                        <div class="col-lg-12 mt-20 text-center">
                                            <button class="primary-btn fix-gr-bg">
                                                <span class="ti-check"></span>
                                                <?php echo e(__('common.Save')); ?>

                                            </button>
                                        </div>
                                    </td>
                                 </tr>
                             </tbody>
                         </table>
                     </div>



                 </div>
             </div>
         </div>
     </section>
 </form>

<!-- End Modal Area -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
    // payroll calculate for staff
     function calculateSalary() {
        var basicSalary = $("#basicSalary").val();
        if (basicSalary == 0) {
            toastr.warning('Please Add Employees Basic Salary from Staff Update Form First');
        } else {
            var earningsType = document.getElementsByName('earningsValue[]');
            var earningsValue = document.getElementsByName('earningsValue[]');
            var tax = $("#tax").val();
            var total_earnings = 0;
            var total_deduction = 0;
            var deductionstype = document.getElementsByName('deductionstype[]');
            var deductionsValue = document.getElementsByName('deductionsValue[]');
            for (var i = 0; i < earningsValue.length; i++) {
                var inp = earningsValue[i];
                if (inp.value == '') {
                    var inpvalue = 0;
                } else {
                    var inpvalue = inp.value;
                }
                total_earnings += parseInt(inpvalue);
            }
            for (var j = 0; j < deductionsValue.length; j++) {
                var inpd = deductionsValue[j];
                if (inpd.value == '') {
                    var inpdvalue = 0;
                } else {
                    var inpdvalue = inpd.value;
                }
                total_deduction += parseInt(inpdvalue);
            }
            var gross_salary = parseInt(basicSalary) + parseInt(total_earnings) - parseInt(total_deduction);
            var net_salary = parseInt(basicSalary) + parseInt(total_earnings) - parseInt(total_deduction) - parseInt(tax);

            $("#total_earnings").val(total_earnings);
            $("#total_deduction").val(total_deduction);
            $("#gross_salary").val(gross_salary);
            $("#final_gross_salary").val(gross_salary);
            $("#net_salary").val(net_salary);

            if ($('#total_earnings').val() != '') {
                $('#total_earnings').focus();
            }

            if ($('#total_deduction').val() != '') {
                $('#total_deduction').focus();
            }

            if ($('#net_salary').val() != '') {
                $('#net_salary').focus();
            }
        }
    }

    // for add staff earnings in payroll
    function addMoreEarnings(){
        var table = document.getElementById("tableID");
        var table_len = (table.rows.length);
        var id = parseInt(table_len);
        var row = table.insertRow(table_len).outerHTML = "<tr id='row" + id + "'><td width='70%' class='pr-30'><div class='input-effect mt-10'><input class='primary-input form-control has-contnet' type='text' id='earningsType" + id + "' name='earningsType[]'><label for='earningsType" + id + "'>" + trans('js.type') + "</label><span class='focus-border'></span></div></td><td width='20%'><div class='input-effect mt-10'><input class='primary-input form-control has-contnet' type='number' oninput='this.value = Math.abs(this.value)' id='earningsValue" + id + "' name='earningsValue[]'><label for='earningsValue" + id + "'>" + trans('js.value') + "</label><span class='focus-border'></span></div></td><td width='10%' class='pt-30'><button class='primary-btn icon-only fix-gr-bg close-deductions' onclick='delete_earings(" + id + ")'><span class='ti-close'></span></button></td></tr>";
    }

    function delete_earings(id){
        var table = document.getElementById("tableID");
        var rowCount = table.rows.length;
        $("#row" + id).html("");
    }

    // for minus staff deductions in payroll
    function addDeductions(){
        var table = document.getElementById("tableDeduction");
        var table_len = (table.rows.length);
        var id = parseInt(table_len);
        var row = table.insertRow(table_len).outerHTML = "<tr id='DeductionRow" + id + "'><td width='50%' class='pr-30'><div class='input-effect mt-10'><input class='primary-input form-control  has-content' type='text' id='deductionstype" + id + "' name='deductionstype[]'><label for='deductionstype" + id + "'>" + trans('js.type') + "</label><span class='focus-border'></span></div></td><td width='20%'><div class='input-effect mt-10'><input class='primary-input form-control has-content' oninput='this.value = Math.abs(this.value)' type='number' id='deductionsValue" + id + "' name='deductionsValue[]'><label for='deductionsValue" + id + "'>" + trans('js.value') + "</label><span class='focus-border'></span></div></td><td width='10%' class='pt-30'><button type='button' class='primary-btn icon-only fix-gr-bg close-deductions' onclick='delete_deduction(" + id + ")'><span class='ti-close'></span></button></td></tr>";
    }

     function delete_deduction(id){
        var table = document.getElementById("tableDeduction");
        var rowCount = table.rows.length;
        $("#row" + id).html("");

    }

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' =>'Payroll'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Payroll/Resources/views/payrolls/generatePayroll.blade.php ENDPATH**/ ?>