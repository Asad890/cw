

<?php $__env->startPush('css'); ?>

<style>

.login-resistration-area .main-login-area.login-res-v2::before {
    background-image: url(<?php echo e(asset(config('configs')->where('key', 'login_backgroud_image')->first()->value)); ?>)
}

.login-resistration-area .main-login-area .main-content .media-link {
    float: left;
}


</style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>
<div class="main-content">
		<div class="logo_img">
			<a href="<?php echo e(route('home')); ?>">
				<img src="<?php echo e(asset(config('configs')->where('key', 'site_logo')->first()->value)); ?>" alt="Logo Image" class="img img-responsive">
			</a>
		</div>

			<h3 class="sho_web d-none d-md-block"> <?php echo e(__('auth.Welcome back, Please login')); ?> <br> <?php echo e(__('auth.to your account')); ?></h3>
			<h3 class="sho_mb d-sm-block d-md-none"><?php echo e(__('auth.Welcome To Login')); ?></h3>

			<?php if(Illuminate\Support\Facades\Config::get('app.app_sync')): ?>

			<?php
				$admin =  DB::table('users')->select('email')->where('role_id',1)->first();

				$lawyer =  DB::table('users')->select('email')->where('role_id',2)->first();
			?>

                <div class="media-link">

					<?php if($admin): ?>
                    <form action="<?php echo e(route('login')); ?>" id="content_form1" class="customer-input" method="POST">
						<?php echo csrf_field(); ?>
					            <input type="hidden" name="email" value="<?php echo e($admin->email); ?>">
                                <input type="hidden" name="password" value="12345678">
								<button type="submit" name="submit" class="parents submit">Super Admin</button>
								<button type="button" class="parents submitting" style="display:none" disabled>Loging...</button>
						</form>

						<?php endif; ?>

						<?php if($lawyer): ?>
						<form action="<?php echo e(route('login')); ?>" id="content_form2" class="customer-input" method="POST">
						<?php echo csrf_field(); ?>
					            <input type="hidden" name="email" value="<?php echo e($lawyer->email); ?>">
                                <input type="hidden" name="password" value="12345678">
								<button type="submit" name="submit" class="parents submit">Lawyer</button>
								<button type="button" class="parents submitting" style="display:none" disabled>Loging...</button>
						</form>
						<?php endif; ?>
					</div>

			<?php endif; ?>


			<form  method="POST" action="<?php echo e(route('login')); ?>" id="content_form3" class="customer-input" >

			<?php echo csrf_field(); ?>

				<input required name="email" id="email" type="text" placeholder="<?php echo e(__('auth.Enter email address')); ?>" autofocus class="">

				<input required name="password" id="password" type="password" placeholder="<?php echo e(__('auth.Password')); ?>" class="" value="">


				<div class="forgot-pass">
					<div class="check-remamber-field">
						<div class="round">
							<input type="checkbox" id="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>/>
							<label for="checkbox"></label>
						</div>
						<label class="remember-me"  for="checkbox">
							<?php echo e(__('auth.Remember me')); ?>

						</label>
					</div>
					<span>

					<?php if(Route::has('password.request')): ?>
						<a href="<?php echo e(route('password.request')); ?>">
							<?php echo e(__('auth.Forgot Your Password?')); ?>

						</a>
						<?php endif; ?>
				</span>
				</div>


				<button type="submit" class="login-res-btn submit"><?php echo e(__('auth.Login')); ?></button>
				<button type="button" class="login-res-btn submitting" style="display:none" disabled><?php echo e(__('auth.Logging in')); ?>...</button>
			</form>


	</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.guest', ['title' => 'Login'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/auth/login.blade.php ENDPATH**/ ?>