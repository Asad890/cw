<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                    <li class="nav-item ">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-cnr-number" role="tab" aria-controls="pills-contact" aria-selected="true"><h3 class="mb-0 "><?php echo e(__('CNR Number')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-case-no" role="tab" aria-controls="pills-home" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('case.By Case Number')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-party-nm" role="tab" aria-controls="pills-profile" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('case.By Party Name')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-advo-nm" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('case.By Advocate')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-custom" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('case.Custom Case')); ?></h3></a>
                        </li>
                        
                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade" id="pills-case-no" role="tabpanel" aria-labelledby="pills-case-no-tab">

                            <div class="col-12" id="case_no" >
                                <div class="white_box_50px box_shadow_white">
                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Hearing Court', __('Hearing Court'))); ?>

                                            <?php echo e(Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('State', __('State'))); ?>

                                            <?php echo e(Form::select('state_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Select State'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('District', __('District'))); ?>

                                            <?php echo e(Form::select('district_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('District'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Court/Bench', __('Court/Bench'))); ?>

                                            <?php echo e(Form::select('bench_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Case Type', __('Case Type'))); ?>

                                            <?php echo e(Form::select('case_type_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Case Type'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Case Number', __('Case Number'))); ?>

                                            <?php echo e(Form::text('case_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Number')])); ?>

                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Case Year', __('Case Year'))); ?>

                                            <?php echo e(Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')])); ?>

                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Fetch')); ?>

                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>


                                </div>
                            </div>
                               <!--  -->
                               <br>
                                <div class="tab-pane fade show active" id="pills-cnr-number_unde" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                    <div class="col-12" id="custom_case" >
                                                        <div class="white_box_50px box_shadow_white">

                                                            <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                                            <!--  -->
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no.', __('CNR NO.'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" value="MHNG" class="primary_input_field" placeholder="CNR NO." readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no.', __('Brief NO.'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" value="225" class="primary_input_field" placeholder="CNR NO." readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('State:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Maharashtra" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('District:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Court:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="MAC Court No.2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Case Type:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="M.A.C.P" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Case No.'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="802" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Case Year:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Previous Date:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Next Date:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- stop -->

                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Petitioner:'))); ?>

                                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#petitionerModal">
                                                                +
                                                                </button>
                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Parshant" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Petitioner Advocate:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="P S Mirache" class="primary_input_field" placeholder="Petitioner Advocate" readonly>
                                                                </div>
                                                            </div>

                                                            
                                                            <!-- Respondant -->
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Respondand:'))); ?>

                                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#respondentModal">
                                                                +
                                                                </button>
                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Respondent Advocate:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="Respondent Advocate" readonly>
                                                                </div>
                                                            </div>

                                                                                            
                                                            <!-- start -->
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Judge Name:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Deshpande" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Court Room No.'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="219" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Case Stage:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Defence Evidence" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Brief For:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Organization:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="ICICI Lombars" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Tags/Refrense:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Police Station:'))); ?>

                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="-----------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="primary_input col-md-3">
                                                                <?php echo e(Form::label('case_no', __('Clients Name:'))); ?>

                                                                <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="Modal" data-target="#clientModal">
                                                                +
                                                                </button>
                                                                </div>
                                                                <div class="primary_input col-md-9">
                                                                    <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Contact:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="0234XXXXXX" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Email Id:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="abc@gmail.com" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Address:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                        
                                                            <!-- Decided Toggle -->
                                                            <h5>Decided</h5>
                                                        <div class="custom-control">
                                                        <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="active_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                        </label>
                                                        </div>
                                                        <!-- Close Decided -->
                                                        <!-- Abandoned -->
                                                        <h5>Abandoned</h5>
                                                        <div class="custom-control">
                                                        <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="active_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                        </label>
                                                        </div>
                                                        <!-- Close Abamdoned -->
                                                        <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                            <!--  -->
                                                            <div class="text-center mt-3">
                                                                <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                                        class="ti-check"></i><?php echo e(__('common.Save')); ?>

                                                                </button>

                                                            </div>

                                                            <?php echo Form::close(); ?>

                                                        </div>
                                                    </div>



                                </div>
                                <!--  -->

                        </div>


                        <!-- tab 2 =-->
                        <div class="tab-pane fade" id="pills-party-nm" role="tabpanel" aria-labelledby="pills-party-tab">
                            <div class="col-12" id="party_tab" >
                                <div class="white_box_50px box_shadow_white">
                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row form-group">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Hearing Court', __('Hearing Court'))); ?>

                                            <?php echo e(Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Court', __('case.Court'))); ?>

                                            <?php echo e(Form::select('bench_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Party Name', __('Party Name'))); ?>

                                            <?php echo e(Form::text('party_nm', null, ['class' => 'primary_input_field', 'placeholder' => __('Party Name')])); ?>

                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Case Year', __('Case Year'))); ?>

                                            <?php echo e(Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')])); ?>

                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Fetch')); ?>

                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>


                                </div>
                            </div>
                                       <!--  -->
                                       <br>
                                        <div class="tab-pane fade show active" id="pills-cnr-number_une" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                            <div class="col-12" id="custom_case" >
                                                                <div class="white_box_50px box_shadow_white">

                                                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                                                    <!--  -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('CNR NO.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" value="MHNG" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Brief NO.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" value="225" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('State:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Maharashtra" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('District:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Court:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="MAC Court No.2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Type:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="M.A.C.P" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case No.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="802" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Year:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Previous Date:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Next Date:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <!-- stop -->

                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Petitioner:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#petitionerModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Parshant" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Petitioner Advocate:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="P S Mirache" class="primary_input_field" placeholder="Petitioner Advocate" readonly>
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    <!-- Respondant -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Respondand:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#respondentModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Respondent Advocate:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="Respondent Advocate" readonly>
                                                                        </div>
                                                                    </div>

                                                                                                    
                                                                    <!-- start -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Judge Name:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Deshpande" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Court Room No.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="219" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Stage:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Defence Evidence" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Brief For:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Organization:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="ICICI Lombars" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Tags/Refrense:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Police Station:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="-----------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Clients Name:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="Modal" data-target="#clientModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no', __('Contact:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="0234XXXXXX" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no', __('Email Id:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="abc@gmail.com" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no.', __('Address:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO." readonly>
                                                                                </div>
                                                                            </div>
                                                                
                                                                    
                                                                    <!-- Decided Toggle -->
                                                                    <h5>Decided</h5>
                                                                <div class="custom-control">
                                                                <label class="switch_toggle" for="active_checkbox">
                                                                    <input type="checkbox" id="active_checkbox" 
                                                                    value="check" onchange="update_active_status(this)" checked>
                                                                    <div class="slider round"></div>
                                                                </label>
                                                                </div>
                                                                <!-- Close Decided -->
                                                                <!-- Abandoned -->
                                                                <h5>Abandoned</h5>
                                                                <div class="custom-control">
                                                                <label class="switch_toggle" for="active_checkbox">
                                                                    <input type="checkbox" id="active_checkbox" 
                                                                    value="check" onchange="update_active_status(this)" checked>
                                                                    <div class="slider round"></div>
                                                                </label>
                                                                </div>
                                                                <!-- Close Abamdoned -->
                                                                <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                    <!--  -->
                                                                    <div class="text-center mt-3">
                                                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                                                class="ti-check"></i><?php echo e(__('common.Save')); ?>

                                                                        </button>

                                                                    </div>

                                                                    <?php echo Form::close(); ?>

                                                                </div>
                                                            </div>



                                        </div>
                                        <!--  -->

                        </div>

                        <!-- tab 3 =-->
                        <div class="tab-pane fade" id="pills-advo-nm" role="tabpanel" aria-labelledby="pills-advocate-tab">
                            <div class="col-12" id="advocate_tab" >
                                <div class="white_box_50px box_shadow_white">
                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Hearing Court', __('Hearing Court'))); ?>

                                            <?php echo e(Form::select('court_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Hearing Court'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Advocate Number', __('Advocate Name'))); ?>

                                            <?php echo e(Form::text('advocate_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Advocate Name')])); ?>

                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Year', __('case.Year'))); ?>

                                            <?php echo e(Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Case Year')])); ?>

                                        </div>
                                    </div>

                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Fetch')); ?>

                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>


                                </div>
                            </div>

                                       <!--  -->
                                       <br>
                                        <div class="tab-pane fade show active" id="pills-cnr-number_uned" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                            <div class="col-12" id="custom_case" >
                                                                <div class="white_box_50px box_shadow_white">

                                                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                                                    <!--  -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('CNR NO.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" value="MHNG" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Brief NO.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" value="225" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('State:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Maharashtra" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('District:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Court:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="MAC Court No.2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Type:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="M.A.C.P" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case No.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="802" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Year:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Previous Date:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Next Date:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <!-- stop -->

                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Petitioner:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#petitionerModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Parshant" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Petitioner Advocate:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="P S Mirache" class="primary_input_field" placeholder="Petitioner Advocate" readonly>
                                                                        </div>
                                                                    </div>

                                                                    
                                                                    <!-- Respondant -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Respondand:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#respondentModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Respondent Advocate:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="Respondent Advocate" readonly>
                                                                        </div>
                                                                    </div>

                                                                                                    
                                                                    <!-- start -->
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Judge Name:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Deshpande" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Court Room No.'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="219" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Case Stage:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Defence Evidence" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Brief For:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Organization:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="ICICI Lombars" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Tags/Refrense:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Police Station:'))); ?>

                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="-----------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <div class="primary_input col-md-3">
                                                                        <?php echo e(Form::label('case_no', __('Clients Name:'))); ?>

                                                                        <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="Modal" data-target="#clientModal">
                                                                        +
                                                                        </button>
                                                                        </div>
                                                                        <div class="primary_input col-md-9">
                                                                            <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no', __('Contact:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="0234XXXXXX" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no', __('Email Id:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="abc@gmail.com" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="primary_input col-md-3">
                                                                                <?php echo e(Form::label('case_no', __('Address:'))); ?>

                                                                                </div>
                                                                                <div class="primary_input col-md-9">
                                                                                    <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                                </div>
                                                                            </div>
                                                                
                                                                    
                                                                    <!-- Decided Toggle -->
                                                                    <h5>Decided</h5>
                                                                <div class="custom-control">
                                                                <label class="switch_toggle" for="active_checkbox">
                                                                    <input type="checkbox" id="active_checkbox" 
                                                                    value="check" onchange="update_active_status(this)" checked>
                                                                    <div class="slider round"></div>
                                                                </label>
                                                                </div>
                                                                <!-- Close Decided -->
                                                                <!-- Abandoned -->
                                                                <h5>Abandoned</h5>
                                                                <div class="custom-control">
                                                                <label class="switch_toggle" for="active_checkbox">
                                                                    <input type="checkbox" id="active_checkbox" 
                                                                    value="check" onchange="update_active_status(this)" checked>
                                                                    <div class="slider round"></div>
                                                                </label>
                                                                </div>
                                                                <!-- Close Abamdoned -->
                                                                <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                    <!--  -->
                                                                    <div class="text-center mt-3">
                                                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                                                class="ti-check"></i><?php echo e(__('common.Save')); ?>

                                                                        </button>

                                                                    </div>

                                                                    <?php echo Form::close(); ?>

                                                                </div>
                                                            </div>



                                        </div>
                                        <!--  -->

                        </div>


                        <!-- tab 4 =-->
                        <div class="tab-pane fade" id="pills-custom" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row form-group">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('BRIEF NO.', __('CNR NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('BRIEF NO', __('case.BRIEF NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF NO')])); ?>

                                        </div>
                                    </div>
                                    <div class="row">

                                            <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('PREVIOUS DATE', __('case.PREVIOUS DATE'))); ?>

                                            </div>
                                            <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"previous_date",'placeholder' => __('case.PREVIOUS DATE')])); ?>

                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Court/Bench', __('Court/Bench'))); ?>

                                            <span id="court_id_error"></span>
                                            </div>
                                            <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::select('bench_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'),  'data-parsley-errors-container' => '#court_id_error'])); ?> -->
                                            <select class="primary_select" >
                                                <option>District Courts and Tribunals</option>
                                                <option>High Court</option>
                                            </select>
                                            <span id="court_id_error"></span>
                                            </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-12">
                                            <div class="row">
                                                <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('case_no', __('case.Case No.'))); ?>

                                                </div>
                                                <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('case_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Case No')])); ?>

                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

                                                <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#exampleModal">+</button>
                                                </div>
                                                <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER')])); ?>

                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('petitioner_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('RESPONDENT', __('RESPONDENT'))); ?>

                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#respondendModal">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('respondent', null, ['class' => 'primary_input_field', 'placeholder' => __('case.RESPONDENT')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('RESPONDENT ADVOCATE', __('RESPONDENT ADVOCATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('respondent_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('RESPONDENT ADVOCATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CASE STAGE', __('case.CASE STAGE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('case_stage', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CASE STAGE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Sr. No. in Court', __('Sr. No. in Court'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('Sr_no_in_court', null, ['class' => 'primary_input_field', 'placeholder' => __('Sr. No. in Court')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('COURT ROOM NO', __('COURT ROOM NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                        <select class="primary_select">
                                            <option>Select Court</option>
                                            <option> District</option>
                                            <option> Highcourt</option>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="primary_input col-md-3">
                                        <?php echo e(Form::label('BRIEF FOR', __('JUDGE Name'))); ?>

                                    </div>
                                    <div class="primary_input col-md-9">
                                        <?php echo e(Form::text('brief_for', null, ['class' => 'primary_input_field', 'placeholder' => __('case.JUDGE Name')])); ?>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="primary_input col-md-3">
                                        <?php echo e(Form::label('BRIEF FOR', __('case.BRIEF FOR'))); ?>

                                    </div>
                                    <div class="primary_input col-md-9">
                                        <?php echo e(Form::text('brief_for', null, ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF FOR')])); ?>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    </div>
                                    <div class="row">
                                    <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('organization_id', __('case.ORGANIZATION'))); ?>

                                            </div>
                                    </div>
                                    <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('TAGS/REFERENCE', __('case.TAGS/REFERENCE'))); ?>


                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('tags', null, ['class' => 'primary_input_field', 'placeholder' => __('case.TAGS/REFERENCE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('POLICE STATION', __('case.POLICE STATION'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('police_station', null, ['class' => 'primary_input_field', 'placeholder' => __('case.POLICE STATION')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('NEXT DATE', __('NEXT DATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('next_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"next_date",'placeholder' => __('case.NEXT DATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CNR NO.', __('CNR NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('cnr_no', null, ['class' => 'primary_input_field', 'placeholder' => __('CNR NO.')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('state', __('STATE'))); ?>

                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('district_id', __('District'))); ?>

                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                                <div class="d-flex justify-content-between">
                                                    <?php echo e(Form::label('district_id', __('CASE TYPE'))); ?>

                                                </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                                <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CASE YEAR', __('CASE YEAR'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('next_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"next_date",'placeholder' => __('case.NEXT DATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        
                                        <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('PETITIONER', __('Client Name'))); ?>

                                                <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#clientModal1">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('Client Name')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('DECIDED TOGGLE', __('DECIDED TOGGLE'))); ?>

                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                    <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="active_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                    </label>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('ABANDONED TOGGLE', __('ABANDONED TOGGLE'))); ?>

                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                    <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="activee_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                    </label>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Add')); ?>

                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>

                                </div>
                            </div>



                        </div>
                        <!-- Tab 5 -->
                        <br>
                        <div class="tab-pane fade show active" id="pills-cnr-number" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row">
                                        <div class="primary_input col-md-12">
                                            
                                            <?php echo e(Form::label('case_no', __('case.CNR NO.'))); ?>

                                            <?php echo e(Form::text('cnr_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO')])); ?>

                                        </div>
                                        
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Fetch')); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>

                                </div>
                            </div>


                                   <!--  -->
                                   <br>
                                    <div class="tab-pane fade show active" id="pills-cnr-number_under" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                        <div class="col-12" id="custom_case" >
                                                            <div class="white_box_50px box_shadow_white">

                                                                <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                                                <!--  -->
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('CNR NO.'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" value="MHNG" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Brief NO.'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" value="225" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('State:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Maharashtra" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('District:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Court:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="MAC Court No.2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Case Type:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="M.A.C.P" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Case No.'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="802" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Case Year:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Previous Date:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Next Date:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="20/07/2013" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                
                                                                <!-- stop -->

                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Petitioner:'))); ?>

                                                                    <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#petitionerModal">
                                                                    +
                                                                    </button>
                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Parshant" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>

                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Petitioner Advocate:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="P S Mirache" class="primary_input_field" placeholder="Petitioner Advocate" readonly>
                                                                    </div>
                                                                </div>

                                                                
                                                                <!-- Respondant -->
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Respondand:'))); ?>

                                                                    <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="modal" data-target="#respondentModal">
                                                                    +
                                                                    </button>
                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Respondent Advocate:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Santosh" class="primary_input_field" placeholder="Respondent Advocate" readonly>
                                                                    </div>
                                                                </div>

                                                                                                
                                                                <!-- start -->
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Judge Name:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Deshpande" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Court Room No.'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="219" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Case Stage:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Defence Evidence" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Brief For:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Organization:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="ICICI Lombars" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Tags/Refrense:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Police Station:'))); ?>

                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="-----------" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="primary_input col-md-3">
                                                                    <?php echo e(Form::label('case_no', __('Clients Name:'))); ?>

                                                                    <button class="btn1 primary-btn small fix-gr-bg btn-sm " type="button" data-toggle="Modal" data-target="#clientModal">
                                                                    +
                                                                    </button>
                                                                    </div>
                                                                    <div class="primary_input col-md-9">
                                                                        <input type="text" name="" value="Respondand No 2" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                            <div class="primary_input col-md-3">
                                                                            <?php echo e(Form::label('case_no', __('Contact:'))); ?>

                                                                            </div>
                                                                            <div class="primary_input col-md-9">
                                                                                <input type="text" name="" value="0234XXXXXX" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <div class="primary_input col-md-3">
                                                                            <?php echo e(Form::label('case_no', __('Email Id:'))); ?>

                                                                            </div>
                                                                            <div class="primary_input col-md-9">
                                                                                <input type="text" name="" value="abc@gmail.com" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <div class="primary_input col-md-3">
                                                                            <?php echo e(Form::label('case_no', __('Address:'))); ?>

                                                                            </div>
                                                                            <div class="primary_input col-md-9">
                                                                                <input type="text" name="" value="Nagpur" class="primary_input_field" placeholder="CNR NO" readonly>
                                                                            </div>
                                                                        </div>
                                                            
                                                               
                                                                <!-- Decided Toggle -->
                                                                <h5>Decided</h5>
                                                            <div class="custom-control">
                                                            <label class="switch_toggle" for="active_checkbox">
                                                                <input type="checkbox" id="active_checkbox" 
                                                                value="check" onchange="update_active_status(this)" checked>
                                                                <div class="slider round"></div>
                                                            </label>
                                                            </div>
                                                            <!-- Close Decided -->
                                                            <!-- Abandoned -->
                                                            <h5>Abandoned</h5>
                                                            <div class="custom-control">
                                                            <label class="switch_toggle" for="active_checkbox">
                                                                <input type="checkbox" id="active_checkbox" 
                                                                value="check" onchange="update_active_status(this)" checked>
                                                                <div class="slider round"></div>
                                                            </label>
                                                            </div>
                                                            <!-- Close Abamdoned -->
                                                            <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                <!--  -->
                                                                <div class="text-center mt-3">
                                                                    <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                                            class="ti-check"></i><?php echo e(__('common.Save')); ?>

                                                                    </button>

                                                                </div>

                                                                <?php echo Form::close(); ?>

                                                            </div>
                                                        </div>



                                    </div>
                                    <!--  -->

                        </div>

                    </div>
                    <!-- form end =-->
                </div>
            </div>


        </div>

      
    </section>

    <!-- <div class="modal fade animated act_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated client_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated client_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated lawyer_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated case_stage_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>
    <div class="modal fade animated court_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated court_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>
    <div class="modal fade animated case_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div> -->
            <!-- Petitioner Modal -->

    
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Petitioner </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="#">
                <div class="row">
                    <div class="primary_input col-md-10">
                        <!--  -->
                    </div>
                    <div class="primary_input col-md-2">
                    <button type="button" class="primary-btn small fix-gr-bg add_field_button">+</button>
                    </div>
                </div>
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">PETITIONER</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Petitioner Advocate</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner Advocate">
                    </div>
                </div>

                <div class="row" >
                    <div class="input_fields_wrap primary_input col-md-12">
                        
                    </div>
                </div>
            
            
            <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn small fix-gr-bg tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
<!--Petitioner Modal -->

<!-- Respondent Modal -->

<div class="modal fade" id="respondendModal" tabindex="-1" role="dialog" aria-labelledby="respondandModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondandModalLabel">Respondand </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
      <form action="#">
                <div class="row">
                    <div class="primary_input col-md-10">
                        <!--  -->
                    </div>
                    <div class="primary_input col-md-2">
                    <button type="button" class="primary-btn small fix-gr-bg add_field_button_res">+</button>
                    </div>
                </div>
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Respondent</label>
                        <input type="text" class="primary_input_field" placeholder="Respondent">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Respondent Advocate</label>
                        <input type="text" class="primary_input_field" placeholder="Respondent Advocate">
                    </div>
                </div>

                <div class="row" >
                    <div class="input_fields_wrap_res primary_input col-md-12">
                        
                    </div>
                </div>
            
            
            <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
        </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
    </div>
    </div>
  </div>
</div>

<!--  -->


            <!--Respondent Modal -->

<!-- Client Modal For View -->

<div class="modal fade" id="clientModal1" tabindex="-1" role="dialog" aria-labelledby="clientModal1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondentModalLabel">Client </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="#">
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Contact</label>
                        <input type="text" class="primary_input_field" placeholder="Contact">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Email</label>
                        <input type="text" class="primary_input_field" placeholder="Email">
                    </div>
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Address</label>
                        <input type="text" class="primary_input_field" placeholder="Address">
                    </div>
                </div>
                <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
                </div>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>


<!-- Petitioner modal for View -->
<div class="modal fade" id="petitionerModal" tabindex="-1" role="dialog" aria-labelledby="petitionerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="petitionerModalLabel">Petitioner </h5>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="">
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">PETITIONER 1</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner" readonly>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Petitioner Advocate 1</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner Advocate" readonly>
                    </div>
                </div>
                <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
                </div>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>

<!-- Respodent Modal For View -->

<div class="modal fade" id="respondentModal" tabindex="-1" role="dialog" aria-labelledby="respondentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondentModalLabel">Respondent </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="">
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Respondent</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Respondent Advocate</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner Advocate">
                    </div>
                </div>
                <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
                </div>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>

<!-- Client Modal For View -->

<div class="modal fade" id="clientModal" tabindex="-1" role="dialog" aria-labelledby="clientModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondentModalLabel">Client </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="">
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Contact</label>
                        <input type="text" class="primary_input_field" placeholder="Contact">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Email</label>
                        <input type="text" class="primary_input_field" placeholder="Email">
                    </div>
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Address</label>
                        <input type="text" class="primary_input_field" placeholder="Address">
                    </div>
                </div>
                <div class="text-center mt-3">
                <button class="primary-btn small fix-gr-bg submit" type="submit" hidden><i
                        class="ti-check"></i><?php echo e(__('Add')); ?>

                </button>
                </div>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn small fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startPush('admin.scripts'); ?>
<script>
    
</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/create.blade.php ENDPATH**/ ?>