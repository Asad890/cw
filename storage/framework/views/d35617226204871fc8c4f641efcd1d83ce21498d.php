<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('contact.Add Contact')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::open(['route' => 'contact.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-12">

                                <div class="d-flex justify-content-between">
                                    <?php echo e(Form::label('contact_category_id', __('contact.Category'))); ?>

                                    <?php if(permissionCheck('category.contact.store')): ?>
                                        <label class="primary_input_label green_input_label" for="">
                                            <a href="<?php echo e(route('category.contact.create', ['quick_add' => true])); ?>"
                                               class="btn-modal"
                                               data-container="contact_category_add_modal"><?php echo e(__('case.Create New')); ?>

                                                <i class="fas fa-plus-circle"></i></a></label>
                                    <?php endif; ?>
                                </div>
                                <?php echo e(Form::select('contact_category_id', $contact_categories, null, ['class' => 'primary_select', 'data-parsley-errors-container' => '#contact_category_id_error'])); ?>

                                <span id="contact_category_id_error"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-12">
                                <?php echo e(Form::label('name', __('contact.Name'), ['class' => 'required'])); ?>

                                <?php echo e(Form::text('name', null, ['required' => '','class' => 'primary_input_field required', 'placeholder' => __('contact.Name')])); ?>

                            </div>
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('mobile_no', __('contact.Mobile No'))); ?>

                                <?php echo e(Form::number('mobile_no', null, ['class' => 'primary_input_field ', 'placeholder' => __('contact.Mobile No')])); ?>

                            </div>
                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('email', __('contact.Email'))); ?>

                                <?php echo e(Form::email('email', null, ['class' => 'primary_input_field', 'placeholder' => __('contact.Email')])); ?>

                            </div>
                        </div>

                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <div class="primary_input">
                            <?php echo e(Form::label('description', __('contact.Description'))); ?>

                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('contact.Contact Description'), 'rows' => 5, 'data-parsley-errors-container' =>
                            '#description_error' ])); ?>

                            <span id="description_error"></span>
                        </div>

                        <div class="text-center mt-3">
                            <button type="submit" class="primary-btn semi_large2 fix-gr-bg submit" id="submit"
                                    value="submit"><?php echo e(__
                                ('common.Create')); ?></button>

                            <button type="button" class="primary-btn semi_large2 fix-gr-bg submitting" id="submit"
                                    disabled style="display: none;"><?php echo e(__
                                ('common.Creating')); ?>...
                            </button>
                        </div>
                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade animated contact_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function () {
            _formValidation();
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('contact.Create New Contact')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/contact/create.blade.php ENDPATH**/ ?>