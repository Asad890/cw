

<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px">
                                <?php if(request()->is('my-task')): ?>
                                    <?php echo e(__('task.My Task')); ?>

                                <?php elseif(request()->is('completed-task')): ?>
                                    <?php echo e(__('task.Completed Task')); ?>

                                <?php else: ?>
                                    <?php echo e(__('task.Task List')); ?>

                                <?php endif; ?>

                            </h3>
                            <?php if(request()->is('task')): ?>
                                <ul class="d-flex">
                                    <?php if(permissionCheck('task.store')): ?>
                                        <li><a class="primary-btn radius_30px mr-10 fix-gr-bg"
                                                href="<?php echo e(route('task.create')); ?>"><i
                                                    class="ti-plus"></i><?php echo e(__('task.New Task')); ?></a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                        <tr>
                                            <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Name')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Priority')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Case')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Assigned to')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Due Date')); ?></th>
                                            <th scope="col"><?php echo e(__('task.Stage')); ?></th>
                                            <th scope="col"><?php echo e(__('common.Status')); ?></th>
                                            <th scope="col"><?php echo e(__('common.Actions')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>

                                                <td><?php echo e($loop->index + 1); ?></td>
                                                <td><?php echo e($model->name); ?></td>
                                                <td><?php echo e(__('common.' . $model->priority)); ?></td>
                                                
                                                <td><?php echo e($model->case_id); ?></td>
                                                <td><?php echo e($model->case_id); ?></td>
                                                <td><?php echo e(formatDate($model->due_date)); ?></td>
                                                <td><?php echo e(@$model->stage->name); ?></td>
                                                <td><?php echo $model->status != 1
                                                    ? '<span class="badge_3"> ' . __('common.Pending') . ' </span>'
                                                    : '<span class="badge_1"> ' . __('common.Complete') . '  </span>'; ?></td>
                                                <td>
                                                    <div class="dropdown CRM_dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                            <?php echo e(__('common.Select')); ?>

                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right"
                                                            aria-labelledby="dropdownMenu2">
                                                            <?php if(permissionCheck('task.edit')): ?>
                                                                <a href="<?php echo e(route('task.edit', $model->id)); ?>"
                                                                    class="dropdown-item edit_brand"><?php echo e(__('common.Edit')); ?></a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('task.show')): ?>
                                                                <a href="<?php echo e(route('task.show', $model->id)); ?>"
                                                                    class="dropdown-item edit_brand"><?php echo e(__('common.Show')); ?></a>
                                                            <?php endif; ?>
                                                            <?php if(permissionCheck('task.destroy')): ?>
                                                                <span style="cursor: pointer;"
                                                                    data-url="<?php echo e(route('task.destroy', $model->id)); ?>"
                                                                    id="delete_item"
                                                                    class="dropdown-item edit_brand"><?php echo e(__('common.Delete')); ?></span>
                                                            <?php endif; ?>
                                                            <?php if($model->status != 1): ?>
                                                                <a href="<?php echo e(route('task.completed', $model->id)); ?>"
                                                                    class="dropdown-item edit_brand"><?php echo e(__('common.Mark as completed')); ?></a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function() {

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('task.Task')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Task/Resources/views/task/index.blade.php ENDPATH**/ ?>