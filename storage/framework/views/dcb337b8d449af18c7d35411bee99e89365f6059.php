<table class="table Crm_table_active3">
    <thead>
    <tr>

        <th scope="col"><?php echo e(__('common.SL')); ?></th>
        <th><?php echo e(__('case.Case')); ?></th>
        <th><?php echo e(__('case.Client')); ?></th>
        <th><?php echo e(__('case.Details')); ?></th>
        <th class="noprint"><?php echo e(__('common.Actions')); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($loop->index + 1); ?></td>
            <td>
                <b><?php echo e(__('case.Case No.')); ?>: </b> <?php echo e($model->case_category? $model->case_category->name : ''); ?>/<?php echo e($model->case_no); ?> <br>
                <a href="<?php echo e(route('category.case.show', $model->case_category_id)); ?>"><b><?php echo e(__('case.Category')); ?>: </b> <?php echo e($model->case_category? $model->case_category->name : ''); ?></a>  <br>
                <a href="<?php echo e(route('case.show', $model->id)); ?>"><b><?php echo e(__('case.Title')); ?>: </b><?php echo e($model->title); ?>

                </a>
                <br>
                <b><?php echo e(__('case.Next Hearing Date')); ?>: </b> <?php echo e(formatDate($model->hearing_date)); ?> <br>
                <b><?php echo e(__('case.Filing Date')); ?>: </b> <?php echo e(formatDate($model->filling_date)); ?>

            </td>
            <td>
                <?php if($model->client == 'Plaintiff' and $model->plaintiff_client): ?>
                    <a href="<?php echo e(route('client.show', $model->plaintiff_client->id)); ?>"><b><?php echo e(__('case.Name')); ?></b>: <?php echo e($model->plaintiff_client->name); ?></a> <br>
                    <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->plaintiff_client->mobile); ?> <br>
                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->plaintiff_client->email); ?> <br>
                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->plaintiff_client->address); ?>

                    <?php echo e($model->plaintiff_client->district ? ', '. $model->plaintiff_client->district->name : ''); ?>

                    <?php echo e($model->plaintiff_client->division ? ', '. $model->plaintiff_client->division->name : ''); ?>

                <?php elseif($model->client == 'Opposite' and $model->opposite_client): ?>
                    <a href="<?php echo e(route('client.show', $model->opposite_client->id)); ?>"><b><?php echo e(__('case.Name')); ?></b>: <?php echo e($model->opposite_client->name); ?></a> <br>
                    <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->opposite_client->mobile); ?> <br>
                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->opposite_client->email); ?> <br>
                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->opposite_client->address); ?>

                    <?php echo e($model->opposite_client->district ? ', '. $model->opposite_client->district->name : ''); ?>

                    <?php echo e($model->opposite_client->division ? ', '. $model->opposite_client->division->name : ''); ?>

                <?php endif; ?>
            </td>
            <td>
                <?php if($model->court): ?>
                    <a href="<?php echo e(route('master.court.show', $model->court_id)); ?>"><b><?php echo e(__('case.Court')); ?></b>: <?php echo e($model->court->name); ?> </a><br>
                    <a href="<?php echo e(route('category.court.show', $model->court_category_id)); ?>"> <b><?php echo e(__('case.Category')); ?></b>: <?php echo e($model->court->court_category ? $model->court->court_category->name : ''); ?> </a><br>
                    <b><?php echo e(__('case.Room No')); ?>: </b> <?php echo e($model->court->room_number); ?> <br>
                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->court->location); ?>

                    <?php echo e($model->court->district ? ', '. $model->court->district->name : ''); ?>

                    <?php echo e($model->court->division ? ', '. $model->court->division->name : ''); ?>

                <?php endif; ?>
            </td>

            <td>


                <div class="dropdown CRM_dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenu2" data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <?php echo e(__('common.Select')); ?>

                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">

                        <a href="<?php echo e(route('case.show', $model->id)); ?>" class="dropdown-item"><i class="icon-file-eye"></i> <?php echo e(__('common.View')); ?></a>
                        <?php if(!$model->judgement): ?>
                            <a href="<?php echo e(route('case.edit', $model->id)); ?>" class="dropdown-item"><i class="icon-pencil mr-2"></i><?php echo e(__('common.Edit')); ?></a>
                            <a href="<?php echo e(route('date.create', ['case' => $model->id])); ?>" class="dropdown-item"><i
                                    class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Date')); ?></a>
                            <a href="<?php echo e(route('putlist.create', ['case' => $model->id])); ?>" class="dropdown-item"><i
                                    class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Put Up Date')); ?></a>
                            <a href="<?php echo e(route('judgement.create', ['case' => $model->id])); ?>" class="dropdown-item"><i
                                    class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Judgement Date')); ?></a>
                        <?php endif; ?>

                    </div>
                </div>


            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/causelist_data.blade.php ENDPATH**/ ?>