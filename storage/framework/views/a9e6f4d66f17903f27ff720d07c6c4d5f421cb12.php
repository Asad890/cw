<!-- sidebar part here -->
<nav id="sidebar" class="sidebar ">

    <div class="sidebar-header update_sidebar">
        <a class="large_logo" href="<?php echo e(url('/home')); ?>">
            <img src="<?php echo e(asset(config('configs')->where('key','site_logo')->first()->value)); ?>" alt="">
        </a>
        <a class="mini_logo" href="<?php echo e(url('/home')); ?>">
            <img src="<?php echo e(asset(config('configs')->where('key','site_logo')->first()->value)); ?>" alt="">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    <?php if(auth()->user()->role_id): ?>
        <ul id="sidebar_menu">

            <li>
                <a class="<?php echo e(spn_active_link('home')); ?>" href="<?php echo e(url('/home')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('dashboard.Dashboard')); ?></span>
                    </div>
                </a>
            </li>

            <?php if(permissionCheck('contact.index')): ?>

                <?php
                    $contact = ['contact.index', 'contact.create', 'contact.edit', 'contact.show'];
                    $category = ['category.contact.index', 'category.contact.create', 'category.contact.edit', 'category.contact.show'];
                    $nav = array_merge($contact, $category)
                ?>

                <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">

                            <span class="far fa-address-book"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('contact.Contact')); ?></span>
                        </div>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo e(route('contact.index')); ?>"
                               class="<?php echo e(spn_active_link($contact, 'active')); ?>">  <?php echo e(__('contact.Contact List')); ?></a>
                        </li>
                        <?php if(permissionCheck('category.contact.index')): ?>
                            <li>
                                <a href="<?php echo e(route('category.contact.index')); ?>"
                                   class="<?php echo e(spn_active_link($category, 'active')); ?>"><?php echo e(__('contact.Contact  Category')); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if(permissionCheck('client')): ?>
                <?php

                    $client = ['client.index', 'client.create', 'client.edit', 'client.show'];
                     $category = ['category.client.index', 'category.client.create', 'category.client.edit', 'category.client.show'];
                     $nav = array_merge($client, $category, ['client.settings'])

                ?>

                <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">

                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-users"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('client.Client')); ?></span>
                        </div>
                    </a>
                    <ul>
                    
                        <?php if(permissionCheck('client.index')): ?>
                        <li>
                            <a href="<?php echo e(route('client.index')); ?>"
                               class="<?php echo e(spn_active_link($client, 'active')); ?>">  <?php echo e(__('client.Client List')); ?></a>
                        </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('category.client.index')): ?>
                            <li>
                                <a href="<?php echo e(route('category.client.index')); ?>"
                                   class="<?php echo e(spn_active_link($category, 'active')); ?>"><?php echo e(__('client.Client Category')); ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if ($__env->exists('clientlogin::menu')) echo $__env->make('clientlogin::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if(permissionCheck('case.index')): ?>
                <?php
                    $case = ['case.index', 'case.edit', 'case.show', 'date.create', 'date.edit', 'putlist.create', 'putlist.edit', 'judgement.create', 'judgement.edit', 'case.court.change', 'date.send_mail' ];
                    $category = ['category.case.index', 'category.case.create', 'category.case.edit', 'category.case.show'];

                    $nav = array_merge($case, $category, ['causelist.index', 'case.create', 'judgement.index', 'judgement.closed', 'judgement.reopen', 'judgement.close', 'case.filter'])
                ?>

                <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">

                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">

                            <span class="fas fa-list-ul"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('case.Case')); ?></span>
                        </div>
                    </a>
                    <ul>
                    <?php if(permissionCheck('case.store')): ?>
                            <li>
                                <a href="<?php echo e(route('case.create')); ?>"
                                   class="<?php echo e(spn_active_link('case.create', 'active')); ?>"> <?php echo e(__('case.Add New Case')); ?></a>
                            </li>
                        <?php endif; ?>
                        <li>
                                <a href="<?php echo e(route('case.active')); ?>"
                                   class="<?php echo e(spn_active_link('case.active', 'active')); ?>"> <?php echo e(__('Active Cases')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('case.today')); ?>"
                               class="<?php echo e(spn_active_link('case.today', 'active')); ?>"> <?php echo e(__('Today Cases')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('case.tomorrow')); ?>"
                               class="<?php echo e(spn_active_link('case.tomorrow', 'active')); ?>"> <?php echo e(__('Tomorrow Cases')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('case.dailyboard')); ?>"
                               class="<?php echo e(spn_active_link('case.dailyboard', 'active')); ?>"> <?php echo e(__('Daily Board')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('case.dateawaited')); ?>"
                               class="<?php echo e(spn_active_link('case.dateawaited', 'active')); ?>"> <?php echo e(__('Date Awaited Cases')); ?></a>
                        </li>
                        <?php if(permissionCheck('causelist.index')): ?>
                            <li>
                                <a href="<?php echo e(route('causelist.index')); ?>"
                                   class="<?php echo e(spn_active_link('causelist.index', 'active')); ?>">  <?php echo e(__('case.Cause List')); ?></a>
                            </li>
                        <?php endif; ?>

                        <li>
                            <a href="<?php echo e(route('case.index')); ?>"
                               class="<?php echo e((isset($page_title) and $page_title != 'Running') ? 'active' : ''); ?>"> <?php echo e(__('All Cases')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('case.index', ['status' => 'Running'])); ?>"
                               class="<?php echo e((isset($page_title) and $page_title== 'Running') ? 'active' : ''); ?>"> <?php echo e(__('case.Running Case')); ?></a>
                        </li>
                        
                        <?php if(permissionCheck('category.case.index')): ?>
                            <li>
                                <a href="<?php echo e(route('category.case.index')); ?>"
                                   class="<?php echo e(spn_active_link($category, 'active')); ?>"><?php echo e(__('case.Case  Category')); ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.index')): ?>
                            <li>
                                <a href="<?php echo e(route('judgement.index')); ?>"
                                   class="<?php echo e(spn_active_link(['judgement.index', 'judgement.reopen', 'judgement.close'], 'active')); ?>"> <?php echo e(__('case.Judgement Case')); ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('judgement.closed')): ?>
                            <li>
                                <a href="<?php echo e(route('judgement.closed')); ?>"
                                   class="<?php echo e(spn_active_link(['judgement.closed'], 'active')); ?>"> <?php echo e(__('case.Closed Case')); ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if(permissionCheck('case.filter')): ?>
                            <li>
                                <a href="<?php echo e(route('case.filter')); ?>"
                                   class="<?php echo e(spn_active_link(['case.filter'], 'active')); ?>"> <?php echo e(__('case.Filter Case')); ?></a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>

            <li>
                <a class="<?php echo e(spn_active_link('advance/search')); ?>" href="<?php echo e(url('/advance/search')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Advance Search')); ?></span>
                    </div>
                </a>
            </li>
            <li>
                <a class="<?php echo e(spn_active_link('bare_acts')); ?>" href="<?php echo e(url('/bare_acts')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Bare Facts Proforma')); ?></span>
                    </div>
                </a>
            </li>
            <li>
                <a class="<?php echo e(spn_active_link('archive')); ?>" href="<?php echo e(url('/archive')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Archive')); ?></span>
                    </div>
                </a>
            </li>
            <li>
                <a class="<?php echo e(spn_active_link('display_board')); ?>" href="<?php echo e(url('/display_board')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Display Board')); ?></span>
                    </div>
                </a>
            </li>
            <li>
                <a class="<?php echo e(spn_active_link('connected_matters')); ?>" href="<?php echo e(url('/connected_matters')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Connected Matters')); ?></span>
                    </div>
                </a>
            </li>
            <!-- Calculator -->
            <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">

                            <span class="far fa-address-book"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('Calculator')); ?></span>
                        </div>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo e(url('/court_fee_cal')); ?>"
                               class="<?php echo e(spn_active_link('court_list', 'active')); ?>">  <?php echo e(__('Court fee')); ?></a>
                        </li>
                        
                            <li>
                                <a href="<?php echo e(url('/calculation')); ?>"
                                   class="<?php echo e(spn_active_link('calculation', 'active')); ?>"><?php echo e(__('Limitation')); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('/calculation_of_int')); ?>"
                                   class="<?php echo e(spn_active_link('calculation_of_int','active')); ?>"><?php echo e(__('Interest')); ?></a>
                            </li>
                        
                    </ul>
            </li>
            <!-- Calculator -->
            <li>
                <a class="<?php echo e(spn_active_link('my_client')); ?>" href="<?php echo e(url('/my_client')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('My Clients')); ?></span>
                    </div>
                </a>
            </li>
            <li>
                <a class="<?php echo e(spn_active_link('court_list')); ?>" href="<?php echo e(url('/court_list')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Court List')); ?></span>
                    </div>
                </a>
            </li>
            <!-- <li>
                <a class="<?php echo e(spn_active_link('court_fee_cal')); ?>" href="<?php echo e(url('/court_fee_cal')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Court Fee')); ?></span>
                    </div>
                </a>
            </li> -->
            <!-- <li>
                <a class="<?php echo e(spn_active_link('calculation')); ?>" href="<?php echo e(url('/calculation')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Limitation')); ?></span>
                    </div>
                </a>
            </li> -->
            <!-- <li>
                <a class="<?php echo e(spn_active_link('calculation_of_int')); ?>" href="<?php echo e(url('/calculation_of_int')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Interest')); ?></span>
                    </div>
                </a>
            </li> -->
            <li>
                <a class="<?php echo e(spn_active_link('ocr')); ?>" href="<?php echo e(url('/ocr')); ?>">
                    <div class="nav_icon_small">
                        <span class="fas fa-th"></span>
                    </div>
                    <div class="nav_title">
                        <span><?php echo e(__('Image to Text')); ?></span>
                    </div>
                </a>
            </li>
            <?php if(permissionCheck('lawyer.index')): ?>
                <?php
                    $lawyer = ['lawyer.index', 'lawyer.create', 'lawyer.edit', 'lawyer.show']
                ?>

                <li class="<?php echo e(spn_active_link($lawyer, 'mm-active')); ?>">
                    <a href="<?php echo e(route('lawyer.index')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-users"></span>
                        </div>
                        <div class="nav_title">
                            <span> <?php echo e(__('lawyer.Opposite Lawyer')); ?></span>
                        </div>
                    </a>
                </li>
            <?php endif; ?>


            <?php if(permissionCheck('lobbying.index')): ?>
                <li class="<?php echo e(spn_active_link(['lobbying.index', 'lobbying.edit', 'lobbying.show'], 'mm-active')); ?>">
                    <a href="<?php echo e(route('lobbying.index')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-th"></span>
                        </div>
                        <div class="nav_title">
                            <span> <?php echo e(__('case.Lobbying List')); ?></span>
                        </div>
                    </a>
                </li>
            <?php endif; ?>
            <?php if(permissionCheck('putlist.index')): ?>
                <li class="<?php echo e(spn_active_link('putlist.index', 'mm-active')); ?>">
                    <a href="<?php echo e(route('putlist.index')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-th"></span>
                        </div>
                        <div class="nav_title">
                            <span> <?php echo e(__('case.Put Up Date List')); ?></span>
                        </div>
                    </a>
                </li>
            <?php endif; ?>



            <?php if(permissionCheck('court.index')): ?>
                <?php
                    $court = ['master.court.index', 'master.court.edit', 'master.court.show', 'master.court.create'];
                    $category = ['category.court.index', 'category.court.create', 'category.court.edit', 'category.court.show'];
                    $nav = array_merge($court, $category)
                ?>

                <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-gavel"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('court.Court')); ?></span>
                        </div>
                    </a>
                    <ul>
                        <?php if(permissionCheck('master.court.index')): ?>
                        <li>
                            <a href="<?php echo e(route('master.court.index')); ?>"
                               class="<?php echo e(spn_active_link($court, 'active')); ?>"> <?php echo e(__('court.Court List')); ?></a>
                        </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('category.court.index')): ?>
                        <li>
                                <a href="<?php echo e(route('category.court.index')); ?>"
                                   class="<?php echo e(spn_active_link($category, 'active')); ?>"> <?php echo e(__('court.Court Category')); ?></a>
                        </li>
                        <?php endif; ?>

                    </ul>
                </li>

            <?php endif; ?>

            <?php if(permissionCheck('appointment.index')): ?>
                <?php
                    $appoinment = ['appointment.index', 'appointment.create', 'appointment.edit', 'appointment.show']
                ?>

                <li class="<?php echo e(spn_active_link($appoinment, 'mm-active')); ?>">
                    <a href="<?php echo e(route('appointment.index')); ?>">
                        <div class="nav_icon_small">
                            <span class="far fa-handshake"></span>
                        </div>
                        <div class="nav_title">
                            <span> <?php echo e(__('appointment.Appointment')); ?></span>
                        </div>
                    </a>
                </li>
            <?php endif; ?>

            <?php echo $__env->make('task::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('todo::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('partials.hr-menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('leave::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


            <?php if(permissionCheck('setup')): ?>
                <?php
                    $stage = ['master.stage.index', 'master.stage.edit', 'master.stage.show', 'master.stage.create'];
                    $act = ['master.act.index', 'master.act.edit', 'master.act.show', 'master.act.create'];
                    $city = ['setup.city.index', 'setup.city.edit', 'setup.city.show', 'setup.city.create'];
                    $state = ['setup.state.index', 'setup.state.edit', 'setup.state.show', 'setup.state.create'];
                    $country = ['setup.country.index', 'setup.country.edit', 'setup.country.show', 'setup.country.create'];

                    $nav = array_merge($stage, $act, $city, $state, $country);
                ?>

                <li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
                    <a href="javascript:" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
                        <div class="nav_icon_small">
                            <span class="fas fa-user"></span>
                        </div>
                        <div class="nav_title">
                            <span><?php echo e(__('common.Setup')); ?></span>
                        </div>
                    </a>
                    <ul>
                        <?php if(permissionCheck('master.stage.index')): ?>
                            <li>
                                <a href="<?php echo e(route('master.stage.index')); ?>"
                                   class="<?php echo e(spn_active_link($stage, 'active')); ?>">  <?php echo e(__('case.Case Stage')); ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('master.act.index')): ?>
                            <li>
                                <a href="<?php echo e(route('master.act.index')); ?>"
                                   class="<?php echo e(spn_active_link($act, 'active')); ?>"><?php echo e(__('case.Act')); ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if(permissionCheck('setup.city.index')): ?>
                            <li>
                                <a href="<?php echo e(route('setup.city.index')); ?>"
                                   class="<?php echo e(spn_active_link($city, 'active')); ?>"><?php echo e(__('setting.City')); ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if(permissionCheck('setup.state.index')): ?>
                            <li>
                                <a href="<?php echo e(route('setup.state.index')); ?>"
                                   class="<?php echo e(spn_active_link($state, 'active')); ?>"><?php echo e(__('setting.State')); ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if(permissionCheck('setup.country.index')): ?>
                            <li>
                                <a href="<?php echo e(route('setup.country.index')); ?>"
                                   class="<?php echo e(spn_active_link($country, 'active')); ?>"><?php echo e(__('court.Country')); ?></a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>


            <?php if ($__env->exists('customfield::menu')) echo $__env->make('customfield::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <?php if ($__env->exists('finance::menu')) echo $__env->make('finance::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php if ($__env->exists('setting::menu')) echo $__env->make('setting::menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </ul>
    <?php else: ?>
        <?php if ($__env->exists('clientlogin::sidebar')) echo $__env->make('clientlogin::sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
</nav>
<!-- sidebar part end -->
<?php /**PATH D:\updates 123\cw\resources\views/partials/sidebar.blade.php ENDPATH**/ ?>