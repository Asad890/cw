<?php if(permissionCheck('human_resource')): ?>
<?php
    $staffs = ['staffs.index', 'staffs.edit', 'staffs.view', 'staffs.create'];
    $roles = ['permission.roles.index', 'permission.roles.edit', 'permission.roles.view', 'permission.roles.create', 'permission.permissions.index'];
    $events = ['events.index', 'events.edit', 'events.view', 'events.create'];
    $payroll = ['payroll.index', 'payroll.edit', 'payroll.view', 'payroll.create', 'genrate_payroll', 'staff_search_for_payroll'];
    
    
    $nav = array_merge($staffs, $roles, $events, $payroll, ['attendances.index', 'attendance_report.index', 'payroll_reports.index', 'payroll_reports.search', 'attendance_report.search']);
?>

<li class="<?php echo e(spn_nav_item_open($nav, 'mm-active')); ?>">
        <a href="javascript:;" class="has-arrow" aria-expanded="<?php echo e(spn_nav_item_open($nav, 'true')); ?>">
            <div class="nav_icon_small">
                <span class="fas fa-users"></span>
            </div>
            <div class="nav_title">
                <span><?php echo e(__('common.Human Resource')); ?></span>
            </div>
        </a>
        <ul>
            <?php if(permissionCheck('staffs.index')): ?>
                <li>
                    <a href="<?php echo e(route('staffs.index')); ?>" class="<?php echo e(spn_active_link($staffs, 'active')); ?>"><?php echo e(__('common.Staff')); ?></a>
                </li>
            <?php endif; ?>
            <?php if(permissionCheck('permission.roles.index')): ?>
                <li>
                    <a href="<?php echo e(route('permission.roles.index')); ?>" class="<?php echo e(spn_active_link($roles, 'active')); ?>"><?php echo e(__('role.Role')); ?></a>
                </li>
            <?php endif; ?>
          
            <?php if(permissionCheck('attendances.index')): ?>
                <li>
                    <a href="<?php echo e(route('attendances.index')); ?>" class="<?php echo e(spn_active_link('attendances.index', 'active')); ?>"><?php echo e(__('attendance.Attendance')); ?></a>
                </li>
            <?php endif; ?>
            <?php if(permissionCheck('attendance_report.index')): ?>
                <li>
                    <a href="<?php echo e(route('attendance_report.index')); ?>" class="<?php echo e(spn_active_link(['attendance_report.index', 'attendance_report.search'], 'active')); ?>"><?php echo e(__('attendance.Attendance Report')); ?></a>
                </li>
            <?php endif; ?>
            <?php if(permissionCheck('events.index')): ?>
                <li>
                    <a href="<?php echo e(route('events.index')); ?>" class="<?php echo e(spn_active_link($events, 'active')); ?>"><?php echo e(__('event.Event')); ?></a>
                </li>
            <?php endif; ?>
            
            <?php if(permissionCheck('payroll.index')): ?>
                <li>
                    <a href="<?php echo e(route('payroll.index')); ?>" class="<?php echo e(spn_active_link($payroll, 'active')); ?>"><?php echo e(__('payroll.Payroll')); ?></a>
                </li>
            <?php endif; ?>
            <?php if(permissionCheck('payroll_reports.index')): ?>
                <li>
                    <a href="<?php echo e(route('payroll_reports.index')); ?>" class="<?php echo e(spn_active_link(['payroll_reports.index', 'payroll_reports.search'], 'active')); ?>"><?php echo e(__('payroll.Payroll Reports')); ?></a>
                </li>
            <?php endif; ?>
        
        </ul>
    </li>
    <?php endif; ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/partials/hr-menu.blade.php ENDPATH**/ ?>