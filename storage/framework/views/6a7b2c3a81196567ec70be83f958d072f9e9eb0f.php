

<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('case.Filter Case')); ?> </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-3">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::open(['route' => 'case.filter', 'method' => 'get']); ?>


                        <div class="row">
                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('case_category_id', __('case.Case Category'))); ?>

                                <?php echo e(Form::select('case_category_id', $case_categories, $case_category_id, ['class' => 'primary_select', 'data-placeholder' => __('case.Select Case Category'), 'data-parsley-errors-container' => '#case_category_id_error'])); ?>

                                <span id="case_category_id_error"></span>
                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('case_no', __('case.Case No'))); ?>

                                <?php echo e(Form::text('case_no', $case_no, ['class' => 'primary_input_field', 'placeholder' => __('case.Case No')])); ?>

                            </div>
                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('file_no', __('case.Case File No'))); ?>

                                <?php echo e(Form::text('file_no', $file_no, ['class' => 'primary_input_field', 'placeholder' => __('case.Case File No')])); ?>

                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('acts', __('case.Case Acts'))); ?>

                                <?php echo e(Form::select('acts[]', $db_acts, $acts, ['class' => 'primary_select', 'data-placeholder' => __('case.Select Acts'), 'data-parsley-errors-container' => '#act_error', 'multiple' => '', 'id' => 'acts'])); ?>

                                <span id="act_error"></span>
                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('client_id', __('case.Client'))); ?>

                                <?php echo e(Form::select('client_id', $clients->prepend(__('case.Select Client'), ''), $client_id, ['class' => 'primary_select', 'data-placeholder' => __('case.Select Client'), 'data-parsley-errors-container' => '#client_id_error'])); ?>

                                <span id="client_id_error"></span>
                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('court_id', __('case.Court'))); ?>

                                <?php echo e(Form::select('court_id', $courts, $court_id, ['class' => 'primary_select', 'data-placeholder' => __('case.Select Court'), 'data-parsley-errors-container' => '#court_id_error'])); ?>

                                <span id="court_id_error"></span>
                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('stage_id', __('case.Case Stage'))); ?>

                                <?php echo e(Form::select('stage_id', $stages, $stage_id, ['class' => 'primary_select', 'data-placeholder' => __('case.Select Case Stage'), 'data-parsley-errors-container' => '#stage_id_error'])); ?>

                                <span id="stage_id_error"></span>
                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('receiving_date', __('case.Receiving Date'))); ?>

                                <?php echo e(Form::text('receiving_date', $receiving_date, ['class' => 'primary_input_field primary-input date form-control date', 'id' => 'receiving_date', 'placeholder' => __('case.Date')])); ?>

                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('filling_date', __('case.Filling Date'))); ?>

                                <?php echo e(Form::text('filling_date', $filling_date, ['class' => 'primary_input_field primary-input date form-control date', 'id' => 'filling_date', 'placeholder' => __('case.Filling Date')])); ?>

                            </div>

                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('hearing_date', __('case.Hearing Date'))); ?>

                                <?php echo e(Form::text('hearing_date', $hearing_date, ['class' => 'primary_input_field primary-input date form-control date', 'id' => 'hearing_date', 'placeholder' => __('case.Hearing Date')])); ?>

                            </div>
                            <div class="primary_input col-md-3">
                                <?php echo e(Form::label('judgement_date', __('case.Judgement Date'))); ?>

                                <?php echo e(Form::text('judgement_date', $judgement_date, ['class' => 'primary_input_field primary-input date form-control date', 'id' => 'judgement_date', 'placeholder' => __('case.Judgement Date')])); ?>

                            </div>


                            <div class="col-md-3 mt-40">
                                <div class="primary_input mb-15 d-flex align-items-center justify-content-between gap_10">
                                    <button type="submit" class="primary-btn fix-gr-bg flex-grow-1 text-nowrap"
                                        id="submit" value="submit"><i
                                            class="ti-search"></i><?php echo e(__('case.Get List')); ?></button>
                                    <a href="<?php echo e(route('case.filter')); ?>"
                                        class="primary-btn fix-gr-bg flex-grow-1 text-nowrap"><i
                                            class="ti-back-left"></i><?php echo e(__('case.Reset')); ?></a>
                                </div>

                            </div>


                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('case.Filter Case List')); ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                        <tr>

                                            <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                            <th><?php echo e(__('case.Case')); ?></th>
                                            <th><?php echo e(__('case.Plaintiff')); ?></th>
                                            <th><?php echo e(__('case.Opposite')); ?></th>
                                            <th><?php echo e(__('case.Court')); ?></th>
                                            <th><?php echo e(__('case.Date')); ?></th>
                                            <th><?php echo e(__('case.Acts')); ?></th>
                                            <th class="noprint"><?php echo e(__('common.Actions')); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($loop->index + 1); ?></td>
                                                <td>

                                                    <b><?php echo e(__('case.Case No.')); ?>: </b>
                                                    <?php echo e($model->case_category ? $model->case_category : ''); ?>/<?php echo e($model->case_no); ?>,
                                                    <br>
                                                    <b><?php echo e(__('case.File No.')); ?>: </b> <?php echo e($model->file_no); ?>, <br>
                                                    <b><?php echo e(__('case.Category')); ?>: </b>
                                                    <?php echo e($model->case_category ? $model->case_category : ''); ?>, <br>
                                                    <b><?php echo e(__('case.Title')); ?>: </b><?php echo e($model->title); ?>,
                                                    <br>
                                                    <b><?php echo e(__('case.Case Stage')); ?>: </b> <?php echo e(@$model->stage->name); ?>,
                                                </td>
                                                <td>

                                                    <b><?php echo e(__('case.Name')); ?></b>: <?php echo e(@$model->plaintiff_client->name); ?>

                                                    <br>
                                                    <b><?php echo e(__('case.Mobile')); ?>: </b>
                                                    <?php echo e(@$model->plaintiff_client->mobile); ?> <br>
                                                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e(@$model->plaintiff_client->email); ?>

                                                    <br>
                                                    <b><?php echo e(__('case.Address')); ?>: </b>
                                                    <?php echo e(@$model->plaintiff_client->address); ?>

                                                    <?php echo e(@$model->plaintiff_client->district ? ', ' . $model->plaintiff_client->district->name : ''); ?>

                                                    <?php echo e(@$model->plaintiff_client->division ? ', ' . $model->plaintiff_client->division->name : ''); ?>


                                                </td>
                                                <td>
                                                    <b><?php echo e(__('case.Name')); ?></b>: <?php echo e(@$model->opposite_client->name); ?>

                                                    <br>
                                                    <b><?php echo e(__('case.Mobile')); ?>: </b>
                                                    <?php echo e(@$model->opposite_client->mobile); ?> <br>
                                                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e(@$model->opposite_client->email); ?>

                                                    <br>
                                                    <b><?php echo e(__('case.Address')); ?>: </b>
                                                    <?php echo e(@$model->opposite_client->address); ?>

                                                    <?php echo e(@$model->opposite_client->district ? ', ' . $model->opposite_client->district->name : ''); ?>

                                                    <?php echo e(@$model->opposite_client->division ? ', ' . $model->opposite_client->division->name : ''); ?>

                                                </td>
                                                <td>
                                                    <?php if($model->court): ?>
                                                        <b><?php echo e(__('case.Court')); ?></b>: <?php echo e(@$model->court->name); ?><br>
                                                        <b><?php echo e(__('case.Category')); ?></b>:
                                                        <?php echo e(@$model->court->court_category ? $model->court->court_category->name : ''); ?><br>
                                                        <b><?php echo e(__('case.Room No')); ?>: </b>
                                                        <?php echo e(@$model->court->room_number); ?> <br>
                                                        <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e(@$model->court->location); ?>

                                                        <?php echo e(@$model->court->district ? ', ' . $model->court->district->name : ''); ?>

                                                        <?php echo e(@$model->court->division ? ', ' . $model->court->division->name : ''); ?>

                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <b><?php echo e(__('case.Next Hearing Date')); ?>: </b>
                                                    <?php echo e(formatDate($model->hearing_date)); ?> <br>
                                                    <b><?php echo e(__('case.Filing Date')); ?>: </b>
                                                    <?php echo e(formatDate($model->filling_date)); ?>,
                                                    <br>
                                                    <b><?php echo e(__('case.Receiving Date')); ?>: </b>
                                                    <?php echo e(formatDate($model->receiving_date)); ?>,
                                                    <br>
                                                    <b><?php echo e(__('case.Judgment Date')); ?>: </b>
                                                    <?php echo e(formatDate($model->judgment_date)); ?>,
                                                </td>
                                                <td>
                                                    <b><?php echo e(__('case.Acts')); ?>: </b>
                                                    <?php if($model->acts): ?>
                                                        <?php $__currentLoopData = $model->acts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $act): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($act->act ? $act->act->name . ', ' : ''); ?>

                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </td>

                                                <td>


                                                    <div class="dropdown CRM_dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                            <?php echo e(__('common.Select')); ?>

                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right"
                                                            aria-labelledby="dropdownMenu2">

                                                            <a href="<?php echo e(route('case.show', $model->id)); ?>"
                                                                class="dropdown-item"><i class="icon-file-eye"></i>
                                                                <?php echo e(__('common.View')); ?></a>
                                                            <?php if(!$model->judgement): ?>
                                                                <a href="<?php echo e(route('case.edit', $model->id)); ?>"
                                                                    class="dropdown-item"><i
                                                                        class="icon-pencil mr-2"></i><?php echo e(__('common.Edit')); ?></a>
                                                                <a href="<?php echo e(route('date.create', ['case' => $model->id])); ?>"
                                                                    class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Date')); ?></a>
                                                                <a href="<?php echo e(route('putlist.create', ['case' => $model->id])); ?>"
                                                                    class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Put Up Date')); ?></a>
                                                                <a href="<?php echo e(route('judgement.create', ['case' => $model->id])); ?>"
                                                                    class="dropdown-item"><i
                                                                        class="icon-calendar3 mr-2"></i><?php echo e(__('case.New Judgement Date')); ?></a>
                                                            <?php endif; ?>

                                                        </div>
                                                    </div>


                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>
        $(document).ready(function() {

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\updates 123\cw\resources\views/case/filter.blade.php ENDPATH**/ ?>