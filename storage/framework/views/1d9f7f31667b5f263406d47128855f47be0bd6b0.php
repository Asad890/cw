<!DOCTYPE html>

<?php if(rtl()): ?>
    <html dir="rtl" class="rtl">
    <?php else: ?>
        <html>
        <?php endif; ?>
        <head>

            <!-- Required meta tags -->
            <meta charset="utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            <link rel="icon" href="<?php echo e(asset(config('configs')->where('key','favicon_logo')->first()->value)); ?>"
                  type="image/png"/>

            <title><?php echo e(isset($title) ? $title .' | '. config('configs')->where('key','site_title')->first()->value :  config('configs')->where('key','site_title')->first()->value); ?></title>

            <meta name="_token" content="<?php echo csrf_token(); ?>"/>


            <!-- Bootstrap CSS -->


            <?php if(rtl()): ?>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/bootstrap.min.css')); ?>"/>
            <?php else: ?>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/bootstrap.css"/>
            <?php endif; ?>


            <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/vendors/font_awesome/css/all.min.css"/>

            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/themify-icons.css"/>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/flaticon.css"/>
            <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/font-awesome.min.css"/>


            <?php echo $__env->yieldContent('css'); ?>


            <?php if(rtl()): ?>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/style.css')); ?>"/>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/rtl/infix.css')); ?>"/>
            <?php else: ?>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/style.css')); ?>"/>
                <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/infix.css')); ?>"/>
            <?php endif; ?>

            <link rel="stylesheet" href="<?php echo e(asset('public/frontend/')); ?>/css/style.css"/>
            <!--  -->
            <?php echo $__env->yieldPushContent('css_before'); ?>

            <style>
                #main-content {
                    margin-right: 0;
                    margin-left: 0;
                    overflow: hidden;
                }

                @media (min-width: 1200px) {
                    #main-content {
                        padding: 0px;
                    }
                }

                #main-content {
                    width: 100%;
                }
                .white-box{
                    padding: 0px;
                }
                .student-details{
                    margin-top: 0px;
                }

            </style>

        </head>

        <body>

        <div class="main-wrapper" >

            <div id="main-content">

                <?php echo $__env->yieldContent('mainContent'); ?>


            </div>
        </div>


        <!-- ================Footer Area ================= -->
        <footer class="footer-area">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 text-center">
                        <p> <?php echo config('configs')->where('key', 'copyright_text')->first()->value; ?> </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- ================End Footer Area ================= -->


        <script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery-3.4.1.min.js"></script>

        <script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/popper.js">
        </script>

        <script src="<?php echo e(asset('public/backEnd/')); ?>/css/rtl/bootstrap.min.js"></script>

        <?php echo Toastr::message(); ?>

        <?php echo $__env->yieldPushContent('admin.scripts'); ?>
        <?php echo $__env->yieldPushContent('js_before'); ?>
        <?php echo $__env->yieldPushContent('js_after'); ?>
        <?php echo $__env->yieldPushContent('scripts'); ?>
        <script type="text/javascript">

            $(document).ready(function (){
                window.print();
                setTimeout(function (){
                    window.close()
                }, 3000);
            })

        </script>


        <?php echo $__env->yieldContent('script'); ?>

        </body>
        </html>


<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/layouts/print.blade.php ENDPATH**/ ?>