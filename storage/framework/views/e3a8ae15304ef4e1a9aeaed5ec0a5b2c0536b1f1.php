<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header">
                        <div class="main-title d-flex justify-content-between w-100">
                            <h3 class="mb-0 mr-30"><?php echo e(__('appointment.Create Appointment')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="white_box_50px box_shadow_white">

                        <?php echo Form::open(['route' => 'appointment.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                        <div class="row">
                            <div class="primary_input col-md-12">
                                <?php echo e(Form::label('title', __('appointment.Title'),['class' => 'required'])); ?>

                                <?php echo e(Form::text('title', null, ['required' => '','class' => 'primary_input_field', 'placeholder' => __('appointment.Title')])); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="primary_input col-md-6">

                                <div class="d-flex justify-content-between">
                                    <?php echo e(Form::label('contact_id', __('appointment.Contact'), ['class' => 'required'])); ?>

                                    <?php if(permissionCheck('contact.store')): ?>
                                        <label class="primary_input_label green_input_label" for="">
                                            <a href="<?php echo e(route('contact.create', ['quick_add' => true])); ?>"
                                               class="btn-modal"
                                               data-container="contact_add_modal"><?php echo e(__('case.Create New')); ?>

                                                <i class="fas fa-plus-circle"></i></a></label>
                                    <?php endif; ?>
                                </div>
                                <?php echo e(Form::select('contact_id', $contacts, null, ['required' => '', 'class' => 'primary_select', 'data-parsley-errors-container' => '#contact_id_error'])); ?>

                                <span id="contact_id_error"></span>
                            </div>

                            <div class="primary_input col-md-6">
                                <?php echo e(Form::label('date', __('appointment.Appointment Date'),['class' => 'required'])); ?>

                                <?php echo e(Form::text('date', date('Y-m-d H:i'), ['required' => '','class' => 'primary_input_field primary-input datetime form-control', "id"=>"fromDate",'placeholder' => __('appointment.Date')])); ?>

                            </div>
                        </div>
                        <div class="primary_input">
                            <?php echo e(Form::label('motive', __('appointment.Motive'),['class' => 'required'])); ?>

                            <?php echo e(Form::textarea('motive', null, ['required' => '','class' => 'primary_input_field', 'placeholder' => __('appointment.Appointment Motive'), 'rows' => 5, 'data-parsley-errors-container' =>
                            '#motive_error' ])); ?>

                            <span id="motive_error"></span>
                        </div>
                        <?php if ($__env->exists('customfield::fields', ['fields' => $fields, 'model' => null])) echo $__env->make('customfield::fields', ['fields' => $fields, 'model' => null], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        <div class="primary_input">
                            <?php echo e(Form::label('notes', __('appointment.Notes'))); ?>

                            <?php echo e(Form::textarea('notes', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('appointment.Appointment Notes'), 'rows' => 5, 'data-parsley-errors-container' =>
                            '#notes_error' ])); ?>

                            <span id="notes_error"></span>
                        </div>


                        <div class="text-center mt-3">
                            <button class="primary_btn_large submit" type="submit"><i
                                    class="ti-check"></i><?php echo e(__('common.Create')); ?>

                            </button>

                            <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                            </button>
                        </div>
                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade animated contact_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated contact_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {
            _formValidation();

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('appointment.Create New Appointment')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/appointment/create.blade.php ENDPATH**/ ?>