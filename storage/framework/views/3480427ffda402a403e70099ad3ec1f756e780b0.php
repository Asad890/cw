

<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab 1 =-->
                        <div class="tab-pane fade show active" id="count-days" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                <!-- Form --><h4 style="text-align:center ;">CALCULATION OF DAYS</h4>
                               
                                <div class="row form-group">
                                    <div class="primary_input col-md-3">
                                            <label>Start Date</label>
                                            <input type="text" name="court" class="primary_input_field primary-input form-control datetime" placeholder="Start Date">
                                    </div>
                                    <div class="primary_input col-md-6">
                                        <!-- Just Aisay hi -->
                                    </div>
                                    <div class="primary_input col-md-3">
                                            <label>End Date</label>
                                            <input type="text" name="court" class="primary_input_field primary-input form-control datetime" placeholder="End Date">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    
                                    
                                    <div class="primary_input col-md-12 text-center">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Calculate Duration')); ?>

                                        </button>
                                    </div>
                                 
                                </div>
                                <div class="row form-group">
                                    <div class="primary_input col-md-2">
                                            <label>Days:</label>
                                    </div>
                                    <div class="primary_input col-md-2">
                                            <input type="text" class="primary_input_field" placeholder="792 Days">
                                    </div>
                                    <div class="primary_input col-md-2">
                                        <label>Month and Days:</label>
                                    </div>
                                    <div class="primary_input col-md-2">
                                            <input type="text" class="primary_input_field" placeholder="234days">
                                    </div>
                                    <div class="primary_input col-md-2">
                                        <label>Year, Month and Days:</label>
                                    </div>
                                    <div class="primary_input col-md-2">
                                            <input type="text" class="primary_input_field" placeholder="304fs">
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>
                       
                    <!-- form end =-->
                </div>
            </div>


        </div>

        
        </section>
<?php $__env->stopSection(); ?>
      



<?php echo $__env->make('layouts.master', ['title' => __('Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/calculation/index.blade.php ENDPATH**/ ?>