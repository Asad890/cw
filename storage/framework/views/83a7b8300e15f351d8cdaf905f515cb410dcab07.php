<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('court.Court List')); ?></h3>
                            <ul class="d-flex">
                                <?php if(permissionCheck('master.court.store')): ?>
                                    <li><a class="primary-btn radius_30px mr-10 fix-gr-bg"
                                           href="<?php echo e(route('master.court.create')); ?>"><i class="ti-plus"></i><?php echo e(__
                        ('court.New Court')); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                    <tr>

                                        <th scope="col"><?php echo e(__('court.SL')); ?></th>
                                        <th><?php echo e(__('court.Category')); ?></th>
                                        <th><?php echo e(__('court.Court')); ?></th>
                                        <?php if(moduleStatusCheck('EmailtoCL')): ?>
                                            <th><?php echo e(__('case.Email')); ?></th>
                                        <?php endif; ?>
                                        <th><?php echo e(__('court.Location')); ?></th>
                                        <th><?php echo e(__('court.Actions')); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>

                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td>
                                                <?php if($model->court_category): ?>
                                                    <a href="<?php echo e(route('category.court.show', $model->court_category->id)); ?>"> <?php echo e($model->court_category->name); ?> </a>
                                                <?php else: ?>
                                                    <span class="badge_4"><?php echo e(__('court.No Category')); ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo e(route('master.court.show', $model->id)); ?>"><?php echo e($model->name); ?> <?php echo e($model->room_number ? ' ('.$model->room_number .')' : ''); ?></a>
                                            </td>
                                            <?php if(moduleStatusCheck('EmailtoCL')): ?>
                                                <td><?php echo e($model->email); ?></td>
                                            <?php endif; ?>
                                            <td>
                                                <?php echo e($model->state ? $model->state->name .', ' : ''); ?>

                                                <?php echo e($model->city ? $model->city->name .', ' : ''); ?>

                                                <?php echo e($model->location); ?>

                                            </td>

                                            <td>


                                                <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="dropdownMenu2">
                                                        <?php if(permissionCheck('master.court.show')): ?>
                                                            <a href="<?php echo e(route('master.court.show', $model->id)); ?>"
                                                               class="dropdown-item"><i class="icon-file-eye"></i> <?php echo e(__
                                                        ('common.View')); ?></a>
                                                        <?php endif; ?>
                                                        <?php if(permissionCheck('master.court.edit')): ?>
                                                            <a href="<?php echo e(route('master.court.edit', $model->id)); ?>"
                                                               class="dropdown-item"><i
                                                                    class="icon-pencil"></i> <?php echo e(__('common.Edit')); ?></a>
                                                        <?php endif; ?>
                                                        <?php if(permissionCheck('master.court.destroy')): ?>
                                                            <span id="delete_item" data-id="<?php echo e($model->id); ?>" data-url="<?php echo e(route
                                                            ('master.court.destroy', $model->id)); ?>" class="dropdown-item"><i class="icon-trash"></i> <?php echo e(__('common.Delete')); ?> </span>
                                                        <?php endif; ?>

                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {

        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('court.Court')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/master/court/index.blade.php ENDPATH**/ ?>