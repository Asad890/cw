
<?php $__env->startSection('mainContent'); ?>
<section class="mb-40 student-details">

<?php if(session()->has('message-success')): ?>
   <div class="alert alert-success">
      <?php echo e(session()->get('message-success')); ?>

   </div>
   <?php elseif(session()->has('message-danger')): ?>
   <div class="alert alert-danger">
      <?php echo e(session()->get('message-danger')); ?>

   </div>
   <?php endif; ?>
   <div class="container-fluid p-0">
      <div class="row">
         <div class="col-lg-3">
            <!-- Start Student Meta Information -->
            <div class="main-title">
               <h3 class="mb-20"><?php echo app('translator')->get('common.Staff Info'); ?></h3>
            </div>
            <div class="student-meta-box">
               <div class="student-meta-top"></div>
               <img class="student-meta-img img-100" src="<?php echo e(file_exists(@$staffDetails->user->avatar) ? asset(@$staffDetails->user->avatar) : asset('backEnd/img/staff.jpg')); ?>"  alt="">
               <div class="white-box">
                  <div class="single-meta mt-10">
                     <div class="d-flex justify-content-between">
                        <div class="name">
                           <?php echo e(__('common.Name')); ?>

                        </div>
                        <div class="value">
                           <?php if(isset($staffDetails)): ?><?php echo e(@$staffDetails->user->name); ?><?php endif; ?>
                        </div>
                     </div>
                  </div>

                  <div class="single-meta">
                     <div class="d-flex justify-content-between">
                        <div class="name">
                           <?php echo e(__('role.Role')); ?>

                        </div>
                        <div class="value">
                           <?php if(isset($staffDetails)): ?><?php echo e(@$staffDetails->user->role->name); ?><?php endif; ?>
                        </div>
                     </div>
                  </div>

                   <?php if($staffDetails->user->role_id != 1): ?>
                      <div class="single-meta">
                         <div class="d-flex justify-content-between">
                            <div class="name">
                               <?php echo e(__('common.Date of Joining')); ?>

                            </div>
                            <div class="value">
                               <?php if(isset($staffDetails)): ?>
                               <?php echo e(formatDate($staffDetails->date_of_joining)); ?>

                               <?php endif; ?>
                            </div>
                         </div>
                      </div>
                      <div class="single-meta">
                         <div class="d-flex justify-content-between">
                            <div class="name">
                               <?php echo e(__('common.Employment Type')); ?>

                            </div>
                            <div class="value">
                               <?php if(isset($staffDetails)): ?>
                               <?php echo e($staffDetails->employment_type); ?>

                               <?php endif; ?>
                            </div>
                         </div>
                      </div>
                      <div class="single-meta">
                         <div class="d-flex justify-content-between">
                            <div class="name">
                               <?php echo e(__('common.Last Date Of Provisional Period')); ?>

                            </div>
                            <div class="value">
                               <?php if(isset($staffDetails)): ?>
                               <?php echo e(formatDate(\Carbon\Carbon::now()->addMonths($staffDetails->provisional_months))); ?>

                               <?php endif; ?>
                            </div>
                         </div>
                      </div>
                  <?php endif; ?>
               </div>
            </div>
            <!-- End Student Meta Information -->
         </div>
         <!-- Start Student Details -->
         <div class="col-lg-9 staff-details">
            <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" href="#studentProfile" role="tab" data-toggle="tab"><?php echo e(__('common.Profile')); ?></a>
               </li>
               <?php if($staffDetails->user->role->type != "system_user"): ?>
                   <li class="nav-item">
                      <a class="nav-link" href="#payroll" role="tab" data-toggle="tab"><?php echo e(__('payroll.Payroll')); ?></a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="#leaves" role="tab" data-toggle="tab"><?php echo e(__('leave.Leave')); ?></a>
                   </li>
               <?php endif; ?>
               <li class="nav-item edit-button">
                  <a href="<?php echo e(route('staffs.edit', $staffDetails->id)); ?>" class="primary-btn small fix-gr-bg"><?php echo e(__('common.Edit')); ?>

                  </a>
               </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <!-- Start Profile Tab -->
               <div role="tabpanel" class="tab-pane fade show active" id="studentProfile">
                  <div class="white-box">
                     <h4 class="stu-sub-head"><?php echo e(__('common.Personal Info')); ?></h4>
                     <div class="single-info">
                        <div class="row">
                           <div class="col-lg-5 col-md-5">
                              <div class="">
                                 <?php echo e(__('common.Phone')); ?>

                              </div>
                           </div>
                           <div class="col-lg-7 col-md-6">
                              <div class="">
                                 <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->phone); ?><?php endif; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="single-info">
                        <div class="row">
                           <div class="col-lg-5 col-md-6">
                              <div class="">
                                 <?php echo e(__('common.Email')); ?>

                              </div>
                           </div>
                           <div class="col-lg-7 col-md-7">
                              <div class="">
                                 <?php if(isset($staffDetails)): ?><?php echo e(@$staffDetails->user->email); ?><?php endif; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php if($staffDetails->user->role_id != 1): ?>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-6">
                                  <div class="">
                                    <?php echo e(__('common.Date of Birth')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-7">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?>
                                     <?php echo e($staffDetails->date_of_birth != ""? date('m/d/Y', strtotime($staffDetails->date_of_birth)):''); ?>

                                     <?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- Start Parent Part -->
                         <h4 class="stu-sub-head mt-40"><?php echo e(__('common.Address')); ?></h4>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                     <?php echo e(__('common.Current Address')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->current_address); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                     <?php echo e(__('common.Permanent Address')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->permanent_address); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                     <!-- End Parent Part -->
                         <!-- Start Transport Part -->
                         <h4 class="stu-sub-head mt-40"><?php echo e(__('common.Bank Account Details')); ?></h4>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                     <?php echo e(__('common.Account Name')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->bank_account_name); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                    <?php echo e(__('common.Bank Account Number')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->bank_account_no); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                     <?php echo e(__('common.Bank Name')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->bank_name); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="single-info">
                            <div class="row">
                               <div class="col-lg-5 col-md-5">
                                  <div class="">
                                     <?php echo e(__('common.Bank Branch Name')); ?>

                                  </div>
                               </div>
                               <div class="col-lg-7 col-md-6">
                                  <div class="">
                                     <?php if(isset($staffDetails)): ?><?php echo e($staffDetails->bank_branch_name); ?><?php endif; ?>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!-- End Transport Part -->
                     <?php endif; ?>

                      <?php if(moduleStatusCheck('CustomField') and $staffDetails->customFields): ?>
                          <h4 class="stu-sub-head mt-40"><?php echo e(__('custom_fields.more_info')); ?></h4>

                          <?php if ($__env->exists('customfield::details.show', ['customFields' => $staffDetails->customFields])) echo $__env->make('customfield::details.show', ['customFields' => $staffDetails->customFields], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                          <?php endif; ?>
                  </div>
               </div>
               <!-- End Profile Tab -->
               <?php if(isset($staffDetails)): ?><input type="hidden" name="user_id" id="user_id" value="<?php echo e(@$staffDetails->user->id); ?>"><?php endif; ?>
               <!-- Start payroll Tab -->
               <div role="tabpanel" class="tab-pane fade" id="payroll">
                  <div class="white-box">
                      <div class="QA_section QA_section_heading_custom check_box_table">
                          <div class="QA_table ">
                              <table class="table Crm_table_active">
                                  <thead>
                                  <tr>
                                      <th scope="col"><?php echo e(__('payroll.Payslip')); ?> <?php echo e(__('common.ID')); ?></th>
                                      <th scope="col"><?php echo e(__('attendance.Month')); ?>-<?php echo e(__('attendance.Year')); ?></th>
                                      <th scope="col"><?php echo e(__('attendance.Date')); ?></th>
                                      <th scope="col"><?php echo e(__('payroll.Payment Method')); ?></th>
                                      <th scope="col"><?php echo e(__('payroll.Net Salary')); ?></th>
                                      <th scope="col"><?php echo e(__('common.Status')); ?></th>
                                      <th scope="col"><?php echo e(__('common.Action')); ?></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <?php $__currentLoopData = $payrollDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $payrollDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <tr>
                                              <td><?php echo e($payrollDetail->id); ?></td>
                                              <td><?php echo e($payrollDetail->payroll_month); ?> - <?php echo e($payrollDetail->payroll_year); ?></td>
                                              <td><?php echo e(formatDate($payrollDetail->payment_date)); ?></td>
                                              <td><?php echo e($payrollDetail->payment_mode); ?></td>
                                              <td><?php echo e(single_price($payrollDetail->net_salary)); ?></td>
                                              <td>
                                                  <?php if($payrollDetail->payroll_status == "G"): ?>
                                                      <span class="badge_3"><?php echo e(__('payroll.Generated')); ?></span>
                                                  <?php elseif($payrollDetail->payroll_status == "P"): ?>
                                                      <span class="badge_1"><?php echo e(__('payroll.Paid')); ?></span>
                                                  <?php else: ?>
                                                      <span class="badge_4"><?php echo e(__('payroll.Not generated')); ?></span>
                                                  <?php endif; ?>
                                              </td>
                                              <td>
                                                  <?php if(!empty($payrollDetail)): ?>
                                                      <?php if($payrollDetail->payroll_status == 'G'): ?>
                                                          <a title="Proceed to pay" onclick="payrollPayment(<?php echo e($payrollDetail->id); ?>,<?php echo e(@$staffDetails->user->role_id); ?>)"><button class="primary-btn btn-sm tr-bg"><?php echo e(__('payroll.Proceed To Pay')); ?></button></a>
                                                      <?php endif; ?>
                                                      <?php if($payrollDetail->payroll_status == 'P'): ?>
                                                          <a onclick="viewSlip(<?php echo e($payrollDetail->id); ?>)"><button class="primary-btn btn-sm tr-bg"><?php echo e(__('payroll.View PaySlip')); ?></button></a>
                                                      <?php endif; ?>
                                                  <?php else: ?>
                                                      <a class="" href="<?php echo e(url('payroll/generate-Payroll/'.$staffDetails->user_id.'/'.$m.'/'.$y)); ?>"><button class="primary-btn btn-sm tr-bg"> <?php echo e(__('payroll.Generate Payroll')); ?></button></a>
                                                  <?php endif; ?>
                                              </td>
                                          </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
               </div>
               <!-- End payroll Tab -->
               <!-- Start leave Tab -->
               <div role="tabpanel" class="tab-pane fade" id="leaves">
                   <?php
                       $remaining_leave_days = 0;
                       $extra_leave_days =  0;
                       if ($total_leave->sum('total_days') > $leaveDetails->sum('total_days')) {
                           $remaining_leave_days = $total_leave->sum('total_days') - $leaveDetails->sum('total_days');
                       }else {
                           $extra_leave_days =  $apply_leave_histories->sum('total_days') - $leaveDetails->sum('total_days');
                       }
                   ?>
                   <div class="row mb-3">
                       <div class="col-lg-4">
                           <div class="white-box single-summery">
                               <div class="d-flex justify-content-between">
                                   <div>
                                       <h3><?php echo e(__('leave.Total Leave')); ?></h3>
                                   </div>
                                   <h1 class="gradient-color2"><?php echo e($total_leave->sum('total_days')); ?> <?php echo e(__('leave.Days')); ?></h1>
                               </div>
                           </div>
                       </div>
                       <div class="col-lg-4">
                           <div class="white-box single-summery">
                               <div class="d-flex justify-content-between">
                                   <div>
                                       <h3><?php echo e(__('leave.Remaining Total Leave')); ?></h3>
                                   </div>
                                   <h1 class="gradient-color2"><?php echo e($remaining_leave_days); ?> <?php echo e(__('leave.Days')); ?></h1>
                               </div>
                           </div>
                       </div>
                       <div class="col-lg-4">
                           <div class="white-box single-summery">
                               <div class="d-flex justify-content-between">
                                   <div>
                                       <h3><?php echo e(__('leave.Extra Taken Leave')); ?></h3>
                                   </div>
                                   <h1 class="gradient-color2"><?php echo e($extra_leave_days); ?> <?php echo e(__('leave.Days')); ?></h1>
                               </div>
                           </div>
                       </div>
                   </div>
                  <div class="white-box">
                      <div class="QA_section QA_section_heading_custom check_box_table">
                          <div class="QA_table ">
                              <table class="table Crm_table_active">
                                  <thead>
                                  <tr>
                                      <th scope="col"><?php echo e(__('leave.Leave')); ?> <?php echo e(__('leave.Type')); ?></th>
                                      <th scope="col"><?php echo e(__('leave.Leave From')); ?></th>
                                      <th scope="col"><?php echo e(__('leave.Leave To')); ?></th>
                                      <th scope="col"><?php echo e(__('leave.Apply Date')); ?></th>
                                      <th scope="col"><?php echo e(__('common.Status')); ?></th>
                                      <th scope="col"><?php echo e(__('common.Action')); ?></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <?php $__currentLoopData = $leaveDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $leaveDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <tr>
                                              <td><?php echo e(@$leaveDetail->leave_type->name); ?></td>
                                              <td><?php echo e(formatDate($leaveDetail->start_date)); ?></td>
                                              <td><?php echo e(formatDate($leaveDetail->end_date)); ?></td>
                                              <td><?php echo e(formatDate($leaveDetail->apply_date)); ?></td>
                                              <td>
                                                  <?php if($leaveDetail->status == 0): ?>
                                                      <span class="badge_3">Pending</span>
                                                  <?php elseif($leaveDetail->status == 1): ?>
                                                      <span class="badge_1">Approved</span>
                                                  <?php else: ?>
                                                      <span class="badge_4">Cancelled</span>
                                                  <?php endif; ?>
                                              </td>
                                              <td>
                                                  <div class="dropdown CRM_dropdown">
                                                      <button class="btn btn-secondary dropdown-toggle" type="button"
                                                              id="dropdownMenu2" data-toggle="dropdown"
                                                              aria-haspopup="true"
                                                              aria-expanded="false">
                                                          <?php echo e(__('common.Select')); ?>

                                                      </button>
                                                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                                                          <a href="#" class="dropdown-item" onclick="edit_apply_leave_modal(<?php echo e($leaveDetail->id); ?>)"><?php echo e(__('common.View')); ?></a>
                                                      </div>
                                                  </div>
                                              </td>
                                          </tr>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
               </div>
               <!-- End leave Tab -->

               <!-- End Documents Tab -->
               <!-- Add Document modal form start-->
               <div class="modal fade admin-query" id="add_document_madal">
                  <div class="modal-dialog modal-dialog-centered">
                     <div class="modal-content">
                        <div class="modal-header">
                           <h4 class="modal-title"><?php echo e(__('common.Upload Document')); ?></h4>
                           <button type="button" class="close" data-dismiss="modal">
                            <i class="ti-close"></i>
                           </button>
                        </div>
                        <div class="modal-body">
                           <div class="container-fluid">
                              <form class="" action="<?php echo e(route('staff_document.store')); ?>" method="post" enctype="multipart/form-data">
                                  <?php echo csrf_field(); ?>
                                  <div class="row">
                                    <input type="hidden" name="staff_id" value="<?php echo e($staffDetails->id); ?>">
                                    <div class="col-xl-12">
                                        <div class="primary_input mb-25">
                                            <label class="primary_input_label" for=""><?php echo e(__('common.Name')); ?></label>
                                            <input name="name" class="primary_input_field name" placeholder="<?php echo e(__('common.Name')); ?>" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="primary_input mb-15">
                                            <label class="primary_input_label" for=""><?php echo e(__('common.Avatar')); ?></label>
                                            <div class="primary_file_uploader">
                                                <input class="primary-input" type="text" id="placeholderFileOneName"
                                                       placeholder="<?php echo e(__('common.Browse file')); ?>" readonly="">
                                                <button class="" type="button">
                                                    <label class="primary-btn small fix-gr-bg" for="document_file_1"><?php echo e(__('common.Browse')); ?></label>
                                                    <input type="file" class="d-none" name="file" id="document_file_1">
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-12 text-center mt-40">
                                        <div class="mt-40 d-flex justify-content-between">
                                           <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                                           <button class="primary-btn fix-gr-bg" type="submit"><?php echo e(__('common.Save')); ?></button>
                                        </div>
                                     </div>
                                  </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>


            </div>
         </div>
      </div>
   </div>
</section>
<div class="edit_form">

</div>

<?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
        function edit_apply_leave_modal(el){
            var user_id = $('#user_id').val();
            $.post('<?php echo e(route('apply_leave.view')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el, user_id:user_id}, function(data){
                $('.edit_form').html(data);
                $('#Apply_Leave_Edit').modal('show');
                $('select').niceSelect();
            });
        }
        function payrollPayment(el, role_id)
        {
            $.post('<?php echo e(route('payroll_payment_modal')); ?>',{_token:'<?php echo e(csrf_token()); ?>', id:el, role_id:role_id}, function(data){
                $(".edit_form").html(data);
                $('#PaymentForm').modal('show');
                $('select').niceSelect();
            });
        }

        function viewSlip(el)
        {
            $.post('<?php echo e(route('payroll_view_slip_modal')); ?>',{_token:'<?php echo e(csrf_token()); ?>', id:el}, function(data){
                $(".edit_form").html(data);
                $('#SlipForm').modal('show');
            });
        }

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('Staff Details')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/backEnd/staffs/viewStaff.blade.php ENDPATH**/ ?>