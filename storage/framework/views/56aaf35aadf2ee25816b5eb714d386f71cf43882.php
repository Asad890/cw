<?php
    $to_dos = ['to_dos.index', 'to_dos.create', 'to_dos.edit', 'to_dos.show'];
?>

<li class="<?php echo e(spn_active_link($to_dos, 'mm-active')); ?>">

    <a  href="<?php echo e(route('to_dos.index')); ?>" >
        <div class="nav_icon_small">
            <span class="fas fa-list-ul"></span>
        </div>
        <div class="nav_title">
            <span> <?php echo e(__('todo.Worklist')); ?></span>
        </div>
    </a>
</li>
<?php /**PATH D:\updates 123\case\cw\Modules/Todo\Resources/views/menu.blade.php ENDPATH**/ ?>