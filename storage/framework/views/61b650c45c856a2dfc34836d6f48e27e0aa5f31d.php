<?php $__env->startSection('mainContent'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('Modules/ModuleManager/Resources/assets/sass/manage_module.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/vendor/spondonit/css/parsley.css')); ?>">

    <section class="admin-visitor-area">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-9 col-xs-6 col-md-6 col-6 no-gutters ">
                            <div class="main-title sm_mb_20 sm2_mb_20 md_mb_20 mb-30 ">
                                <h3 class="mb-0"> <?php echo e(__('setting.Module')); ?> <?php echo e(__('setting.Manage')); ?></h3>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6 col-md-6 col-6 no-gutters ">
                            <a data-toggle="modal"
                               data-target="#add_module" href="#"
                               class="primary-btn fix-gr-bg small pull-right">  <?php echo e(__('common.Add')); ?>

                                / <?php echo e(__('common.Update')); ?> <?php echo e(__('setting.Module')); ?></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <table class="display school-table school-table-style" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?php echo e(__('setting.SL')); ?></th>
                                    <th><?php echo e(__('setting.Module')); ?><?php echo e(__('setting.Name')); ?></th>
                                    <th><?php echo e(__('setting.Description')); ?></th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>

                                <tbody>
                                <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                                <?php
                                    $count=1;
                                ?>
                                <?php $__currentLoopData = $is_module_available; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php
                                        $is_module_available = base_path().'/'.'Modules/' . $row->name. '/Providers/' .$row->name. 'ServiceProvider.php';
                                        $configFile = 'Modules/' . $row->name. '/' .$row->name. '.json';
                                        $is_data =  \Modules\ModuleManager\Entities\InfixModuleManager::where('name', $row->name)->first();

                                    try {
                                        $config =file_get_contents(base_path().'/'.$configFile);
                                         }catch (\Exception $exception){
                                            $config =null;
                                         }
                                    ?>
                                    <tr>
                                        <td><?php echo e(@$count++); ?></td>
                                        <td>
                                            <?php echo e(@$row->name); ?>

                                            <?php if(!empty($is_data->purchase_code)): ?> <p class="text-success">
                                                <?php echo e(__('setting.Verified')); ?>

                                                on <?php echo e(date("F jS, Y", strtotime(@$is_data->activated_date))); ?></p>
                                            <a href="#" class="module_switch" data-id="<?php echo e(@$row->name); ?>"
                                               id="module_switch_label<?php echo e(@$row->name); ?>"
                                               data-item="<?php echo e($row); ?>">
                                                <?php echo e(__('common.'.moduleStatusCheck($row->name )  == false? 'Activate':'Deactivate')); ?>



                                            </a>
                                            <div id="waiting_loader"
                                                 class="waiting_loader<?php echo e(@$row->name); ?>">
                                                <img
                                                    src="<?php echo e(asset('public/backEnd/img/demo_wait.gif')); ?>"
                                                    width="20" height="20"/><br>

                                            </div>

                                            <?php else: ?><p class="text-danger">
                                                <?php if(! file_exists($is_module_available)): ?>
                                                <?php else: ?>

                                                    <a class=" verifyBtn"
                                                       data-toggle="modal" data-id="<?php echo e(@$row->name); ?>"
                                                       data-target="#Verify"
                                                       href="#">   <?php echo e(__('setting.Verify')); ?></a>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php if(isset($config)): ?>
                                                <?php
                                                    $name = $row->name;
                                                    $config= json_decode($config);
                                                    if (isset($config->$name->notes[0])){
                                                    echo $config->$name->notes[0];
                                                    echo '<br>';
                                                    echo 'Version: '.$config->$name->versions[0].' | Developed By <a href="https://casewise.in/">Casewise</a>';

                                                    }
                                                ?>
                                            <?php else: ?>
                                                <?php
                                                    if (isset($row->details)){
                                                        echo $row->details;
                                                    }
                                                ?>
                                            <?php endif; ?>
                                        </td>

                                        <td class="text-right">
                                            <?php if(! file_exists($is_module_available)): ?>
                                                <div class="row">
                                                    <div class="col-lg-12 ">
                                                        <a class="primary-btn fix-gr-bg" style="white-space: nowrap;"
                                                           href="mailto:support@casewise.in">   <?php echo e(__('common.Buy Now')); ?></a>

                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if(file_exists($is_module_available)): ?>
                                                <?php
                                                    $system_settings= \Modules\Setting\Entities\Config::where('key', $row->name)->first();
                                                     $is_moduleV= $is_data;
                                                     $configName = $row->name;
                                                     $availableConfig=0;

                                                ?>

                                                <?php if(@$availableConfig==0 || @@$is_moduleV->purchase_code== null): ?>
                                                    <input type="hidden" name="name" value="<?php echo e(@$configName); ?>">


                                                <?php else: ?>
                                                    <div class="row">
                                                        <div class="col-lg-12 ">
                                                            <?php if('RolePermission' != $row->name && 'TemplateSettings' != $row->name ): ?>
                                                                <div id="waiting_loader"
                                                                     class="waiting_loader<?php echo e(@$row->name); ?>">
                                                                </div>
                                                            <?php endif; ?>

                                                        </div>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                        </td>


                                    </tr>


                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade admin-query" id="add_module">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New / Update Module</h4>
                    <button type="button" class="close " data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="<?php echo e(route('modulemanager.uploadModule')); ?>" method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="row">

                            <div class="col-xl-12">
                                <div class="primary_input mb-35">

                                    <div class="primary_file_uploader">
                                        <input class="primary-input filePlaceholder" type="text"
                                               id="document_file_placeholder"
                                               placeholder="Select Module File"
                                               readonly="">
                                        <button class="" type="button">
                                            <label class="primary-btn small fix-gr-bg"
                                                   for="document_file"><?php echo e(__('common.Browse')); ?></label>
                                            <input type="file" class="d-none fileUpload" name="module"
                                                   id="document_file" onchange="getFileName(this.value, '#document_file_placeholder')">
                                        </button>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="col-lg-12 text-center pt_15">
                            <div class="d-flex justify-content-center">
                                <button class="primary-btn semi_large2  fix-gr-bg" id="save_button_parent"
                                        type="submit"><i
                                        class="ti-check"></i> <?php echo e(__('common.Save')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade admin-query" id="Verify">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Module Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><i class="ti-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <?php echo e(Form::open(['id'=>"content_form",'class' => 'form-horizontal', 'files' => true, 'route' => 'ManageAddOnsValidation', 'method' => 'POST'])); ?>

                    <input type="hidden" name="name" value="" id="moduleName">
                    <input type="hidden" name="row" value="1">

                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label for="user">Envato Email Address :</label>
                        <input type="text" class="form-control " name="envatouser"
                               required="required"
                               placeholder="Enter Your Envato Email Address"
                               value="<?php echo e(old('envatouser')); ?>">
                    </div>
                    <div class="form-group">
                        <label for="purchasecode">Envato Purchase Code:</label>
                        <input type="text" class="form-control" name="purchase_code"
                               required="required"
                               placeholder="Enter Your Envato Purchase Code"
                               value="<?php echo e(old('purchasecode')); ?>">
                    </div>
                    <div class="form-group">
                        <label for="domain">Installation Path:</label>
                        <input type="text" class="form-control"
                               name="installationdomain" required="required"
                               placeholder="Enter Your Installation Domain"
                               value="<?php echo e(url('/')); ?>" readonly>
                    </div>
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg submit">
                                <span class="ti-check"></span>
                                <?php echo e(__('setting.Verify')); ?>

                            </button>
                            <button type="button" class="primary-btn fix-gr-bg submitting" style="display: none">
                                <i class="fas fa-spinner fa-pulse"></i>
                                Verifying
                            </button>
                        </div>
                    </div>

                    <?php echo e(Form::close()); ?>

                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('public/backEnd/js/module.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/vendor/spondonit/js/parsley.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/vendor/spondonit/js/function.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/vendor/spondonit/js/common.js')); ?>"></script>
    <script type="text/javascript">
        _formValidation('content_form');
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/ModuleManager/Resources/views/manage_module.blade.php ENDPATH**/ ?>