<?php $__env->startSection('mainContent'); ?>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px"><?php echo e(__('leave.Carry Forward')); ?></h3>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <table class="table Crm_table_active3">
                                <thead>
                                <tr>
                                    <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                    <th scope="col"><?php echo e(__('common.Type')); ?></th>
                                    <th scope="col"><?php echo e(__('common.Name')); ?></th>
                                    <th scope="col"><?php echo e(__('common.Username')); ?></th>
                                    <th scope="col"><?php echo e(__('common.Email')); ?></th>
                                    <th scope="col"><?php echo e(__('leave.Carry Forward')); ?></th>
                                    <th scope="col"><?php echo e(__('common.Action')); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <?php
                                            $staff = $user->staff;
                                            $carry_forward = $staff ?  $staff->carry_forward : 0;
                                            $status = $staff ?  $staff->is_carry_active : 0;
                                        ?>
                                        <th><?php echo e($key+1); ?></th>
                                        <td><?php echo e(str_replace('_', ' ', @$user->role->type)); ?></td>
                                        <td><?php echo e(@$user->name); ?></td>
                                        <td><?php echo e(@$user->username); ?></td>
                                        <td><a href="mailto:<?php echo e(@$user->email); ?>"><?php echo e(@$user->email); ?></a></td>
                                        <td><?php echo e($carry_forward); ?> (<?php echo e(__('leave.Available')); ?> :
                                            <?php echo e($user->CarryForward - $carry_forward); ?>)
                                        </td>
                                        <td>
                                            <label class="switch_toggle" for="active_checkbox<?php echo e($user->id); ?>">
                                                <input type="checkbox" id="active_checkbox<?php echo e($user->id); ?>"
                                                       <?php echo e($status == 1 ? 'checked' : ''); ?>

                                                       value="<?php echo e($user->CarryForward); ?>"
                                                       onchange="update_active_status(this,<?php echo e($staff ? $staff->id : ''); ?>)" <?php echo e(permissionCheck('languages.update_active_status') ? '' : 'disabled'); ?>>
                                                <div class="slider round"></div>
                                            </label>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="getDetails">
    </div>
    <?php echo $__env->make('backEnd.partials.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
        function update_active_status(el, id) {
            if (el.checked) {
                var status = 1;
            } else {
                var status = 0;
            }
            $.post('<?php echo e(route('carry.forward.update')); ?>', {
                _token: '<?php echo e(csrf_token()); ?>',
                day: el.value,
                id: id,
                status: status
            }, function (data) {
                toastr.success(data);
            });
        }
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/Leave/Resources/views/apply_leaves/carry_forward.blade.php ENDPATH**/ ?>