<div class="col-12">
    <div class="row attach-petitioner-row">
        <?php $__currentLoopData = $data['petitioners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-12" id="petetioner-<?php echo e($key); ?>">
                <div class="row attach-item">
                    <div class="primary_input col-md-3">
                        <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

                    </div>
                    <div class="primary_input col-md-8">
                        <?php echo e(Form::text('petitioner[]', $row->petitioner, ['class' => 'primary_input_field'])); ?>

                        <br><br>
                    </div>
                    <div class="primary_input col-md-1">

                        <?php if($key == 0): ?>
                            <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button"
                                onclick="petitioner_add();"> <i class="ti-plus"></i> </span>
                        <?php else: ?>
                            <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" type="button"
                                onclick="remove_petetioner('petetioner-<?php echo e($key); ?>');"> <i class="ti-trash"></i>
                            </span>
                        <?php endif; ?>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="primary_input col-md-3">
                        <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

                    </div>
                    <div class="primary_input col-md-9">
                        <?php echo e(Form::text('petitioner_advocate[]', $data['petitioners_advocate'][$key]->petitioner_advocate, ['class' => 'primary_input_field'])); ?>

                        <br><br>

                    </div>
                    <?php if($key < count($data['petitioners'])): ?>
                        <br>
                        <br>
                        <br>
                    <?php endif; ?>

                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>

<script>
    var index = 0;

    function petitioner_add() {
        index = $('.attach-item').length
        addNewPetitioner(index)
    }

    function addNewPetitioner(index) {
        "use strict";

        // var attachFile = '<div class="attach-file-section d-flex align-items-center">\n' +
        //     '        <div class="primary_input flex-grow-1">\n' +
        //     '            <div class="primary_file_uploader">\n' +
        //     '                <input class="primary-input" type="text" id="placeholderStaffsName" placeholder="' + trans('js.Browse File') + '" readonly>\n' +
        //     '                <button class="" type="button">\n' +
        //     '                    <label class="primary-btn small fix-gr-bg"\n' +
        //     '                           for="attach_file_' + index + '">' + trans('js.Browse') + '</label>\n' +
        //     '                    <input type="file" class="d-none file-upload-multi" name="file[]" id="attach_file_' + index + '">\n' +
        //     '                </button>\n' +
        //     '            </div>\n' +
        //     '        </div>\n' +
        //     '        <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only case-attach" type="button" > <i class="ti-trash"></i> </span>\n' +
        //     '    </div>';

        var add_petitioner = `
        <div class="col-12" id="petetioner-` + index +
            `">
            <div class="row">    
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

                </div>
                <div class="primary_input col-md-8">
                    <?php echo e(Form::text('petitioner[]', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER'), 'required' => 'required'])); ?>

                </div>
                <div class="primary_input col-md-1">
                    <span style="cursor:pointer;" class="primary-btn small fix-gr-bg icon-only" onclick="remove_petetioner('petetioner-` +
            index + `');" type="button" > <i class="ti-trash"></i> </span>
                </div>
                <br>
                <br>
                <br>
                <div class="primary_input col-md-3">
                    <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

                </div>
                <div class="primary_input col-md-9">
                    <?php echo e(Form::text('petitioner_advocate[]', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE'), 'required' => 'required'])); ?>

                </div>
                <br><br><br>
            </div>
        </div>`

        $('.attach-petitioner-row').append(add_petitioner);
    }


    function remove_petetioner(id) {
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }
</script>
<?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/case/edit_petitioner.blade.php ENDPATH**/ ?>