<?php $__env->startSection('mainContent'); ?>

    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">

            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                    
                    <li class="nav-item">
                            <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-custom" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('case.Custom Case')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-party-nm" role="tab" aria-controls="pills-profile" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('Connected Matters')); ?></h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-advo-nm" role="tab" aria-controls="pills-contact" aria-selected="false"><h3 class="mb-0 "><?php echo e(__('Interlocatory')); ?></h3></a>
                        </li>
                       
                        
                    </ul>

                    <!-- form start =-->
                    <div class="tab-content" id="pills-tabContent">
                        
                    <!-- Connected Mtters 2 =-->
                        <div class="tab-pane fade" id="pills-party-nm" role="tabpanel" aria-labelledby="pills-party-tab">
                            <div class="col-12" id="party_tab" >
                                <div class="white_box_50px box_shadow_white">
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'to_dos.store',
                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validateToDoForm()'])); ?>


                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="row">
                                            <div class="col-lg-12" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Primary Case')); ?></label>
                                                    <select class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row container1">
                                            <div class="col-lg-10" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                    <label class="primary_input_label"
                                                           for=""><?php echo e(__('Select Connected Matters')); ?></label>
                                                    <select name="mytext[]" class="primary_input_field">
                                                                <option>Category 1</option>
                                                                <option>Category 2</option>
                                                                <option>Category 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-2" id="sibling_class_div">
                                                <div class="primary_input mb-15">
                                                <label class="primary_input_label"
                                                           for=""><?php echo e(__('.')); ?></label>
                                                <a href="#" class="primary-btn small fix-gr-bg add_form_field">
                                                    <span class="ti-plus pr-2"></span>
                                                </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="container1">
    <button class="add_form_field">Add New Field &nbsp; 
      <span style="font-size:16px; font-weight:bold;">+ </span>
    </button>
    <div><input type="text" name="mytext[]"></div>
</div> -->

                                    </div>
                                </div>
                                <?php echo e(Form::close()); ?>


                                </div>
                            </div>


                        </div>

                        <!-- InterLocatory 3 =-->
                        <div class="tab-pane fade" id="pills-advo-nm" role="tabpanel" aria-labelledby="pills-advocate-tab">
                            <div class="col-12" id="advocate_tab" >
                                <div class="white_box_50px box_shadow_white">
                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row">
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Hearing Court', __('Order No.'))); ?>

                                            <?php echo e(Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('Order No.')])); ?>

                                            <span id="court_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Advocate Number', __('Order Date'))); ?>

                                            <?php echo e(Form::date('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control', "id"=>"previous_date",'placeholder' => __('Order DATE')])); ?>

                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="primary_input col-md-6">
                                            <?php echo e(Form::label('Year', __('Order Details'))); ?>

                                            <?php echo e(Form::text('case_year', null, ['class' => 'primary_input_field', 'placeholder' => __('Order Details')])); ?>

                                        </div>
                                    </div>

                                    <div class="text-center mt-3">
                                        <button class="primary_btn_large submit" type="submit" style="display: none;><i
                                                class="ti-check"></i><?php echo e(__('common.Create')); ?>

                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>


                                </div>
                            </div>

                        </div>


                        <!-- tab 4 =-->
                        <div class="tab-pane fade show active" id="pills-custom" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-12" id="custom_case" >
                                <div class="white_box_50px box_shadow_white">

                                    <?php echo Form::open(['route' => 'case.store', 'class' => 'form-validate-jquery', 'id' => 'content_form', 'files' => false, 'method' => 'POST']); ?>

                                    <div class="row form-group">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('BRIEF NO.', __('CNR NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CNR NO')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('BRIEF NO', __('case.BRIEF NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('brief_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF NO')])); ?>

                                        </div>
                                    </div>
                                    <div class="row">

                                            <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('PREVIOUS DATE', __('case.PREVIOUS DATE'))); ?>

                                            </div>
                                            <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('previous_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"previous_date",'placeholder' => __('case.PREVIOUS DATE')])); ?>

                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Court/Bench', __('Court/Bench'))); ?>

                                            <span id="court_id_error"></span>
                                            </div>
                                            <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('bench_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('Court/Bench'),  'data-parsley-errors-container' => '#court_id_error'])); ?>

                                            <span id="court_id_error"></span>
                                            </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-12">
                                            <div class="row">
                                                <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('case_no', __('case.Case No.'))); ?>

                                                </div>
                                                <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('case_no', null, ['class' => 'primary_input_field', 'placeholder' => __('case.Case No')])); ?>

                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('PETITIONER', __('case.PETITIONER'))); ?>

                                                <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#exampleModal">+</button>
                                                </div>
                                                <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('case.PETITIONER')])); ?>

                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('PETITIONER’S ADVOCATE', __('PETITIONER’S ADVOCATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('petitioner_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('PETITIONER’S ADVOCATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('RESPONDENT', __('RESPONDENT'))); ?>

                                            <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#respondendModal">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('respondent', null, ['class' => 'primary_input_field', 'placeholder' => __('case.RESPONDENT')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('RESPONDENT ADVOCATE', __('RESPONDENT ADVOCATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('respondent_advocate', null, ['class' => 'primary_input_field', 'placeholder' => __('RESPONDENT ADVOCATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CASE STAGE', __('case.CASE STAGE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('case_stage', null, ['class' => 'primary_input_field', 'placeholder' => __('case.CASE STAGE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Sr. No. in Court', __('Sr. No. in Court'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('Sr_no_in_court', null, ['class' => 'primary_input_field', 'placeholder' => __('Sr. No. in Court')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('COURT ROOM NO', __('COURT ROOM NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                        <select class="primary_select">
                                            <option>Select Court</option>
                                            <option> District</option>
                                            <option> Highcourt</option>
                                        </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="primary_input col-md-3">
                                        <?php echo e(Form::label('BRIEF FOR', __('JUDGE Name'))); ?>

                                    </div>
                                    <div class="primary_input col-md-9">
                                        <?php echo e(Form::text('brief_for', null, ['class' => 'primary_input_field', 'placeholder' => __('case.JUDGE Name')])); ?>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="primary_input col-md-3">
                                        <?php echo e(Form::label('BRIEF FOR', __('case.BRIEF FOR'))); ?>

                                    </div>
                                    <div class="primary_input col-md-9">
                                        <?php echo e(Form::text('brief_for', null, ['class' => 'primary_input_field', 'placeholder' => __('case.BRIEF FOR')])); ?>

                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    </div>
                                    <div class="row">
                                    <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('organization_id', __('case.ORGANIZATION'))); ?>

                                            </div>
                                    </div>
                                    <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('TAGS/REFERENCE', __('case.TAGS/REFERENCE'))); ?>


                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('tags', null, ['class' => 'primary_input_field', 'placeholder' => __('case.TAGS/REFERENCE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('POLICE STATION', __('case.POLICE STATION'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('police_station', null, ['class' => 'primary_input_field', 'placeholder' => __('case.POLICE STATION')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('NEXT DATE', __('NEXT DATE'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('next_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"next_date",'placeholder' => __('case.NEXT DATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CNR NO.', __('CNR NO.'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('cnr_no', null, ['class' => 'primary_input_field', 'placeholder' => __('CNR NO.')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('state', __('STATE'))); ?>

                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('district_id', __('District'))); ?>

                                            </div>
                                            <span id="organization_id_error"></span>
                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                            <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                                <div class="d-flex justify-content-between">
                                                    <?php echo e(Form::label('district_id', __('CASE TYPE'))); ?>

                                                </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                <?php echo e(Form::select('organization_id', $data['courts'], null, ['required' => '', 'class' => 'primary_select', 'data-placeholder' => __('case.Select'),  'data-parsley-errors-container' => '#organization_id_error'])); ?>

                                                <span id="organization_id_error"></span>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('CASE YEAR', __('CASE YEAR'))); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::text('next_date', date('Y-m-d H:i'), ['class' => 'primary_input_field primary-input form-control datetime', "id"=>"next_date",'placeholder' => __('case.NEXT DATE')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        
                                        <div class="primary_input col-md-3">
                                                <?php echo e(Form::label('PETITIONER', __('Client Name'))); ?>

                                                <button type="button" class="btn1 primary-btn small fix-gr-bg btn-sm" data-toggle="modal" data-target="#clientModal1">+</button>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                <?php echo e(Form::text('petitioner', null, ['class' => 'primary_input_field', 'placeholder' => __('Client Name')])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('DECIDED TOGGLE', __('DECIDED TOGGLE'))); ?>

                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                    <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="active_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                    </label>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <div class="d-flex justify-content-between">
                                                <?php echo e(Form::label('ABANDONED TOGGLE', __('ABANDONED TOGGLE'))); ?>

                                            </div>
                                        </div>
                                        <div class="primary_input col-md-9">
                                                    <label class="switch_toggle" for="active_checkbox">
                                                            <input type="checkbox" id="activee_checkbox" 
                                                            value="check" onchange="update_active_status(this)" checked>
                                                            <div class="slider round"></div>
                                                    </label>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Remarks', 'Remarks')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <?php echo e(Form::textarea('remarks', null, ['class' => 'primary_textarea', 'placeholder' => __('case.Remarks'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?>

                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="primary_input col-md-3">
                                            <?php echo e(Form::label('Notes', 'Notes')); ?>

                                        </div>
                                        <div class="primary_input col-md-9">
                                            <!-- <?php echo e(Form::textarea('notes', null, ['class' => 'primary_textarea', 'placeholder' => __('Notes'), 'rows' => 3, 'data-parsley-errors-container' => '#judgement_error', 'id' => 'remarks' ])); ?> -->
                                            <?php echo e(Form::textarea('description', null, ['class' => 'primary_input_field summernote', 'placeholder' => __('court.Court  Description'), 'rows' => 5, 'maxlength' => 1500, 'data-parsley-errors-container' =>
                                            '#description_error' ])); ?>

                                        </div>
                                    </div>
                                  
                                    <?php if ($__env->exists('case.file')) echo $__env->make('case.file', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                                    <div class="text-center mt-3">
                                        <button class="primary-btn small fix-gr-bg submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Add')); ?>

                                        </button>

                                        <button class="primary-btn small fix-gr-bg submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

                                    <?php echo Form::close(); ?>

                                </div>
                            </div>



                        </div>
                       

                 </div>
                    <!-- form end =-->
                </div>
            </div>


        </div>

        <div class="text-center mt-3">
                                        <button class="primary_btn_large submit" type="submit"><i
                                                class="ti-check"></i><?php echo e(__('Save')); ?>

                                        </button>

                                        <button class="primary_btn_large submitting" type="submit" disabled style="display: none;">
                                            <i class="ti-check"></i><?php echo e(__('common.Creating') . '...'); ?>

                                        </button>

                                    </div>

    </section>

    <!-- <div class="modal fade animated act_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated client_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated client_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated lawyer_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated case_stage_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>
    <div class="modal fade animated court_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>

    <div class="modal fade animated court_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1" role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div>
    <div class="modal fade animated case_category_add_modal infix_advocate_modal" id="remote_modal" tabindex="-1"
         role="dialog"
         aria-labelledby="remote_modal_label" aria-hidden="true" data-backdrop="static">
    </div> -->
    <!-- Petitioner Modal -->
    
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Petitioner </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="/action_page.php">
                <div class="row">
                    <div class="primary_input col-md-10">
                        <!--  -->
                    </div>
                    <div class="primary_input col-md-2">
                    <button type="button" class="primary_btn_large add_field_button">+</button>
                    </div>
                </div>
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">PETITIONER</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Petitioner Advocate</label>
                        <input type="text" class="primary_input_field" placeholder="Petitioner Advocate">
                    </div>
                </div>

                <div class="row" >
                    <div class="input_fields_wrap primary_input col-md-12">
                        
                    </div>
                </div>
            
            
            <div class="text-center mt-3">
                <button class="primary_btn_large submit" type="submit"><i
                        class="ti-check"></i><?php echo e(__('common.Create')); ?>

                </button>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
<!--Petitioner Modal -->

<!-- Respondent Modal -->
<div class="modal fade" id="respondendModal" tabindex="-1" role="dialog" aria-labelledby="respondandModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondandModalLabel">Respondand </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="/action_page.php">
                <div class="row">
                    <div class="primary_input col-md-10">
                        <!--  -->
                    </div>
                    <div class="primary_input col-md-2">
                    <button type="button" class="primary_btn_large add_field_button_res">+</button>
                    </div>
                </div>
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Respondent</label>
                        <input type="text" class="primary_input_field" placeholder="Respondent">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Respondent Advocate</label>
                        <input type="text" class="primary_input_field" placeholder="Respondent Advocate">
                    </div>
                </div>

                <div class="row" >
                    <div class="input_fields_wrap_res primary_input col-md-12">
                        
                    </div>
                </div>
            
            
            <div class="text-center mt-3">
                <button class="primary_btn_large submit" type="submit"><i
                        class="ti-check"></i><?php echo e(__('common.Create')); ?>

                </button>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>
<!--Respondent Modal -->

<!-- Client Modal For View -->

<div class="modal fade" id="clientModal1" tabindex="-1" role="dialog" aria-labelledby="clientModal1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="respondentModalLabel">Client </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="/action_page.php">
                <div class="row">
                <div class="primary_input col-md-12">
                        <label for="petitioner">Contact</label>
                        <input type="text" class="primary_input_field" placeholder="Contact">
                    </div>
                    
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Email</label>
                        <input type="text" class="primary_input_field" placeholder="Email">
                    </div>
                </div>
                <div class="row">
                    <div class="primary_input col-md-12">
                    <label for="petitioner">Address</label>
                        <input type="text" class="primary_input_field" placeholder="Address">
                    </div>
                </div>
                <div class="text-center mt-3">
                <button class="primary_btn_large submit" type="submit" hidden><i
                        class="ti-check"></i><?php echo e(__('common.Create')); ?>

                </button>
                </div>
    </form>
        <div class="col-lg-12 text-center">
            <div class="mt-40 d-flex justify-content-between">
                <button type="button" class="primary-btn tr-bg"
                    data-dismiss="modal"><?php echo e(__('common.Cancel')); ?></button>
                <input class="primary-btn fix-gr-bg" type="submit" value="<?php echo e(__('common.Save')); ?>">
            </div>
        </div>
        <!-- close button -->
      </div>
    </div>
  </div>
</div>



<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>

    <script>
        $(document).ready(function () {
            _formValidation();
            _componentAjaxChildLoad('#content_form', '#court_category_id', '#court_id', 'court');
            $(document).on('click', '#hearing_date_yes', function () {
                if (this.checked) {
                    $('#hearing_date').show();
                } else {
                    $('#hearing_date').hide();
                }
            });

            $(document).on('click', '#filling_date_yes', function () {
                if (this.checked) {
                    $('#filling_date').show();
                } else {
                    $('#filling_date').hide();
                }
            });

            $(document).on('click', '#judgement_date_yes', function () {
                if (this.checked) {
                    $('#judgement_date').show();
                    $('#judgement_row').show();
                } else {
                    $('#judgement_date').hide();
                    $('#judgement_row').hide();
                }
            });

            $(document).on('click', '#receiving_date_yes', function () {
                if (this.checked) {
                    $('#receiving_date').show();
                } else {
                    $('#receiving_date').hide();
                }
            });
        });

        _componentAjaxChildLoad('#client_quick_add_form', '#country_id', '#state_id', 'state')
        _componentAjaxChildLoad('#client_quick_add_form', '#state_id', '#city_id', 'city')

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('case.Create New Case')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/case/edit.blade.php ENDPATH**/ ?>