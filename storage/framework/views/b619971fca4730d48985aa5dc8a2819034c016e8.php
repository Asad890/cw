<div class="modal-dialog  modal-dialog-centered modal-lg">
    <div class="modal-content">


        <div class="modal-header">
            <h4 class="modal-title"><?php echo e($file->user_filename); ?></h4>
            <button type="button" class="close " data-dismiss="modal">
                <i class="ti-close "></i>
            </button>
        </div>

        <div class="modal-body">
            <div class="row">
            <div class="col-12">
                <?php echo case_file($file); ?>

            </div>

            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/fresh231/lawyer.casewise.in/resources/views/file/show.blade.php ENDPATH**/ ?>