<?php $__env->startSection('mainContent'); ?>


    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="box_header common_table_header xs_mb_0">
                        <div class="main-title d-md-flex">
                            <h3 class="mb-0 mr-30 mb_xs_15px mb_sm_20px" >
                                <?php echo e(__('client.'.$from)); ?>

                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="QA_section QA_section_heading_custom check_box_table">
                        <div class="QA_table ">
                            <!-- table-responsive -->
                            <div class="">
                                <table class="table Crm_table_active3">
                                    <thead>
                                    <tr>
                                        <th scope="col"><?php echo e(__('common.SL')); ?></th>
                                        <th scope="col"><?php echo e(__('case.Case')); ?></th>
                                        <th scope="col"><?php echo e(__('case.Client')); ?></th>
                                        <th scope="col"><?php echo e(__('case.Details')); ?></th>
                                        <th scope="col"><?php echo e(__('common.Actions')); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index + 1); ?></td>
                                            <td>
                                                <b><?php echo e(__('case.Case No.')); ?>: </b>
                                                <?php echo e($model->case_category? $model->case_category->name : ''); ?>/<?php echo e($model->case_no); ?> <br>
                                                <?php if($model->case_category_id): ?>
                                                    <b><?php echo e(__('case.Category')); ?>:
                                                        </b> <?php echo e($model->case_category? $model->case_category->name : ''); ?>

                                                <?php else: ?>
                                                    <b><?php echo e(__('case.Category')); ?>: </b>
                                                    <?php echo e($model->case_category? $model->case_category->name : ''); ?>

                                                <?php endif; ?><br>
                                                <a href="<?php echo e(route('client.case.show', $model->id)); ?>"><b><?php echo e(__('case.Title')); ?>: </b><?php echo e($model->title); ?>

                                                </a>
                                                <br>
                                                <b><?php echo e(__('case.Next Hearing Date')); ?>: </b> <?php echo e(formatDate($model->hearing_date)); ?> <br>
                                                <b><?php echo e(__('case.Filing Date')); ?>: </b> <?php echo e(formatDate($model->filling_date)); ?>

                                            </td>
                                            <td>
                                                <?php if($model->client == 'Plaintiff' and $model->plaintiff_client): ?>
                                                    <b><?php echo e(__('case.Name')); ?></b>:
                                                        <?php echo e($model->plaintiff_client->name); ?> <br>
                                                    <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->plaintiff_client->mobile); ?> <br>
                                                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->plaintiff_client->email); ?> <br>
                                                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->plaintiff_client->address); ?>

                                                    <?php echo e($model->plaintiff_client->city ? ', '. $model->plaintiff_client->city->name : ''); ?>

                                                    <?php echo e($model->plaintiff_client->state ? ', '. $model->plaintiff_client->state->name : ''); ?>

                                                <?php elseif($model->client == 'Opposite' and $model->opposite_client): ?>
                                                    <b><?php echo e(__('case.Name')); ?></b>:
                                                        <?php echo e($model->opposite_client->name); ?> <br>
                                                    <b><?php echo e(__('case.Mobile')); ?>: </b> <?php echo e($model->opposite_client->mobile); ?> <br>
                                                    <b><?php echo e(__('case.Email')); ?>: </b> <?php echo e($model->opposite_client->email); ?> <br>
                                                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->opposite_client->address); ?>

                                                    <?php echo e($model->opposite_client->city ? ', '. $model->opposite_client->city->name : ''); ?>

                                                    <?php echo e($model->opposite_client->state ? ', '. $model->opposite_client->state->name : ''); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if($model->court): ?>
                                                   <b><?php echo e(__('case.Court')); ?></b>:
                                                        <?php echo e($model->court->name); ?> <br>
                                                    <b><?php echo e(__('case.Category')); ?></b>:
                                                        <?php echo e($model->court->court_category ? $model->court->court_category->name : ''); ?>

                                                   <br>
                                                    <b><?php echo e(__('case.Room No')); ?>: </b> <?php echo e($model->court->room_number); ?> <br>
                                                    <b><?php echo e(__('case.Address')); ?>: </b> <?php echo e($model->court->location); ?>

                                                    <?php echo e($model->court->city ? ', '. $model->court->city->name : ''); ?>

                                                    <?php echo e($model->court->state ? ', '. $model->court->state->name : ''); ?>

                                                <?php endif; ?>
                                            </td>


                                            <td>


                                                <div class="dropdown CRM_dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenu2" data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <?php echo e(__('common.Select')); ?>

                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                                                        <a href="<?php echo e(route('client.case.show', $model->id)); ?>" class="dropdown-item"><i
                                                                class="icon-file-eye"></i> <?php echo e(__
                                                            ('common.View')); ?></a>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('admin.scripts'); ?>
    <script>


    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', ['title' => __('client.'.$from)], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fresh231/lawyer.casewise.in/Modules/ClientLogin/Resources/views/my_case.blade.php ENDPATH**/ ?>