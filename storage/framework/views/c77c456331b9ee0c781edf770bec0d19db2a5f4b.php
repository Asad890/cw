
</div>
</div>

<div class="has-modal modal fade" id="showDetaildModal">
    <div class="modal-dialog modal-dialog-centered" id="modalSize">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="showDetaildModalTile">New Client Information</h4>
                <button type="button" class="close icons" data-dismiss="modal">
                    <i class="ti-close"></i>
                </button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="showDetaildModalBody">

            </div>

            <!-- Modal footer -->

        </div>
    </div>
</div>


<!--  Start Modal Area -->
<div class="modal fade invoice-details" id="showDetaildModalInvoice">
    <div class="modal-dialog large-modal modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Invoice</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="ti-close"></i>
                </button>
            </div>

            <div class="modal-body" id="showDetaildModalBodyInvoice">
            </div>

        </div>
    </div>
</div>


<!-- ================Footer Area ================= -->
<footer class="footer-area pt-10 pb-20">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 text-center">
        <p> <?php echo config('configs')->where('key', 'copyright_text')->first()->value; ?> </p>
            </div>
        </div>
    </div>
</footer>
<!-- ================End Footer Area ================= -->
<?php if ($__env->exists('partials.deleteModalAjax')) echo $__env->make('partials.deleteModalAjax', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Popover -->

<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script> -->
<!-- Close Popover -->

<!-- For Current Date -->

<script>
date = new Date();
year = date.getFullYear();
month = date.getMonth() + 1;
day = date.getDate();
document.getElementById("current_date").innerHTML = month + "/" + day + "/" + year;
</script>

<!-- Close -->

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery-3.4.1.min.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery-ui.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/popper.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/css/rtl/bootstrap.min.js"></script>

<script src="<?php echo e(asset('public/frontend/')); ?>/vendors/text_editor/summernote-bs4.js"></script>


<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery.data-tables.js"></script>


<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery.mask.min.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/dataTables.buttons.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/buttons.flash.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jszip.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/pdfmake.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/vfs_fonts.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/buttons.html5.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/buttons.print.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/dataTables.rowReorder.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/dataTables.responsive.min.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/buttons.colVis.min.js"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/nice-select.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/fastselect.standalone.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/raphael-min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/morris.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/toastr.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/moment.min.js"></script>

<!-- <script
</script> -->
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/vendors/js/daterangepicker.min.js')); ?>"></script>

<!-- tagsinput  -->
<script src="<?php echo e(asset('public/frontend/')); ?>/vendors/tagsinput/tagsinput.js"></script>
<!-- summernote  -->
<!--  -->

<!-- nestable  -->
<script src="<?php echo e(asset('public/frontend/')); ?>/vendors/nestable/jquery.nestable.js"></script>

<!-- chage  -->

<!-- metisMenu js  -->
<script src="<?php echo e(asset('public/frontend/')); ?>/js/metisMenu.js"></script>


<!-- progressbar  -->
<script src="<?php echo e(asset('public/frontend/')); ?>/vendors/progressbar/circle-progress.min.js"></script>
<!-- color picker  -->


<script type="text/javascript" src="<?php echo e(asset('public/backEnd/')); ?>/js/jquery.validate.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/select2/select2.min.js"></script>

<script src="<?php echo e(route('assets.lang.js')); ?>"></script>
<script src="<?php echo e(asset('public/backEnd/vendors/js/loadah.min.js')); ?>"></script>
<script>
    function trans(string, args){
        let value = _.get(window.jsi18n, string);

        _.eachRight(args, (paramVal, paramKey) => {
            value = paramVal.replace(`:${paramKey}`, value);
        });

        if(typeof value == 'undefined'){
            return string;
        }

        return value;
    }
</script>
<script src="<?php echo e(asset('public/js/parsley.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/backEnd/js/parsley_i18n/'.session()->get('locale', Config::get('app.locale')).'.js')); ?>"></script>

<script src="<?php echo e(asset('public/backEnd/')); ?>/js/sweetalert.min.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/custom.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/main.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/developer.js"></script>
<script src="<?php echo e(asset('public/js/main.js')); ?>"></script>
<script src="multiselect.min.js"></script>

<?php echo Toastr::message(); ?>

<?php echo $__env->yieldPushContent('admin.scripts'); ?>
<?php echo $__env->yieldPushContent('js_before'); ?>
<?php echo $__env->yieldPushContent('js_after'); ?>
<?php echo $__env->yieldPushContent('scripts'); ?>
<script type="text/javascript">

    _formValidation('#item_delete_form', true, 'deleteAdvocateItemModal')
    setTimeout(function() {
        $('.preloader').fadeOut('slow', function() {
            $(this).hide();
        });
    }, 0);


    // for select2 multiple dropdown in send email/Sms in Individual Tab
    $("#selectStaffss").select2();
    $("#checkbox").click(function () {
        if ($("#checkbox").is(':checked')) {
            $("#selectStaffss > option").prop("selected", "selected");
            $("#selectStaffss").trigger("change");
        } else {
            $("#selectStaffss > option").removeAttr("selected");
            $("#selectStaffss").trigger("change");
        }
    });


    // for select2 multiple dropdown in send email/Sms in Class tab
    $("#selectSectionss").select2();
    $("#checkbox_section").click(function () {
        if ($("#checkbox_section").is(':checked')) {
            $("#selectSectionss > option").prop("selected", "selected");
            $("#selectSectionss").trigger("change");
        } else {
            $("#selectSectionss > option").removeAttr("selected");
            $("#selectSectionss").trigger("change");
        }
    });

</script>

 <script>


    $('.close_modal').on('click', function() {
        $('.custom_notification').removeClass('open_notification');
    });
    $('.notification_icon').on('click', function() {
        $('.custom_notification').addClass('open_notification');
    });
    $(document).click(function(event) {
        if (!$(event.target).closest(".custom_notification").length) {
            $("body").find(".custom_notification").removeClass("open_notification");
        }
    });



    $(document).ready(function () {
        $('#languageChange').on('change', function () {
        var str = $('#languageChange').val();
        var url = $('#url').val();
        var formData = {
            id: $(this).val()
        };
        // get section for student
        $.ajax({
            type: "POST",
            data: formData,
            dataType: 'json',
            url: url + '/' + 'language-change',
            success: function (data) {
                url= url + '/' + 'locale'+ '/' + data[0].language_universal;
                window.location.href = url;
            },
            error: function (data) {

            }
        });
    });
});
</script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/search.js"></script>
<?php echo $__env->yieldContent('script'); ?>


<script>
$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});

$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<div class="row"><div class="primary_input col-md-12"><label for="petitioner">Petitioner '+x+' </label><input type="text" class="primary_input_field" placeholder="Petitioner" name="mytext[]"/></div></div>'+'<div class="row"><div class="primary_input col-md-12"><label for="petitioner">Petitioner Advocate '+x+' </label><input type="text" class="primary_input_field" placeholder="Petitioner Advocate" name="mytext[]"/></div></div>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});


$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap_res"); //Fields wrapper
	var add_button      = $(".add_field_button_res"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<div class="row"><div class="primary_input col-md-12"><label for="petitioner">Respondent '+x+' </label><input type="text" class="primary_input_field" placeholder="Respondent" name="mytext_res[]"/></div></div>'+'<div class="row"><div class="primary_input col-md-12"><label for="petitioner">Respondent Advocate '+x+' </label><input type="text" class="primary_input_field" placeholder="Petitioner Advocate" name="mytext_res[]"/></div></div>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});

// For Connectet Matters

$(document).ready(function() {
    var max_fields = 50;
    var wrapper = $(".container1");
    var add_button = $(".add_form_field");

    var x = 1;
    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $(wrapper).append('<div class="col-lg-12" id="sibling_class_div"><div class="primary_input mb-15"><label class="primary_input_label"for="">Select Connected Matters</label><select name="mytext[]" class="primary_input_field"><option>Category 1</option><option>Category 2</option><option>Category 3</option></select><a href="#" class="delete"  style="text-align: right;">Delete</a></div></div>'); //add input box
        } else {
            alert('You Reached the limits')
        }
    });

    $(wrapper).on("click", ".delete", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })
});

function removeDummy() {
        var elem = document.getElementsByClassName('container1');
        elem.parentNode.removeChild(elem);
        return false;
    }

// Popover

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger: 'hover'
    });
});

// Close Popover

</script>



</body>
</html>

<?php /**PATH C:\Users\user\Desktop\bilal projects\cw (1)\cw\resources\views/partials/footer.blade.php ENDPATH**/ ?>