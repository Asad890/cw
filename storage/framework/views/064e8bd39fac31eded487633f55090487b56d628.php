<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/error.css">

    <title>
        <?php echo $__env->yieldContent('code'); ?> | <?php echo $__env->yieldContent('title'); ?>
    </title>

    <style>
        body {
            background: url(<?php echo e(asset('public/backEnd/img/login-bg.jpg')); ?>);
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body class="antialiased font-sans">

<div class="md:flex min-h-screen">
    <div class="w-full md:w-1/2   flex items-center justify-center">
        <?php echo $__env->yieldContent('content'); ?>

    </div>


</div>
</body>

</html>
<?php /**PATH D:\updates 123\cw\resources\views/errors/minimal.blade.php ENDPATH**/ ?>