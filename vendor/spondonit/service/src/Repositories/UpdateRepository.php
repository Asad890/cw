<?php

namespace SpondonIt\Service\Repositories;
ini_set('max_execution_time', 0);

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SpondonIt\Service\Repositories\InitRepository;
use Illuminate\Validation\ValidationException;
use Throwable;

class UpdateRepository
{
    protected $init;

    public function __construct(
        InitRepository $init
    ) {
        $this->init = $init;
    }

    public function download()
    {
            throw ValidationException::withMessages(['message' => trans('service::install.no_update_available')]);
    }

    public function update($params)
    {
        throw ValidationException::withMessages(['message' => trans('service::install.invalid_action')]);
    }

     /**
    * Mirage tables to database
    */
    protected function migrateDB() {
        try {
            Artisan::call('migrate', array('--force' => true));
        } catch (Throwable $e) {
            Log::info($e->getMessage());
            $sql = base_path('database/' . config('spondonit.database_file'));
            if (File::exists($sql)) {
                DB::unprepared(file_get_contents($sql));
            }

        }
   }
}
