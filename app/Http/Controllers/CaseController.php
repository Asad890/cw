<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\CaseAct;
use App\Models\CaseCategory;
use App\Models\CaseCategoryLog;
use App\Models\CaseCourt;
use App\Models\Cases;
use App\Models\ApiCaseType;
use App\Models\Client;
use App\Models\ClientCategory;
use App\Models\Court;
use App\Models\CourtCategory;
use App\Models\District;
use App\Models\HearingDate;
use App\Models\Lawyer;
use App\Models\Petitioner;
use App\Models\PetitionerAdvocate;
use App\Models\Respondent;
use App\Models\RespondentAdvocate;
use App\Models\State;
use App\Models\Stage;
use App\Models\CaseImage;
use App\Models\Organization;
use App\Models\RefTag;
use App\Models\ApiCourt;
use App\Models\ApiState;
use App\Models\ApiDistrict;
use App\Models\ApiBench;
use App\Traits\CustomFields;
use App\Traits\ImageStore;
use Carbon\Carbon;
use Exception;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Brian2694\Toastr\Facades\Toastr;
use Modules\EmailtoCL\Jobs\SendMailToLawyerJob;
use Illuminate\Support\Facades\Http;

class CaseController extends Controller
{
    use ImageStore, CustomFields;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $models = Cases::all();
        $page_title = 'All';
        if ($request->status and $request->status == 'Archieved') {
            $models = Cases::where('judgement_status', 'Judgement')->get();
            $page_title = 'Archieved';
        } else if ($request->status and $request->status == 'Waiting') {
            $models = Cases::where('status', 'Open')->where('hearing_date', '<', date('Y-m-d'))->get();
            $page_title = 'Waiting';
        } else if ($request->status and $request->status == 'Running') {
            $models = Cases::where('status', 'Open')->orWhereIn('judgement_status', ['Open', 'Reopen'])->get();
            $page_title = 'Running';
        }

        return view('case.index', compact('models', 'page_title'));
    }

    public function indexx(Request $request)
    {
        $active_cases = Cases::latest()->where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'District Courts and Tribunals')->orderBy('created_at', 'desc')->get();
        $active_cases_high_court = Cases::latest()->where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'High Court')->orderBy('created_at', 'desc')->get();
        return view('case.active', compact('active_cases', 'active_cases_high_court'));
    }

    public function indexxx(Request $request)
    {
        $today_cases = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'District Courts and Tribunals')->whereDate('next_date', Carbon::now()->format('Y-m-d'))->orderBy('created_at', 'desc')->get();
        $today_cases_high_court = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'High Court')->whereDate('next_date', Carbon::now()->format('Y-m-d'))->orderBy('created_at', 'desc')->get();
        return view('case.today', compact('today_cases', 'today_cases_high_court'));
    }

    public function indexxxx(Request $request)
    {
        $tomarrow_cases = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'District Courts and Tribunals')->whereDate('next_date',  \Carbon\Carbon::tomorrow()->format('Y-m-d'))->orderBy('created_at', 'desc')->get();
        $tomarrow_cases_high_court = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'High Court')->whereDate('next_date', \Carbon\Carbon::tomorrow()->format('Y-m-d'))->orderBy('created_at', 'desc')->get();
        return view('case.tomorrow', compact('tomarrow_cases', 'tomarrow_cases_high_court'));
    }
    public function indexxxxx($date)
    {
        $dailyboard_cases = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'District Courts and Tribunals')->whereDate('next_date',  date('Y-m-d',strtotime($date)))->orderBy('created_at', 'desc')->get();
        $dailyboard_cases_high_court = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'High Court')->whereDate('next_date',  date('Y-m-d',strtotime($date)))->orderBy('created_at', 'desc')->get();
        return view('case.dailyboard', compact('dailyboard_cases', 'dailyboard_cases_high_court'));
    }
    public function indexxxxxx(Request $request)
    {
        $date_awaited_cases = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'District Courts and Tribunals')->whereDate('next_date', '<' ,\Carbon\Carbon::now()->format('Y-m-d'))->orWhere('next_date',null)->orderBy('created_at', 'desc')->get();
        $date_awaited_cases_high_court = Cases::where('lawyer_id',auth()->id())->where('status', 'Open')->where('tabs', 'High Court')->whereDate('next_date', \Carbon\Carbon::now()->format('Y-m-d'))->orWhere('next_date',null)->orderBy('created_at', 'desc')->get();
        return view('case.dateawaited', compact('date_awaited_cases', 'date_awaited_cases_high_court'));
    }
    /**
     * Show the form for creating a new resource.<,<<<<<,,,...0987654321)
     *
     * @return Response
     */
    public function create()    //see
    {
        ini_set('memory_limit', '256M');
        $data['clients'] = Client::where('user_id', auth()->id())->pluck('name', 'id');
        $data['client_categories'] = ClientCategory::all()->pluck('name', 'id')->prepend(__('case.Select Client Category'), '');
        $data['stages'] = Stage::all()->pluck('name', 'id')->prepend(__('case.Select Case Stage'), '');
        $data['case_categories'] = CaseCategory::all()->pluck('name', 'id')->prepend(__('case.Select Case Categories'), '');
        $data['court_categories'] = CourtCategory::all()->pluck('name', 'id')->prepend(__('case.Select Court Categories'), '');
        $data['lawyers'] = Lawyer::all()->pluck('name', 'id');
        $data['acts'] = Act::all()->pluck('name', 'id');
        // $data['courts'] = ['' => __('case.Select Court')];
        $data['courts'] = Court::all()->pluck('name', 'id');
        $data['api_courts'] = ApiCourt::all()->pluck('name', 'id');
        $data['refs'] = RefTag::where('adv_id', auth()->id())->pluck('name', 'id');
        $data['orgs'] = Organization::where('adv_id', auth()->id())->where('parent_id', null)->get()->pluck('organization_name', 'id');
        $data['states'] = State::all()->pluck('name', 'id');
        $data['api_states'] = ApiState::all()->pluck('name', 'id');
        $data['api_district'] = ApiDistrict::where('id',1)->pluck('name', 'id');
        $data['api_banches'] = ApiBench::where('id',1)->pluck('name', 'id');
        $data['api_case_types'] = ApiCaseType::where('id',1)->pluck('name', 'id');
        $data['districts'] = District::all()->pluck('name', 'id');
        $data['stages'] = Stage::all()->pluck('name', 'id');

        $fields = null;

        if (moduleStatusCheck('CustomField')) {
            $fields = getFieldByType('case');
        }

        return view('case.create', compact('data', 'fields'));
    }


    public function store(Request $request)
    {

        // return $request->all();
        if (!$request->json()) {
            abort(404);
        }


        $validate_rules = [
            // 'case_category_id' => 'required|integer',
            'case_no' => 'sometimes|nullable|string',
            'file_no' => 'sometimes|nullable|string|max:20',
            'petitioner' => 'required',
            // 'acts*' => 'required|integer',
            // 'plaintiff' => 'required|integer',
            // 'opposite' => 'required|integer',
            // 'client_category_id' => 'required|integer',
            // 'court_category_id' => 'required|integer',
            // 'court_id' => 'required|integer',
            // 'lawyer_id*' => 'sometimes|nullable|integer',
            // 'stage_id' => 'sometimes|nullable|integer',
            // 'case_charge' => 'sometimes|nullable|numeric',
            // 'receiving_date' => 'sometimes|nullable|date',
            // 'filling_date' => 'sometimes|nullable|date',
            // 'hearing_date' => 'sometimes|nullable|date',
            // 'judgement_date' => 'sometimes|nullable|date',
            // 'description' => 'sometimes|nullable|string',
            // 'file.*' => 'sometimes|nullable|mimes:jpg,bmp,png,doc,docx,pdf,jpeg,txt',
        ];

        // if (moduleStatusCheck('CustomField')) {
        //     $validate_rules = array_merge($validate_rules, $this->generateValidateRules('case'));
        // }

        // $request->validate($validate_rules, validationMessage($validate_rules));
        // $judgement_date = Null;
        // if ($request->judgement_date_yes) {

        //     $judgement_date = date_format(date_create($request->judgement_date), 'Y-m-d H:i:s');
        //     $judgement = $request->judgement;

        //     if (!$judgement) {
        //         throw ValidationException::withMessages(['judgement' => __('case.Judgment field is required ')]);
        //     }
        // }
        // if ($request->plaintiff == $request->opposite) {
        //     throw ValidationException::withMessages(['plaintiff' => __('case.Plaintiff can not be opposite')]);
        // }

        // try {

        $hearing_date = Null;
        $filling_date = Null;

        $receiving_date = Null;
        $judgement = Null;

        if ($request->hearing_date_yes) {
            $hearing_date = date_format(date_create($request->hearing_date), 'Y-m-d H:i:s');
        }
        if ($request->filling_date_yes) {
            $filling_date = date_format(date_create($request->filling_date), 'Y-m-d H:i:s');
        }

        if ($request->receiving_date_yes) {
            $receiving_date = date_format(date_create($request->receiving_date), 'Y-m-d H:i:s');
        }

        // $plaintiff = Client::find($request->plaintiff);
        // $opposite = Client::find($request->opposite);
        // $title = $plaintiff->name . ' v/s ' . $opposite->name;
        // $client_category = ClientCategory::find($request->client_category_id);
        // $client = $client_category->plaintiff ? 'Plaintiff' : 'Opposite';
        $model = new Cases();
        $model->lawyer_id = auth()->id();
        // $model->title = $title;
        // $model->client = $client;
        if ($request->judgement_date_yes) {
            $model->judgement_status = 'Judgement';
            $model->status = 'Judgement';
            // $model->judgement_date = $judgement_date;
            $model->judgement = $judgement;
        } else {
            $model->status = 'Open';
        }




        $model->brief_no = isset($request->brief_no) ? $request->brief_no : '---';
        // $model->previous_date = isset($request->previous_date) ? $request->previous_date : '';
        $model->previous_date = $request->previous_date;
        // $model->petitioner = $request->petitioner;
        // $model->petitioner_advocate = $request->petitioner_advocate;
        // $model->respondent = $request->respondent;
        // $model->respondent_advocate = $request->respondent_advocate;
        $model->case_stage = isset($request->stage) ? $request->stage : '---';

        $model->sr_no_in_court = isset($request->sr_no_in_court) ? $request->sr_no_in_court : '---';
        $model->brief_for = isset($request->brief_for) ? $request->brief_for : '---';
        $model->police_station = isset($request->police_station) ? $request->police_station : '---';
        $model->organization_id = $request->organization_id;
        $model->tags = $request->tags;
        $model->cnr_no = $request->cnr_no;
        $model->remarks = isset($request->remarks) ? $request->remarks : '---';
        $model->notes_description  = isset($request->description) ? $request->description : '<p>---</p>';
        $model->court_room_no  = isset($request->court_room_no) ? $request->court_room_no : '---';
        $model->judge_name  = isset($request->judge_name) ? $request->judge_name : '---';
        $model->state_id  = isset($request->state_id) ? $request->state_id : 1;
        // $model->next_date  = isset($request->next_date) ? $request->next_date : '';
        $model->next_date  = $request->next_date;
        $model->district_id  = isset($request->district_id) ? $request->district_id : 1;
        $model->tabs = $request->tabs;
        $model->court_bench = isset($request->court_bench) ? $request->court_bench : '---';
        $model->case_year = isset($request->case_year) ? $request->case_year : '---';

        $model->case_category = isset($request->case_category) ? $request->case_category : '---';
        $model->case_no = isset($request->case_no) ? $request->case_no : '---';
        $model->client_id = $request->client_id;
        $model->decided_toggle = $request->decided_toggle;
        $model->abbondend_toggle = $request->abbondend_toggle;



        // $model->file_no = $request->file_no;
        // $model->plaintiff = $request->plaintiff;
        // $model->case_charge = $request->case_charge;
        // $model->opposite = $request->opposite;
        // $model->client_category_id = $request->client_category_id;
        // $model->court_category_id = $request->court_category_id;
        // $model->court_id = $request->court_id;
        // $model->ref_name = $request->ref_name;
        // $model->ref_mobile = $request->ref_mobile;
        // $model->stage_id = $request->stage_id;
        $model->receiving_date = $receiving_date;
        $model->filling_date = $filling_date;
        $model->hearing_date = $hearing_date;


        $model->save();
        // if ($request->petitioner) {
        //     foreach ($request->petitioner as $p) {
        //         Petitioner::create(['case_id' => $model->id, 'petitioner' => $p]);
        //     }
        // }
        // if ($request->petitioner_advocate) {
        //     foreach ($request->petitioner_advocate as $p) {
        //         PetitionerAdvocate::create(['case_id' => $model->id, 'petitioner_advocate' => $p]);
        //     }
        // }
        // if ($request->respondent) {
        //     foreach ($request->respondent as $r) {
        //         Respondent::create(['case_id' => $model->id, 'respondent' => $r]);
        //     }
        // }
        // if ($request->respondent_advocate) {
        //     foreach ($request->respondent_advocate as $r) {
        //         RespondentAdvocate::create(['case_id' => $model->id, 'respondent_advocate' => $r]);
        //     }
        // }
        // if (moduleStatusCheck('CustomField')) {
        //     $this->storeFields($model, $request->custom_field, 'case');
        // }

        if ($request->petitioner) {

            foreach ($request->petitioner as $key => $p) {
                if ($p != null) {
                    Petitioner::create(['case_id' => $model->id, 'petitioner' => $p]);
                } else {
                    if ($key == 0) {
                        Petitioner::create(['case_id' => $model->id, 'petitioner' => '---']);
                    }
                }
            }
        }
        if ($request->petitioner_advocate and count($request->petitioner_advocate) > 0) {
            foreach ($request->petitioner_advocate as $p) {
                if ($p != null) {
                    PetitionerAdvocate::create(['case_id' => $model->id, 'petitioner_advocate' => $p]);
                } else {
                    if ($key == 0) {
                        PetitionerAdvocate::create(['case_id' => $model->id, 'petitioner_advocate' => '---']);
                    }
                }
            }
        }
        if ($request->respondent and count($request->respondent) > 0) {
            foreach ($request->respondent as $r) {
                if ($r != null) {
                    Respondent::create(['case_id' => $model->id, 'respondent' =>  $r]);
                } else {
                    if ($key == 0) {
                        Respondent::create(['case_id' => $model->id, 'respondent' => '---']);
                    }
                }
            }
        }
        if ($request->respondent_advocate  and count($request->respondent_advocate) > 0) {
            foreach ($request->respondent_advocate as $r) {
                if ($r != null) {
                    RespondentAdvocate::create(['case_id' => $model->id, 'respondent_advocate' => $r]);
                } else {
                    if ($key == 0) {
                        RespondentAdvocate::create(['case_id' => $model->id, 'respondent_advocate' => '---']);
                    }
                }
            }
        }
        if (moduleStatusCheck('CustomField')) {
            $this->storeFields($model, $request->custom_field, 'case');
        }

        if (!$request->file_no) {
            $file_no = str_pad($model->id, 4, '0', STR_PAD_LEFT);
            $model->file_no = $file_no;
            $model->save();
        }

        if ($request->acts and count($request->acts) > 0) {
            foreach ($request->acts as $value) {
                $act = new CaseAct();
                $act->acts_id = $value;
                $act->cases_id = $model->id;
                $act->save();
            }
        }

        if ($request->lawyer_id and count($request->lawyer_id) > 0) {
            foreach ($request->lawyer_id as $lawyer) {
                $lawyer = Lawyer::find($lawyer);
                if ($lawyer) {
                    DB::table('cases_lawyer')
                        ->insert([
                            'cases_id' => $model->id,
                            'lawyer_id' => $lawyer->id,
                            'created_at' => Carbon::now(),
                        ]);

                    if ($lawyer->email and $request->send_email_to_lawyer and moduleStatusCheck('EmailtoCL')) {
                        dispatch(new SendMailToLawyerJob($lawyer, $model));
                    }
                }
            }
        }

        if ($request->hearing_date_yes) {
            $date = new HearingDate();
            $date->cases_id = $model->id;
            $date->stage_id = $request->stage_id;
            $date->date = $hearing_date;
            $date->description = $request->description;
            $date->save();
        }

        if ($request->judgement_date_yes) {
            $date = new HearingDate();
            $date->cases_id = $model->id;
            $date->date = $judgement_date;
            $date->description = $judgement;
            $date->type = 'judgement';
            $date->save();
        }
        // if (request()->hasFile('file')){
        //     foreach($request->file as $f){
        //         dd(request()->hasFile('file'));
        //         $uploadedImage = $f->file('file');
        //         $imageName = time() . '.' . $uploadedImage->getClientOriginalExtension();
        //         $destinationPath = public_path('/img');
        //         $uploadedImage->move($destinationPath, $imageName);
        //         $image->imagePath = $destinationPath . $imageName;
        //     }


        // }

        if ($request->file) {
            foreach ($request->file as $file) {
                $path = './assets/images/case/' . date("Y") . '/' . date("m") . '/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $filenameWithExt = $model->id;
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $uniqueFilename = time() . '-' . $filename . '.' . $extension;
                $file->move($path, $uniqueFilename);

                $post = new CaseImage();
                $post->case_id = $model->id;
                $post->path = $path;
                $post->image = $uniqueFilename;
                $post->save();
            }
        }


        // if ($request->file) {
        //     foreach ($request->file as $file) {
        //         $file_path = $this->storeFile($file, $model->id);
        //         dd($file_path);
        //         CaseImage::create(['case_id'=>$model->id, 'image'=> $file_path]);
        //     }
        // }

        // $response = [
        //     'goto' => route('case.show', $model->id),
        //     'model' => $model,
        //     'message' => __('case.Case Added Successfully'),
        // ];
        // dd("1");
        return redirect()->route('case.active');

        // } catch (Exception $e) {
        //     throw ['message' => $e->getMessage()];
        // }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // return('okay');
        // $model = Cases::with('acts', 'hearing_dates');
        // if (moduleStatusCheck('Finance')) {
        //     $model->with('transactions', 'invoices');
        // }

        // $model = $model->findOrFail($id);

        
        $model = Cases::with('acts', 'images')->findOrFail($id);
        $data['client'] = Client::where('id', $model->client_id)->first();
        $data['client_categories'] = ClientCategory::all()->pluck('name', 'id')->prepend(__('case.Select Client Category'), '');
        $data['stages'] = Stage::all()->pluck('name', 'id')->prepend(__('case.Select Case Stage'), '');
        $data['case_categories'] = CaseCategory::all()->pluck('name', 'id')->prepend(__('case.Select Case Categories'), '');
        $data['court_categories'] = CourtCategory::all()->pluck('name', 'id')->prepend(__('case.Select Court Categories'), '');
        $data['lawyers'] = Lawyer::all()->pluck('name', 'id')->prepend(__('case.Select Lawyer'), '');
        $data['courts'] = Court::where('court_category_id', $model->court_category_id)->pluck('name', 'id')->prepend(__('case.Select Court'), '');
        $data['acts'] = Act::all()->pluck('name', 'id');
        $data['selected_acts'] = $model->acts()->pluck('acts_id');
        $data['state'] = ApiState::where('id', $model->state_id)->first();
        $data['district'] = ApiDistrict::where('id', $model->district_id)->first();
        $data['stages'] = Stage::all()->pluck('name', 'id');
        $data['petitioners'] = Petitioner::where('case_id', $id)->get();
        $data['petitioners_advocate'] = PetitionerAdvocate::where('case_id', $id)->get();
        $data['respondents'] = Respondent::where('case_id', $id)->get();
        $data['respondents_advocate'] = RespondentAdvocate::where('case_id', $id)->get();
        $data['org'] = Organization::where('id', $model->organization_id)->first();
        $data['refs'] = RefTag::where('id', $model->tags)->first();
        $data['attachements'] = CaseImage::where('case_id', $id)->count();
        //  return  $data['attachements'];
        $fields = null;

        if (moduleStatusCheck('CustomField')) {
            $fields = getFieldByType('case');
        }

        if (request()->print) {
            return view('case.print', compact('model', 'data'));
         }

        return view('case.show', compact('model', 'data', 'fields'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Cases::with('acts')->findOrFail($id);
        $data['clients'] = Client::all()->pluck('name', 'id');
        $data['client_categories'] = ClientCategory::all()->pluck('name', 'id')->prepend(__('case.Select Client Category'), '');
        $data['stages'] = Stage::all()->pluck('name', 'id')->prepend(__('case.Select Case Stage'), '');
        $data['case_categories'] = CaseCategory::all()->pluck('name', 'id')->prepend(__('case.Select Case Categories'), '');
        $data['court_categories'] = CourtCategory::all()->pluck('name', 'id')->prepend(__('case.Select Court Categories'), '');
        $data['lawyers'] = Lawyer::all()->pluck('name', 'id')->prepend(__('case.Select Lawyer'), '');
        $data['courts'] = Court::where('court_category_id', $model->court_category_id)->pluck('name', 'id')->prepend(__('case.Select Court'), '');
        $data['acts'] = Act::all()->pluck('name', 'id');
        $data['selected_acts'] = $model->acts()->pluck('acts_id');
        $data['states'] = ApiState::all()->pluck('name', 'id');
        $data['districts'] = ApiDistrict::all()->pluck('name', 'id');
        $data['stages'] = Stage::all()->pluck('name', 'id');
        $data['petitioners'] = Petitioner::where('case_id', $id)->get();
        $data['petitioners_advocate'] = PetitionerAdvocate::where('case_id', $id)->get();
        $data['respondents'] = Respondent::where('case_id', $id)->get();
        $data['respondents_advocate'] = RespondentAdvocate::where('case_id', $id)->get();
        $data['orgs'] = Organization::where('adv_id', auth()->id())->get()->pluck('organization_name', 'id');
        $data['refs'] = RefTag::where('adv_id', auth()->id())->get()->pluck('name', 'id');
        $fields = null;

        if (moduleStatusCheck('CustomField')) {
            $fields = getFieldByType('case');
        }

        return view('case.edit', compact('model', 'data', 'fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {

        // dd($request->all());
        if (!$request->json()) {
            abort(404);
        }
        // $validate_rules = [
        //     'case_category_id' => 'required|integer',
        //     'case_no' => 'sometimes|nullable|string',
        //     'file_no' => 'sometimes|nullable|string|max:20',
        //     'acts*' => 'required|integer',
        //     'plaintiff' => 'required|integer',
        //     'opposite' => 'required|integer',
        //     'client_category_id' => 'required|integer',
        //     'court_category_id' => 'required|integer',
        //     'court_id' => 'required|integer',
        //     'stage_id' => 'sometimes|nullable|integer',
        //     'case_charge' => 'sometimes|nullable|numeric',
        //     'receiving_date' => 'sometimes|nullable|date',
        //     'filling_date' => 'sometimes|nullable|date',
        //     'hearing_date' => 'sometimes|nullable|date',
        //     'judgement_date' => 'sometimes|nullable|date',
        //     'description' => 'sometimes|nullable|string',
        //     'file.*' => 'sometimes|nullable|mimes:jpg,bmp,png,doc,docx,pdf,jpeg,txt',
        // ];

        // if (moduleStatusCheck('CustomField')) {
        //     $validate_rules = array_merge($validate_rules, $this->generateValidateRules('case'));
        // }
        // $request->validate($validate_rules, validationMessage($validate_rules));

        // $filling_date = Null;
        // $receiving_date = Null;


        // if ($request->filling_date_yes) {
        //     $filling_date = date_format(date_create($request->filling_date), 'Y-m-d H:i:s');
        // }

        // if ($request->plaintiff == $request->opposite) {
        //     Toastr::error(__('case.Plaintiff can not be opposite'));
        //     throw ValidationException::withMessages(['plaintiff' => __('case.Plaintiff can not be opposite')]);
        // }
        // if ($request->receiving_date_yes) {
        //     $receiving_date = date_format(date_create($request->receiving_date), 'Y-m-d H:i:s');
        // }


        $model = Cases::findOrFail($id);
        // $plaintiff = Client::find($request->plaintiff);
        // $opposite = Client::find($request->opposite);
        // $title = $plaintiff->name . ' v/s ' . $opposite->name;
        // $client_category = ClientCategory::find($request->client_category_id);
        // $client = $client_category->plaintiff ? 'Plaintiff' : 'Opposite';
        // $model->title = $title;
        // $model->client = $client;
        $model->brief_no = $request->Brief_no;
        $model->previous_date = $request->previous_date;
        // $model->petitioner = $request->petitioner;
        // $model->petitioner_advocate = $request->petitioner_advocate;
        // $model->respondent = $request->respondent;
        // $model->respondent_advocate = $request->respondent_advocate;
        $model->case_stage = $request->case_stage;

        $model->sr_no_in_court = $request->sr_no_in_court;
        $model->brief_for = $request->brief_for;
        $model->police_station = $request->police_station;
        $model->organization_id = $request->organization_id;
        $model->tags = $request->tags;
        $model->cnr_no = $request->cnr_no;
        $model->remarks = $request->remarks;
        $model->notes_description  = $request->notes_description;
        $model->court_room_no  = $request->court_room_no;
        $model->judge_name  = $request->judge_name;
        $model->state_id  = $request->state_id;
        $model->next_date  = $request->next_date;
        $model->district_id  = $request->district_id;
        $model->tabs = $request->tabs;
        $model->court_bench = $request->court_bench;
        $model->case_year = $request->case_year;

        $model->case_category = $request->case_category;
        $model->case_no = $request->case_no;

        $model->decided_toggle = $request->decided_toggle;
        $model->abbondend_toggle = $request->abbondend_toggle;




        // $model->file_no = $request->file_no;
        // $model->plaintiff = $request->plaintiff;
        // $model->case_charge = $request->case_charge;
        // $model->opposite = $request->opposite;
        // $model->client_category_id = $request->client_category_id;
        // $model->court_category_id = $request->court_category_id;
        // $model->court_id = $request->court_id;
        // $model->ref_name = $request->ref_name;
        // $model->ref_mobile = $request->ref_mobile;
        // $model->stage_id = $request->stage_id;
        // $model->receiving_date = $receiving_date;
        // $model->filling_date = $filling_date;
        // $model->hearing_date = $hearing_date;


        $model->update();
        // if ($request->petitioner and count($request->petitioner) > 0) {

        //     Petitioner::where('case_id', $model->id)->delete();
        //     foreach ($request->petitioner as $p) {
        //         Petitioner::create(['petitioner' => $p]);
        //     }
        // }
        // if ($request->petitioner_advocate and count($request->petitioner_advocate) > 0) {
        //     foreach ($request->petitioner_advocate as $p) {
        //         PetitionerAdvocate::where('case_id', $model->id)->update(['petitioner_advocate' => $p]);
        //     }
        // }
        // if ($request->respondent and count($request->respondent) > 0) {
        //     foreach ($request->respondent as $r) {
        //         Respondent::where('case_id', $model->id)->update(['respondent' => $r]);
        //     }
        // }
        // if ($request->respondent_advocate  and count($request->respondent_advocate) > 0) {
        //     foreach ($request->respondent_advocate as $r) {
        //         RespondentAdvocate::where('case_id', $model->id)->update(['respondent_advocate' => $r]);
        //     }
        // }
        // if (moduleStatusCheck('CustomField')) {
        //     $this->storeFields($model, $request->custom_field, 'case');
        // }
        if ($request->petitioner and count($request->petitioner) > 0) {
            Petitioner::where('case_id', $model->id)->delete();
            foreach ($request->petitioner as $p) {
                Petitioner::create(['case_id' => $model->id, 'petitioner' => $p]);
            }
        }
        if ($request->petitioner_advocate and count($request->petitioner_advocate) > 0) {
            PetitionerAdvocate::where('case_id', $model->id)->delete();
            foreach ($request->petitioner_advocate as $p) {
                PetitionerAdvocate::create(['case_id' => $model->id, 'petitioner_advocate' => $p]);
            }
        }
        if ($request->respondent and count($request->respondent) > 0) {
            Respondent::where('case_id', $model->id)->delete();
            foreach ($request->respondent as $r) {
                Respondent::create(['case_id' => $model->id, 'respondent' => $r]);
            }
        }
        if ($request->respondent_advocate  and count($request->respondent_advocate) > 0) {
            RespondentAdvocate::where('case_id', $model->id)->delete();
            foreach ($request->respondent_advocate as $r) {
                RespondentAdvocate::create(['case_id' => $model->id, 'respondent_advocate' => $r]);
            }
        }
        if (moduleStatusCheck('CustomField')) {
            $this->storeFields($model, $request->custom_field, 'case');
        }

        if (!$request->file_no) {
            $file_no = str_pad($model->id, 4, '0', STR_PAD_LEFT);
            $model->file_no = $file_no;
            $model->save();
        }

        if ($request->acts and count($request->acts) > 0) {
            foreach ($request->acts as $value) {
                $act = new CaseAct();
                $act->acts_id = $value;
                $act->cases_id = $model->id;
                $act->save();
            }
        }

        if ($request->lawyer_id and count($request->lawyer_id) > 0) {
            foreach ($request->lawyer_id as $lawyer) {
                $lawyer = Lawyer::find($lawyer);
                if ($lawyer) {
                    DB::table('cases_lawyer')
                        ->insert([
                            'cases_id' => $model->id,
                            'lawyer_id' => $lawyer->id,
                            'created_at' => Carbon::now(),
                        ]);

                    if ($lawyer->email and $request->send_email_to_lawyer and moduleStatusCheck('EmailtoCL')) {
                        dispatch(new SendMailToLawyerJob($lawyer, $model));
                    }
                }
            }
        }

        if ($request->hearing_date_yes) {
            $date = new HearingDate();
            $date->cases_id = $model->id;
            $date->stage_id = $request->stage_id;
            $date->date = $hearing_date;
            $date->description = $request->description;
            $date->save();
        }

        if ($request->judgement_date_yes) {
            $date = new HearingDate();
            $date->cases_id = $model->id;
            $date->date = $judgement_date;
            $date->description = $judgement;
            $date->type = 'judgement';
            $date->save();
        }
        // if (request()->hasFile('file')){
        //     foreach($request->file as $f){
        //         dd(request()->hasFile('file'));
        //         $uploadedImage = $f->file('file');
        //         $imageName = time() . '.' . $uploadedImage->getClientOriginalExtension();
        //         $destinationPath = public_path('/img');
        //         $uploadedImage->move($destinationPath, $imageName);
        //         $image->imagePath = $destinationPath . $imageName;
        //     }


        // }

 
        if ($request->file) {
            foreach ($request->file as $file) {
                $path = './assets/images/case/' . date("Y") . '/' . date("m") . '/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $filenameWithExt = $model->id;
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $uniqueFilename = time() . '-' . $filename . '.' . $extension;
                $file->move($path, $uniqueFilename);

                $post = new CaseImage();
                $post->case_id = $model->id;
                $post->path = $path;
                $post->image = $uniqueFilename;
                $post->save();
            }
        }


        // if ($request->file) {
        //     foreach ($request->file as $file) {
        //         $file_path = $this->storeFile($file, $model->id);
        //         dd($file_path);
        //         CaseImage::create(['case_id'=>$model->id, 'image'=> $file_path]);
        //     }
        // }

        // $response = [
        //     'goto' => route('case.show', $model->id),
        //     'model' => $model,
        //     'message' => __('case.Case Added Successfully'),
        // ];
        // dd("1");
        return redirect()->route('case.show',$model->id);

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     * @throws ValidationException
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->json()) {
            abort(404);
        }

        $model = Cases::with('tasks')->find($id);
        if (!$model) {
            throw ValidationException::withMessages(['message' => __('case.Case Not Found')]);
        }

        if ($model->tasks->count()) {
            throw ValidationException::withMessages(['message' => __('case.Case is assigned with tasks')]);
        }

        if (moduleStatusCheck('CustomField')) {
            $model->load('invoices');
            if ($model->invoices->count()) {
                throw ValidationException::withMessages(['message' => __('case.Case is assigned with invoices')]);
            }
        }

        if (moduleStatusCheck('CustomField')) {
            $model->load('customFields');
            $model->customFields()->delete();
        }


        $model->delete();

        return response()->json(['message' => __('case.Case Deleted Successfully'), 'goto' => route('case.index')]);
    }

    public function causelist(Request $request)
    {
        $data['models'] =  Cases::where(['status' => 'Open'])->orWhereIn('judgement_status', ['Open', 'Reopen']);
        if ($request->start_date) {
            $data['start_date'] = getFormatedDate($request->start_date, true);
            $data['models'] = $data['models']->whereDate('hearing_date', '>=', $data['start_date']);
        }


        if ($request->end_date) {
            $data['end_date'] = getFormatedDate($request->end_date, true);
            $data['models'] = $data['models']->whereDate('hearing_date', '<=', $data['end_date']);
        }


        $data['models'] = $data['models']->get();


        if ($request->ajax()) {
            return view('case.causelist_data', $data);
        }


        return view('case.causelist', $data);
    }


    public function category_change($id)
    {
        $model = Cases::FindOrFail($id);
        $category = CaseCategory::all()->pluck('name', 'id')->prepend(__('case.Select Case Category'), '');
        return view('case.category-change', compact('model', 'category'));
    }

    public function category_store(Request $request)
    {
        $model = Cases::FindOrFail($request->id);
        $category = CaseCategory::FindOrFail($model->case_category_id);
        $old_category = $category->name;
        $n_category = CaseCategory::FindOrFail($request->category);
        $new_category = $n_category->name;
        $model->case_category_id = $request->category;
        $model->save();

        $user = new CaseCategoryLog();
        $user->date = $request->date;
        $user->case_id = $request->id;
        $user->category_id = $request->category;
        $user->save();

        $description = 'Court Category Change: Form (' . $old_category . ") To (" . $new_category . ")";
        $date = new HearingDate();
        $date->cases_id = $model->id;
        $date->date = $request->date;
        $date->description = $description;
        $date->type = 'court_category_change';
        $date->save();

        $response = [
            'goto' => route('case.show', $model->id),
            'message' => __('case.Case Category Updated'),
        ];

        return response()->json($response);
    }


    public function court_change($id)
    {
        $model = Cases::FindOrFail($id);
        $court = Court::all()->pluck('name', 'id')->prepend(__('case.Select Court'), '');
        return view('case.court-change', compact('model', 'court'));
    }

    public function court_store(Request $request)
    {
        $this->validate($request, [
            'file.*' => 'sometimes|nullable|mimes:jpg,bmp,png,doc,docx,pdf,jpeg,txt',
        ]);

        $model = Cases::FindOrFail($request->id);
        $court = Court::FindOrFail($model->court_id);
        $old_court = $court->name;
        $n_court = Court::FindOrFail($request->court);
        $court_category_id = $n_court->court_category_id;
        $new_court = $n_court->name;
        $model->court_id = $request->court;
        $model->court_category_id = $court_category_id;
        $model->save();

        $user = new CaseCourt();
        $user->date = $request->date;
        $user->case_id = $request->id;
        $user->court_id = $request->court;
        $user->save();

        $description = 'Court Change: Form (' . $old_court . ") To (" . $new_court . ")";
        $date = new HearingDate();
        $date->cases_id = $model->id;
        $date->type = 'court_change';
        $date->date = $request->date;
        $date->description = $description;
        $date->type = 'court_change';
        $date->save();

        if ($request->file) {
            foreach ($request->file as $file) {
                $this->storeFile($file, $model->cases_id, $date->id);
            }
        }

        $response = [
            'goto' => route('case.show', $model->id),
            'message' => __('case.Case Court Updated'),
        ];

        return response()->json($response);
    }

    public function remove_lawyer($case_id, $lawyer_id)
    {
        $case = Cases::find($case_id);
        if ($case) {
            DB::table('cases_lawyer')
                ->where('cases_id', $case_id)
                ->where('lawyer_id', $lawyer_id)
                ->update(array('deleted_at' => DB::raw('NOW()')));
        }

        $response = [
            'goto' => route('case.show', $case_id),
            'message' => __('case.Lawyer removed from case.'),
        ];

        return response()->json($response);
    }

    public function add_lawyer($case_id)
    {
        $data['case'] = Cases::with('lawyers')->find($case_id);
        $previous_lawyer_ids = $data['case']->lawyers()->whereNull('deleted_at')->pluck('lawyer_id');

        $data['lawyers'] = Lawyer::whereNotIn('id', $previous_lawyer_ids)->pluck('name', 'id');
        return view('case.add_lawyer', $data);
    }

    public function post_lawyer(Request $request, $case_id)
    {
        $case = Cases::with('lawyers')->find($case_id);
        if ($case) {
            $sync = [];
            foreach ($request->lawyer_id as $lawyer) {
                DB::table('cases_lawyer')
                    ->insert([
                        'cases_id' => $case_id,
                        'lawyer_id' => $lawyer,
                        'created_at' => $request->date,
                    ]);
            }
        }
        $response = [
            'goto' => route('case.show', $case_id),
            'message' => __('case.Lawyer added to case.'),
        ];

        return response()->json($response);
    }

    public function filter(Request $request)
    {
        $data = [];
        $data['models'] = Cases::query();
        $data['clients'] = Client::all()->pluck('name', 'id');
        $data['client_id'] = $request->client_id;
        $data['stages'] = Stage::all()->pluck('name', 'id')->prepend(__('case.Select Case Stage'), '');
        $data['stage_id'] = $request->stage_id;
        $data['case_categories'] = CaseCategory::all()->pluck('name', 'id')->prepend(__('case.Select Case Categories'), '');
        $data['case_category_id'] = $request->case_category_id;
        $data['hearing_date'] = $request->hearing_date;
        $data['courts'] = Court::all()->pluck('name', 'id')->prepend(__('case.Select Court'), '');
        $data['court_id'] = $request->court_id;
        $data['judgement_status'] = $request->judgement_status;
        $data['status'] = $request->status;
        $data['filling_date'] = $request->filling_date;
        $data['receiving_date'] = $request->receiving_date;
        $data['judgement_date'] = $request->judgement_date;
        $data['case_no'] = $request->case_no;
        $data['file_no'] = $request->file_no;
        $data['db_acts'] = Act::all()->pluck('name', 'id');
        $data['acts'] = $request->acts;

        if ($request->client_id) {
            $data['models']->where(function ($q) use ($request) {
                return $q->where('plaintiff', $request->client_id)->orWhere('opposite', $request->client_id);
            });
        }

        if ($request->acts) {
            $data['models']->whereHas('acts', function ($q) use ($request) {
                return $q->whereIn('acts_id', $request->acts);
            });
        }

        if ($request->stage_id) {
            $data['models']->where('stage_id', $request->stage_id);
        }

        if ($request->case_no) {
            $data['models']->where('case_no', 'like', '%' . $request->case_no . '%');
        }

        if ($request->file_no) {
            $data['models']->where('file_no', 'like', '%' . $request->file_no . '%');
        }

        if ($request->case_category_id) {
            $data['models']->where('case_category_id', $request->case_category_id);
        }

        if ($request->hearing_date) {
            $data['models']->whereDate('hearing_date', $request->hearing_date);
        }
        if ($request->filling_date) {
            $data['models']->whereDate('filling_date', $request->filling_date);
        }
        if ($request->judgement_date) {
            $data['models']->whereDate('judgement_date', $request->judgement_date);
        }
        if ($request->receiving_date) {
            $data['models']->whereDate('receiving_date', $request->receiving_date);
        }
        if ($request->court_id) {
            $data['models']->where(['court_id' => $request->court_id]);
        }

        if ($request->status) {
            $data['models']->where(['status' => $request->status]);
        }
        if ($request->judgement_status) {
            $data['models']->where(['judgement_status' => $request->judgement_status]);
        }

        $data['models'] = $data['models']->orderBy('hearing_date', 'asc')->get();


        return view('case.filter', $data);
    }



    public function response()
    {
        $response = Http::withHeaders([
            'Cookie' => 'x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0YWU4NzU3OGM2ODVhMzI3M2Y2MGQiLCJmbmFtZSI6IkFkaXR5YSIsImxuYW1lIjoiSm9zaGkiLCJlbWFpbCI6InRlc2xhcGFibG80NUBnbWFpbC5jb20iLCJyb2xlIjoic3Vic2NyaWJlciIsImFjdGl2ZSI6dHJ1ZSwiZXhwIjoyMDI3OTI1NzQ1LCJpYXQiOjE2Njc5MjU3NDV9.bgVamxs7dfFUbi4QGwsgJUwQIwnx85JSL-2rPWkZgxw',
        ])->get('https://mercury.lawyer/api/getCourtsList');
        // $response = Http::get('https://mercury.lawyer/api/getCourtsList');
        $response = json_decode($response);
        foreach ($response as $row)
            if (isset($row->beta)) {
                ApiCourt::create([
                    'name' => $row->name,
                    'val' => $row->val,
                    'beta' => $row->beta,
                ]);
            } else {
                ApiCourt::create([
                    'name' => $row->name,
                    'val' => $row->val,
                ]);
            }

        return $response;
    }


    public function state_list()
    {
        $response = Http::withHeaders([
            'Cookie' => 'x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0YWU4NzU3OGM2ODVhMzI3M2Y2MGQiLCJmbmFtZSI6IkFkaXR5YSIsImxuYW1lIjoiSm9zaGkiLCJlbWFpbCI6InRlc2xhcGFibG80NUBnbWFpbC5jb20iLCJyb2xlIjoic3Vic2NyaWJlciIsImFjdGl2ZSI6dHJ1ZSwiZXhwIjoyMDI3OTI1NzQ1LCJpYXQiOjE2Njc5MjU3NDV9.bgVamxs7dfFUbi4QGwsgJUwQIwnx85JSL-2rPWkZgxw',
        ])->get('https://mercury.lawyer/api/getStatesList');
        // $response = Http::get('https://mercury.lawyer/api/getStatesList');
        $response = json_decode($response);
        foreach ($response as $row)

            ApiState::create([
                'name' => $row->name,
                'val' => $row->val,
            ]);


        return $response;
    }

    public function district_list(Request $request)
    {
        // return $request->ids;
        foreach ($request->ids as $id) {
            $response = Http::withHeaders([
                'Cookie' => 'x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0YWU4NzU3OGM2ODVhMzI3M2Y2MGQiLCJmbmFtZSI6IkFkaXR5YSIsImxuYW1lIjoiSm9zaGkiLCJlbWFpbCI6InRlc2xhcGFibG80NUBnbWFpbC5jb20iLCJyb2xlIjoic3Vic2NyaWJlciIsImFjdGl2ZSI6dHJ1ZSwiZXhwIjoyMDI3OTI1NzQ1LCJpYXQiOjE2Njc5MjU3NDV9.bgVamxs7dfFUbi4QGwsgJUwQIwnx85JSL-2rPWkZgxw',
            ])->get('https://mercury.lawyer/api/getDistrictsList?state_val=' . $id);
            // $response = Http::get('https://mercury.lawyer/api/getDistrictsList');
            $response = json_decode($response);
            foreach ($response as $row)

                ApiDistrict::create([
                    'name' => $row->name,
                    'val' => $row->val,
                    'state_value' => $id
                ]);
        }



        return $response;
    }

    public function benches_list(Request $request)
    {
        // return $request->ids;
        ini_set('max_execution_time', 9800);
        $courts = ApiCourt::all();
        $array = [];
        // $states = ApiState::where('state_value','>=', 3)->get();
        for ($i = 11; $i <= 12; $i++) {
            $districts = ApiDistrict::where('state_value', $i)->get();
            foreach ($courts as $court) {
                foreach ($districts as $district) {
                    try {
                        $response = Http::withHeaders([
                            'Cookie' => 'x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0YWU4NzU3OGM2ODVhMzI3M2Y2MGQiLCJmbmFtZSI6IkFkaXR5YSIsImxuYW1lIjoiSm9zaGkiLCJlbWFpbCI6InRlc2xhcGFibG80NUBnbWFpbC5jb20iLCJyb2xlIjoic3Vic2NyaWJlciIsImFjdGl2ZSI6dHJ1ZSwiZXhwIjoyMDI5Nzk1MzMyLCJpYXQiOjE2Njk3OTUzMzJ9.EiW11jHKlmsCzFVZ7uXUzj_83PcnrfFF0e63fBdoAIQ',
                        ])->get('https://mercury.lawyer/api/getBenchesList?courtID=' . $court->val . '&state_val=' . $i . '&district_val=' . $district->val);
                        $response = json_decode($response);

                        //dump($response);
                        // return $response;
                        foreach ($response as $row)
                            ApiBench::create([
                                'name' => $row->name,
                                'val' => $row->val,
                                'state_val' => $i,
                                'court_val' => $court->val,
                                'district_val' => $district->val
                            ]);
                    } catch (\Exception $e) {
                        $arr = [
                            "message" => $e->getMessage(),
                            "court_val" => $court->val,
                            "state_val" => $i,
                            "district_val" => $district->val
                        ];
                        array_push($array, $arr);
                    }

                    // $response = Http::get('https://mercury.lawyer/api/getDistrictsList');

                }
            }
        }

        return $array;
    }



    public function case_type_list(Request $request)
    {
        // return $request->ids;
        ini_set('max_execution_time', 1800);
        $courts = ApiCourt::all();
        $states = ApiState::all();
        $districts = ApiDistrict::all();
        $benches = ApiBench::all();
        foreach ($courts as $court) {
            foreach ($states as $state) {
                foreach ($districts as $district) {
                    foreach ($benches as $bench) {
                        $response = Http::withHeaders([
                            'Cookie' => 'x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0YWU4NzU3OGM2ODVhMzI3M2Y2MGQiLCJmbmFtZSI6IkFkaXR5YSIsImxuYW1lIjoiSm9zaGkiLCJlbWFpbCI6InRlc2xhcGFibG80NUBnbWFpbC5jb20iLCJyb2xlIjoic3Vic2NyaWJlciIsImFjdGl2ZSI6dHJ1ZSwiZXhwIjoyMDI3OTI1NzQ1LCJpYXQiOjE2Njc5MjU3NDV9.bgVamxs7dfFUbi4QGwsgJUwQIwnx85JSL-2rPWkZgxw',
                        ])->get('https://mercury.lawyer/api/getCaseTypesList?courtID=' . $court->val . '&state_val=' . $state->val . '&district_val=' . $district->val . '$bench_val=' . $bench->val);
                        // $response = Http::get('https://mercury.lawyer/api/getDistrictsList');
                        $response = json_decode($response);
                        foreach ($response as $row) {
                            ApiBench::create([
                                'name' => $row->name,
                                'val' => $row->val,
                                'state_val' => $state->val,
                                'court_val' => $court->val,
                                'district_val' => $district->val,
                                'bench_val' => $bench->val
                            ]);
                        }
                    }
                }
            }
        }

        return "success";
    }






    public function  update_next_date(Request $request)
    {

        $case_details = Cases::where('id', $request->id)->first();
        $case_details->previous_date = $case_details->next_date;
        $case_details->next_date = $request->next_date;
        $case_details->case_stage = $request->stage;
        $case_details->save();
        return "success";
    }

    public function  case_delete($id)
    {

        $case_details = Cases::where('id', $id)->delete();
        return "success";
    }
}
