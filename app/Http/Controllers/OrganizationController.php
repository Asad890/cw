<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\RefTag;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function store(Request $request)
    {
        // return $request->name;
        // $request['adv_id'] = auth()->id();
        $p_organization = Organization::create([
            'adv_id' => auth()->id(),
            'organization_name' =>  $request->organization_name,
            'representator' => $request->representator[1],
            'contact' => $request->contact[1],
            'email' => $request->email[1],
            'address' => $request->address
        ]);
        $p_organization->save();

        foreach ($request->email as $key => $email) {
            if ($key == 0 or $key == 1) {
                continue;
            }
            $organization = Organization::create([
                'adv_id' => auth()->id(),
                'organization_name' =>  $request->organization_name,
                'representator' => $request->representator[$key],
                'contact' => $request->contact[$key],
                'email' => $email[$key],
                'address' => $request->address
            ]);
            $organization->parent_id = $p_organization->id;
            $organization->save();
        }


        return response()->json($p_organization);
    }

    public function ref_store(Request $request)
    {
        $request['adv_id'] = auth()->id();
        $referance = RefTag::create($request->except('token'));
        return response()->json($referance);
    }
}
