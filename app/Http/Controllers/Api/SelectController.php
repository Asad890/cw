<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Court;
use App\Models\State;
use App\Models\City;
use App\Models\Package;
use Illuminate\Http\Request;

class SelectController extends Controller
{

	public function state(Request $request)
	{
		$country = $request->value;
		$state = State::where('country_id', $country)->get();
		return $state;
	}

	public function city(Request $request)
	{
		$state = $request->value;
		$city = City::where('state_id', $state)->get();
		return $city;
	}

	public function court(Request $request)
	{
		$court_category_id = $request->value;
		$court = Court::where('court_category_id', $court_category_id)->get();
		return $court;
	}


	public function storage_link()
	{
		$result = \Artisan::call('storage:link');

		return response()->json([
			"message" => "command executed successfully",
			"status" => 200,
		]);
	}
	public function subscriptions(Request $request)
	{
		$stripe = new \Stripe\StripeClient(
			env('STRIPE_SK')
		);
		$packages = ['Basic', 'Standard', 'Premium'];
		$models = ['3-regular_user', '5-regular_user', '6-regular_user'];
		$prices = ['50000', '70000', '70000'];
		foreach ($packages as $key => $package) {
			$product = $stripe->products->create([
				'name' => $package,
			]);
			$price =	$stripe->prices->create([
				'unit_amount' => $prices[$key],
				'currency' => 'inr',
				'recurring' => ['interval' => 'month'],
				'product' => $product->id,
			]);
			Package::create([
				'Title' => '$package',
				'product_id' => $product->id,
				'price_id' => $price->id,
				'models' => $models[$key],
			]);
		}
	}
}
