<?php

namespace App\Http\Controllers;

use App\Models\ApiBench;
use App\Models\ApiCaseType;
use App\Models\ApiDistrict;
use App\Models\ApiState;
use App\Models\Dropdown;
use Illuminate\Http\Request;

class DropdownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        ini_set('max_execution_time', 18000);
        // $states =  ::groupBy('state_code')->orderBy('state_code','ASC')->get()->unique();
        // ApiState::create([
        //     'name'=>'---',
        //     'val'=>0
        // ]);
        // foreach($states as $state){
        //     ApiState::create([
        //         'name'=>$state->State_Name,
        //         'val'=>$state->State_Code
        //     ]);
        // }



        // $api_states = ApiState::orderBy('val','ASC')->get();
        // ApiDistrict::create([
        //     'state_value'=>'0',
        //     'name'=>'---',
        //     'val'=>0
        // ]);
        // foreach($api_states as $api_state){
            $districts =  Dropdown::where('State_Code',3)->groupBy('district_code')->get()->unique();
            // return $districts;

            foreach($districts as $district){
                dump($district->District_Name);
                // ApiDistrict::create([
                //     'state_value'=>$district->State_Code,
                //     'name'=>$district->District_Name,
                //     'val'=>$district->District_Code
                // ]);
            }
        // }


        // return Dropdown::where('State_Code',1)->groupBy('district_code')->get()->unique();

        // $api_states = ApiState::orderBy('val','ASC')->get();
        // ApiCaseType::create([
        //     'state_val'=>0,
        //     'district_val'=>0,
        //     'bench_val'=>0,
        //     'name'=>'---',
        //     'val'=>0
        // ]);
        // dump(count($api_states));
        // foreach($api_states as $state_key => $api_state){
        //     $state_id=13;
        //     $api_districts = ApiDistrict::where('state_value',$state_id)->whereNotIn('val',[1,2,3,4,5,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57])->orderBy('val','ASC')->get();//->whereNotIn('val',[1,10,11,12,13,14,15,17,18,19,2,20,21,23,24,25,26,27,28,29,30,31,3,32,33,34,35,36,37,38,39,4])
        //     dump($state_id.'  '.count($api_districts));

        //     foreach($api_districts as $dis_key => $api_district){
        //         $benchs =  ApiBench::where('state_val',$state_id)->where('district_val',$api_district->val)->get();
        //         dump(($dis_key+1).'  '.$api_district->val.'  '.$api_district->name.'  '.count($benchs));
        //         foreach($benchs as $ben_key => $bench){
        //             $types =  Dropdown::where('State_Code',$state_id)->where('District_Code',$api_district->val)->where('Bench_Code',$bench->val)->groupBy('CaseType_Code')->get()->unique();
        //             foreach($types as $type){
        //                 ApiCaseType::create([
        //                     'state_val'=>$type->State_Code,
        //                     'district_val'=>$type->District_Code,
        //                     'bench_val'=>$type->Bench_Code,
        //                     'name'=>$type->CaseType_Name,
        //                     'val'=>$type->CaseType_Code
        //                 ]);

        //             }
        //             sleep(2);
        //             // dump('state'.$state_id.'  district'.$api_district->val.'  bench'.$bench->val);
        //         }
        //         sleep(2);


        //     }
        // // }
        // return ApiCaseType::count();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
