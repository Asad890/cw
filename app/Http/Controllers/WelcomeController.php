<?php

namespace App\Http\Controllers;

use App\Traits\CustomFields;
use App\Traits\Notification;
use Illuminate\Http\Request;
use App\StaffDocument;
use Brian2694\Toastr\Facades\Toastr;
use App\Repositories\UserRepositoryInterface;
use Modules\Leave\Repositories\LeaveRepository;
use Modules\Payroll\Repositories\PayrollRepositoryInterface;
use App\Http\Requests\StaffRequest;
use App\Http\Requests\StaffUpdateRequest;
use App\Models\Package;
use App\Models\Client;
use App\Models\Organization;
use App\Models\RefTag;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Modules\RolePermission\Entities\Role;
use Illuminate\Support\Facades\Hash;

class WelcomeController extends Controller
{

    use Notification, CustomFields;

    protected $userRepository, $leaveRepository, $payrollRepository, $applyLoanRepository;

    public function __construct(UserRepositoryInterface $userRepository, LeaveRepository $leaveRepository, PayrollRepositoryInterface $payrollRepository)
    {
        $this->userRepository = $userRepository;
        $this->leaveRepository = $leaveRepository;
        $this->payrollRepository = $payrollRepository;
    }

    public function index(){
        $roles = Role::with('permissions')->where('type','regular_user')->get();

        return view('welcome',compact('roles'));
    }

    public function store(Request $request){
        $request['bank_name']='bank_name';
        $request['bank_branch_name']='bank_branch_name';
        $request['bank_account_name']='bank_account_name';
        $request['bank_account_no']='bank_account_no';
        $request['provisional_months']=2;
        $request['basic_salary']=0;
        $request['employment_type']=null;
        $request['date_of_joining']=date('Y-m-d');
        $request['auth']='guest';
// dd($request->all());

        DB::beginTransaction();
        try {
            if ($request->password) {
                try {
                    $staff = $this->userRepository->store($request->except("_token"));
                    if (moduleStatusCheck('CustomField')) {
                        $this->storeFields($staff, $request->custom_field, 'staff');
                    }

                    DB::commit();
                    Toastr::success(__('common.Staff has been added Successfully'));
                    return redirect()->route('staffs.index');
                } catch (\Exception $e) {

                    DB::rollBack();
                    dd($e);
                    Toastr::error(__('common.Something Went Wrong'));
                    return back();
                }
            } else {
                DB::rollBack();
                dd($e);
                Toastr::error(__('common.Something Went Wrong'));
                return back();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            Toastr::error(__('common.Something Went Wrong'));
            return back();
        }
    }
}
