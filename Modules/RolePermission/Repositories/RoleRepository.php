<?php

namespace Modules\RolePermission\Repositories;

use App\Repositories\UserRepository;
use Modules\RolePermission\Entities\Role;
use Modules\RolePermission\Entities\Permission;
use Auth;
use App\Models\Package;
use Illuminate\Http\Request;
use Modules\RolePermission\Repositories\RoleRepositoryInterface;

class RoleRepository implements RoleRepositoryInterface
{
    public function all()
    {
        return Role::orderBy('id', 'desc')->get();
    }

    public function create(array $data)
    {
        $role = new Role();
        $role->name = $data['name'];
        $role->type = $data['type'];
        $role->price = $data['price'];
        $role->save();

        $stripe = new \Stripe\StripeClient(
			env('STRIPE_SK')
		);
        $product = $stripe->products->create([
            'name' => $data['name'],
        ]);
        $price =	$stripe->prices->create([
            'unit_amount' => $data['price'],
            'currency' => 'inr',
            'recurring' => ['interval' => 'month'],
            'product' => $product->id,
        ]);
        Package::create([
            'Title' => '$package',
            'product_id' => $product->id,
            'price_id' => $price->id,
            'models' => $role->id.'-'.$role->type,
        ]);
    }

    public function update(array $data, $id)
    {
        return Role::findOrFail($id)->update($data);
    }

    public function delete($id)
    {
        $role = Role::with('users')->findOrFail($id);
        if ($role->users){
            return false;
        }
        return $role->delete();
    }

    public function normalRoles()
    {
        return Role::where('type','!=','system_user')->get();
    }

    public function regularRoles()
    {
        return Role::where('type', 'regular_user')->get();
    }

}
